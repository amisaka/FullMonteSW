#include <FullMonteSW/Kernels/FPGACL/FPGACLMCKernelBase.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <boost/align/aligned_alloc.hpp>

void FPGACLMCKernelBase::prepare_() {
    parentPrepare();
}


void FPGACLMCKernelBase::start_() {
    parentStart();
}

void FPGACLMCKernelBase::awaitFinish() {
    parentSync();
}

void FPGACLMCKernelBase::gatherResults() {
    // get and clear the output data
    OutputDataCollection* C = results();
    C->clear();
    
    // give child class the chance to gather    
    parentGather();
}

unsigned long long FPGACLMCKernelBase::simulatedPacketCount() const {
    // TODO: ??
    return 0;
}

