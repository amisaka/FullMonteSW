#include <FullMonteSW/Kernels/FPGACL/TetraMCFPGACLKernel.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

// Intentionally empty - if we implement stuff in here SWIG cannot find it for whatever reason :/

