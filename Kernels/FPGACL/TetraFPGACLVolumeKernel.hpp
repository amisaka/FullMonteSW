#ifndef KERNELS_SOFTWARE_TETRAFPGACLVOLUMEKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRAFPGACLVOLUMEKERNEL_HPP_

#include "TetraMCFPGACLKernel.hpp"
    
/**
 * Intel FPGA OpenCL version of TetraVolumeKernel.
 * Uses FPGACLAccel to accelerate MC-simulator and track ONLY volume absorption events.
 */
class TetraFPGACLVolumeKernel: public TetraMCFPGACLKernel<RNG_SFMT_AVX> {
public:
    TetraFPGACLVolumeKernel(){}
};

#endif

