#ifndef KERNELS_SOFTWARE_FPGACLMCKERNELBASE_HPP_
#define KERNELS_SOFTWARE_FPGACLMCKERNELBASE_HPP_

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <boost/random/additive_combine.hpp>

#include <vector>

class Geometry;

/**
 * The base class for all MC-based Intel FPGA OpenCL simulators.
 */
class FPGACLMCKernelBase : public MCKernelBase {
public:
    virtual unsigned long long simulatedPacketCount() const final override;
    virtual void prepare_() final override;

protected:
    // implement in derived class
    virtual void parentPrepare()=0;
    virtual void parentSync()=0;
    virtual void parentStart()=0;
    virtual void parentGather()=0;

private:
    // overriding functions from MCKernelBase
    virtual void awaitFinish() final override;
    virtual void gatherResults() final override;
    virtual void start_() final override;
};

#endif

