#ifndef KERNELS_SOFTWARE_FPGACLTETRASFROMTETRAMESH_HPP_
#define KERNELS_SOFTWARE_FPGACLTETRASFROMTETRAMESH_HPP_

#include <FullMonteSW/External/FPGACLAccel/FullMonteFPGACLTypes.h>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <vector>

class Partition;
class TetraMesh;

/**
 * Creates FPGACL kernel tetras from a TetraMesh description
 */
class FPGACLTetrasFromTetraMesh {
public:
    void mesh(const TetraMesh* M);
    void update();

    bool checkKernelFaces() const;
    const std::vector<FPGACL::Tetra>& tetras() const;

private:
    void makeKernelTetras();

    static FPGACL::Tetra convert(const TetraMesh& M, TetraMesh::TetraDescriptor T);

    const TetraMesh*                m_mesh=nullptr;
    std::vector<FPGACL::Tetra>      m_tetras;
};

#endif /* KERNELS_SOFTWARE_TETRASFROMTETRAMESH_HPP_ */
