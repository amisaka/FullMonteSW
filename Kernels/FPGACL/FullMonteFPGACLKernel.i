#if defined(SWIGTCL)
%module FullMonteFPGACLKernelTCL
#elif defined(SWIGPYTHON)
%module FPGACLKernel
#else
    #warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_vector.i"
%include "std_string.i"

%include <FullMonteSW/Geometry/FullMonteGeometry_types.i>

%{
#include <FullMonteSW/Kernels/Kernel.hpp>
#include <FullMonteSW/Kernels/KernelObserver.hpp>
#include <FullMonteSW/Kernels/OStreamObserver.hpp>
#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <FullMonteSW/Kernels/FPGACL/FPGACLMCKernelBase.hpp>
#include <FullMonteSW/Kernels/FPGACL/TetraMCFPGACLKernel.hpp>
#include <FullMonteSW/Kernels/FPGACL/TetraFPGACLVolumeKernel.hpp>
%}

%include "../Kernel.hpp"
%include "MCKernelBase.hpp" 
%include "FPGACLMCKernelBase.hpp"
%include "TetraMCFPGACLKernel.hpp"

%template (TetraMCFPGACLKernelAVX) TetraMCFPGACLKernel<RNG_SFMT_AVX>;

%include "TetraFPGACLVolumeKernel.hpp"

