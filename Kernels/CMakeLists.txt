# create kernel base directory
ADD_LIBRARY(FullMonteKernelBase SHARED Kernel.cpp OStreamObserver.cpp SeedSweep.cpp)
TARGET_LINK_LIBRARIES(FullMonteKernelBase FullMonteGeometry FullMonteData ${Boost_SYSTEM_LIBRARY})

# TCL wrapping
IF(WRAP_TCL)
    INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})

    SET_SOURCE_FILES_PROPERTIES(FullMonteKernels.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMonteKernelsTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMonteKernels.i
    )
    SWIG_LINK_LIBRARIES(FullMonteKernelsTCL 
        FullMonteKernelBase 
        ${TCL_LIBRARY}
    )

    INSTALL(TARGETS FullMonteKernelsTCL LIBRARY 
        DESTINATION lib
    )	
ENDIF()

# Python wrapping
IF(WRAP_PYTHON)
    INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})
    SET_SOURCE_FILES_PROPERTIES(FullMonteKernels.i PROPERTIES CPLUSPLUS ON)

    SWIG_ADD_LIBRARY(Kernels 
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMonteKernels.i
    )
    SWIG_LINK_LIBRARIES(Kernels 
        FullMonteKernelBase 
        ${Python3_LIBRARIES}
    )
    SET_TARGET_PROPERTIES(_Kernels PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)	
ENDIF()

# install target
INSTALL(TARGETS FullMonteKernelBase DESTINATION lib)

## add subdirectories
# software kernels
MESSAGE("${Blue}Entering subdirectory Kernels/Software${ColourReset}")
ADD_SUBDIRECTORY(Software)

# CUDA kernels
IF("${USE_HW}" STREQUAL "ON")
    MESSAGE("Hardware is enabled: \ 
  Including the HW kernel for Power8 \ 
")
MESSAGE("${Blue}Entering subdirectory Kernels/P8${ColourReset}")
    ADD_SUBDIRECTORY(P8)    
ENDIF()

IF(BUILD_CUDAACCEL)
    ADD_SUBDIRECTORY(CUDA)
    MESSAGE("Hardware is enabled: \ 
  Including the GPU CUDA kernel \ 
")
    MESSAGE("${Blue}Entering subdirectory Kernels/CUDA${ColourReset}")
ENDIF()

# Intel FPGA OpenCL kernels 
IF(BUILD_FPGACLACCEL)
    ADD_SUBDIRECTORY(FPGACL)
    MESSAGE("Hardware is enabled: \ 
    Including the Intel FPGA OpenCL kernel \ 
")
MESSAGE("${Blue}Entering subdirectory Kernels/FPGACL${ColourReset}")
ENDIF()

MESSAGE("${Blue}Leaving subdirectory Kernels${ColourReset} \ 
")

