#ifndef KERNELS_SOFTWARE_CUDATETRASFROMLAYERED_HPP_
#define KERNELS_SOFTWARE_CUDATETRASFROMLAYERED_HPP_

#include <FullMonteSW/External/CUDAAccel/FullMonteCUDATypes.h>

#include <vector>

class Layered;

/** Create CUDA kernel tetras from a layered geometry description
 *
 * Uses degenerate tetras (with 2 faces at infinity) that will never see a photon interaction
 *
 *
 * For i=1..N, tetra i spans faces i-1,i and has thickness t_i
 * Face i is at z_i
 *
 * z_0 = 0
 * z_i = z_(i-1)+t_i
 *
 * Initial launch direction should be +z
 */
class CUDATetrasFromLayered {
public:
    void                                layers(const Layered* L);
    void                                update();

    const std::vector<CUDA::Tetra>&     tetras() const;

private:
    const Layered*                      m_layers=nullptr;
    std::vector<CUDA::Tetra>            m_tetras;
};

#endif
