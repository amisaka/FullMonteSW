#ifndef KERNELS_SOFTWARE_CUDATETRASFROMTETRAMESH_HPP_
#define KERNELS_SOFTWARE_CUDATETRASFROMTETRAMESH_HPP_

#include <FullMonteSW/External/CUDAAccel/FullMonteCUDATypes.h>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <vector>

class Partition;
class TetraMesh;

/**
 * Creates CUDA kernel tetras from a TetraMesh description
 */
class CUDATetrasFromTetraMesh {
public:
    void mesh(const TetraMesh* M);
    void update();

    bool checkKernelFaces() const;
    const std::vector<CUDA::Tetra>& tetras() const;

private:
    void makeKernelTetras();

    static CUDA::Tetra convert(const TetraMesh& M, TetraMesh::TetraDescriptor T);

    const TetraMesh*                m_mesh=nullptr;
    std::vector<CUDA::Tetra>        m_tetras;
};

#endif /* KERNELS_SOFTWARE_TETRASFROMTETRAMESH_HPP_ */
