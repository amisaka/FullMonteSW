#ifndef KERNELS_SOFTWARE_TETRACUDASVKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRACUDASVKERNEL_HPP_

#include "TetraMCCUDAKernel.hpp"
    
/**
 * CUDA version of TetraSVKernel.
 * Uses CUDAAccel to accelerate MC-simulator and track volume absorption and mesh surface exit events.
 */
class TetraCUDASVKernel : public TetraMCCUDAKernel<RNG_SFMT_AVX> {
public:
    TetraCUDASVKernel(){
        m_cudaAccelerator.scoreVolume = true;
        m_cudaAccelerator.scoreSurfaceExit = true;
        m_cudaAccelerator.scoreDirectedSurface = false;
    }
};

#endif

