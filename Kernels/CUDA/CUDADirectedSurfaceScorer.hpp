#ifndef KERNELS_SOFTWARE_CUDADIRECTEDSURFACESCORER_HPP_
#define KERNELS_SOFTWARE_CUDADIRECTEDSURFACESCORER_HPP_

#include <FullMonteSW/Kernels/Kernel.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>

#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <vector>

class Permutation;
class GeometryPredicate;
class VolumeCellPredicate;
class SurfaceCellPredicate;

/**
 * So. CUDA needs to score surface events as well. The best scenario
 * would be to re-use the DirectedSurfaceScorer but they work on single
 * accumulations at a time in SW and with CUDA we do batch transactions.
 * Therefore, I (sadly) had to create a class just like 
 * AbstractDirectedSurfaceScorer so that we can provide the same TCL interface
 * and 'reuse' (i.e. copy and paste :/) the code to deal with surfaces.
 * On the plus side, this means the CUDA version can track multiple regions!
 */
class CUDADirectedSurfaceScorer {
public:
	void addScoringRegionBoundary(VolumeCellPredicate* vol);
	void addScoringSurface(SurfaceCellPredicate* surf);

	void removeScoringRegionBoundary(VolumeCellPredicate* vol);
	void removeScoringSurface(SurfaceCellPredicate* surf);

	Permutation* getOutputSurface(unsigned i) const;
	unsigned getNumberOfOutputSurfaces() const;

protected:
	void createOutputSurfaces(Kernel* K);

private:
	void clearSurfaces();
	void addGeometryPredicate(GeometryPredicate*);
	void removeGeometryPredicate(GeometryPredicate*);

	std::vector<GeometryPredicate*>	m_outputCellPredicates;
	std::vector<Permutation*> m_outputSurfaces;
};

#endif


