## CUDA software Kernels

This directory contains the **HOST** software kernels that use the FullMonte CUDA accelerator 'CUDAAccel' (Externel/CUDAAccel). There isin't actually any 'acceleration' in these files, that is all in the submodule Externel/CUDAAccel these files simply wrap around the CUDAAccel API and provide the same interface as the software kernels in FullMonteSW (i.e. TetraMCKernel).

Currently the files in this directory include
- **CMakeLists.txt**: The CMake file for this directory. Builds the shared library FullMonteCUDAKernel.so and FullMonteCUDAKernelTCL.so.
- **CUDATetrasFromTetraMesh.{cpp/hpp}**: Converts the intermediate geometry TetraMesh into a list of CUDA::Tetra objects for the device kernel.
- **CUDATetrasFromLayered.{cpp/hpp}**: Converts the intermediate geometry Layered into a list of CUDA::Tetra objects for the device kernel.
- **CUDADirectedSurfaceScorer.{cpp/hpp}**: A CUDA version of the AbstractDirectedSurfaceScorer. Used to score surface exit/enter events.
- **CUDAMCKernelBase.{cpp/hpp}**: The base class of all CUDA based kernels. Inherits from Kernels/MCKernelBase and provides a high level outline of the flow.
- **TetraMCCUDAKernel.{cpp/hpp}**: Again, a base class of all CUDA based kernels (not my choice, following the format set by FullMonteSW). A large amount of the accelerator logic (i.e. interacting with CUDAAccel) is in here.
- **TetraCUDAVolumeKernel.{cpp/hpp}**: Inherits from TetraMCCUDAKernel and performs the simulation with only volume absorptions.
- **FullMonteCUDAKernel.i**: The interface file for SWIG to generate TCL interface for all the kernels.

The inheritance is as follows where left is the highest level and far right is the lowest level:<br/>
*TetraCUDAVolumeKernel* <== *TetraMCCUDAKernel* <== *CUDAMCKernelBase* || <== *MCKernelBase* <== *Kernel*

This directory (along with the CUDAAccel submodule) depend on the developer having the CUDA SDK and runtime installed. Currently, CUDA SDK version 10.0 is known to work but other versions should (but are not tested).

We currently build for NVIDIA compute capability 6.1. We currently require double precision atomics so that limits us to a minimum compute capability of 3.1. As we target more devices we will have to increase the different compute capabilities we support (using the `nvcc` flags).

