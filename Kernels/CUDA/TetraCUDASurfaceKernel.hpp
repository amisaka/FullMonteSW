#ifndef KERNELS_SOFTWARE_TETRACUDASURFACEKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRACUDASURFACEKERNEL_HPP_

#include "TetraMCCUDAKernel.hpp"

/**
 * CUDA version of TetraSurfaceKernel.
 * Track all energy absorbed into volumes and leaving the outer surface of the mesh.
 */
class TetraCUDASurfaceKernel : public TetraMCCUDAKernel<RNG_SFMT_AVX> {
public:
    TetraCUDASurfaceKernel(){
        m_cudaAccelerator.scoreVolume = true;
        m_cudaAccelerator.scoreSurfaceExit = true;
        m_cudaAccelerator.scoreDirectedSurface = false;
    }
};

#endif


