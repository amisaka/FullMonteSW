#include "CUDATetrasFromLayered.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>

#include <limits>
#include <iostream>

using namespace std;

void CUDATetrasFromLayered::layers(const Layered *L) {
    m_layers=L;
}

void CUDATetrasFromLayered::update() {
    unsigned Nl = m_layers->layerCount();
    m_tetras.clear();
    m_tetras.resize(Nl+2);

    // layer edges.
    // layer i (1..N) spans (edges[i-1], edges[i]) and contains material i
    // material[0] is top exterior, material[N+1] is bottom exterior

    // current limitation: top & bottom exterior must be same material

    vector<float> edges(Nl+1);

    edges[0] = 0.0f;
    for(unsigned i=1;i<=Nl;++i) {
        edges[i] = edges[i-1] + m_layers->layer(i)->thickness();
    }

    // top exterior tetra
    m_tetras[0].matID = 0;
    m_tetras[0].adjTetras = {0U,0U,0U,0U};
    m_tetras[0].IDfds = {0U,0U,0U,0U};
    m_tetras[0].faceFlags = 0;
    m_tetras[0].nx = m_tetras[0].ny = m_tetras[0].nz = m_tetras[0].C = {0., 0., 0., 0.};
    
    for(unsigned i=1;i<=Nl;++i) {
		m_tetras[i].matID = i;
		m_tetras[i].faceFlags = 0;
		m_tetras[i].adjTetras = {i-1, i+1, 0U, 0U};
		m_tetras[i].IDfds = {2*i, 2*i+3, 0U, 0U};
        
        m_tetras[i].nx = {0.0f, 0.0f, 0.0f, 0.0f};
        m_tetras[i].ny = {0.0f, 0.0f, 0.0f, 0.0f};
        m_tetras[i].nz = {1.0f, -1.0f, 0.0f, 0.0f};
        m_tetras[i].C  = {edges[i-1], -edges[i], -numeric_limits<float>::infinity(), -numeric_limits<float>::infinity()};
        
        LOG_DEBUG << "Tetra [" << i << "] ranges (" << edges[i-1] << ',' << edges[i] << ")" << endl;
    }

    // bottom exterior tetra
    m_tetras[Nl].adjTetras.y = 0U;		// bottom tetra links down to tetra 0 (exterior) -- ideally would be a separate tetra
	m_tetras[Nl+1] = m_tetras[0];

    for(unsigned i=0;i<m_tetras.size();++i) {
        LOG_DEBUG << "Tetra [" << i << "] links to " << m_tetras[i].adjTetras.x<< " and " << m_tetras[i].adjTetras.y << endl;
    }
}

const vector<CUDA::Tetra>& CUDATetrasFromLayered::tetras() const {
    return m_tetras;
}
