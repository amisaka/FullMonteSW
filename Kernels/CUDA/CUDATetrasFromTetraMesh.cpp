#include "CUDATetrasFromTetraMesh.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <functional>
#include <iostream>
#include <iomanip>

void CUDATetrasFromTetraMesh::mesh(const TetraMesh* M) {
    m_mesh=M;
}

void CUDATetrasFromTetraMesh::update() {
	// build the kernel tetras (halfspace representation) from the provided mesh
	makeKernelTetras();

	checkKernelFaces();
}


/**
 * Validates the orientation of faces within the kernel tetra structure.
 */

bool CUDATetrasFromTetraMesh::checkKernelFaces() const {
    bool status_ok=true;
    LOG_DEBUG << "Checking faces on " << m_tetras.size() << " tetras" << endl;

	// iterate over all tetras
	for(unsigned t=1; t < m_tetras.size(); ++t) {
        CUDA::Tetra T = m_tetras[t];
        array<unsigned,4> T_adjTetras = {
            T.adjTetras.x,
            T.adjTetras.y,
            T.adjTetras.z,
            T.adjTetras.w
        };

		// face reciprocity: each face points to either zero tetra or a tetra that points back to this one
        for(unsigned f=0;f<4;++f) {
            CUDA::Tetra adjTetra;

			if(T_adjTetras[f] >= m_tetras.size()) {
                status_ok=false;
                LOG_ERROR << "  Tetra " << t << " face " << f << " connects to a tetra out of array range" << endl;
			} else {
                adjTetra = m_tetras[T_adjTetras[f]];
            }

            /*
            if(T_adjTetras[f] != 0 && boost::count_if(adjTetra.adjTetras, [t](unsigned i){ return i==t; }) != 1) {
                cout << "  Tetra " << t << " face " << f << " connects to tetra " << T_adjTetras[f] << " but that tetra connects to ";
                cout << m_tetras[t].adjTetras.x << ' ' << 
                cout << m_tetras[t].adjTetras.y << ' ' << 
                cout << m_tetras[t].adjTetras.z << ' ' << 
                cout << m_tetras[t].adjTetras.w << ' ' << endl;
                
                status_ok = false;
            }
            */
        }
    }
    
    return status_ok;
}

/**
 * Converts the TetraMesh representation to the packed data structures used by the kernel.
 * Produces: m_tetras
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
/// union allows you to store different data types in the same memory location. i.e. f[0] = first 32 bit of __m128 v
union __m128_union {
	std::array<float,4>		f; // size 4x32 Bit = 128 Bit
	__m128					v; // size 128 Bit packed as 4 32 Bit floats
	__m128i					vi; // size 128 Bit packed as 2 64 Bit longs
}; // complete size of union: 128 Bit 

CUDA::Tetra CUDATetrasFromTetraMesh::convert(const TetraMesh& M, TetraMesh::TetraDescriptor t) {
    CUDA::Tetra T;

	T.faceFlags = 0;
	
    //Get all faces F of the tetra which is described by the TetraDescriptor t from the tetra mesh M
    const auto F = get(directed_faces,M,t);

    __m128_union mtx[4];
    // Loop through all 4 faces of the tetra
    array<unsigned,4> IDfds, adjTetras;
    for(unsigned i=0;i<4;++i) {
        IDfds[i] = get(id,M,F[i]);
        adjTetras[i] = get(
            id,
            M,
            get(tetra_below_face,M,F[i])
        );

        array<double,3> n;
        float c;

        // This calculates the normal and the constant c for the given tetra face (quite non descriptive get method...)
        tie(n,c) = get(face_plane,M,F[i]);

        // save the matrix transposed so all x/y/z/c values live in a single __m128
        for(unsigned j=0;j<3;++j)
            mtx[j].f[i] = n[j];
        mtx[3].f[i] = c;
    }

    // copy IDfds and adjTetras to the actual Tetra
    T.IDfds = {
        IDfds[0],
        IDfds[1],
        IDfds[2],
        IDfds[3]
    };
    T.adjTetras = {
        adjTetras[0],
        adjTetras[1],
        adjTetras[2],
        adjTetras[3]
    };
    
    T.nx = {
        mtx[0].f[0],
        mtx[0].f[1],
        mtx[0].f[2],
        mtx[0].f[3]
    };
    T.ny = {
        mtx[1].f[0],
        mtx[1].f[1],
        mtx[1].f[2],
        mtx[1].f[3]
    };
    T.nz = {
        mtx[2].f[0],
        mtx[2].f[1],
        mtx[2].f[2],
        mtx[2].f[3]
    };
    T.C = {
        mtx[3].f[0],
        mtx[3].f[1],
        mtx[3].f[2],
        mtx[3].f[3]
    };

    T.matID = get(region,M,t);
    
    return T;
}

#else

#include <FullMonteSW/Geometry/NonSSE.hpp>

CUDA::Tetra CUDATetrasFromTetraMesh::convert(const TetraMesh& M,TetraMesh::TetraDescriptor t) {
    CUDA::Tetra T;

    T.faceFlags = 0;
    //Get all faces F of the tetra which is described by the TetraDescriptor t from the tetra mesh M
    const auto F = get(directed_faces,M,t);

    m128 mtx[4];
    // Loop through all 4 faces of the tetra
    array<unsigned,4> IDfds, adjTetras;
    for(unsigned i=0;i<4;++i) {
        IDfds[i] = get(id,M,F[i]);
        adjTetras[i] = get(
            id,
            M,
            get(tetra_below_face,M,F[i])
        );

        array<double,3> n;
        float c;

        // This calculates the normal and the constant c for the given tetra face (quite non descriptive get method...)
        tie(n,c) = get(face_plane,M,F[i]);

        // save the matrix transposed so all x/y/z/c values live in a single __m128
        for(unsigned j=0;j<3;++j)
            mtx[j][i] = n[j];
        mtx[3][i] = c;
    }
    
    // copy IDfds and adjTetras to the actual Tetra
    T.IDfds = {
        IDfds[0],
        IDfds[1],
        IDfds[2],
        IDfds[3]
    };
    T.adjTetras = {
        adjTetras[0],
        adjTetras[1],
        adjTetras[2],
        adjTetras[3]
    };
	
    T.nx = {
        mtx[0][0],
        mtx[0][1],
        mtx[0][2],
        mtx[0][3]
    };
    T.ny = {
        mtx[1][0],
        mtx[1][1],
        mtx[1][2],
        mtx[1][3]
    };
    T.nz = {
        mtx[2][0],
        mtx[2][1],
        mtx[2][2],
        mtx[2][3]
    };
    T.C = {
        mtx[3][0],
        mtx[3][1],
        mtx[3][2],
        mtx[3][3]
    };

    T.matID = get(region,M,t);

    return T;
}
#endif

void CUDATetrasFromTetraMesh::makeKernelTetras() {
    m_tetras.resize(get(num_tetras,*m_mesh));
    
    boost::transform(
        m_mesh->tetras(),
        m_tetras.begin(),
        std::bind(convert,*m_mesh,std::placeholders::_1)
    );
}

const std::vector<CUDA::Tetra>& CUDATetrasFromTetraMesh::tetras() const {
    return m_tetras;
}

