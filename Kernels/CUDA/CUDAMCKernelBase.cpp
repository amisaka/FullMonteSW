#include <FullMonteSW/Kernels/CUDA/CUDAMCKernelBase.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <boost/align/aligned_alloc.hpp>

void CUDAMCKernelBase::prepare_() {
    parentPrepare();
}


void CUDAMCKernelBase::start_() {
    parentStart();
}

void CUDAMCKernelBase::awaitFinish() {
    parentSync();
}

void CUDAMCKernelBase::gatherResults() {
    // get and clear the output data
    OutputDataCollection* C = results();
    C->clear();
    
    // give child class the chance to gather    
    parentGather();
}

unsigned long long CUDAMCKernelBase::simulatedPacketCount() const {
    // TODO:??
    return 0;
}

