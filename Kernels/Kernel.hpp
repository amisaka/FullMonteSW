/*
 * Kernel.hpp
 *
 *  Created on: Apr 22, 2015
 *      Author: jcassidy
 */

#ifndef KERNELS_KERNEL_HPP_
#define KERNELS_KERNEL_HPP_

#include <vector>
#include <memory>

#include <thread>
#include <mutex>
#include <condition_variable>

#include <string>
#include <list>

class OutputDataCollection;
class KernelObserver;
class MaterialSet;
class Geometry;

namespace Source { class Abstract; }

class OutputData;

class Kernel {
public:
	enum Status { Idle, Preparing, Running, Finished };

	virtual ~Kernel(){}

	void runSync();
	void startAsync();

	void finishAsync();

	bool 			done() const { return m_status == Finished; }
	virtual float 	progressFraction() 	const=0;

	/// get/set source (multiple sources accommodated by Source::Composite)
	void						source(const Source::Abstract* s)			{ m_src=s;			}
	const Source::Abstract*		source()							const 	{ return m_src; 	}

	/// get/set material definitions
	void						materials(MaterialSet* m);
	MaterialSet*				materials() const;

	// virtual to allow deriving classes to override
	virtual void				geometry(const Geometry* G);
	virtual const Geometry*		geometry() const;

	/// Get results
	OutputDataCollection*		results();

	enum EnergyPower {Watt, Joule, NONE};
	/// Get/set if the photon packets represent Power or Energy in the simulation
	void isEnergy_or_Power(std::string str);
	EnergyPower getUnit() const;
	std::string isEnergy_or_Power() const;

	/// get/set the energy/power value that the photon packets represent
	void energyPowerValue(float e);
	float energyPowerValue() const;

protected:
	const Source::Abstract*				m_src=nullptr;

	OutputDataCollection*				m_results=nullptr;

	MaterialSet*						m_materialSet=nullptr;

	bool								m_dirtyGeometry=true;

	void awaitStatus(Status st);
	void updateStatus(Status st);
	Status status() const;

private:

	const Geometry*				m_geometry=nullptr;

	virtual void 	awaitFinish()		=0;

	std::vector<KernelObserver*> m_observers;

	virtual void gatherResults()=0;

	virtual void prepare_()=0;
	virtual void start_()=0;

	Status						m_status=Idle;
	std::mutex					m_statusMutex;
	std::condition_variable 	m_statusCV;

	std::thread 				m_parentThread;
	float 						m_energy_power=1.0;
	EnergyPower 				m_unit=NONE;
};

#endif /* KERNELS_KERNEL_HPP_ */

