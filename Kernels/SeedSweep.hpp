/*
 * SeedSweep.hpp
 *
 *  Created on: Jul 8, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_SEEDSWEEP_HPP_
#define KERNELS_SEEDSWEEP_HPP_

#include <vector>
#include <string>

class OutputDataCollection;
class MCKernelBase;

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/any_range.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

/** Invokes a provided Kernel multiple times with different random seeds, and combines the results together by type.
 */

class SeedSweep
{
public:
	SeedSweep();
	~SeedSweep();

	void kernel(MCKernelBase* k);

	void printProgress(bool p);

	void seedRange(unsigned from,unsigned to);
	void run();

	OutputDataCollection* resultsByName(std::string t) const;
	unsigned runs() const;

private:
	MCKernelBase*							m_kernel=nullptr;
	std::vector<OutputDataCollection*>		m_resultsByType;

	typedef  boost::any_range<
		unsigned,
		boost::forward_traversal_tag,
		unsigned&,
		std::ptrdiff_t> seed_range;

	bool										m_printProgress=false;
	seed_range								m_seedRange;
};

#endif /* KERNELS_SEEDSWEEP_HPP_ */
