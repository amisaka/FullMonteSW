#include "OStreamObserver.hpp"
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

void OStreamObserver::notify_create(const Kernel&)
{
	os << "Run created" << endl;
}

void OStreamObserver::notify_start(const Kernel&)
{
	os << "Run started" << endl;
}

void OStreamObserver::notify_finish(const Kernel&)
{
	os << "Run finished" << endl;
}

void OStreamObserver::notify_result(const Kernel&,const OutputData* lr)
{
	os << "Result available: " << lr->type()->name() << endl;
}

