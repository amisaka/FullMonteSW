/*
 * TetraMCKernel.hpp
 *
 *  Created on: Mar 4, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_TETRAMCP8KERNEL_HPP_
#define KERNELS_P8_TETRAMCP8KERNEL_HPP_

#include <FullMonteSW/Geometry/Sources/Print.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>			//	TODO: Only here until a Layered factory exists
#include <FullMonteSW/Kernels/Software/Emitters/Point.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Directed.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/MCEventCounts.hpp>
#include <FullMonteSW/OutputTypes/MCConservationCounts.hpp>

#include <FullMonteSW/Geometry/Partition.hpp>

#include <FullMonteSW/Kernels/Software/Material.hpp>

#include "P8MCKernelBase.hpp"
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>

#include "P8TetrasFromLayered.hpp"
#include "P8TetrasFromTetraMesh.hpp"
#include <FullMonteSW/Kernels/Software/TetrasFromTetraMesh.hpp>

#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>

#include <thread>
#include <chrono>

class Partition;
class TetraMesh;

#include <boost/align/aligned_alloc.hpp>
#include <FullMonteSW/Kernels/Software/Logger/tuple_for_each.hpp>

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

#include <BlueLink/Host/AFU.hpp>
#include <BlueLink/Host/WED.hpp>

#include <FullMonteHW/Host/CXXFullMonteTypes.hpp>
#include <BDPIDevice/BitPacking/Packer.hpp>
#include <BDPIDevice/BitPacking/Unpacker.hpp>
#include <BDPIDevice/BitPacking/VerilogStringFormat.hpp>

class TetraP8Kernel
{
public:
	/*void markTetraFaceForFluenceScoring(unsigned IDt, unsigned faceIdx, bool en) {
		// mark 'faceIdx' of tetra 'IDt' to either score or not score ('en') surface fluence events
		unsigned mask = 1 << (faceIdx<<3);
		if(en) {
			m_tetras[IDt].faceFlags |= mask;
		} 
		else {
			m_tetras[IDt].faceFlags &= ~mask;
		}
	}*/

protected:

	std::vector<FullMonteHW::TetraDef> m_tetras;
};

template<class RNGT>class TetraMCP8Kernel : public P8MCKernelBase, public TetraP8Kernel
{
public:
	TetraMCP8Kernel():afu(DEVICE_STRING){
		// allocate sizeof(RNG) bytes aligned to 32 bytes
        // NOTE: this does NOT initialize the memory, that is done below
        void* p = boost::alignment::aligned_alloc(32,sizeof(RNG));

        // ensure the allocation succeeded
        if (!p) {
	        cerr << "Allocation failure in TetraMCP8Kernel<RNGT>(); the constructor" << endl;
		    throw std::bad_alloc();
	    }

        // memory allocation was good, now use this aligned memory to initialize the RNG
        // NOTE: this calls the constructor of RNG (and therefore all child constructors).
        // If you simply cast p (i.e. m_rng = (RNG*) p) you won't get compile errors but the
        // RNG (notably the position into the buffer 'm_pos') will not be initialized and segfaults occur.
        m_rng = new (p) RNG();

	}

	struct Output
	{
		float weight;
		unsigned ID;
		bool is_face;
	};

	//convert the packets to a Hardware compatible type with the right bit precision
    static FullMonteHW::LaunchPacket convertFromFloatType(const LaunchPacket& j)
	{
		FullMonteHW::LaunchPacket ip; 
        std::array<float,3> pos, d, a, b;

		for(int i=0; i<3; i++)
		{
			pos[i] = j.pos[i];
			d[i] = j.dir.d[i];
			a[i] = j.dir.a[i];
			b[i] = j.dir.b[i];
		}
		    
        ip.tetID = FullMonteHW::TetraID(j.element);
        ip.dir.d = FullMonteHW::UVect3D_18(d);//18 bit precision
        ip.dir.a = FullMonteHW::UVect3D_18(a);//18 bit precision
        ip.dir.b = FullMonteHW::UVect3D_18(b);//18 bit precision
        ip.pos = FullMonteHW::Point3D_18(pos);//18 bit precision

		return ip; 
	};

	//convert the packets from a Hardware compatible type with the right bit precision to normal float types.
    static Output convertToFloatType(const FullMonteHW::Output& j)
	{
		Output out;
		out.weight = (float) j.photon_weight;
		out.ID = (unsigned) j.ID;
		out.is_face = (bool) j.is_face;
		return out;
	};

	typedef RNGT RNG;

	void inputBits(std::size_t bits)    { m_inputBits=bits; }
    std::size_t inputBits()     const   { return m_inputBits; }

protected:

	RNG*	 m_rng;

	virtual void parentPrepare() 			override;

	virtual void parentStart()	 			override;

	//virtual void parentFinish()	 			override;

	virtual void parentResults() 			override;

	void launchPacket_thread();
	void outputGather_thread();

	Emitter::EmitterBase<RNG>* 						m_emitter;

    // randLaunch is a vector that stores all generated launchpackets
	vector< LaunchPacket > randLaunch;

	//input vector stores the data which is going to be sent from host to the FPGA
	//output vector stores the data which is going to be sent from FPGA to the host
	//data is aligned in 128 (cache line size aligned)
	//each input container is std::array<uint64_t,4> which can hold 512 bits in total (size of each bitstream sent to FPGA)
    vector<std::array<uint64_t,4>,boost::alignment::aligned_allocator<std::array<uint64_t,4>,128>> input;
    vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> output_buffer0;
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> output_buffer1;
	vector<float> outputVolume;
	vector<float> outputSurface;
	unsigned int m_faces_size = 0;
	vector<float> outputDirectedSurface;

	vector<std::array<uint64_t,16>,boost::alignment::aligned_allocator<std::array<uint64_t,16>,128>> m_tetras_serialized;
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> m_mats_serialized;
	vector<std::array<uint64_t,1>,boost::alignment::aligned_allocator<std::array<uint64_t,1>,128>> m_interfaceDef_serialized;

	FullMonteHW::PipelineStats PipelineStat; // struct instance to unpack the HW output data  from PipelineStats into
	FullMonteHW::CacheStats CacheStat; // struct instance to unpack the HW output data  from CacheStats into
	FullMonteHW::EventCacheStats EventCacheStat; // struct instance to unpack the HW output data from EventCacheStats into

	// average writeLatency over the whole simulation measured by the hardware
	float writeLatencyHW_mean = 0;
	// average processing latency of the host over the whole simulation measured by the hardware
	float readWriteLatencyHostHW_mean = 0;

	// used in double buffering for the LaunchPackets. The block to be transferred should be copied into the buffer before streaming
    // 32 -> maximum number of tags that the hardware currently supports
	const size_t input_bufferSize=32;
	// used in double buffering for the absorption/exit events.
    // 64 -> arbitrary number of elements that are currently transferred to the host by the hardware
	const size_t output_bufferSize=64;
	
	//Number of bytes per copy in each stream
	const size_t NbytesPerCopy=512;

	// Number of Bits that are sent in one packet
	std::size_t m_inputBits=512;
    //get bytes per stream from the user
	void bytesperStream( size_t N)      { NbytesPerCopy=N; }

	struct MemcopyWED 
    {
		uint64_t        addr_inStart; // start address of array to copy from host (to FPGA).
		uint64_t        addr_inEnd; // end addresses of array to copy from host.
		uint64_t        addr_out0; // start address of array to copy to host (from FPGA)
		uint64_t        addr_out1; // 2 addresses for double buffering
		uint64_t		addr_mesh; // start address of tetramesh array on the host system
		uint64_t		addr_mats; // start address of the material set for the simulation on the host system
		uint64_t        mats_size; // number of materials we need to load into HW
		uint64_t		addr_ifc; // start address of the interface definitions on the host system
		uint64_t		ifc_size; // number of interface definitions we need to load into HW
		uint64_t        input_size; // number of bytes to copy to HW in each iteration (from host to FPGA)
		uint64_t        output_size; // number of bytes to copy to host in each iteration (from FPGA to host)
		uint64_t        wmin; // minimum weight for roulette
		uint64_t        resv[4]; //Assumming 1024 bit total size reserved for WED

    };

	//AFU: APPLICATION FUNCTION UNIT used in PSL
	AFU afu;
	
    //WED: WORK ELEMENT DESCRIPTION used in PSL
	StackWED<MemcopyWED,128,128> wed;

};

template<class RNGT>void TetraMCP8Kernel<RNGT>::outputGather_thread()
{
	unsigned long long hwStatus=0;
	unsigned long long writeLatencyHW = 0;
	unsigned long long readWriteLatencyHostHW = 0;
	unsigned latency_count = 1;
	//Instantiate the Unpacker. It must have the same type as the Packer which was previously instantiated
	// Unpacker<std::array<uint64_t,2>> packetUnpacker(FullMonteHW::Vec_Output::bits_, output_buffer1[0]);
	// FullMonteHW::Output out;
	int mask = 1 << 27;
	double weight;
	unsigned face;
	unsigned ID;
	hwStatus=afu.mmio_read64(0);
	while ( hwStatus != STATUS_DONE && hwStatus != STATUS_ERROR)
	{
		/**** Debug code to read out the states of the hardware*************************************
		int num = 0;
		LOG_INFO << "Input valid numbers 0 (master), 3 (output), or 4(launchPacket)" << endl;
		cin >> num;
		switch (afu.mmio_read64(num<<3))
		{
			case STATUS_READY: LOG_INFO << "Ready for state " << num << endl; break;
			case STATUS_WAITING: LOG_INFO << "Waiting for state " << num << endl; break;
			case STATUS_RUNNING: LOG_INFO << "Running for state " << num << endl; break;
			case STATUS_DONE: LOG_INFO << "Done for state " << num << endl; break;
			case READ_READY: LOG_INFO << "Read Ready for state " << num << endl; break;
			default: break;
		}
		*******************************************************************************************/
		//LOG_INFO << "Read from AFU Packet count: " << afu.mmio_read64(1<<3) << endl;
		// This reads out the 3rd case of DWordRead in the 'interface Server mmio' in the subinterface 'interface Put request'
		// We need to shift the actual number 3 by 3 bits to the left to access the data correctly.
		// LOG_INFO << "  Photon Pipeline not emtpy: " << afu.mmio_read64(6<<3) << endl
				//  << " tetraLookup_FIFO not emtpy: " << afu.mmio_read64(7<<3) << endl
				//  << "launchpacket_FIFO not emtpy: " << afu.mmio_read64(8<<3) << endl;
		switch (afu.mmio_read64(3<<3))
		{
		case READ_READY:
		{
			for (unsigned i=0; i<output_bufferSize; ++i)
			{
				// LOG_INFO << hex << output_buffer1[i][0] << " "
				// 			<< output_buffer1[i][1] << " "
				// 			<< output_buffer1[i][2] << " "
				// 			<< output_buffer1[i][3] << " "
				// 			<< output_buffer1[i][4] << " "
				// 			<< output_buffer1[i][5] << " "
				// 			<< output_buffer1[i][6] << " "
				// 			<< output_buffer1[i][7] << dec << endl;

				// De-serialization of HW output data back to a software type FullMonteHW::Output
				// packetUnpacker & out;
				weight = (double) output_buffer0[i][0] / pow(2.0, 36);
				face = output_buffer0[i][1] >> 27;
				ID = (output_buffer0[i][1] & ~mask) | ((0 << 27) & mask);

				//LOG_INFO << "Output from SW: " << endl << out << endl;
				// Convert FullMonteHW::Output to Output struct from FullMonteSW
				// Output weight_increment = convertToFloatType(out);
			
				if (ID == 0)
				{
					// LOG_INFO << "NULL packet detected. Discarding..." << endl;
				}
				else if (face == 0)
				{
					// cout << dec << weight_increment.TetraID << endl;
					// LOG_INFO << "Volume Scoring detected:"<< endl
					// << "TetraID to write weight to: " << ID << endl
					// << "Value to increment weight by: " << weight << endl;
					outputVolume[ID] += weight;
				}
				else // if (weight_increment.is_face == 1)
				{
					// LOG_INFO << "Surface Exit Scoring detected:"<< endl 
					// << "FaceID to write weight to: " << ID << endl
					// << "Weight value exiting the mesh: " << weight << endl;
					outputSurface[ID] += weight;
				}			
			}

			// Signal the hardware that the memory can be overwritten again
			afu.mmio_write64(0,0x4ULL);
			
			// Get the write latency measurements from the hardware
			writeLatencyHW = 0;
			writeLatencyHW = afu.mmio_read64(6<<3);
			writeLatencyHW_mean += ((float) writeLatencyHW  - writeLatencyHW_mean) / latency_count;

			readWriteLatencyHostHW = 0;
			readWriteLatencyHostHW = afu.mmio_read64(7<<3);
			readWriteLatencyHostHW_mean += ((float) readWriteLatencyHostHW - readWriteLatencyHostHW_mean) / latency_count;

			latency_count++;
			
			// LOG_DEBUG << "Latency measured by the HW:" << endl
			// 			<< " HW Write Latency: " << writeLatencyHW << endl
			// 			<< " HW Write Latency + Host Read Latency: " << readWriteLatencyHostHW << endl;
			break;
		}
		default:
			break;
		}
		//usleep(100);
		hwStatus=afu.mmio_read64(0);
	}
}

template<class RNGT>void TetraMCP8Kernel<RNGT>::parentPrepare()
{
	////// Geometry setup

	const Geometry* G = geometry();

	if (!G)
		throw std::logic_error("TetraMCP8Kernel<RNG>::parentPrepare() no geometry specified");
		
	// We need the material information to accurately create the HW tetras
	const MaterialSet * MS = materials();
	
	if (!MS)
		throw std::logic_error("TetraMCP8Kernel<RNGT>::parentPrepare() no materials specified");

	/////// Conversion HW compatible Mesh 
	if (const TetraMesh* M = dynamic_cast<const TetraMesh*>(G))
	{
		LOG_INFO << "Building kernel tetras from TetraMesh" << endl;
		P8TetrasFromTetraMesh TF;

		std::array<double,3> translation_vector;
		double scale = 1;
		tie(translation_vector, scale) = TF.mesh(M);
		scaleValue(scale);
		translationVector(translation_vector);

		TF.update(MS);

		m_faces_size = TF.face_numbers();
		m_tetras = TF.tetras();
		m_tetras_serialized.resize(m_tetras.size());

		// TetraDef is 484 bits large and is defined in CXXFullMonteTypes.hpp
		Packer<std::array<uint64_t,16>> tetraPacker(FullMonteHW::TetraDef::bits,m_tetras_serialized[0]);
		
/*
		//For verification only this does not belong here
		TetrasFromTetraMesh Test;
		Test.mesh(M);
		Test.update();
		
		std::vector<Tetra> tetras = Test.tetras();
*/
		size_t index = 0;
		for(auto& i : m_tetras_serialized)
		{
			tetraPacker.reset(i);

			tetraPacker & m_tetras[index];
/*			
			//Test to check if the HW optimized Tetra representation is the same as the SW representation 
			if (index < 10)
			{
				LOG_INFO << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::TetraDef::bits) << tetraPacker.value() << endl;
				m_tetras[index].print(cout);
				//m_tetras[index].printHex(cout);
				//printTetra(tetras[i],cout);
				cout << endl;
			}
*/			
			index++;
		}
		//m_tetras[25515].print(cout);
		//m_tetras[25515].printHex(cout);
/*
		for(unsigned i=0; i<10; i++)
		{
			LOG_INFO << hex << m_tetras_serialized[i][0] << " "
						<< m_tetras_serialized[i][1] << " "
						<< m_tetras_serialized[i][2] << " "
						<< m_tetras_serialized[i][3] << " "
						<< m_tetras_serialized[i][4] << " "
						<< m_tetras_serialized[i][5] << " "
						<< m_tetras_serialized[i][6] << " "
						<< m_tetras_serialized[i][7] << " "
						<< m_tetras_serialized[i][8] << dec << endl;
		}
*/
		// Create the interfaceDef struct for all material combinations that might exist within the mesh
		FullMonteHW::InterfaceDef interface;
		
		// the number of interfaces that are transferred to the hardware have to be dividable 16 (1 memory transfer is 1024 bit (128 byte), 16 interfaces fit in 128 byte)
		// We round up the number of materials to the next number dividable by 16
		m_interfaceDef_serialized.resize( ( MS->size()*MS->size() )+ ( 256 - ( MS->size()*MS->size() ) )%16 );
		Packer<std::array<uint64_t,1>> interfaceDefPacker(FullMonteHW::InterfaceDef::bits,m_interfaceDef_serialized[0]); // Packer for serializing InterfaceDef input
		LOG_INFO << " Creating interface definitions for the Reflect/Refract module" << endl;
		unsigned k = 0,j = 0, count = 0;
		for(auto& i : m_interfaceDef_serialized)
		{
			if ( count < (MS->size()*MS->size()) )
			{
				interface.ifcID = TF.getInterfaceID(k,j);
				float ni_nt = MS->get(k)->refractiveIndex() / MS->get(j)->refractiveIndex();
				interface.ni_nt_ratio = ni_nt;
				float nt_ni = 1/ni_nt;
				interface.nt_ni_ratio = nt_ni;
				// calculation of critical angle theta_crit = arcsin(n_t/n_i) -> cos_crit = cos(arcsin(n_t/n_i))
				if (nt_ni >= 1)
				{
					interface.costheta_crit = 0;
				}
				else
				{
					interface.costheta_crit = cosf(asinf(nt_ni));
				}
			}
			else
			{
				interface.ifcID = 0;
				interface.ni_nt_ratio = 0;
				interface.nt_ni_ratio = 0;			
				interface.costheta_crit = 0;
			}
			interfaceDefPacker.reset(i);
			interfaceDefPacker & interface;

			// LOG_INFO << "               InterfaceID: " << interface.ifcID.value() << endl
			// 		 << " critical angle cos(theta):" << interface.costheta_crit.integer_value() << endl
			// 		 << "                   n_i/n_t: " << interface.ni_nt_ratio.integer_value() << endl
			// 		 << "                   n_t/n_i:" << interface.nt_ni_ratio.integer_value() << endl
			// 		 << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::InterfaceDef::bits) << interfaceDefPacker.value() << endl;
			count++;
			if(++j == MS->size())
			{
				j = 0;
				k++;
			}

		}
	//////////////////////////////////////////////////
		if (!m_src)
			throw std::logic_error("TetraMCKernel<RNGT>::parentPrepare() no sources specified");

		// TODO: Move this out of here as it doesn't quite belong
		Emitter::TetraEmitterFactory<RNGT> factory(M, MS);

		((Source::Abstract*)m_src)->acceptVisitor(&factory);

		m_emitter = factory.emitter();

		//LOG_INFO << "Sources set up" << endl;
	}
	else if (const Layered* L = dynamic_cast<const Layered*>(G))
	{
		LOG_INFO << "Building kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
		P8TetrasFromLayered TL;

		TL.layers(L);
		TL.update(MS);

		m_tetras = TL.tetras();

		if (m_src)
		{
			LOG_INFO << "TetraMCP8Kernel::parentPrepare() ignoring provided source with Layered geometry (only PencilBeam allowed)" << endl;
		}

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		SSE::UnitVector3 d{{{ 0.0f, 0.0f, 1.0f }}};
		SSE::UnitVector3 a{{{ 1.0f, 0.0f, 0.0f }}};
		SSE::UnitVector3 b{{{ 0.0f, 1.0f, 0.0f }}};

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#else
		SSE::UnitVector3 d({0.0f, 0.0f, 1.0f, 0.0f });
		SSE::UnitVector3 a({1.0f, 0.0f, 0.0f, 0.0f });
		SSE::UnitVector3 b({0.0f, 1.0f, 0.0f, 0.0f });

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#endif
		Emitter::Point P{1U,SSE::Vector3(origin.data())};
		Emitter::Directed D(PacketDirection(d,a,b));

		m_emitter = new Emitter::PositionDirectionEmitter<RNGT,Emitter::Point,Emitter::Directed>(P,D);
	}
	else
		throw std::logic_error("TetraMCP8Kernel<RNG>::parentPrepare() geometry is neither Layered nor TetraMesh");

	//LOG_INFO << "Done with " << m_tetras.size() << " tetras" << endl;
	
	////// Material setup
	FullMonteHW::MaterialStruct m_mats;
	// the number of materials that are transferred to the hardware have to be dividable 8 (1 memory transfer is 1024 bit (128 byte), 8 materials fit in 128 byte)
	// We round up the number of materials to the next number dividable by 8
	m_mats_serialized.resize(MS->size()+(16-MS->size())%8);
	
	// Materialstruct is 128 bits large and is defined in CXXFullMonteTypes.hpp
	Packer<std::array<uint64_t,2>> materialPacker(FullMonteHW::MaterialStruct::bits,m_mats_serialized[0]);
	// copy materials
	size_t index = 0;
	for(auto& i : m_mats_serialized)
	{
		materialPacker.reset(i);
		::Material* m;
		// Once we reach the number of materials defined for the mesh, we set the data to 0 and only increase the material index in order to not
		// mess with the defined materials (we have to do this because our material IDs start at 0)
		if(index < MS->size())
		{
			m = MS->get(index);
			m_mats = FullMonteHW::MaterialStruct::calculateFromProperties(m->absorptionCoeff()*scaleValue(), m->scatteringCoeff()*scaleValue(), m->anisotropy());
		}
		else
		{
			m_mats = FullMonteHW::MaterialStruct::calculateFromProperties(0, 0, 0);
		}
		m_mats.matID = index; 
		
		materialPacker & m_mats;
		index++;
		//LOG_INFO << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::MaterialStruct::bits) << materialPacker.value() << endl;
	}
/*
	for(unsigned i=0; i<m_mats_serialized.size(); i++)
		{
			LOG_INFO << hex << m_mats_serialized[i][0] << " "
						<< m_mats_serialized[i][1] << endl;
		}
*/

	// The fluence output vector must have the same size as the number of tetras because potentially all tetras could have a fluence scored in them
	outputVolume.resize(m_tetras.size());
	outputSurface.resize(m_faces_size);

	output_buffer0.resize(output_bufferSize);
	output_buffer1.resize(output_bufferSize);
	resetSeedRng();
	
}

template<class RNGT>void TetraMCP8Kernel<RNGT>::parentStart()
{
	////// Launchpacket setup
	randLaunch.resize(Npkt_);
	
	// Input vector for the accelerator which contains the all launchpackets
	input.resize(Npkt_);

	// We need to seed the RNG. If we want to get consistent reuslts for debug purposes, we need to hardcode a value into the seed.
	m_rng->seed(getUnsignedRNGSeed());
	LaunchPacket lpkt_in;
	for(unsigned i=0;i<Npkt_;i++)
	{
		//use emit from the emitter factory to generate launch packets
		lpkt_in = m_emitter->emit(*m_rng,Npkt_,i);
		
/*		
		LOG_INFO << std::hex << std::setprecision(4) << "TetraID: " << lpkt_in.element << endl
						 << "pos: " << std::setw(7) << lpkt_in.pos[0]	<< ',' << std::setw(7) << lpkt_in.pos[1]   << ',' << std::setw(7) << lpkt_in.pos[2]   << endl 
						 << "dir: " << std::setw(7) << lpkt_in.dir.d[0]  << ',' << std::setw(7) << lpkt_in.dir.d[1] << ',' << std::setw(7) << lpkt_in.dir.d[2] << endl 
						 << "a: "   << std::setw(9) << lpkt_in.dir.a[0]  << ',' << std::setw(9) << lpkt_in.dir.a[1] << ',' << std::setw(9) << lpkt_in.dir.a[2] << endl 
						 << "b: "   << std::setw(9) << lpkt_in.dir.b[0]  << ',' << std::setw(9) << lpkt_in.dir.b[1] << ',' << std::setw(9) << lpkt_in.dir.b[2] << endl << endl;
*/		
		double scale = scaleValue();
        std::array<double,3> translation_vector = translationVector();
        lpkt_in.pos = (lpkt_in.pos - SSE::Vector<3>(translation_vector)) / SSE::Scalar(scale);
		randLaunch[i] = lpkt_in;
	}

	// Pack launch packets into hardware compatible packets with the right bit precision 
	
    // Pack input of LaunchPacket into 256 bits (4*64bit uint) -> The launch packet fits in 256 bits but we always trasnfer 1024 bits to the hardware
	// so in this case, the hardware wil always read out 4 launch packets at the same time
    Packer<std::array<uint64_t,4>> packetPacker(FullMonteHW::LaunchPacket::bits_,input[0]);

	size_t index=0;
	for (auto& i : input)
    {
		packetPacker.reset(i);

        // Combine the bits in packetPacker with the bits from the output of convertFromFloatType so basically:
        // Serialize the data from randLaunch to a sequence of bytes; see boost serialization for more information
        // packetPacker = (packetPacker append convertFromFloatType)
		packetPacker & convertFromFloatType(randLaunch[index]);
		index++;
/*
		if (index < 10)
		{
			LOG_INFO << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::LaunchPacket::bits_) << packetPacker.value() << endl;
		}
*/
	}
/*
	for(unsigned i=0; i< 10; i++)
	{
		LOG_INFO << hex << input[i][0] << " "
					<< input[i][1] << " "
					<< input[i][2] << " "
					<< input[i][3] << endl;
	}
*/
///////////////////////////// Start Launching in HW///////////////////////////////////////////////////////////////////////////////
	//stream packets from host to FPGA and copy back from FPGA to host for validation
	


	vector<std::array<uint64_t,4>,boost::alignment::aligned_allocator<std::array<uint64_t,4>,128>> buffer0(input_bufferSize), buffer1(input_bufferSize);
	assert(boost::alignment::is_aligned(128,buffer0.data()));
	assert(boost::alignment::is_aligned(128,buffer1.data()));

	assert(boost::alignment::is_aligned(128,input.data()));
	assert(boost::alignment::is_aligned(128,output_buffer0.data()));
	assert(boost::alignment::is_aligned(128,output_buffer1.data()));

	// load first input buffer
	// buffers for double-buffered transfer
	int bufferNum = 0;
	for (unsigned i=0; i<input_bufferSize; i++) 
	{
		if (input.size() > i)
			buffer0[i] = input[i];
		else
		{
			buffer0[i][0] = 0;
			buffer0[i][1] = 0;
			buffer0[i][2] = 0;
			buffer0[i][3] = 0;
		}
	}
	bufferNum = 1;
	// wed->addr_inStart = (uint64_t)input.begin().base();
	// wed->addr_inEnd = (uint64_t)input.end().base();
	wed->addr_inStart = (uint64_t)buffer0.data();
	wed->addr_inEnd = (uint64_t)buffer1.data();
	wed->addr_out0 = (uint64_t)output_buffer0.data();
	wed->addr_out1 = (uint64_t)output_buffer1.data();
	wed->addr_mesh = (uint64_t)m_tetras_serialized.data();
	wed->addr_mats = (uint64_t)m_mats_serialized.data();
	wed->addr_ifc = (uint64_t) m_interfaceDef_serialized.data();
	// This determines the amount of bytes needed to transfer "input_bufferSize" launch packets to the hardware
	// 128 -> minimum number of bytes that are transferred to the hardware (a PSL cache line is 128 Bytes long)
	// 32  -> size of the LaunchPacket struct in Byte
	// ceil() -> calculates the number of 128 Byte transfers we need to get all launch packet data from the buffer sent to the hardware
	wed->input_size = 128*(ceil(32*input_bufferSize/128));
	// This determines the amount of bytes needed to transfer "output_bufferSize" absorption/exit event packets to the host
	// 128 -> minimum number of bytes that are transferred to the hardware (a PSL cache line is 128 Bytes long)
	// 16  -> size of the Output struct in Byte
	// ceil() -> calculates the number of 128 Byte transfers we need to get all material data sent to the hardware
	wed->output_size = 128*(ceil(16*output_bufferSize/128));
	// This determines the amount of bytes needed to transfer all materials from the main memory to the hardware
	// 128 -> minimum number of bytes that are transferred to the hardware (a PSL cache line is 128 Bytes long)
	// 16  -> size of the material struct in Byte
	// ceil() -> calculates the number of 128 Byte transfers we need to get all material data sent to the hardware
	// 128 Bytes * ceil(16 Bytes * # materials / 128 Bytes) = Number of bytes to be transferred
	wed->mats_size = 128*(ceil(16*m_mats_serialized.size()/128));
	// This determines the amount of bytes needed to transfer all interface definition from the main memory to the hardware
	// 128 -> minimum number of bytes that are transferred to the hardware (a PSL cache line is 64 Bytes long)
	// 8   -> size of the interface struct in Bytes
	// ceil() -> calculates the number of 128 Byte transfers we need to get all material data sent to the hardware
	wed->ifc_size = 128*(ceil(8*m_interfaceDef_serialized.size())/128);
	// The minimum weight as defined in the tcl script to perform roulette on a packet to determine if it lives or dies
	wed->wmin = (uint64_t) FullMonteHW::Weight(wmin_);

	LOG_INFO << "Addresses and Size of Hardware Resources:" << endl
	<< "       Input Address Start: " << hex << setw(16) << wed->addr_inStart << endl
	<< "         Input Address End: " << hex << setw(16) << wed->addr_inEnd << endl
	<< "       Transfer size Input: " << dec << setw(16) << wed->input_size << endl
	<< "            Output Buffer0: " << hex << setw(16) << wed->addr_out0 << endl
	<< "            Output Buffer1: " << hex << setw(16) << wed->addr_out1 << endl
	<< "      Transfer size Output: " << dec << setw(16) << wed->output_size << endl
	<< "                    Tetras: " << hex << setw(16) << wed->addr_mesh << endl
	<< "                 Materials: " << hex << setw(16) << wed->addr_mats << endl
	<< "   Transfer size Materials: " << dec << setw(16) << wed->mats_size << endl
	<< "                Interfaces: " << hex << setw(16) << wed->addr_ifc << endl
	<< "  Transfer size Interfaces: " << dec << setw(16) << wed->ifc_size << endl
	<< "           Roulette weight: " << dec << setw(16) << wed->wmin << endl;
	// exit(0);
	afu.start(wed.get(), 2);

	unsigned long long st=0;
	
	// instead of interrupt, poll for Waiting status
	unsigned N;
	for(N=0;N<100 && (st=afu.mmio_read64(0)) != STATUS_WAITING;++N)
	{
		LOG_INFO << "  Waiting for 'waiting' status (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
		usleep(100);
	}
	std::thread getResults(&TetraMCP8Kernel<RNGT>::outputGather_thread, this);
	for(unsigned i=0;i<4;++i)
		LOG_INFO << "i = " << i << ", MMIO[" << setw(6) << hex << (i<<3) << "] " << setw(16) << dec << afu.mmio_read64(i<<3) << dec << endl;

	afu.mmio_write64(0,0x0ULL);             // start signal: write 0 to MMIO 0

	uint64_t idx_counter;
	for (idx_counter=0; idx_counter < Npkt_; idx_counter += input_bufferSize) {
		// afu.print_details();
		if (Npkt_ < input_bufferSize)
		{
			LOG_INFO << "100.00% of packets transferred";
		}
		else
		{
			LOG_INFO << setprecision(5) << (float(idx_counter+input_bufferSize)/float(Npkt_))*100.0f <<"% of packets transferred\r" << flush;
		}
		
		// LOG_INFO << "Starting copy " << idx_counter / bufferSize << ", input is on buffer " << bufferNum << endl;
		// LOG_INFO << "Number of transferred packets: " << idx_counter << endl;

		// load input buffer for next copy
		if (idx_counter + input_bufferSize < Npkt_) 
		{
			if (bufferNum == 0) 
			{
				// LOG_INFO << "Copying to buffer 0...  ";
				for (unsigned i=0; i<input_bufferSize; i++) 
				{
					if (input.size() > (input_bufferSize + idx_counter + i))
						buffer0[i] = input[input_bufferSize + idx_counter + i];
					else
					{
						buffer0[i][0] = 0;
						buffer0[i][1] = 0;
						buffer0[i][2] = 0;
						buffer0[i][3] = 0;
					}
				}
				// LOG_INFO << "Finished." << endl;

				bufferNum = 1;
			}
			else 
			{
				// LOG_INFO << "Copying to buffer 1...  ";
				for (unsigned i=0; i<input_bufferSize; i++) 
				{
					if (input.size() > (input_bufferSize + idx_counter + i))
						buffer1[i] = input[input_bufferSize + idx_counter + i];
					else
					{
						buffer1[i][0] = 0;
						buffer1[i][1] = 0;
						buffer1[i][2] = 0;
						buffer1[i][3] = 0;
					}
				}
				// LOG_INFO << "Finished." << endl;

				bufferNum = 0;
			}
			while ( ( st=afu.mmio_read64(4<<3) ) != STATUS_WAITING)
			{
				LOG_DEBUG << "  Waiting for HW read operation to complete (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
			}
			// Send "Continue" signal to Hardware -> HW will set the continueCopy signal to True
			afu.mmio_write64(0,0x1ULL);
		}
		else
		{
			while ( ( st=afu.mmio_read64(4<<3) ) != STATUS_WAITING)
			{
				LOG_DEBUG << "  Waiting for HW read operation to complete (st=" << st << " looking for " << STATUS_WAITING << ")" << endl;
			}
			// LaunchPacket transfer is complete -> HW should set the continueCopy signal to Falsee
			afu.mmio_write64(0,0x2ULL);
		}		
	}
	cout << endl;
	LOG_INFO << "LaunchPacket stream complete" << endl;

	getResults.join();
	// if(afu.mmio_read64(0) == -1)
	// 	exit(0);
	LOG_INFO <<"DONE WITH STREAMING"<<endl;
	afu.process_event_blocking ();

	
	
	while((st=afu.mmio_read64(0)) != STATUS_DONE)
	{
		LOG_INFO << "  Waiting for 'done' status (st=" << st << " looking for " << STATUS_DONE << ")" << endl;
		usleep(100);
	}

	vector<std::array<uint64_t,18>,boost::alignment::aligned_allocator<std::array<uint64_t,18>,128>> outputPipelineStats;
	outputPipelineStats.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// Unpacker for de-serializing the data into the StepResult struct
	Unpacker<std::array<uint64_t,18>> PipelineStatsUnpacker(FullMonteHW::PipelineStats::bits, outputPipelineStats[0]);

	outputPipelineStats[0][0] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][1] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][2] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][3] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][4] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][5] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][6] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][7] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][8] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][9] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][10] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][11] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][12] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][13] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][14] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][15] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][16] = afu.mmio_read64(5<<3);
	outputPipelineStats[0][17] = afu.mmio_read64(5<<3);

	// De-serialization of HW output data back to a software type FullMonteHW::Output
	PipelineStatsUnpacker & PipelineStat;

	vector<std::array<uint64_t,3>,boost::alignment::aligned_allocator<std::array<uint64_t,3>,128>> outputCacheStats;
	
	outputCacheStats.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// Unpacker for de-serializing the data into the StepResult struct
	Unpacker<std::array<uint64_t,3>> CacheStatsUnpacker(FullMonteHW::CacheStats::bits, outputCacheStats[0]);

	outputCacheStats[0][0] = afu.mmio_read64(5<<3);
	outputCacheStats[0][1] = afu.mmio_read64(5<<3);
	outputCacheStats[0][2] = afu.mmio_read64(5<<3);

	// De-serialization of HW output data back to a software type FullMonteHW::Output
	CacheStatsUnpacker & CacheStat;

	vector<std::array<uint64_t,5>,boost::alignment::aligned_allocator<std::array<uint64_t,5>,128>> outputEventCacheStats;
	
	outputEventCacheStats.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// Unpacker for de-serializing the data into the StepResult struct
	Unpacker<std::array<uint64_t,5>> EventCacheStatsUnpacker(FullMonteHW::EventCacheStats::bits, outputEventCacheStats[0]);

	outputEventCacheStats[0][0] = afu.mmio_read64(5<<3);
	outputEventCacheStats[0][1] = afu.mmio_read64(5<<3);
	outputEventCacheStats[0][2] = afu.mmio_read64(5<<3);
	outputEventCacheStats[0][3] = afu.mmio_read64(5<<3);
	outputEventCacheStats[0][4] = afu.mmio_read64(5<<3);

	// De-serialization of HW output data back to a software type FullMonteHW::Output
	EventCacheStatsUnpacker & EventCacheStat;

	LOG_INFO << "Shutting down Hardware" << endl;
	afu.mmio_write64(0,0x3ULL);
}

template<class RNGT>void TetraMCP8Kernel<RNGT>::parentResults()
{
	OutputDataCollection* C = results();
	C->clear();
	
	LOG_INFO << endl 
			 << "             Stalls in Pipeline: " << (unsigned long long) PipelineStat.nIntTestStall << endl
			 << "          Mean HW Write Latency: " << writeLatencyHW_mean << endl
			 << " Mean Host Smart Memory Latency: " << readWriteLatencyHostHW_mean << endl
			 << "----------------Tetra Lookup Cache Statistics----------------" << endl
			 << "                 Total accesses: " << (unsigned long long) CacheStat.nAccess << endl
			 << "                           Hits: " << (unsigned long long) CacheStat.nHit << endl
			 << "                         Misses: " << (unsigned long long) CacheStat.nMiss << endl
			 << "                        Balance: " << (unsigned long long) CacheStat.nAccess - (unsigned long long) CacheStat.nMiss - (unsigned long long) CacheStat.nHit << endl
			 << "                        Hitrate: " << (float) CacheStat.nHit.value() / (float) CacheStat.nAccess.value() << endl
			 << "----------------Event Cache Statistics----------------" << endl
			 << "                 Total accesses: " << (unsigned long long) EventCacheStat.nAccess << endl
			 << "                           Hits: " << (unsigned long long) EventCacheStat.nHit << endl
			 << "                         Misses: " << (unsigned long long) EventCacheStat.nMiss << endl
			 << "              Compulsory Misses: " << (unsigned long long) EventCacheStat.nCompulsory << endl
			 << "       Capacity+Conflict Misses: " << (unsigned long long) EventCacheStat.nConflictCapacity << endl
			 << "                   Miss Balance: " << (unsigned long long) EventCacheStat.nMiss - (unsigned long long) EventCacheStat.nCompulsory - (unsigned long long) EventCacheStat.nConflictCapacity << endl
			 << "                  Total Balance: " << (unsigned long long) EventCacheStat.nAccess - (unsigned long long) EventCacheStat.nMiss - (unsigned long long) EventCacheStat.nHit << endl
			 << "                        Hitrate: " << (float) EventCacheStat.nHit.value() / (float) EventCacheStat.nAccess.value() << endl << endl;
			 
	MCEventCounts HW_events;
	HW_events.Nlaunch		= (unsigned long long) PipelineStat.nLaunch;
    HW_events.Nabsorb		= (unsigned long long) PipelineStat.nAbsorb;
    HW_events.Nscatter		= (unsigned long long) PipelineStat.nScatter;
    HW_events.Nbound		= (unsigned long long) PipelineStat.nBoundary;
    HW_events.Ntir			= (unsigned long long) PipelineStat.nTIR;
    HW_events.Nfresnel		= (unsigned long long) PipelineStat.nFresnel;
	HW_events.Nrefr 		= (unsigned long long) PipelineStat.nRefract;
	HW_events.Ninterface	= (unsigned long long) PipelineStat.nBoundaryIfc;
	HW_events.Nexit			= (unsigned long long) PipelineStat.nExit;
	HW_events.Ndie			= (unsigned long long) PipelineStat.nDead;
	HW_events.Nwin			= (unsigned long long) PipelineStat.nRouletteWin;
	HW_events.Nabnormal		= 0;
	HW_events.Ntime			= 0;
	HW_events.Nnohit		= 0;

	MCEventCountsOutput* events = new MCEventCountsOutput(HW_events);
	events->name("EventCounts");
	
	C->add(events);

	MCConservationCounts HW_energy;
 	HW_energy.w_launch		= (double) PipelineStat.wLaunch;		///< Amount of energy launched (generally 1.0 * Npackets)
    HW_energy.w_absorb		= (double) PipelineStat.wAbsorb;		///< Amount of energy absorbed
    HW_energy.w_die			= (double) PipelineStat.wDead;			///< Amount of energy terminated in roulette
    HW_energy.w_exit		= (double) PipelineStat.wExit;			///< Amount of energy exiting
    HW_energy.w_roulette	= (double) PipelineStat.wRouletteWin;	///< Amount of energy added by winning roulette
    HW_energy.w_abnormal	= (double) 0.0;							///< Amount of energy terminated due to abnormal circumstances
    HW_energy.w_time		= (double) 0.0;							///< Amount of energy terminated due to time gate expiry
    HW_energy.w_nohit		= (double) 0.0;							///< Amount of energy terminated for failure to find an intersecting face

	MCConservationCountsOutput* conservation = new MCConservationCountsOutput(HW_energy);
	conservation->name("ConservationCounts");

	C->add(conservation);
	outputVolume.erase(outputVolume.begin());
	SpatialMap<float> *vmap = new SpatialMap<float>(std::move(outputVolume), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
	vmap->name("VolumeEnergy");

	C->add(vmap);

	SpatialMap<float> *smap = new SpatialMap<float>(std::move(outputSurface), AbstractSpatialMap::Surface, AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
    smap->name("SurfaceExitEnergy");

	C->add(smap);
}

#endif /* KERNELS_P8_TETRAMCP8KERNEL_HPP_ */


