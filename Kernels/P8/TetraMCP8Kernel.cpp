/*
 * TetraMCP8Kernel.cpp
 *
 *  Created on: Jun 9, 2016
 *      Author: jcassidy
 */

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include "TetraMCP8Kernel.hpp"

template class Emitter::TetraEmitterFactory<RNG_SFMT_AVX>;

