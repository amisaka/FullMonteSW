#if defined(SWIGTCL)
%module FullMontePowerKernelTCL
#elif defined(SWIGPYTHON)
%module P8Kernel
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");



%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_vector.i"
%include "std_string.i"

%include <FullMonteSW/Geometry/FullMonteGeometry_types.i>

%{
#include <FullMonteSW/Kernels/Kernel.hpp>
#include <FullMonteSW/Kernels/KernelObserver.hpp>
#include <FullMonteSW/Kernels/OStreamObserver.hpp>
#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <FullMonteSW/Kernels/P8/P8MCKernelBase.hpp>
//#include <FullMonteSW/Kernels/P8/P8DirectedSurfaceScorer.hpp>
#include <FullMonteSW/Kernels/P8/TetraMCP8Kernel.hpp>
#include <FullMonteSW/Kernels/P8/TetraMCP8DebugKernel.hpp>

#include <FullMonteSW/Kernels/P8/TetraP8InternalKernel.hpp>
#include <FullMonteSW/Kernels/P8/TetraP8DebugInternalKernel.hpp>
%}

%include "../Kernel.hpp"
%include "MCKernelBase.hpp" 
%include "P8MCKernelBase.hpp"
//%include "P8DirectedSurfaceScorer.hpp"
%include "TetraMCP8Kernel.hpp"
%include "TetraMCP8DebugKernel.hpp"

%template (TetraMCP8KernelAVX_V) TetraMCP8Kernel<RNG_SFMT_AVX>;
%template (TetraMCP8DebugKernelAVX_Debug) TetraMCP8DebugKernel<RNG_SFMT_AVX>;

%include "TetraP8InternalKernel.hpp"
%include "TetraP8DebugInternalKernel.hpp"
