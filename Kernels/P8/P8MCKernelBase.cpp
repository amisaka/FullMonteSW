/*
 * ThreadedMCKernelBase.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: jcassidy
 */

#include "P8MCKernelBase.hpp"

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <boost/align/aligned_alloc.hpp>

void P8MCKernelBase::awaitFinish()
{
	//parentFinish();
}


unsigned long long P8MCKernelBase::simulatedPacketCount() const
{
 return 0;
}

void P8MCKernelBase::start_()
{
	parentStart();
}

void P8MCKernelBase::prepare_()
{
	parentPrepare();
}

P8MCKernelBase::~P8MCKernelBase()
{

}

void P8MCKernelBase::gatherResults()
{
	OutputDataCollection* C = results();
	C->clear();

	parentResults();

}
