/*
 * TetraMCP8DebugKernel.cpp
 *
 *  Created on: Jun 9, 2016
 *      Author: jcassidy
 */

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include "TetraMCP8DebugKernel.hpp"

template class Emitter::TetraEmitterFactory<RNG_SFMT_AVX>;

