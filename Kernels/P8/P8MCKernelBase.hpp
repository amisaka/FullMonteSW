/*
 * P8MCKernelBase.hpp
 *
 *  Created on: Mar 4, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_P8MCKERNELBASE_HPP_
#define KERNELS_P8_P8MCKERNELBASE_HPP_

#include <FullMonteSW/Kernels/MCKernelBase.hpp>
#include <thread>
#include <vector>

#include <FullMonteSW/Config.h>

#include <boost/random/additive_combine.hpp>

#define DEVICE_STRING "/dev/cxl/afu0.0d"
#define STATUS_READY 0x1ULL
#define STATUS_WAITING 0x2ULL
#define STATUS_RUNNING 0x3ULL
#define STATUS_DONE 0x4ULL
#define READ_READY 0x5ULL
#define STATUS_ERROR 0xFFFFFFFFFFFFFFFFULL

class Geometry;

class P8MCKernelBase : public MCKernelBase
{
public:
	~P8MCKernelBase();

	void				threadCount(unsigned Nth)					{ m_threadCount=Nth;	}
	unsigned			threadCount()						const	{ return m_threadCount;	}

	// Final overrides: distribute the requests to the underlying threads
	virtual unsigned long long 	simulatedPacketCount() 	const 	final override;

	// Final override: do parent prep and then create HW interface
	virtual void 				prepare_()						final override;

	void resetSeedRng()
	{
		m_seedGenerator.seed(rngSeed_);
	}

	unsigned getUnsignedRNGSeed()
	{
		unsigned rnd = m_seedGenerator();
		m_seedGenerator.discard(1000);
		return rnd;
	}

	void					scaleValue(double scale)						{ m_scaleValue = scale; 	}
	double					scaleValue()									{ return m_scaleValue; 	}

	void					translationVector(std::array<double,3> vector)	{ m_translationVector = vector; 	}
	std::array<double,3>	translationVector()								{ return m_translationVector; 	}

protected:


private:
	virtual void				awaitFinish()					final override;
	virtual void 				start_() 						final override;

	virtual void				gatherResults()					final override;

	// implemented by derived classes
	//virtual Thread*			makeThread()=0; // No threads in HW acceleration for now. perhaps later
	virtual void				parentPrepare()=0;
	virtual void				parentStart()=0;
	//virtual void				parentFinish()=0;
	virtual void				parentResults()=0;

	boost::random::ecuyer1988 	m_seedGenerator;

	double										m_scaleValue=1;
	std::array<double,3>						m_translationVector;
	
	unsigned 									m_threadCount=MAX_THREAD_COUNT; ////Currently not needed because we are not using threads and HW acceleration
	//std::vector<P8MCKernelBase::Thread*> 	m_workers;


};



#ifndef SWIG
// Currently we do not need software threads in the hardware acceleration code
// maybe for future work to improve performance 
/*class P8MCKernelBase::Thread
{
public:
	virtual ~Thread(){}

	void start(P8MCKernelBase* kernel,unsigned long long N);
	bool done() const;
	void awaitFinish();

protected:
	unsigned long long 	m_nPktReq=0;
	unsigned long long 	m_nPktDone=0;

private:
	virtual void doWork(P8MCKernelBase* kernel)=0;

	static void threadFunction(P8MCKernelBase* kernel,P8MCKernelBase::Thread* t);

	bool 				m_done=false;

	std::thread m_thread;

	friend class P8MCKernelBase;
};*/

#endif

#endif /* KERNELS_P8_P8MCKERNELBASE_HPP_ */
