/*
 * TetraMCP8DebugKernel.hpp
 *
 *  Created on: Mar 4, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_TETRAMCP8DEBUGKERNEL_HPP_
#define KERNELS_P8_TETRAMCP8DEBUGKERNEL_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Sources/Print.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>			//	TODO: Only here until a Layered factory exists
#include <FullMonteSW/Kernels/Software/Emitters/Point.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/Directed.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/Geometry/Partition.hpp>

#include <FullMonteSW/Kernels/Software/Tetra.hpp>
#include <FullMonteSW/Kernels/Software/Material.hpp>

#include "P8MCKernelBase.hpp"
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>

#include "P8TetrasFromLayered.hpp"
#include <FullMonteSW/Kernels/Software/TetrasFromLayered.hpp>
#include "P8TetrasFromTetraMesh.hpp"
#include <FullMonteSW/Kernels/Software/TetrasFromTetraMesh.hpp>

#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>

#include <FullMonteSW/Kernels/Software/AutoStreamBuffer.hpp>

#include <thread>
#include <cmath>

class Partition;
class TetraMesh;

#include <boost/align/aligned_alloc.hpp>

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

#include <BlueLink/Host/AFU.hpp>
#include <BlueLink/Host/WED.hpp>

#include <FullMonteHW/Host/CXXFullMonteTypes.hpp>
#include <BDPIDevice/BitPacking/Packer.hpp>
#include <BDPIDevice/BitPacking/Unpacker.hpp>
#include <BDPIDevice/BitPacking/VerilogStringFormat.hpp>

#define READY_STATE 0x1ULL
#define WAITING_STATE 0x2ULL
#define INTERSECTION_DONE_STATE 0x3ULL
#define STEPFINISH_LIVE_STATE 0x4ULL
#define STEPFINISH_DEAD_STATE 0x5ULL
#define BOUNDARY_TRANSMIT_STATE 0x6ULL
#define BOUNDARY_REFRACTIVE_INTERFACE_STATE 0x7ULL
#define REFLECTED_STATE 0x8ULL
#define REFRACTED_STATE 0x9ULL

class TetraP8DebugKernel
{
public:
	/*void markTetraFaceForFluenceScoring(unsigned IDt, unsigned faceIdx, bool en) {
		// mark 'faceIdx' of tetra 'IDt' to either score or not score ('en') surface fluence events
		unsigned mask = 1 << (faceIdx<<3);
		if(en) {
			m_tetras[IDt].faceFlags |= mask;
		} 
		else {
			m_tetras[IDt].faceFlags &= ~mask;
		}
	}*/

protected:

	std::vector<FullMonteHW::TetraDef> m_tetrasHW;
	std::vector<Tetra> m_tetrasSW;
};

template<class RNGT>class TetraMCP8DebugKernel : public P8MCKernelBase, public TetraP8DebugKernel
{
public:
	TetraMCP8DebugKernel():afu(DEVICE_STRING){
		
		// allocate sizeof(RNG) bytes aligned to 32 bytes
        // NOTE: this does NOT initialize the memory, that is done below
        void* p = boost::alignment::aligned_alloc(32,sizeof(RNG));

        // ensure the allocation succeeded
        if (!p) {
	        cerr << "Allocation failure in TetraMCP8Kernel<RNGT>(); the constructor" << endl;
		    throw std::bad_alloc();
	    }

        // memory allocation was good, now use this aligned memory to initialize the RNG
        // NOTE: this calls the constructor of RNG (and therefore all child constructors).
        // If you simply cast p (i.e. m_rng = (RNG*) p) you won't get compile errors but the
        // RNG (notably the position into the buffer 'm_pos') will not be initialized and segfaults occur.
        m_rng = new (p) RNG();

	}

	typedef RNGT RNG;

	// set and get method for variables controlling the flow of the program
	void setFullPipeline(bool fullPipeline)    		{ m_fullPipeline=fullPipeline; }
    bool getFullPipeline()     				const   { return m_fullPipeline; }

	void setIntersection(bool intersection)    		{ m_intersection=intersection; }
    bool getIntersection()     				const   { return m_intersection; }

	void setStepFinish(bool stepFinish)    			{ m_stepFinish=stepFinish; }
    bool getStepFinish()     				const   { return m_stepFinish; }

	void setBoundary(bool boundary)    				{ m_boundary=boundary; }
    bool getBoundary()     					const   { return m_boundary; }

	void setReflectRefract(bool reflectRefract)   	{ m_reflectRefract=reflectRefract; }
    bool getReflectRefract()     			const   { return m_reflectRefract; }

	void setVerbose(bool verbose)   				{ m_verbose=verbose; }
    bool getVerbose()		     			const   { return m_verbose; }

	void inputBits(std::size_t bits)    			{ m_inputBits=bits; }
    std::size_t inputBits()     			const	{ return m_inputBits; }


protected:
	// Variables to control which HW module will be executed and evaluated
	bool m_fullPipeline = false;
	bool m_intersection = false;
	bool m_stepFinish = false;
	bool m_boundary = false;
	bool m_reflectRefract = false;
	bool m_verbose = false;
	
	// Number of Bits that are sent in one packet
	std::size_t m_inputBits=512;
	
	RNG*	 m_rng;

	virtual void parentPrepare() 			override;

	virtual void parentStart()	 			override;

	virtual void parentResults() 			override;

	std::vector<x86Kernel::Material> 				m_matsSW;
	std::vector<FullMonteHW::MaterialStruct> 		m_matsHW;

	Emitter::EmitterBase<RNG>* 						m_emitter;

	P8TetrasFromTetraMesh 							P8TF;
	TetrasFromTetraMesh 							TF;

	unsigned int m_faces_size = 0;
	vector<float> outputDirectedSurface;

	vector<std::array<uint64_t,9>,boost::alignment::aligned_allocator<std::array<uint64_t,9>,128>> m_tetras_serialized;
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> m_mats_serialized;

	struct MemcopyWED
    {	
		uint64_t        data[16]; //1024 bit total size available for WED for data transfer
    };

	//AFU: APPLICATION FUNCTION UNIT used in PSL
	AFU afu;
	
    //WED: WORK ELEMENT DESCRIPTION used in PSL
	StackWED<MemcopyWED,128,128> wed;

};

template<class RNGT>void TetraMCP8DebugKernel<RNGT>::parentPrepare()
{
	////// Geometry setup
	const Geometry* G = geometry();

	if (!G)
		throw std::logic_error("TetraMCP8DebugKernel<RNG>::parentPrepare() no geometry specified");
		
	////// Material setup

	const MaterialSet * MS = materials();
									
	if (!MS)
		throw std::logic_error("TetraMCP8DebugKernel<RNGT>::parentPrepare() no materials specified");

	/////// Conversion HW compatible Mesh

	if (const TetraMesh* M = dynamic_cast<const TetraMesh*>(G))
	{
		LOG_INFO << "Building SW kernel tetras from TetraMesh" << endl;


		TF.mesh(M);
		TF.update();

		m_tetrasSW = TF.tetras();

		LOG_INFO << "Building HW kernel tetras from TetraMesh" << endl;

		std::array<double,3> translation_vector;
		double scale = 1;
		tie(translation_vector, scale) = P8TF.mesh(M);
		scaleValue(scale);
		translationVector(translation_vector);

		P8TF.update(MS);

		m_faces_size = P8TF.face_numbers();
		m_tetrasHW = P8TF.tetras();
		m_tetras_serialized.resize(m_tetrasHW.size());

		// TetraDef is 540 bits large and is defined in CXXFullMonteTypes.hpp
		Packer<std::array<uint64_t,9>> tetraPacker(FullMonteHW::TetraDef::bits,m_tetras_serialized[0]);
		
		size_t index = 0;
		for(auto& i : m_tetras_serialized)
		{
			tetraPacker.reset(i);

			tetraPacker & m_tetrasHW[index];
			
			index++;
		}

	//////////////////////////////////////////////////
		if (!m_src)
			throw std::logic_error("TetraMCKernel<RNGT>::parentPrepare() no sources specified");

		// TODO: Move this out of here as it doesn't quite belong
		Emitter::TetraEmitterFactory<RNGT> factory(M, MS);

		((Source::Abstract*)m_src)->acceptVisitor(&factory);

		m_emitter = factory.emitter();

		//cout << "Sources set up" << endl;
	}
	else if (const Layered* L = dynamic_cast<const Layered*>(G))
	{
		LOG_INFO << "Building kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
		TetrasFromLayered TL;

		TL.layers(L);
		TL.update();

		m_tetrasSW = TL.tetras();
		
		LOG_INFO << "Building HW kernel tetras from Layered geometry (" << L->layerCount() << " layers)" << endl;
		P8TetrasFromLayered P8TL;

		P8TL.layers(L);
		P8TL.update(MS);

		m_tetrasHW = P8TL.tetras();

		if (m_src)
		{
			LOG_INFO << "TetraMCP8DebugKernel::parentPrepare() ignoring provided source with Layered geometry (only PencilBeam allowed)" << endl;
		}

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
		SSE::UnitVector3 d{{{ 0.0f, 0.0f, 1.0f }}};
		SSE::UnitVector3 a{{{ 1.0f, 0.0f, 0.0f }}};
		SSE::UnitVector3 b{{{ 0.0f, 1.0f, 0.0f }}};

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#else
		SSE::UnitVector3 d({0.0f, 0.0f, 1.0f, 0.0f });
		SSE::UnitVector3 a({1.0f, 0.0f, 0.0f, 0.0f });
		SSE::UnitVector3 b({0.0f, 1.0f, 0.0f, 0.0f });

		array<float,3> origin{{0.0f,0.0f,0.0f}};
#endif
		Emitter::Point P{1U,SSE::Vector3(origin.data())};
		Emitter::Directed D(PacketDirection(d,a,b));
		
		m_emitter = new Emitter::PositionDirectionEmitter<RNGT,Emitter::Point,Emitter::Directed>(P,D);
	}
	else
		throw std::logic_error("TetraMCP8DebugKernel<RNG>::parentPrepare() geometry is neither Layered nor TetraMesh");																
																																																																																																
	afu.start(wed.get(), 2);

	resetSeedRng();

	m_rng->hgSetSize(MS->size());
	for(unsigned i=0;i<MS->size(); ++i)
		m_rng->gParamSet(i,MS->get(i)->anisotropy());
	
	unsigned counter = 0;
	// Generating the refractive interface lookup memory
	unsigned long long state=0;
	while( (state=afu.mmio_read64(0)) != READY_STATE)																			
	{
		if (counter > 100)
		{																																		
			LOG_INFO << "  Waiting for HW state being 'READY_STATE'. Current state (state=" << state << " looking for " << READY_STATE << ")" << endl;
		}						
		usleep(200);
		counter++;																														
	}

	////// Material conversion

	m_matsHW.resize(MS->size());
	m_matsSW.resize(MS->size());
	m_mats_serialized.resize(MS->size());

	// Materialstruct is 128 bits large and is defined in CXXFullMonteTypes.hpp
	Packer<std::array<uint64_t,2>> materialPacker(FullMonteHW::MaterialStruct::bits,m_mats_serialized[0]);
	// copy materials
	size_t index = 0;
	for(auto& i : m_mats_serialized)
	{
		// commands to convert the material data to the HW compatible material struct
		materialPacker.reset(i);
		::Material* m = MS->get(index);
		m_matsHW[index] = FullMonteHW::MaterialStruct::calculateFromProperties(m->absorptionCoeff()*scaleValue(), m->scatteringCoeff()*scaleValue(),m->anisotropy());
		m_matsHW[index].matID = index;

		m_mats_serialized[index][0] = 0x0;
		m_mats_serialized[index][1] = 0x0;

		materialPacker & m_matsHW[index];

		m_matsSW[index] = x86Kernel::Material(m->absorptionCoeff(), m->scatteringCoeff(),m->refractiveIndex());		

		afu.mmio_write64(11<<3, (uint64_t)m_mats_serialized[index][0]);
		afu.mmio_write64(11<<3, (uint64_t)m_mats_serialized[index][1]); 
		afu.mmio_write64(12<<3, 0x0ULL);
		// increment index
		index++;
		
		// Debug print statement
		// cout << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::MaterialStruct::bits) << materialPacker.value() << endl;
	}	

	// Create the interfaceDef struct for all material combinations that might exist within the mesh
	FullMonteHW::InterfaceDef interface;
	vector<std::array<uint64_t,1>,boost::alignment::aligned_allocator<std::array<uint64_t,1>,128>> interfaceDef_serialized; 
	interfaceDef_serialized.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	Packer<std::array<uint64_t,1>> interfaceDefPacker(FullMonteHW::InterfaceDef::bits,interfaceDef_serialized[0]); // Packer for serializing InterfaceDef input
	LOG_INFO << "  Transferring the interface definitions to the HW" << endl;
	counter = 1;
	for (unsigned i=0; i < MS->size(); i++)
	{
		for(unsigned j=0; j < MS->size(); j++)
		{
			interface.ifcID = P8TF.getInterfaceID(i,j);
			float ni_nt = MS->get(i)->refractiveIndex() / MS->get(j)->refractiveIndex();
			interface.ni_nt_ratio = ni_nt;
			float nt_ni = 1/ni_nt;
			interface.nt_ni_ratio = nt_ni;
			// calculation of critical angle theta_crit = arcsin(n_t/n_i) -> cos_crit = cos(arcsin(n_t/n_i))
			if (nt_ni >= 1)
			{
				interface.costheta_crit = 0;
			}
			else
			{
				interface.costheta_crit = cosf(asinf(nt_ni));
			}
			LOG_INFO << (counter*100)/(MS->size()*MS->size()) <<"% transferred            \r";
			// LOG_INFO << "               InterfaceID: " << interface.ifcID.value() << endl
			// 		 << " critical angle cos(theta):" << interface.costheta_crit.integer_value() << endl
			// 		 << "                   n_i/n_t: " << interface.ni_nt_ratio.integer_value() << endl
			// 		 << "                   n_t/n_i:" << interface.nt_ni_ratio.integer_value() << endl;

			interfaceDef_serialized[0][0] = 0x0;

			interfaceDefPacker.reset(interfaceDef_serialized[0]);
			interfaceDefPacker & interface;

			afu.mmio_write64(9<<3, (uint64_t)interfaceDef_serialized[0][0]); 
			afu.mmio_write64(10<<3, 0x0ULL);
			counter++;
		}
	}
	LOG_INFO << endl << "...Done" << endl;
}

// enum for SW termination check
enum TerminationResult { Continue=0, RouletteWin, RouletteLose, TimeGate, Other=-1  };

template<class RNGT>void TetraMCP8DebugKernel<RNGT>::parentStart()
{
	// We need to seed the RNG. If we want to get consistent results for debug purposes, we need to hardcode a value into the seed.
	m_rng->seed(getUnsignedRNGSeed());
	LaunchPacket lpkt_in;

	double scale = scaleValue(); // scaling factor to convert the HW results back to the original mesh results generated by the SW
	std::array<double,3> shift = translationVector(); // shifting offset to convert the HW results (mainly position) back to the original mesh results generated by the SW
	bool killPacket = false; // variable to control if the photon packet will be killed if we break out of the inner loops

	// reused variables in the loop
	unsigned long long state=0; // Reads out the current HW state to know when the HW finished its execution
	float stepLength[4]; // extract the step length from the SSE datatype __m128
	float position[4]; // extract the position vector from the SSE datatype __m128
	float refrefNormal[4]; // extract the normal of the surface which the photon hits during its step phase
	float negative_height[4]; // extract the negative height from the getIntersection calculations
	float cos_theta[4]; // extract costheta from the getIntersection calcualtions
	std::array<float,3> pos; // convert to a 3 element array for HW conversion
	std::array<float, 4> deflection_azimuth; // stores the RNG values for cos(theta); sin(theta); cos(psi); sin(psi) -> Scattering RNG values for SW and HW
	std::array<float, 2> deflection, azimuth; // divide deflection and azimuth into 2 2D vectors for HW conversion

	// Variables for the Intersection HW module
	FullMonteHW::Packet pkt_HW; // contains the converted launch packet from SW that serves as input to the Intersection HW module
	// serialized input data field for several HW modules that require the Packet struct
	vector<std::array<uint64_t,6>,boost::alignment::aligned_allocator<std::array<uint64_t,6>,128>> pkt_serialized;  
	pkt_serialized.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the Intersection HW module
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> outputStepResult;
	outputStepResult.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	FullMonteHW::StepResult outputStepResultIntersection; // struct instance to unpack the HW output data  from outputStepResult into   

	// Variables for Boundary HW module
	// serialized input data field for the Boundary HW module
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> stepResult_serialized;  
	stepResult_serialized.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the Boundary HW module if there is no refractive index difference
	vector<std::array<uint64_t,6>,boost::alignment::aligned_allocator<std::array<uint64_t,6>,128>> outputBoundaryPacket;
	outputBoundaryPacket.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the Boundary HW module if there is a difference in the refractive indices -> need to perform reflection/refraction calculations
	vector<std::array<uint64_t,6>,boost::alignment::aligned_allocator<std::array<uint64_t,6>,128>> outputBoundaryRefraction;
	outputBoundaryRefraction.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	FullMonteHW::Packet boundaryPacket; // struct instance to unpack the Boundary output data into the Packet struct
	FullMonteHW::BoundaryOutput boundaryOutput; // struct instance to unpack the serialized data into the BoundaryOutput struct

	// Variables for ReflectRefract HW module
	// serialized input data field for the ReflectRefract HW module
	vector<std::array<uint64_t,3>,boost::alignment::aligned_allocator<std::array<uint64_t,3>,128>> reflectRefract_serialized; 
	reflectRefract_serialized.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the Boundary HW module if there is no refractive index difference
	vector<std::array<uint64_t,3>,boost::alignment::aligned_allocator<std::array<uint64_t,3>,128>> outputReflectRefractDirection;
	outputReflectRefractDirection.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	FullMonteHW::ReflectRefractRequest inputReflectRefract; // struct instance to send to the HW
	FullMonteHW::PacketDirection ReflectRefractDirection; // struct instance to unpack the ReflectRefract output data into the PacketDirection struct

	// Variables for StepFinish HW module
	// serialized output data field for the StepFinish HW module -> should contain absorption information
	vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> outputAbsorptionInfo;
	outputAbsorptionInfo.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the StepFinish HW module if photon is alive -> should contain the updated Packet struct after Scattering + Absorption
	vector<std::array<uint64_t,6>,boost::alignment::aligned_allocator<std::array<uint64_t,6>,128>> outputLivePacket;
	outputLivePacket.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	// serialized output data field for the StepFinish module if photon lost roulette and is dead -> should contain the amount of weight that is left in the packet at death
	vector<std::array<uint64_t,1>,boost::alignment::aligned_allocator<std::array<uint64_t,1>,128>> outputDeadPacket;
	outputDeadPacket.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
	FullMonteHW::Packet stepFinishPacket; // contains the Packet struct of the StepFinish HW modules output when the packet is alive
	FullMonteHW::Weight weightRemaining; // contains the remaining weight of the photon when it dies in the StepFinish HW module.
	FullMonteHW::AbsorptionInfo absorptionInfo; // contains the amount of weight absorbed by the tetra with the respective tetraID in the stepFinish HW module

	// Mean error accumulators for HW input/output
	unsigned intersection_count = 1;
	float Input_intersection_pos[3] = {0,0,0}, Input_intersection_pos_error_sum[3] = {0,0,0}, Input_intersection_pos_sum[3] = {0,0,0};
	float Input_intersection_dir[3] = {0,0,0}, Input_intersection_dir_error_sum[3] = {0,0,0}, Input_intersection_dir_sum[3] = {0,0,0};
	float Input_intersection_a[3] = {0,0,0}, Input_intersection_a_error_sum[3] = {0,0,0}, Input_intersection_a_sum[3] = {0,0,0};
	float Input_intersection_b[3] = {0,0,0}, Input_intersection_b_error_sum[3] = {0,0,0}, Input_intersection_b_sum[3] = {0,0,0};
	float Input_intersection_weight = 0, Input_intersection_weight_error_sum = 0, Input_intersection_weight_sum = 0;
	float Input_intersection_step = 0, Input_intersection_step_error_sum = 0, Input_intersection_step_sum = 0;

	float Output_intersection_height = 0, Output_intersection_height_error_sum = 0, Output_intersection_height_sum = 0;
	float Output_intersection_costheta = 0, Output_intersection_costheta_error_sum = 0, Output_intersection_costheta_sum = 0; 

	unsigned boundary_count = 1;
	unsigned boundary_costheta_count = 1;
	float Input_boundary_height = 0, Input_boundary_height_error_sum = 0, Input_boundary_height_sum = 0;
	float Input_boundary_costheta = 0, Input_boundary_costheta_error_sum = 0, Input_boundary_costheta_sum = 0;

	float Output_boundary_step = 0, Output_boundary_step_error_sum = 0, Output_boundary_step_sum = 0;
	float Output_boundary_weight = 0, Output_boundary_weight_error_sum = 0, Output_boundary_weight_sum = 0;
	float Output_boundary_costheta = 0, Output_boundary_costheta_error_sum = 0, Output_boundary_costheta_sum = 0;
	float Output_boundary_pos[3] = {0,0,0}, Output_boundary_pos_error_sum[3] = {0,0,0}, Output_boundary_pos_sum[3] = {0,0,0};
	float Output_boundary_dir[3] = {0,0,0}, Output_boundary_dir_error_sum[3] = {0,0,0}, Output_boundary_dir_sum[3] = {0,0,0};

	unsigned reflectRefract_count = 1;
	float Input_reflectRefract_normal[3] = {0,0,0}, Input_reflectRefract_normal_error_sum[3] = {0,0,0}, Input_reflectRefract_normal_sum[3] = {0,0,0};
	float Input_reflectRefract_rnd = 0, Input_reflectRefract_rnd_error_sum = 0, Input_reflectRefract_rnd_sum = 0;
	float Input_reflectRefract_dir[3] = {0,0,0}, Input_reflectRefract_dir_error_sum[3] = {0,0,0}, Input_reflectRefract_dir_sum[3] = {0,0,0};
	float Input_reflectRefract_costheta = 0, Input_reflectRefract_costheta_error_sum = 0, Input_reflectRefract_costheta_sum = 0;

	float Output_reflectRefract_dir[3] = {0,0,0}, Output_reflectRefract_dir_error_sum[3] = {0,0,0}, Output_reflectRefract_dir_sum[3] = {0,0,0};
	float Output_reflectRefract_a[3] = {0,0,0}, Output_reflectRefract_a_error_sum[3] = {0,0,0}, Output_reflectRefract_a_sum[3] = {0,0,0};
	float Output_reflectRefract_b[3] = {0,0,0}, Output_reflectRefract_b_error_sum[3] = {0,0,0}, Output_reflectRefract_b_sum[3] = {0,0,0};

	unsigned stepFinish_count = 1;
	unsigned stepFinish_dead_count = 1;
	unsigned stepFinish_absorbed_count = 1;
	float Input_stepFinish_hg[2] = {0,0}, Input_stepFinish_hg_error_sum[2] = {0,0}, Input_stepFinish_hg_sum[2] = {0,0};
	float Input_stepFinish_az[2] = {0,0}, Input_stepFinish_az_error_sum[2] = {0,0}, Input_stepFinish_az_sum[2] = {0,0}; 

	float Output_stepFinish_absorbed = 0, Output_stepFinish_absorbed_error_sum = 0, Output_stepFinish_absorbed_sum = 0;
	float Output_stepFinish_dir[3] = {0,0,0}, Output_stepFinish_dir_error_sum[3] = {0,0,0}, Output_stepFinish_dir_sum[3] = {0,0,0};
	float Output_stepFinish_pos[3] = {0,0,0}, Output_stepFinish_pos_error_sum[3] = {0,0,0}, Output_stepFinish_pos_sum[3] = {0,0,0};
	float Output_stepFinish_weight = 0, Output_stepFinish_weight_error_sum = 0, Output_stepFinish_weight_sum = 0;
	float Output_stepFinish_deadweight = 0, Output_stepFinish_deadweight_error_sum = 0, Output_stepFinish_deadweight_sum = 0;
	float Output_stepFinish_step = 0, Output_stepFinish_step_error_sum = 0, Output_stepFinish_step_sum = 0;

	// Counter to  avoid getting the Waiting status of the hardware to often
	unsigned counter = 0;

	for(unsigned i=0;i<Npkt_;i++)
	{
		//use emit from the emitter factory to generate launch packets
		lpkt_in = m_emitter->emit(*m_rng,Npkt_,i);

		// Software execution preparation
		unsigned Nhit,Nstep;
		StepResult stepResult;
		// Select the current Tetra from which the photons should be launched
		// The tetraID is stored in lpkt.element
		Tetra currTetra = m_tetrasSW[lpkt_in.element];

		// Get the material of the current Tetra
		x86Kernel::Material currMat = m_matsSW[currTetra.matID];

		unsigned IDt=lpkt_in.element;

		// f_tmp represents the refractive indices of neighboring tetras and the ratio of them
		// It is only required for reflect and refract calculations
		float f_tmp[4] __attribute__((aligned(16)));
		float &n1 = f_tmp[0];
		float &n2 = f_tmp[1];
		float &ratio = f_tmp[2];
		// Currently, we assume that the next step will be in the same tetra meaning we will not change the tetra during the hop stage of the simulation
    	// This implies that the next material ID is equal to the current material ID.
		unsigned IDt_next=IDt, IDm=currTetra.matID, IDm_next=IDm, IDm_bound;
		
		Packet pkt(lpkt_in);
				
		// start first/another hop
    	for(Nstep=0; Nstep < Nstep_max_; ++Nstep)
		{
			// 	float tmppos[4];
			// 	_mm_store_ps(tmppos,pkt.p);
			// 	cout << "Tetra" << setw(7) << IDt << " material " << IDm << " muT=" << currMat.muT << ' ' <<
			// 			" position " << tmppos[0] << ' ' << tmppos[1] << ' ' << tmppos[2] <<
			//	        " direction " << pkt.dir.d[0] << ' ' << pkt.dir.d[1] << ' ' << pkt.dir.d[2] <<
			//	        " w=" << pkt.w << endl;
			assert(pkt.dir.d.check(SSE::Silent,1e-4) && pkt.dir.a.check(SSE::Silent,1e-4) && pkt.dir.b.check(SSE::Silent,1e-4));
			
//////////////////////////////////////////////// Preparing Simulation ////////////////////////////////////////////////////////////////////////////
#ifdef USE_SSE
			
			// draw a hop length; pkt.s = { physical distance, Mean Free Path (MFP) to go, time, 0 }
			// pkt.s[0] = rand / muT -> physical distance
			// pkt.s[1] = rand * 1   -> dimensionless step length (Mean Free Path)
			// pkt.s[2] = rand * 0   -> time step; start at 0 
			// pkt.s[3] = rand * 0   -> not used
			pkt.s = _mm_mul_ps(
						_mm_load1_ps(m_rng->floatExp()), // duplicate the random number 4 times and load it into a __m128 register
						currMat.m_init); // initial propagation vector (1/muT physical step remaining, 1 dimensionless step remaining, 0 time, 0 X)
			
			assert(0 <= _mm_cvtss_f32(pkt.s));

			_mm_store_ps(stepLength,pkt.s);

			_mm_store_ps(position,pkt.p);
			
#else
			const float* rng = m_rng->floatExp(); // generate one random number...
			std::array<float,4> floatExp = {*rng, *rng, *rng, *rng}; // ...and store it 4 times into a float array
			
			pkt.s = mult128(
						floatExp, // generate one random number and store it 4 times into a m128 register
						currMat.m_init); // initial propagation vector (1/muT physical step remaining, 1 dimensionless step remaining, 0 time, 0 X)

			assert(0 <= pkt.s[0]);

			stepLength[0] = pkt.s[0];
			stepLength[1] = pkt.s[1];
			stepLength[2] = pkt.s[2];
			stepLength[3] = pkt.s[3];

			position[0] = pkt.p[0];
			position[1] = pkt.p[1];
			position[2] = pkt.p[2];
			position[3] = pkt.p[3];
#endif
			pos[0] = (position[0] - shift[0]) / scale;
			pos[1] = (position[1] - shift[1]) / scale;
			pos[2] = (position[2] - shift[2]) / scale;
			if(m_fullPipeline)
			{
				if(state == STEPFINISH_LIVE_STATE)
				{
					pkt_HW.age = 0;
					pkt_HW.dir = stepFinishPacket.dir;
					pkt_HW.l = FullMonteHW::DimensionlessStepLength(stepLength[1]/log(2.0));
					pkt_HW.matID = stepFinishPacket.matID;
					pkt_HW.pos = stepFinishPacket.pos;
					pkt_HW.tag = 0;
					pkt_HW.tetID = stepFinishPacket.tetID;
					pkt_HW.w = stepFinishPacket.w;
				}
				else
				{
					pkt_HW.age = 0;
					pkt_HW.dir.d = FullMonteHW::UVect3D_18(pkt.dir.d.array());
					pkt_HW.dir.a = FullMonteHW::UVect3D_18(pkt.dir.a.array());
					pkt_HW.dir.b = FullMonteHW::UVect3D_18(pkt.dir.b.array());
					pkt_HW.l = FullMonteHW::DimensionlessStepLength(stepLength[1]/log(2.0));
					pkt_HW.matID = FullMonteHW::MaterialID(IDm);
					pkt_HW.pos = FullMonteHW::Point3D_18(pos);
					pkt_HW.tag = 0;
					pkt_HW.tetID = FullMonteHW::TetraID(IDt);
					pkt_HW.w = FullMonteHW::Weight(pkt.w);
				}
			}			
			else
			{
				pkt_HW.age = 0;
				pkt_HW.dir.d = FullMonteHW::UVect3D_18(pkt.dir.d.array());
				pkt_HW.dir.a = FullMonteHW::UVect3D_18(pkt.dir.a.array());
				pkt_HW.dir.b = FullMonteHW::UVect3D_18(pkt.dir.b.array());
				pkt_HW.l = FullMonteHW::DimensionlessStepLength(stepLength[1]/log(2.0));
				pkt_HW.matID = FullMonteHW::MaterialID(IDm);
				pkt_HW.pos = FullMonteHW::Point3D_18(pos);
				pkt_HW.tag = 0;
				pkt_HW.tetID = FullMonteHW::TetraID(IDt);
				pkt_HW.w = FullMonteHW::Weight(pkt.w);
			}

			if (m_verbose && m_intersection)
			{
				LOG_INFO << "Intersection Input - TetraID:" << endl
							<< "HW: " << pkt_HW.tetID.value() << " vs. SW: " << IDt << endl << endl
							<< "Intersection Input - MaterialID:" << endl
							<< "HW: " << pkt_HW.matID.value() << " vs. SW: " << IDm << endl << endl
							<< "Intersection Input - Photon position:" << endl
							<< "HW: " << pkt_HW.pos[0].integer_value() << " (" << pkt_HW.pos[0].integer_value()/pow(2,14) << ") <-> SW: " << pos[0] << endl
							<< "HW: " << pkt_HW.pos[1].integer_value() << " (" << pkt_HW.pos[1].integer_value()/pow(2,14) << ") <-> SW: " << pos[1] << endl
							<< "HW: " << pkt_HW.pos[2].integer_value() << " (" << pkt_HW.pos[2].integer_value()/pow(2,14) << ") <-> SW: " << pos[2] << endl
							<< "Error: " 	<< pos[0] - pkt_HW.pos[0].value() << " " 
										<< pos[1] - pkt_HW.pos[1].value() << " " 
										<< pos[2] - pkt_HW.pos[2].value() << endl
							<< "Intersection Input - Photon direction:" << endl
							<< "HW: " << pkt_HW.dir.d[0].integer_value() << " (" << pkt_HW.dir.d[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[0] << endl
							<< "HW: " << pkt_HW.dir.d[1].integer_value() << " (" << pkt_HW.dir.d[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[1] << endl
							<< "HW: " << pkt_HW.dir.d[2].integer_value() << " (" << pkt_HW.dir.d[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[2] << endl
							<< "Error: " << pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value() << endl 
									<< pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value() << endl 
									<< pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value() << endl << endl
							<< "Intersection Input - Photon orthonormal direction a:" << endl
							<< "HW: " << pkt_HW.dir.a[0].integer_value() << " (" << pkt_HW.dir.a[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[0] << endl 
							<< "HW: " << pkt_HW.dir.a[1].integer_value() << " (" << pkt_HW.dir.a[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[1] << endl 
							<< "HW: " << pkt_HW.dir.a[2].integer_value() << " (" << pkt_HW.dir.a[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[2] << endl
							<< "Error: " << pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value() << endl 
									<< pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value() << endl 
									<< pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value() << endl << endl
							<< "Intersection Input - Photon orthonormal direction b:" << endl
							<< "HW: " << pkt_HW.dir.b[0].integer_value() << " (" << pkt_HW.dir.b[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[0] << endl 
							<< "HW: " << pkt_HW.dir.b[1].integer_value() << " (" << pkt_HW.dir.b[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[1] << endl 
							<< "HW: " << pkt_HW.dir.b[2].integer_value() << " (" << pkt_HW.dir.b[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[2] << endl
							<< "Error: " << pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value() << endl 
									<< pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value() << endl 
									<< pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value() << endl << endl
							<< "Intersection Input - Weight:" << endl
							<< "HW: " << pkt_HW.w.integer_value() << " (" << pkt_HW.w.integer_value()/pow(2,36) << ") "<< " <-> SW: " << pkt.w << endl
							<< "Error: " << pkt.w - pkt_HW.w.value() << endl << endl
							<< "Intersection Input - Dimensionless step length:" << endl
							<< "HW: " << pkt_HW.l.integer_value() << " (" << pkt_HW.l.integer_value()/pow(2,13) << ") "<< " <-> SW: " << stepLength[1]/log(2) << endl
							<< "Error: " << stepLength[1]/log(2) - pkt_HW.l.value() << endl << endl;
			}	 
					

			Input_intersection_pos[0] += (abs(pos[0] - (pkt_HW.pos[0].value()*scale + shift[0])) - Input_intersection_pos[0]) / intersection_count;
			Input_intersection_pos[1] += (abs(pos[1] - (pkt_HW.pos[1].value()*scale + shift[1])) - Input_intersection_pos[1]) / intersection_count;
			Input_intersection_pos[2] += (abs(pos[2] - (pkt_HW.pos[2].value()*scale + shift[2])) - Input_intersection_pos[2]) / intersection_count;
			Input_intersection_dir[0] += (abs(pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value()) - Input_intersection_dir[0]) / intersection_count;
			Input_intersection_dir[1] += (abs(pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value()) - Input_intersection_dir[1]) / intersection_count;
			Input_intersection_dir[2] += (abs(pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value()) - Input_intersection_dir[2]) / intersection_count;
			Input_intersection_a[0] += (abs(pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value()) - Input_intersection_a[0]) / intersection_count;
			Input_intersection_a[1] += (abs(pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value()) - Input_intersection_a[1]) / intersection_count;
			Input_intersection_a[2] += (abs(pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value()) - Input_intersection_a[2]) / intersection_count;
			Input_intersection_b[0] += (abs(pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value()) - Input_intersection_b[0]) / intersection_count;
			Input_intersection_b[1] += (abs(pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value()) - Input_intersection_b[1]) / intersection_count;
			Input_intersection_b[2] += (abs(pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value()) - Input_intersection_b[2]) / intersection_count;
			Input_intersection_weight += (abs(pkt.w - pkt_HW.w.value()) - Input_intersection_weight) / intersection_count;
			Input_intersection_step += (abs(stepLength[1]/log(2) - pkt_HW.l.value()) - Input_intersection_step) / intersection_count;

			Input_intersection_pos_error_sum[0] += abs(position[0] - (pkt_HW.pos[0].value()*scale + shift[0]));
			Input_intersection_pos_error_sum[1] += abs(position[1] - (pkt_HW.pos[1].value()*scale + shift[1]));
			Input_intersection_pos_error_sum[2] += abs(position[2] - (pkt_HW.pos[2].value()*scale + shift[2]));
			Input_intersection_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value());
			Input_intersection_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value());
			Input_intersection_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value());
			Input_intersection_a_error_sum[0] += abs(pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value());
			Input_intersection_a_error_sum[1] += abs(pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value());
			Input_intersection_a_error_sum[2] += abs(pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value());
			Input_intersection_b_error_sum[0] += abs(pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value());
			Input_intersection_b_error_sum[1] += abs(pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value());
			Input_intersection_b_error_sum[2] += abs(pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value());
			Input_intersection_weight_error_sum += abs(pkt.w - pkt_HW.w.value());
			Input_intersection_step_error_sum += abs(stepLength[1]/log(2) - pkt_HW.l.value());

			Input_intersection_pos_sum[0] += abs(position[0]);
			Input_intersection_pos_sum[1] += abs(position[1]);
			Input_intersection_pos_sum[2] += abs(position[2]);
			Input_intersection_dir_sum[0] += abs(pkt.dir.d.array()[0]);
			Input_intersection_dir_sum[1] += abs(pkt.dir.d.array()[1]);
			Input_intersection_dir_sum[2] += abs(pkt.dir.d.array()[2]);
			Input_intersection_a_sum[0] += abs(pkt.dir.a.array()[0]);
			Input_intersection_a_sum[1] += abs(pkt.dir.a.array()[1]);
			Input_intersection_a_sum[2] += abs(pkt.dir.a.array()[2]);
			Input_intersection_b_sum[0] += abs(pkt.dir.b.array()[0]);
			Input_intersection_b_sum[1] += abs(pkt.dir.b.array()[1]);
			Input_intersection_b_sum[2] += abs(pkt.dir.b.array()[2]);
			Input_intersection_weight_sum += abs(pkt.w);
			Input_intersection_step_sum += abs(stepLength[1]/log(2));

//////////////////////////////////////////////// Hardware: Intersection check //////////////////////////////////////////////////////////////////
			if (m_fullPipeline || m_intersection)
			{
				pkt_serialized[0][0] = 0x0;
				pkt_serialized[0][1] = 0x0;
				pkt_serialized[0][2] = 0x0;
				pkt_serialized[0][3] = 0x0;
				pkt_serialized[0][4] = 0x0;
				pkt_serialized[0][5] = 0x0;
			
				// Packer for serializing the Packet struct
				Packer<std::array<uint64_t,6>> packetPackerI(FullMonteHW::Packet::bits_,pkt_serialized[0]);

				packetPackerI.reset(pkt_serialized[0]);
				packetPackerI & pkt_HW;

				//Transfer packet data from host to HW and draw step size for comparison with SW														
			//Transfer packet data from host to HW and draw step size for comparison with SW														
				//Transfer packet data from host to HW and draw step size for comparison with SW														
				// mkIntersection module requires IntersectionRequest and Packet as input																
			// mkIntersection module requires IntersectionRequest and Packet as input																
				// mkIntersection module requires IntersectionRequest and Packet as input																
				// IntersectionRequest: TetraDef; Packet::dir; Packet::pos; Packet::l																							
			// IntersectionRequest: TetraDef; Packet::dir; Packet::pos; Packet::l																							
				// IntersectionRequest: TetraDef; Packet::dir; Packet::pos; Packet::l																							
															
			// serialized FullMonteHW::Packet																														
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][0]); 
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][1]); 
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][2]); 		
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][3]); 		
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][4]);
				afu.mmio_write64(0, (uint64_t)pkt_serialized[0][5]); 

				// serialized FullMonteHW::TetraDef	
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][0]);																					
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][1]);																																
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][2]);																		
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][3]);																										
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][4]);																									
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][5]);																				
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][6]);																								
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][7]);
				afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][8]);

				// Once all required data is transferred to HW, we can signal the HW to start the Intersection module																																																										
				afu.mmio_write64(2<<3,0x0ULL);
				counter = 0;
				while( (state=afu.mmio_read64(0)) != INTERSECTION_DONE_STATE)																						
				{
					if (counter > 100)																												
					{																																		
						LOG_INFO << "  Waiting for HW state being 'INTERSECTION_DONE_STATE'. Current state (state=" << state << " looking for " << INTERSECTION_DONE_STATE << ")" << endl;
					}						
					usleep(200);
					counter++;																														
				}

				// Unpacker for de-serializing the data into the StepResult struct
				Unpacker<std::array<uint64_t,2>> stepResultUnpackerInit(FullMonteHW::StepResult::bits, outputStepResult[0]);
				
				outputStepResult[0][0] = afu.mmio_read64(1<<3);
				outputStepResult[0][1] = afu.mmio_read64(1<<3);

				// De-serialization of HW output data back to a software type FullMonteHW::Output
				stepResultUnpackerInit & outputStepResultIntersection;
			}// if m_fullPipeline || m_intersection

//////////////////////////////////////////////// Intersection Calculation SW ////////////////////////////////////////////////////////////////////
#ifdef USE_SSE
			// Calculate: currTetra.getIntersection(pkt.p,__m128(pkt.direction()),pkt.s);
			__m128 d = __m128(pkt.direction());
			stepResult.idx=-1;

			// s[i] = s[0]
			// just use the physical step length for calculations 
			__m128 s = _mm_shuffle_ps(pkt.s,pkt.s,_MM_SHUFFLE(0,0,0,0));
			
			// dot[i] = nx[i] * d[0]
			__m128 dot = _mm_mul_ps(currTetra.nx,_mm_shuffle_ps(d,d,_MM_SHUFFLE(0,0,0,0)));
			// dot[i] = dot[i] + ny[i] * d[1]
			dot = _mm_add_ps(dot,_mm_mul_ps(currTetra.ny,_mm_shuffle_ps(d,d,_MM_SHUFFLE(1,1,1,1))));
			// dot[i] = dot[i] + nz[i] * d[2]
			dot = _mm_add_ps(dot,_mm_mul_ps(currTetra.nz,_mm_shuffle_ps(d,d,_MM_SHUFFLE(2,2,2,2))));
			
			// h1[i] = nx[i] * p[0]
			__m128 h1 = _mm_mul_ps(currTetra.nx,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(0,0,0,0)));
			// h1[i] = h1[i] + ny[i] * p[1]
			h1 = _mm_add_ps(h1,_mm_mul_ps(currTetra.ny,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(1,1,1,1))));
			// h1[i] = h1[i] + nz[i] * p[2]
			h1 = _mm_add_ps(h1,_mm_mul_ps(currTetra.nz,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(2,2,2,2))));
			// h1[i] = C[i] - h1[i]
			h1 = _mm_sub_ps(currTetra.C,h1);

			// dist[i] = h1[i]/dot[i]
			__m128 dist = _mm_div_ps(h1,dot);

			// (if dot[i] < 0) --> dist[i] = dist[i]
			// (else dot[i] >= 0) --> dist[i] = inf
			dist = _mm_blendv_ps(
						_mm_set1_ps(std::numeric_limits<float>::infinity()),
						dist,
						dot);

			//     -height  dot     h/dot   meaning
			//      -       +       -       OK: inside, facing away (no possible intersection)
			//      -       -       +       OK: inside, facing towards (intersection possible)
			//      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
			//      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

			// require C - n dot p < 0 (above face) and d dot n < 0
			// at most three of dist[i] should be negative
			/// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
			pair<unsigned,__m128> min_idx_val = getMinIndex4p(dist);

			// if (min_idx_val[0] < s[0]) -> return true
			// else resturn false
			stepResult.hit = _mm_ucomilt_ss(min_idx_val.second,s);

			
			// choose the corresponding faceID to the nearest distance -> indices can only be between 0 and 2
			stepResult.IDfe = currTetra.IDfds[min_idx_val.first&3];
			
			// choose the corresponding tetraID of the adjacent tetra with the closest distance 
			stepResult.IDte = currTetra.adjTetras[min_idx_val.first&3];
			
			// will be 4 if no min found... how can we not find a minimum in 4 numbers? Are all inf?
			stepResult.idx = min_idx_val.first;

			// compare the smallest distance min_idx_val.second with the random step length
			// and store the smaller value in result.distance
			stepResult.distance=_mm_min_ps(min_idx_val.second,s);
			
			// compare the result of the last command with 0 and store the larger value in result.distance
			stepResult.distance=_mm_max_ps(_mm_setzero_ps(),stepResult.distance);
			
			 // Pe[i] = p[i] + d[i] * distance[i]
			stepResult.Pe = _mm_add_ps(pkt.p,_mm_mul_ps(d,stepResult.distance));

			// End of getIntersection
			
			_mm_store_ps(negative_height,h1);
			_mm_store_ps(cos_theta,dot);			
			// Assumption: This assertion checks if the source placement actually makes sense. 
			// Why would you place a source in the air?
			assert(_mm_cvtss_f32(pkt.s) < 1e3);
#else  

			//Calculate: currTetra.getIntersection(pkt.p,m128(pkt.direction()),pkt.s);
			m128 d = m128(pkt.direction());
			stepResult.idx=-1;
			
			/**
			 * @brief use the first entry as random step length
			 * s[i] = s[0]
			 * 
			 */
			m128 s = shuffle128(pkt.s,pkt.s,0,0,0,0);

			/**
			 * @brief calculate dot = n (dot) d
			 * calculate the entries of the dot product between n and d for all faces of a tetra
			 * 
			 * dot[i] = nx[i] * d[0]
			 */
			m128 dot = mult128(currTetra.nx,shuffle128(d,d,0,0,0,0));

			/// dot[i] = dot[i] + ny[i] * d[1]
			dot = add128(dot,mult128(currTetra.ny,shuffle128(d,d,1,1,1,1)));

			/// dot[i] = dot[i] + nz[i] * d[2]
			dot = add128(dot,mult128(currTetra.nz,shuffle128(d,d,2,2,2,2)));
			
			/**
			 * @brief calculate constraint if a point lies within the tetra. This also
			 * represents the height which the photon position is over the face of the respective tetra
			 * height = C - n (dot) p
			 * 
			 * h1[i] = nx[i] * p[0]
			 */
			m128 h1 = mult128(currTetra.nx,shuffle128(pkt.p,pkt.p,0,0,0,0));

			/// h1[i] = h1[i] + ny[i] * p[1]
			h1 = add128(h1,mult128(currTetra.ny,shuffle128(pkt.p,pkt.p,1,1,1,1)));

			/// h1[i] = h1[i] + nz[i] * p[2]
			h1 = add128(h1,mult128(currTetra.nz,shuffle128(pkt.p,pkt.p,2,2,2,2)));

			/**
			 * @brief height (= C - n dot p) should be negative if inside tetra, 
			 * may occasionally be (small) positive due to numerical error dot 
			 * negative means facing outwards. Subtract packed single-precision (32-bit) 
			 * floating-point elements in h1 from packed single-precision (32-bit) floating-point 
			 * elements in C, and store the results in h1.
			 * 
			 * h1[i] = C[i] - h1[i]
			 */
			h1 = sub128(currTetra.C,h1);


			// 
			/**
			 * @brief Divide packed single-precision (32-bit) floating-point elements 
			 * in h1 by packed elements in dot, and store the results in dist. dist is the distance 
			 * of the photon to the 4 faces of the tetra.
			 * 
			 * 
			 * dist[i] = h1[i]/dot[i]
			 */
			m128 dist = div128(h1,dot);

			
			/**
			 * @brief Checks if dot is positive or negative (_mm_blendv_ps). If it is negative, use
			 * the value of dist otherwise use infinity (which is apparently s --> )
			 * 
			 * selects dist where dist>0 and dot<0 (facing outwards), s otherwise
			 * 
			 * (if dot[i] < 0) --> dist[i] = dist[i]
			 * (else dot[i] > 0) --> dist[i] = inf
			 */
			m128 inf = set1_ps128(std::numeric_limits<float>::infinity());
			dist = blend128(
						inf,
						dist,
						dot);

			// at most three of the dot products should be negative

			//     -height  dot     h/dot   meaning
			//      -       +       -       OK: inside, facing away (no possible intersection)
			//      -       -       +       OK: inside, facing towards (intersection possible)
			//      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
			//      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

			// require C - n dot p < 0 (above face) and d dot n < 0

			/// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
			pair<unsigned,m128> min_idx_val = getMinIndex128(dist);

			/**
			 * @brief _mm_ucomilt_ss --> Compare the lower single-precision (32-bit) floating-point element  
			 * in min_idx_val.second and s for less-than, and return the boolean result (0 or 1). This instruction will not  
			 * signal an exception for QNaNs.
			 * Is the smallest distance of the photon smaller than the random step length s?
			 * If yes, return true. This means that the photon intersects with one of the tetras
			 * faces within the random step length 
			 * 
			 * if min_idx_val[0] < s[0] 
			 * 
			 */
			stepResult.hit = complo128(min_idx_val.second,s);

			
			/**
			 * @brief Choose the face with the smallest distance to the photons position and store it in
			 * IDfe. Bitwise AND of the index in min_idx_val because the index can also have the 
			 * value 4 which would result in bad memory access (size of IDfds is 4 [0-3])
			 * 
			 */
			stepResult.IDfe = currTetra.IDfds[min_idx_val.first&3];
			
			/**
			 * @brief Choose the adjacent tetra of the nearest face to the photons position and store it in
			 * IDte. Bitwise AND of the index in min_idx_val because the index can also have the 
			 * value 4 which would result in bad memory access (size of IDte is 4 [0-3])
			 * 
			 */
			stepResult.IDte = currTetra.adjTetras[min_idx_val.first&3];
			
			/// will be 4 if no min found
			stepResult.idx = min_idx_val.first;

			/// compare the smallest distance min_idx_val.second with the random step length
			/// and store the smaller value in result.distance
			stepResult.distance=min128(min_idx_val.second,s);
			
			/// compare the result of the last command with 0 and store the larger value in result.distance
			stepResult.distance=max128(set_zero128(),stepResult.distance);
			
			/**
			 * @brief Determine the new position of the photon after the step length. result.distance
			 * has the same value in each float element
			 * 
			 * Pe[i] = p[i] + d[i] * result.distance[i]
			 * 
			 */
			stepResult.Pe = add128(pkt.p,mult128(d,stepResult.distance));
			
			negative_height[0] = h1[0];
			negative_height[1] = h1[1];
			negative_height[2] = h1[2];
			negative_height[3] = h1[3];

			cos_theta[0] = dot[0];
			cos_theta[1] = dot[1];
			cos_theta[2] = dot[2];
			cos_theta[3] = dot[3];

			// Assumption: This assertion checks if the source placement actually makes sense. 
			// Why would you place a source in the air?
			assert(pkt.s[0] < 1e3);
#endif

/////////////////////////////////////// Intersection Results Comparison: HW vs. SW ////////////////////////////////////////////////////////////////
			if (m_fullPipeline || m_intersection)
			{
				if( m_verbose)
				{
					LOG_INFO << "Intersection Output - Adjacent TetraID" << endl
							 << "HW: " << outputStepResultIntersection.adj.tetID.value() << " <-> SW: " << stepResult.IDte << endl << endl
							 << "Intersection Output - Index (which of the 4 tetras):" << endl
							 << "HW: " << outputStepResultIntersection.idx.value() << " vs. SW: " << stepResult.idx << endl << endl
							 << "Intersection Output - Height between photon and nearest tetra:" << endl
							 << "HW: " << outputStepResultIntersection.height.value() << " vs. SW: " << abs(negative_height[stepResult.idx]) << endl 
							 << "Error: " << abs(negative_height[stepResult.idx]) - outputStepResultIntersection.height.value() << endl << endl
							 << "Intersection Output - cos(theta) between photon direction and normal of tetra face nearest to the photon:" << endl
							 << "HW: " << outputStepResultIntersection.costheta.value() << " vs. SW: " << abs(cos_theta[stepResult.idx]) << endl 
							 << "Error: " << abs(cos_theta[stepResult.idx]) - outputStepResultIntersection.costheta.value() << endl << endl
							 << "Intersection Output - Will the photon hit a tetra face or will it stay in the same tetra for this step?:" << endl
							 << "HW: 0->no Hit; 1-> Hit but no refractive index change; 2-> Hit with refractive index change" << endl
							 << "SW: 0->no Hit; 1-> Hit (software does not differentiate between the two possibilities here)" << endl
							 << "HW: " << outputStepResultIntersection.hit.value() << " vs. SW: " << stepResult.hit << endl << endl;
				}		 
					 

				Output_intersection_height += (abs(abs(negative_height[stepResult.idx]) - (outputStepResultIntersection.height.value()*scale)) - Output_intersection_height) / intersection_count;
				Output_intersection_costheta += (abs(abs(cos_theta[stepResult.idx]) - outputStepResultIntersection.costheta.value()) - Output_intersection_costheta) / intersection_count;

				intersection_count++;
			}
/////////////////////////////////////////////////// Update Packet Position ///////////////////////////////////////////////////////////////////////
			// new position of photon after Intersection check. This is the minimum distance that the photon is travelling
			// up to this point
			pkt.p = stepResult.Pe;

			// if we hit a face during this step, we need go into this loop and determine if we are
			// at a refractive boundary (i.e need to calculate reflect and refract) or if the refractive indices are the same
			// and we are just changing the tetra (i.e adapting the step size according to the new tetras mu_t and then checking for an intersection)
			for(Nhit=0; stepResult.hit && Nhit < Nhit_max_; ++Nhit)
			{
/////////////////////////////////////////////////// Hardware: Boundary Calculation ///////////////////////////////////////////////////////////////////////
				
				FullMonteHW::StepResult inputStepResultBoundary;
				if (m_fullPipeline)
				{
					inputStepResultBoundary.hit = 0;
					inputStepResultBoundary.adj = outputStepResultIntersection.adj;
					inputStepResultBoundary.idx = outputStepResultIntersection.idx;
					inputStepResultBoundary.height = outputStepResultIntersection.height;
					inputStepResultBoundary.costheta = outputStepResultIntersection.costheta;
				}
				else
				{
					inputStepResultBoundary.hit = 0;
					inputStepResultBoundary.adj = outputStepResultIntersection.adj;
					inputStepResultBoundary.idx = FullMonteHW::IDx(stepResult.idx);
					inputStepResultBoundary.height = FullMonteHW::Height(abs(negative_height[stepResult.idx]) / scale);
					inputStepResultBoundary.costheta = FullMonteHW::CosTheta(abs(cos_theta[stepResult.idx]));
				}

				if(m_verbose && m_boundary)
				{
					LOG_INFO << "Boundary Input - Height between photon and nearest tetra:" << endl
								<< "HW: " << inputStepResultBoundary.height.value() << " vs. SW: " << abs(negative_height[stepResult.idx]) << endl 
								<< "Error: " << abs(negative_height[stepResult.idx]) - inputStepResultBoundary.height.value() << endl << endl
								<< "Boundary Input - cos(theta) between photon direction and normal of tetra face nearest to the photon:" << endl
								<< "HW: " << inputStepResultBoundary.costheta.value() << " vs. SW: " << abs(cos_theta[stepResult.idx]) << endl 
								<< "Error: " << abs(cos_theta[stepResult.idx]) - inputStepResultBoundary.costheta.value() << endl << endl;
				}		 
						
				
				Input_boundary_height += (abs(abs(negative_height[stepResult.idx]) - inputStepResultBoundary.height.value()*scale) - Input_boundary_height) / boundary_count;
				Input_boundary_costheta += (abs(abs(cos_theta[stepResult.idx]) - inputStepResultBoundary.costheta.value()) - Input_boundary_costheta) / boundary_count;

				Input_boundary_height_error_sum += abs(abs(negative_height[stepResult.idx]) - inputStepResultBoundary.height.value()*scale);
				Input_boundary_costheta_error_sum += abs(abs(cos_theta[stepResult.idx]) - inputStepResultBoundary.costheta.value());

				Input_boundary_height_sum += abs(negative_height[stepResult.idx]);
				Input_boundary_costheta_sum += abs(cos_theta[stepResult.idx]);
					
				if (m_fullPipeline || m_boundary)
				{	
					stepResult_serialized[0][0] = 0x0;
					stepResult_serialized[0][1] = 0x0;
					//Packer for serializing StepFinish input
					Packer<std::array<uint64_t,2>> stepResultPacker(FullMonteHW::StepResult::bits,stepResult_serialized[0]); 
					stepResultPacker.reset(stepResult_serialized[0]);
					stepResultPacker & inputStepResultBoundary;
					//LOG_INFO << VerilogStringFormat(VerilogStringFormat::Hex, FullMonteHW::StepResult::bits) << stepResultPacker.value() << endl;

					// serialized FullMonteHW::StepResult
					afu.mmio_write64(5<<3, (uint64_t)stepResult_serialized[0][0]); 
					afu.mmio_write64(5<<3, (uint64_t)stepResult_serialized[0][1]); 
					// serialized FullMonteHW::Packet															
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][0]); 
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][1]); 
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][2]); 		
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][3]); 		
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][4]);
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][5]);

					// Signal HW to start the Boundary module
					afu.mmio_write64(6<<3,0x0ULL);

					counter = 0;
					state=afu.mmio_read64(0);
					while(state != BOUNDARY_TRANSMIT_STATE && state != BOUNDARY_REFRACTIVE_INTERFACE_STATE)
					{																
						if (counter > 100)
						{																																	
							LOG_INFO << "  Waiting for HW state being 'BOUNDARY_TRANSMIT_STATE' (6) or 'BOUNDARY_REFRACTIVE_INTERFACE_STATE' (7). Current state (state=" 
								<< state << " looking for " << BOUNDARY_TRANSMIT_STATE << " or "<< BOUNDARY_REFRACTIVE_INTERFACE_STATE <<")" << endl;	
						}								
						usleep(200);
						state=afu.mmio_read64(0);
						counter++;																													
					}

					if (state == BOUNDARY_TRANSMIT_STATE)
					{
						// Unpacker for de-serializing the data into the Packet struct
						Unpacker<std::array<uint64_t,6>> BoundaryPacketUnpacker(FullMonteHW::Packet::bits_, outputBoundaryPacket[0]);

						outputBoundaryPacket[0][0] = afu.mmio_read64(1<<3);
						outputBoundaryPacket[0][1] = afu.mmio_read64(1<<3);
						outputBoundaryPacket[0][2] = afu.mmio_read64(1<<3);
						outputBoundaryPacket[0][3] = afu.mmio_read64(1<<3);
						outputBoundaryPacket[0][4] = afu.mmio_read64(1<<3);
						outputBoundaryPacket[0][5] = afu.mmio_read64(1<<3);

						BoundaryPacketUnpacker & boundaryPacket;
					}
					else if (state == BOUNDARY_REFRACTIVE_INTERFACE_STATE)
					{

						// Unpacker for de-serializing the data into the BoundaryOutput struct
						Unpacker<std::array<uint64_t,6>> BoundaryRefractionUnpacker(FullMonteHW::BoundaryOutput::bits, outputBoundaryRefraction[0]);

						outputBoundaryRefraction[0][0] = afu.mmio_read64(1<<3);
						outputBoundaryRefraction[0][1] = afu.mmio_read64(1<<3);
						outputBoundaryRefraction[0][2] = afu.mmio_read64(1<<3);
						outputBoundaryRefraction[0][3] = afu.mmio_read64(1<<3);
						outputBoundaryRefraction[0][4] = afu.mmio_read64(1<<3);
						outputBoundaryRefraction[0][5] = afu.mmio_read64(1<<3);

						BoundaryRefractionUnpacker & boundaryOutput;
					}
				}


////////////////////////////////////////////// Hit Adjacent Tetra Loop //////////////////////////////////////////////////////////////////////////
				//cout << "  hit face " << stepResult.idx << "/4 into tetra " << stepResult.IDte << endl;
				// extremely rarely, this can be a problem; we get no match in the getIntersection routine
				if(stepResult.idx > 3)
				{
					{
						AutoStreamBuffer b(cerr);
						b << "Abnormal condition: stepResult.idx=" << stepResult.idx << " (no tetra intersection), IDte=" << stepResult.IDte <<
							"  Terminating packet\n" << "\tThis may happen occasionally but if it's more than a few ppm of packets, or consistently the same tetra that would be a concern.\n";
					}
					//log_event(logger,kernel->m_scorer,Events::NoHit,pkt,currTetra);
					//log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
					killPacket = true;
					break;
				}

////////////////////////////////////////////// Hit Adjacent Tetra Loop //////////////////////////////////////////////////////////////////////////
#ifdef USE_SSE
				// pkt.s[0] = physical step length - distance -> remaining physical step length
				// pkt.s[1] = dimensionless step length - distance * muT -> remaining dimensionless step length
				// pkt.s[2] = time + n/c0 -> increase the time by the elapsed time the photon has travelled by
				// pkt.s[3] = 0 -> ignored
				pkt.s = _mm_add_ps(
							pkt.s,
							_mm_mul_ps(
									stepResult.distance,
									currMat.m_prop));
#else
				pkt.s = add128(
							pkt.s,
							mult128(
									stepResult.distance,
									currMat.m_prop));
#endif
				
#ifdef USE_SSE				
				_mm_store_ps(stepLength,pkt.s);

				_mm_store_ps(position,pkt.p);
#else
				stepLength[0] = pkt.s[0];
				stepLength[1] = pkt.s[1];
				stepLength[2] = pkt.s[2];
				stepLength[3] = pkt.s[3];

				position[0] = pkt.p[0];
				position[1] = pkt.p[1];
				position[2] = pkt.p[2];
				position[3] = pkt.p[3];
#endif

				if (m_fullPipeline || m_boundary)
				{
					if (state == BOUNDARY_TRANSMIT_STATE)
					{
						if (m_verbose)
						{
							LOG_INFO << "Boundary Transmit Output - Material ID" << endl
									 << "HW: " << boundaryPacket.matID.value() << " vs. SW: " << IDm << endl << endl
									 << "Boundary Transmit Output - Dimensionless step length:" << endl
									 << "HW: " << boundaryPacket.l.value() << " vs. SW: " << stepLength[1]/log(2.0) << endl
									 << "Error: " << stepLength[1]/log(2.0) - boundaryPacket.l.value() << endl << endl
									 << "Boundary Transmit Output - Weight:" << endl
									 << "HW: " << boundaryPacket.w.value() << " vs. SW: " << pkt.w << endl
									 << "Error: " << pkt.w - boundaryPacket.w.value() << endl << endl
									 << "Boundary Transmit Output - TetraID:" << endl
									 << "HW: " << boundaryPacket.tetID.value() << " vs. SW: " << IDt << endl << endl
									 << "Boundary Transmit Output - Photon position:" << endl
									 << "HW: " << boundaryPacket.pos[0].value() << " <-> SW: " << position[0] << endl
									 << "HW: " << boundaryPacket.pos[1].value() << " <-> SW: " << position[0] << endl
									 << "HW: " << boundaryPacket.pos[2].value() << " <-> SW: " << position[0] << endl
									 << "Error: " << position[0] - (boundaryPacket.pos[0].value()*scale + shift[0]) << " "
												  << position[1] - (boundaryPacket.pos[1].value()*scale + shift[1]) << " " 
					 			 			  << position[1] - (boundaryPacket.pos[1].value()*scale + shift[1]) << " " 
												  << position[1] - (boundaryPacket.pos[1].value()*scale + shift[1]) << " " 
												  << position[2] - (boundaryPacket.pos[2].value()*scale + shift[2]) << endl
									 << "Boundary Transmit Output - Photon direction:" << endl
									 << "HW: " << boundaryPacket.dir.d[0].value() << " <-> SW: " << pkt.dir.d.array()[0] << endl
									 << "HW: " << boundaryPacket.dir.d[1].value() << " <-> SW: " << pkt.dir.d.array()[1] << endl
									 << "HW: " << boundaryPacket.dir.d[2].value() << " <-> SW: " << pkt.dir.d.array()[2] << endl
									 << "Error: " << pkt.dir.d.array()[0] - boundaryPacket.dir.d[0].value() << endl 
												  << pkt.dir.d.array()[1] - boundaryPacket.dir.d[1].value() << endl 
												  << pkt.dir.d.array()[2] - boundaryPacket.dir.d[2].value() << endl << endl;
						} 	
						
						Output_boundary_step += (abs(stepLength[1]/log(2.0) - boundaryPacket.l.value()) - Output_boundary_step) / boundary_count;
						Output_boundary_weight += (abs(pkt.w - boundaryPacket.w.value()) - Output_boundary_weight) / boundary_count;
						Output_boundary_pos[0] += (abs(position[0] - (boundaryPacket.pos[0].value()*scale + shift[0])) - Output_boundary_pos[0]) / boundary_count;
						Output_boundary_pos[1] += (abs(position[1] - (boundaryPacket.pos[1].value()*scale + shift[1])) - Output_boundary_pos[1]) / boundary_count;
						Output_boundary_pos[2] += (abs(position[2] - (boundaryPacket.pos[2].value()*scale + shift[2])) - Output_boundary_pos[2]) / boundary_count;
						Output_boundary_dir[0] += (abs(pkt.dir.d.array()[0] - boundaryPacket.dir.d[0].value()) - Output_boundary_dir[0]) / boundary_count;
						Output_boundary_dir[1] += (abs(pkt.dir.d.array()[1] - boundaryPacket.dir.d[1].value()) - Output_boundary_dir[1]) / boundary_count;
						Output_boundary_dir[2] += (abs(pkt.dir.d.array()[2] - boundaryPacket.dir.d[2].value()) - Output_boundary_dir[2]) / boundary_count;

						Output_boundary_step_error_sum += abs(stepLength[1]/log(2.0) - boundaryPacket.l.value());
						Output_boundary_weight_error_sum += abs(pkt.w - boundaryPacket.w.value());
						Output_boundary_pos_error_sum[0] += abs(position[0] - (boundaryPacket.pos[0].value()*scale + shift[0]));
						Output_boundary_pos_error_sum[1] += abs(position[1] - (boundaryPacket.pos[1].value()*scale + shift[1]));
						Output_boundary_pos_error_sum[2] += abs(position[2] - (boundaryPacket.pos[2].value()*scale + shift[2]));
						Output_boundary_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - boundaryPacket.dir.d[0].value());
						Output_boundary_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - boundaryPacket.dir.d[1].value());
						Output_boundary_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - boundaryPacket.dir.d[2].value());

						Output_boundary_step_sum += stepLength[1]/log(2.0);
						Output_boundary_weight_sum += pkt.w;
						Output_boundary_pos_sum[0] += abs(position[0]);
						Output_boundary_pos_sum[1] += abs(position[1]);
						Output_boundary_pos_sum[2] += abs(position[2]);
						Output_boundary_dir_sum[0] += abs(pkt.dir.d.array()[0]);
						Output_boundary_dir_sum[1] += abs(pkt.dir.d.array()[1]);
						Output_boundary_dir_sum[2] += abs(pkt.dir.d.array()[2]);
					}
					else if (state == BOUNDARY_REFRACTIVE_INTERFACE_STATE)
					{
						if(m_verbose)
						{
							LOG_INFO << "Boundary Reflect/Refract Output - Adjacent TetraID" << endl
									 << "HW: " << boundaryOutput.adj.tetID.value() << " vs. SW: " << stepResult.IDte << endl << endl
									 << "Boundary Reflect/Refract Output - Material ID" << endl
									 << "HW: " << boundaryOutput.pkt.matID.value() << " vs. SW: " << IDm << endl << endl
									 << "Boundary Reflect/Refract Output - Dimensionless step length:" << endl
									 << "HW: " << boundaryOutput.pkt.l.value() << " vs. SW: " << stepLength[1]/log(2.0) << endl
									 << "Error: " << stepLength[1]/log(2.0) - boundaryOutput.pkt.l.value() << endl << endl
									 << "Boundary Reflect/Refract Output - Weight:" << endl
									 << "HW: " << boundaryOutput.pkt.w.value() << " vs. SW: " << pkt.w << endl
									 << "Error: " << pkt.w - boundaryOutput.pkt.w.value() << endl << endl
									 << "Boundary Reflect/Refract Output - TetraID:" << endl
									 << "HW: " << boundaryOutput.pkt.tetID.value() << " vs. SW: " << IDt << endl << endl
									 << "Boundary Reflect/Refract Output - cos(theta) between photon direction and normal of tetra face nearest to the photon:" << endl
									 << "HW: " << boundaryOutput.costheta.value() << " vs. SW: " << cos_theta[stepResult.idx] << endl
									 << "Error: " << cos_theta[stepResult.idx] - boundaryOutput.costheta.value() << endl << endl
									 << "Boundary Reflect/Refract Output - Photon position:" << endl
									 << "HW: " << boundaryOutput.pkt.pos[0].value() << " <-> SW: " << position[0] << endl
									 << "HW: " << boundaryOutput.pkt.pos[1].value() << " <-> SW: " << position[0] << endl
									 << "HW: " << boundaryOutput.pkt.pos[2].value() << " <-> SW: " << position[0] << endl
									 << "Error: " << position[0] - (boundaryOutput.pkt.pos[0].value()*scale + shift[0]) << " "
												  << position[1] - (boundaryOutput.pkt.pos[1].value()*scale + shift[1]) << " "
												  << position[2] - (boundaryOutput.pkt.pos[2].value()*scale + shift[2]) << endl
									 << "Boundary Reflect/Refract Output - Photon direction:" << endl
									 << "HW: " << boundaryOutput.pkt.dir.d[0].value() << " <-> SW: " << pkt.dir.d.array()[0] << endl
									 << "HW: " << boundaryOutput.pkt.dir.d[1].value() << " <-> SW: " << pkt.dir.d.array()[1] << endl
									 << "HW: " << boundaryOutput.pkt.dir.d[2].value() << " <-> SW: " << pkt.dir.d.array()[2] << endl
									 << "Error: " << pkt.dir.d.array()[0] - boundaryOutput.pkt.dir.d[0].value() << endl 
												  << pkt.dir.d.array()[1] - boundaryOutput.pkt.dir.d[1].value() << endl 
												  << pkt.dir.d.array()[2] - boundaryOutput.pkt.dir.d[2].value() << endl << endl;
						}

						Output_boundary_step += (abs(stepLength[1]/log(2.0) - boundaryOutput.pkt.l.value()) - Output_boundary_step) / boundary_count;
						Output_boundary_weight += (abs(pkt.w - boundaryOutput.pkt.w.value()) - Output_boundary_weight) / boundary_count;
						Output_boundary_pos[0] += (abs(position[0] - (boundaryOutput.pkt.pos[0].value()*scale + shift[0])) - Output_boundary_pos[0]) / boundary_count;
						Output_boundary_pos[1] += (abs(position[1] - (boundaryOutput.pkt.pos[1].value()*scale + shift[1])) - Output_boundary_pos[1]) / boundary_count;
						Output_boundary_pos[2] += (abs(position[2] - (boundaryOutput.pkt.pos[2].value()*scale + shift[2])) - Output_boundary_pos[2]) / boundary_count;
						Output_boundary_dir[0] += (abs(pkt.dir.d.array()[0] - boundaryOutput.pkt.dir.d[0].value()) - Output_boundary_dir[0]) / boundary_count;
						Output_boundary_dir[1] += (abs(pkt.dir.d.array()[1] - boundaryOutput.pkt.dir.d[1].value()) - Output_boundary_dir[1]) / boundary_count;
						Output_boundary_dir[2] += (abs(pkt.dir.d.array()[2] - boundaryOutput.pkt.dir.d[2].value()) - Output_boundary_dir[2]) / boundary_count;
						Output_boundary_costheta += (abs(abs(cos_theta[stepResult.idx]) - boundaryOutput.costheta.value()) - Output_boundary_costheta) / boundary_costheta_count;
						
						boundary_costheta_count++;

						Output_boundary_step_error_sum += abs(stepLength[1]/log(2.0) - boundaryOutput.pkt.l.value());
						Output_boundary_weight_error_sum += abs(pkt.w - boundaryOutput.pkt.w.value());
						Output_boundary_pos_error_sum[0] += abs(position[0] - (boundaryOutput.pkt.pos[0].value()*scale + shift[0]));
						Output_boundary_pos_error_sum[1] += abs(position[1] - (boundaryOutput.pkt.pos[1].value()*scale + shift[1]));
						Output_boundary_pos_error_sum[2] += abs(position[2] - (boundaryOutput.pkt.pos[2].value()*scale + shift[2]));
						Output_boundary_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - boundaryOutput.pkt.dir.d[0].value());
						Output_boundary_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - boundaryOutput.pkt.dir.d[1].value());
						Output_boundary_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - boundaryOutput.pkt.dir.d[2].value());
						Output_boundary_costheta_error_sum += abs(abs(cos_theta[stepResult.idx]) - boundaryOutput.costheta.value());

						Output_boundary_step += stepLength[1]/log(2.0);
						Output_boundary_weight += pkt.w;
						Output_boundary_pos[0] += abs(position[0]);
						Output_boundary_pos[1] += abs(position[1]);
						Output_boundary_pos[2] += abs(position[2]);
						Output_boundary_dir[0] += abs(pkt.dir.d.array()[0]);
						Output_boundary_dir[1] += abs(pkt.dir.d.array()[1]);
						Output_boundary_dir[2] += abs(pkt.dir.d.array()[2]);
						Output_boundary_costheta += abs(cos_theta[stepResult.idx]);
					}

					boundary_count++;
				}

				IDm_bound = m_tetrasSW[stepResult.IDte].matID;

				if (IDm == IDm_bound) // boundary with no material change
				{ 
					//log_event(logger,kernel->m_scorer,Events::Boundary,pkt.p,stepResult.IDfe,IDt,stepResult.IDte);
					IDt_next = stepResult.IDte;
				}
				else // boundary with material change
				{
					n2 = m_matsSW[IDm_bound].n;
					n1 = currMat.n;

					if (n1 == n2) // no refractive index difference at boundary
					{
						//log_event(logger,kernel->m_scorer,Events::Boundary,pkt.p,stepResult.IDfe,IDt,stepResult.IDte);
						IDt_next = stepResult.IDte;
					}
////////////////////////////////////////////// Hardware: Reflect/Refract Calculation //////////////////////////////////////////////////////////////////////////
					else // refractive index difference at boundary -> do reflect and refract calculations
					{
#ifdef USE_SSE
						//log_event(logger,kernel->m_scorer,Events::Interface,make_pair(pkt.p,__m128(pkt.direction())),stepResult.IDfe,stepResult.IDte);
						__m128 Fn[4];

						Fn[0] = currTetra.nx;
						Fn[1] = currTetra.ny;
						Fn[2] = currTetra.nz;
						Fn[3] = _mm_setzero_ps();
						_MM_TRANSPOSE4_PS(Fn[0],Fn[1],Fn[2],Fn[3]);
						__m128 normal = Fn[stepResult.idx];

						_mm_store_ps(refrefNormal,normal);
#else
						m128 Fn[4];

						Fn[0] = currTetra.nx;
						Fn[1] = currTetra.ny;
						Fn[2] = currTetra.nz;
						Fn[3] = set_zero128();
						
						//Replaces _MM_TRANSPOSE4_PS(Fn[0],Fn[1],Fn[2],Fn[3]);
						array<float,4> tmp3, tmp2, tmp1, tmp0;
						tmp0 = {Fn[0][0], Fn[1][0], Fn[2][0], Fn[3][0]};
						tmp1 = {Fn[0][1], Fn[1][1], Fn[2][1], Fn[3][1]};
						tmp2 = {Fn[0][2], Fn[1][2], Fn[2][2], Fn[3][2]};
						tmp3 = {Fn[0][3], Fn[1][3], Fn[2][3], Fn[3][3]};
						
						Fn[0] = tmp0;
						Fn[1] = tmp1;
						Fn[2] = tmp2;
						Fn[3] = tmp3;
						m128 normal = Fn[stepResult.idx];

						refrefNormal[0] = normal[0];
						refrefNormal[1] = normal[1];
						refrefNormal[2] = normal[2];
						refrefNormal[3] = normal[3];

#endif
						const float* RNGrefref = m_rng->floatU01();

						std::array<float, 3> NormalRefRef = {normal[0], normal[1], normal[2]};
						
						inputReflectRefract.normal = FullMonteHW::UVect3D_18(NormalRefRef);
						inputReflectRefract.bernoulli_rnd = *RNGrefref;

						if (m_fullPipeline)
						{
							inputReflectRefract.ifcID = boundaryOutput.adj.ifcID;
							inputReflectRefract.d = boundaryOutput.pkt.dir.d;
							inputReflectRefract.costheta = boundaryOutput.costheta;
						}
						else
						{
							inputReflectRefract.ifcID = P8TF.getInterfaceID(IDm,IDm_bound);
							inputReflectRefract.d = FullMonteHW::UVect3D_18(pkt.dir.d.array());
							inputReflectRefract.costheta = FullMonteHW::CosTheta(abs(cos_theta[stepResult.idx]));
						}

						if (m_verbose && m_reflectRefract)
						{
							LOG_INFO << "ReflectRefract Input - cos(theta):" << endl
										<< "HW: " << inputReflectRefract.costheta.value() << " vs. SW: " << abs(cos_theta[stepResult.idx]) << endl
										<< "Error: " << abs(cos_theta[stepResult.idx]) - inputReflectRefract.costheta.value() << endl << endl
										<< "ReflectRefract Input - Fresnel random number:" << endl
										<< "HW: " << inputReflectRefract.bernoulli_rnd.value() << " vs. SW: " << *RNGrefref << endl
										<< "Error: " << *RNGrefref - inputReflectRefract.bernoulli_rnd.value() << endl << endl
										<< "ReflectRefract Input - Surface normal:" << endl
										<< "HW: " << inputReflectRefract.normal[0].integer_value() << " (" << inputReflectRefract.normal[0].integer_value()/pow(2,17) << ") <-> SW: " << NormalRefRef[0] << endl
										<< "HW: " << inputReflectRefract.normal[1].integer_value() << " (" << inputReflectRefract.normal[1].integer_value()/pow(2,17) << ") <-> SW: " << NormalRefRef[1] << endl
										<< "HW: " << inputReflectRefract.normal[2].integer_value() << " (" << inputReflectRefract.normal[2].integer_value()/pow(2,17) << ") <-> SW: " << NormalRefRef[2] << endl
										<< "Error: " 	<< NormalRefRef[0] - inputReflectRefract.normal[0].value() << " "
													<< NormalRefRef[1] - inputReflectRefract.normal[1].value() << " "
													<< NormalRefRef[2] - inputReflectRefract.normal[2].value() << endl
										<< "ReflectRefract Input - Photon direction:" << endl
										<< "HW: " << inputReflectRefract.d[0].integer_value() << " (" << inputReflectRefract.d[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[0] << endl
										<< "HW: " << inputReflectRefract.d[1].integer_value() << " (" << inputReflectRefract.d[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[1] << endl
										<< "HW: " << inputReflectRefract.d[2].integer_value() << " (" << inputReflectRefract.d[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[2] << endl
										<< "Error: " << pkt.dir.d.array()[0] - inputReflectRefract.d[0].value() << endl 
													 << pkt.dir.d.array()[1] - inputReflectRefract.d[1].value() << endl 
													 << pkt.dir.d.array()[2] - inputReflectRefract.d[2].value() << endl << endl
										<< "ReflectRefract Input - InterfaceID:" << endl
										<< "HW: " << inputReflectRefract.ifcID.value() << " <-> SW: " << P8TF.getInterfaceID(IDm,IDm_bound) << endl << endl;
						}

						Input_reflectRefract_normal[0] += (abs(normal[0] - inputReflectRefract.normal[0].value()) - Input_reflectRefract_normal[0]) / reflectRefract_count;
						Input_reflectRefract_normal[1] += (abs(normal[1] - inputReflectRefract.normal[1].value()) - Input_reflectRefract_normal[1]) / reflectRefract_count;
						Input_reflectRefract_normal[2] += (abs(normal[2] - inputReflectRefract.normal[2].value()) - Input_reflectRefract_normal[2]) / reflectRefract_count;
						Input_reflectRefract_rnd += (abs(*RNGrefref - inputReflectRefract.bernoulli_rnd.value()) - Input_reflectRefract_rnd) / reflectRefract_count;
						Input_reflectRefract_dir[0] += (abs(pkt.dir.d.array()[0] - inputReflectRefract.d[0].value()) - Input_reflectRefract_dir[0]) / reflectRefract_count;
						Input_reflectRefract_dir[1] += (abs(pkt.dir.d.array()[1] - inputReflectRefract.d[1].value()) - Input_reflectRefract_dir[1]) / reflectRefract_count;
						Input_reflectRefract_dir[2] += (abs(pkt.dir.d.array()[2] - inputReflectRefract.d[2].value()) - Input_reflectRefract_dir[2]) / reflectRefract_count;
						Input_reflectRefract_costheta += (abs(abs(cos_theta[stepResult.idx]) - inputReflectRefract.costheta.value()) - Input_reflectRefract_costheta) / reflectRefract_count;

						Input_reflectRefract_normal_error_sum[0] += abs(normal[0] - inputReflectRefract.normal[0].value());
						Input_reflectRefract_normal_error_sum[1] += abs(normal[1] - inputReflectRefract.normal[1].value());
						Input_reflectRefract_normal_error_sum[2] += abs(normal[2] - inputReflectRefract.normal[2].value());
						Input_reflectRefract_rnd_error_sum += abs(*RNGrefref - inputReflectRefract.bernoulli_rnd.value());
						Input_reflectRefract_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - inputReflectRefract.d[0].value());
						Input_reflectRefract_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - inputReflectRefract.d[1].value());
						Input_reflectRefract_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - inputReflectRefract.d[2].value());
						Input_reflectRefract_costheta_error_sum += abs(abs(cos_theta[stepResult.idx]) - inputReflectRefract.costheta.value());

						Input_reflectRefract_normal_sum[0] += abs(normal[0]);
						Input_reflectRefract_normal_sum[1] += abs(normal[1]);
						Input_reflectRefract_normal_sum[2] += abs(normal[2]);
						Input_reflectRefract_rnd_sum += abs(*RNGrefref);
						Input_reflectRefract_dir_sum[0] += abs(pkt.dir.d.array()[0]);
						Input_reflectRefract_dir_sum[1] += abs(pkt.dir.d.array()[1]);
						Input_reflectRefract_dir_sum[2] += abs(pkt.dir.d.array()[2]);
						Input_reflectRefract_costheta_sum += abs(cos_theta[stepResult.idx]);
						
						if(m_fullPipeline || m_reflectRefract)
						{		
							reflectRefract_serialized[0][0] = 0x0;
							reflectRefract_serialized[0][1] = 0x0;
							reflectRefract_serialized[0][2] = 0x0;

							// Packer for serializing ReflectRefract input
							Packer<std::array<uint64_t,3>> reflectRefractPacker(FullMonteHW::ReflectRefractRequest::bits,reflectRefract_serialized[0]); 
							reflectRefractPacker.reset(reflectRefract_serialized[0]);
							reflectRefractPacker & inputReflectRefract;

							// serialized FullMonteHW::ReflectRefractRequest													
							afu.mmio_write64(7<<3, (uint64_t)reflectRefract_serialized[0][0]); 
							afu.mmio_write64(7<<3, (uint64_t)reflectRefract_serialized[0][1]); 
							afu.mmio_write64(7<<3, (uint64_t)reflectRefract_serialized[0][2]); 		

							// Signal HW to start the Boundary module
							afu.mmio_write64(8<<3,0x0ULL);

							counter = 0;
							state=afu.mmio_read64(0);																		
							while(state != REFRACTED_STATE && state != REFLECTED_STATE)																			
							{
								if (counter > 100)
								{																																																																		
									LOG_INFO << "  Waiting for HW state being 'REFRACTED_STATE' (8) or 'REFLECTED_STATE' (9). Current state (state=" 
										<< state << " looking for " << REFRACTED_STATE << " or "<< REFLECTED_STATE <<")" << endl;	
								}								
								usleep(200);
								state=afu.mmio_read64(0);
								counter++;																													
							}

							// Unpacker for de-serializing the data into the StepResult struct
							Unpacker<std::array<uint64_t,3>> ReflectRefractUnpackerInit(FullMonteHW::PacketDirection::bits, outputReflectRefractDirection[0]);
							
							outputReflectRefractDirection[0][0] = afu.mmio_read64(1<<3);
							outputReflectRefractDirection[0][1] = afu.mmio_read64(1<<3);
							outputReflectRefractDirection[0][2] = afu.mmio_read64(1<<3);
							// De-serialization of HW output data back to a software type FullMonteHW::Output
							ReflectRefractUnpackerInit & ReflectRefractDirection;
						}

#ifdef USE_SSE		
						__m128 costheta = _mm_min_ps(
												_mm_set1_ps(1.0),
												_mm_sub_ps(
														_mm_setzero_ps(),
														__m128(SSE::dot(SSE::Vector3(normal),pkt.direction()))));

						ratio = n1/n2;
						__m128 n1_n2_ratio = _mm_load_ps(f_tmp);

						__m128 sini_cosi_sint_cost = RefractSSE(n1_n2_ratio,costheta);

						__m128 newdir;
						// Total internal reflection calculation
						__m128 newdirTIR = reflect(__m128(pkt.direction()),normal,sini_cosi_sint_cost);

						// Fresnel reflecation and refraction preparation
						// d = dir + normal*cos(theta)
						__m128 d_p = _mm_add_ps(
									__m128(pkt.direction()),
									_mm_mul_ps(
										normal,
										costheta));

						__m128 pr = FresnelSSE(n1_n2_ratio,sini_cosi_sint_cost);

						// Fresnel reflection calculation <- should be the same as TIR calculation
						__m128 newdirFresnel = reflect(__m128(pkt.direction()),normal,sini_cosi_sint_cost);

						// Refraction calculation
						// (dir + normal*cosi) * n1/n2 - (normal * cost)
						__m128 newdirRefraction = _mm_sub_ps(_mm_mul_ps( d_p, _mm_shuffle_ps(n1_n2_ratio,n1_n2_ratio,_MM_SHUFFLE(2,2,2,2))), _mm_mul_ps( normal, _mm_shuffle_ps(sini_cosi_sint_cost, sini_cosi_sint_cost,_MM_SHUFFLE(3,3,3,3))));
						// check for internal reflection
						if (_mm_movemask_ps(_mm_cmplt_ss(_mm_set_ss(1.0),_mm_movehl_ps(sini_cosi_sint_cost,sini_cosi_sint_cost)))&1)
						{
							newdir = newdirTIR;
							if (state == REFRACTED_STATE)
								{
									LOG_INFO << "HW: Refraction ocurred" << endl
											 << "SW: Reflection ocurred (TIR)" << endl;
								}
							else if (state == REFLECTED_STATE)
								{
									LOG_INFO << "HW+SW: Reflection ocurred" << endl;
								}
						}
						else // no internal reflection -> refraction calculation 
						{
							
							if (_mm_movemask_ps(
									_mm_cmplt_ss(
											_mm_load_ss(RNGrefref),
											pr))&1)
							{ // check if fresnel reflection occurs
								newdir = newdirFresnel;
								if (state == REFRACTED_STATE)
								{
									LOG_INFO << "HW: Refraction ocurred" << endl
											 << "SW: Reflection ocurred (Fresnel)" << endl;
								}
							else if (state == REFLECTED_STATE)
								{
									LOG_INFO << "HW+SW: Reflection ocurred" << endl;
								}
							}
							else { // refraction occurs
								newdir = newdirRefraction;
								if (state == REFRACTED_STATE)
								{
									LOG_INFO << "HW+SW: Refraction ocurred" << endl;
								}
							else if (state == REFLECTED_STATE)
								{
									LOG_INFO << "HW: Reflection ocurred" << endl
											 << "SW: Refraction ocurred" << endl;
								}
								IDt_next = stepResult.IDte;
							} // if: fresnel reflection
#else
						m128 costheta = min128(
												set1_ps128(1.0),
												sub128(
														set_zero128(),
														m128(SSE::dot(SSE::Vector3(normal),pkt.direction()))));

						ratio = n1/n2;
						m128 n1_n2_ratio = load_ps128(f_tmp);

						m128 sini_cosi_sint_cost = RefractSSE(n1_n2_ratio,costheta);


						m128 newdir;
						/**
						 * @brief Check for Total Internal Reflection (TIR)?
						 * 
						 */
						if (1.0 < sini_cosi_sint_cost[2])// 1 < sint
						{
							newdir = reflect(m128(pkt.direction()),normal,sini_cosi_sint_cost);
							LOG_INFO << "Total internal reflection occurred" << endl;
						}
						else 
						{
							m128 d_p = add128(
										m128(pkt.direction()),
										mult128(
											normal,
											costheta));

							m128 pr = FresnelSSE(n1_n2_ratio,sini_cosi_sint_cost);
							
							if (*RNGrefref < pr[0])
							{
								newdir = reflect(m128(pkt.direction()),normal,sini_cosi_sint_cost);
								LOG_INFO << "Fresnel reflection occurred" << endl;
							}
							else {
								newdir = sub128(
									mult128(
										d_p,
										shuffle128(n1_n2_ratio,n1_n2_ratio,2,2,2,2)),
									mult128(
										normal,
										shuffle128(sini_cosi_sint_cost,sini_cosi_sint_cost,3,3,3,3)));
								IDt_next = stepResult.IDte;
								LOG_INFO << "Refraction occurred" << endl;
							} // if: fresnel reflection
#endif
						}
						pkt.dir = PacketDirection(SSE::UnitVector3(SSE::Vector3(newdir),SSE::NoCheck));
						
						if (m_fullPipeline || m_reflectRefract)
						{
							if (m_verbose)
							{
								if (state == REFRACTED_STATE)
								{
									LOG_INFO << "HW: Refraction ocurred" << endl;
								}
								else if (state == REFLECTED_STATE)
								{
									LOG_INFO << "HW: Reflection ocurred" << endl;
								}
								LOG_INFO << "ReflectRefract Output - d:" << endl
										<< "HW: " << ReflectRefractDirection.d[0].value() << " vs. SW: " << pkt.dir.d.array()[0] << endl
										<< "Error: " << pkt.dir.d.array()[0] - ReflectRefractDirection.d[0].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.d[1].value() << " vs. SW: " << pkt.dir.d.array()[1] << endl
										<< "Error: " << pkt.dir.d.array()[1] - ReflectRefractDirection.d[1].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.d[2].value() << " vs. SW: " << pkt.dir.d.array()[2] << endl
										<< "Error: " << pkt.dir.d.array()[2] - ReflectRefractDirection.d[2].value() << endl << endl
										<< "ReflectRefract Output - a:" << endl
										<< "HW: " << ReflectRefractDirection.a[0].value() << " vs. SW: " << pkt.dir.a.array()[0] << endl
										<< "Error: " << pkt.dir.a.array()[0] - ReflectRefractDirection.a[0].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.a[1].value() << " vs. SW: " << pkt.dir.a.array()[1] << endl
										<< "Error: " << pkt.dir.a.array()[1] - ReflectRefractDirection.a[1].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.a[2].value() << " vs. SW: " << pkt.dir.a.array()[2] << endl
										<< "Error: " << pkt.dir.a.array()[2] - ReflectRefractDirection.a[2].value() << endl << endl
										<< "ReflectRefract Output - b:" << endl
										<< "HW: " << ReflectRefractDirection.b[0].value() << " vs. SW: " << pkt.dir.b.array()[0] << endl
										<< "Error: " << pkt.dir.b.array()[0] - ReflectRefractDirection.b[0].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.b[1].value() << " vs. SW: " << pkt.dir.b.array()[1] << endl
										<< "Error: " << pkt.dir.b.array()[1] - ReflectRefractDirection.b[1].value() << endl << endl
										<< "HW: " << ReflectRefractDirection.b[2].value() << " vs. SW: " << pkt.dir.b.array()[2] << endl
										<< "Error: " << pkt.dir.b.array()[2] - ReflectRefractDirection.b[2].value() << endl << endl;
							}

							Output_reflectRefract_dir[0] += (abs(pkt.dir.d.array()[0] - ReflectRefractDirection.d[0].value()) - Output_reflectRefract_dir[0]) / reflectRefract_count;
							Output_reflectRefract_dir[1] += (abs(pkt.dir.d.array()[1] - ReflectRefractDirection.d[1].value()) - Output_reflectRefract_dir[1]) / reflectRefract_count;
							Output_reflectRefract_dir[2] += (abs(pkt.dir.d.array()[2] - ReflectRefractDirection.d[2].value()) - Output_reflectRefract_dir[2]) / reflectRefract_count;
							Output_reflectRefract_a[0] += (abs(pkt.dir.a.array()[0] - ReflectRefractDirection.a[0].value()) - Output_reflectRefract_a[0]) / reflectRefract_count;
							Output_reflectRefract_a[1] += (abs(pkt.dir.a.array()[1] - ReflectRefractDirection.a[1].value()) - Output_reflectRefract_a[1]) / reflectRefract_count;
							Output_reflectRefract_a[2] += (abs(pkt.dir.a.array()[2] - ReflectRefractDirection.a[2].value()) - Output_reflectRefract_a[2]) / reflectRefract_count;
							Output_reflectRefract_b[0] += (abs(pkt.dir.b.array()[0] - ReflectRefractDirection.b[0].value()) - Output_reflectRefract_b[0]) / reflectRefract_count;
							Output_reflectRefract_b[1] += (abs(pkt.dir.b.array()[1] - ReflectRefractDirection.b[1].value()) - Output_reflectRefract_b[1]) / reflectRefract_count;
							Output_reflectRefract_b[2] += (abs(pkt.dir.b.array()[2] - ReflectRefractDirection.b[2].value()) - Output_reflectRefract_b[2]) / reflectRefract_count;

							Output_reflectRefract_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - ReflectRefractDirection.d[0].value());
							Output_reflectRefract_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - ReflectRefractDirection.d[1].value());
							Output_reflectRefract_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - ReflectRefractDirection.d[2].value());
							Output_reflectRefract_a_error_sum[0] += abs(pkt.dir.a.array()[0] - ReflectRefractDirection.a[0].value());
							Output_reflectRefract_a_error_sum[1] += abs(pkt.dir.a.array()[1] - ReflectRefractDirection.a[1].value());
							Output_reflectRefract_a_error_sum[2] += abs(pkt.dir.a.array()[2] - ReflectRefractDirection.a[2].value());
							Output_reflectRefract_b_error_sum[0] += abs(pkt.dir.b.array()[0] - ReflectRefractDirection.b[0].value());
							Output_reflectRefract_b_error_sum[1] += abs(pkt.dir.b.array()[1] - ReflectRefractDirection.b[1].value());
							Output_reflectRefract_b_error_sum[2] += abs(pkt.dir.b.array()[2] - ReflectRefractDirection.b[2].value());

							Output_reflectRefract_dir_sum[0] += abs(pkt.dir.d.array()[0]);
							Output_reflectRefract_dir_sum[1] += abs(pkt.dir.d.array()[1]);
							Output_reflectRefract_dir_sum[2] += abs(pkt.dir.d.array()[2]);
							Output_reflectRefract_a_sum[0] += abs(pkt.dir.a.array()[0]);
							Output_reflectRefract_a_sum[1] += abs(pkt.dir.a.array()[1]);
							Output_reflectRefract_a_sum[2] += abs(pkt.dir.a.array()[2]);
							Output_reflectRefract_b_sum[0] += abs(pkt.dir.b.array()[0]);
							Output_reflectRefract_b_sum[1] += abs(pkt.dir.b.array()[1]);
							Output_reflectRefract_b_sum[2] += abs(pkt.dir.b.array()[2]);

							reflectRefract_count++;
						}

					} // if ... else: refractive index difference
////////////////////////////////////////////// END Hardware: Reflect/Refract Calculation //////////////////////////////////////////////////////////////////////////
				} // if: material change


				// changing tetras
				if (IDt_next != IDt)
				{
					if (IDt_next == 0) // The photon is exiting the mesh and needs to be terminated
					{
            			//log_event(logger,kernel->m_scorer,Events::Exit,make_pair(pkt.p,__m128(pkt.direction())),stepResult.IDfe,pkt.w);
                    	//log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
						LOG_INFO << "Photon exiting mesh" << endl;
						killPacket = true;
            			break;
					}
					else // The photon is not exiting mesh and propagates into a new tetra
					{
						//log_event(logger,kernel->m_scorer,Events::NewTetra,pkt,currTetra,stepResult.idx,IDt_next);
						IDt = IDt_next;
						IDm_next = IDm_bound;
						currTetra=m_tetrasSW[IDt];
					}
				}

				// if material ID changed, fetch new material and update step length remaining
				if (IDm != IDm_next)
				{
					IDm = IDm_next;
					currMat = m_matsSW[IDm];

					// TODO: Probable error in t caused here
#ifdef USE_SSE
                	// movehdup_ps(pkt.s) does [a b c d] -> [b b d d], ie. [s l t X] -> [l l X X]
                	// div_ss(a,b) does [a0 a1 a2 a3] [b0 X X X] -> [a0/b0 a1 a2 a3]
					// recalculate the step length based on the new material properties of the new tetra and reset all other entries
					// pkt.s[0] = remaining dimensionless step length / muT -> NEW remaining physical step length
					// pkt.s[1] = remaining dimensionless step length
					// pkt.s[2] = 0 -> reset time to zero since the photon is in a new tetra
					// pkt.s[3] = 0 -> ignored
                	pkt.s = _mm_div_ss(_mm_movehdup_ps(pkt.s), _mm_set_ss(currMat.muT));
#else
                	pkt.s[0] = pkt.s[1] / currMat.muT;
                	pkt.s[2] = pkt.s[3];
#endif
            	}
				
				if(m_fullPipeline)
				{
					if (state == BOUNDARY_TRANSMIT_STATE)
					{
						pkt_HW.age = 0;
						pkt_HW.dir = boundaryPacket.dir;
						pkt_HW.l = boundaryPacket.l;
						pkt_HW.matID = boundaryPacket.matID;
						pkt_HW.pos = boundaryPacket.pos;
						pkt_HW.tag = 0;
						pkt_HW.tetID = boundaryPacket.tetID;
						pkt_HW.w = boundaryPacket.w;
					}
					else 
					{
						if (state == REFRACTED_STATE) // This means we have to change the tetraID of the photon packet to the adjacent tetraID
						{
							pkt_HW.tetID = boundaryOutput.adj.tetID;
							unsigned tetraID = (unsigned) boundaryOutput.adj.tetID;
							unsigned materialID = m_tetrasSW[tetraID].matID;
							pkt_HW.matID = FullMonteHW::MaterialID(materialID);
						}
						else if (state == REFLECTED_STATE)// The tetraID and the materialID do not change
						{
							pkt_HW.matID = boundaryOutput.pkt.matID;
							pkt_HW.tetID = boundaryOutput.pkt.tetID;
						}
						pkt_HW.age = 0;
						pkt_HW.dir = ReflectRefractDirection;
						pkt_HW.l = boundaryOutput.pkt.l;
						pkt_HW.pos = boundaryOutput.pkt.pos;
						pkt_HW.tag = 0;
						pkt_HW.w = boundaryOutput.pkt.w;
					}
				}
				else
				{
					pos[0] = (position[0] - shift[0]) / scale;
					pos[1] = (position[1] - shift[1]) / scale;
					pos[2] = (position[2] - shift[2]) / scale;
					
					pkt_HW.age = 0;
					pkt_HW.dir.d = FullMonteHW::UVect3D_18(pkt.dir.d.array());
					pkt_HW.dir.a = FullMonteHW::UVect3D_18(pkt.dir.a.array());
					pkt_HW.dir.b = FullMonteHW::UVect3D_18(pkt.dir.b.array());
					pkt_HW.l = FullMonteHW::DimensionlessStepLength(stepLength[1]/log(2.0));
					pkt_HW.matID = FullMonteHW::MaterialID(IDm);
					pkt_HW.pos = FullMonteHW::Point3D_18(pos);
					pkt_HW.tag = 0;
					pkt_HW.tetID = FullMonteHW::TetraID(IDt);
					pkt_HW.w = FullMonteHW::Weight(pkt.w);
				}

				if (m_verbose && m_intersection)
				{
					LOG_INFO << "Intersection Input - TetraID:" << endl
								<< "HW: " << pkt_HW.tetID.value() << " vs. SW: " << IDt << endl << endl
								<< "Intersection Input - MaterialID:" << endl
								<< "HW: " << pkt_HW.matID.value() << " vs. SW: " << IDm << endl << endl
								<< "Intersection Input - Photon position:" << endl
								<< "HW: " << pkt_HW.pos[0].integer_value() << " (" << pkt_HW.pos[0].integer_value()/pow(2,14)*scale + shift[0] << ") <-> SW: " << position[0] << endl
								<< "HW: " << pkt_HW.pos[1].integer_value() << " (" << pkt_HW.pos[1].integer_value()/pow(2,14)*scale + shift[1] << ") <-> SW: " << position[1] << endl
								<< "HW: " << pkt_HW.pos[2].integer_value() << " (" << pkt_HW.pos[2].integer_value()/pow(2,14)*scale + shift[2] << ") <-> SW: " << position[2] << endl
								<< "Error: " << position[0] - (pkt_HW.pos[0].value()*scale + shift[0]) << " "
											 << position[1] - (pkt_HW.pos[1].value()*scale + shift[1]) << " "
											 << position[2] - (pkt_HW.pos[2].value()*scale + shift[2]) << endl
								<< "Intersection Input - Photon direction:" << endl
								<< "HW: " << pkt_HW.dir.d[0].integer_value() << " (" << pkt_HW.dir.d[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[0] << endl
								<< "HW: " << pkt_HW.dir.d[1].integer_value() << " (" << pkt_HW.dir.d[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[1] << endl
								<< "HW: " << pkt_HW.dir.d[2].integer_value() << " (" << pkt_HW.dir.d[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.d.array()[2] << endl
								<< "Error: " << pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value() << endl 
											<< pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value() << endl 
											<< pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value() << endl << endl
								<< "Intersection Input - Photon orthonormal direction a:" << endl
								<< "HW: " << pkt_HW.dir.a[0].integer_value() << " (" << pkt_HW.dir.a[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[0] << endl 
								<< "HW: " << pkt_HW.dir.a[1].integer_value() << " (" << pkt_HW.dir.a[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[1] << endl 
								<< "HW: " << pkt_HW.dir.a[2].integer_value() << " (" << pkt_HW.dir.a[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.a.array()[2] << endl
								<< "Error: " << pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value() << endl 
											<< pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value() << endl 
											<< pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value() << endl << endl
								<< "Intersection Input - Photon orthonormal direction b:" << endl
								<< "HW: " << pkt_HW.dir.b[0].integer_value() << " (" << pkt_HW.dir.b[0].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[0] << endl 
								<< "HW: " << pkt_HW.dir.b[1].integer_value() << " (" << pkt_HW.dir.b[1].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[1] << endl 
								<< "HW: " << pkt_HW.dir.b[2].integer_value() << " (" << pkt_HW.dir.b[2].integer_value()/pow(2,17) << ") <-> SW: " << pkt.dir.b.array()[2] << endl
								<< "Error: " << pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value() << endl 
											<< pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value() << endl 
											<< pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value() << endl << endl
								<< "Intersection Input - Weight:" << endl
								<< "HW: " << pkt_HW.w.integer_value() << " (" << pkt_HW.w.integer_value()/pow(2,36) << ") "<< " <-> SW: " << pkt.w << endl
								<< "Error: " << pkt.w - pkt_HW.w.value() << endl << endl
								<< "Intersection Input - Dimensionless step length:" << endl
								<< "HW: " << pkt_HW.l.integer_value() << " (" << pkt_HW.l.integer_value()/pow(2,13) << ") "<< " <-> SW: " << stepLength[1]/log(2) << endl
								<< "Error: " << stepLength[1]/log(2) - pkt_HW.l.value() << endl << endl;
				}

				Input_intersection_pos[0] += (abs(position[0] - (pkt_HW.pos[0].value()*scale + shift[0])) - Input_intersection_pos[0]) / intersection_count;
				Input_intersection_pos[1] += (abs(position[1] - (pkt_HW.pos[1].value()*scale + shift[1])) - Input_intersection_pos[1]) / intersection_count;
				Input_intersection_pos[2] += (abs(position[2] - (pkt_HW.pos[2].value()*scale + shift[2])) - Input_intersection_pos[2]) / intersection_count;
				Input_intersection_dir[0] += (abs(pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value()) - Input_intersection_dir[0]) / intersection_count;
				Input_intersection_dir[1] += (abs(pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value()) - Input_intersection_dir[1]) / intersection_count;
				Input_intersection_dir[2] += (abs(pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value()) - Input_intersection_dir[2]) / intersection_count;
				Input_intersection_a[0] += (abs(pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value()) - Input_intersection_a[0]) / intersection_count;
				Input_intersection_a[1] += (abs(pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value()) - Input_intersection_a[1]) / intersection_count;
				Input_intersection_a[2] += (abs(pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value()) - Input_intersection_a[2]) / intersection_count;
				Input_intersection_b[0] += (abs(pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value()) - Input_intersection_b[0]) / intersection_count;
				Input_intersection_b[1] += (abs(pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value()) - Input_intersection_b[1]) / intersection_count;
				Input_intersection_b[2] += (abs(pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value()) - Input_intersection_b[2]) / intersection_count;
				Input_intersection_weight += (abs(pkt.w - pkt_HW.w.value()) - Input_intersection_weight) / intersection_count;
				Input_intersection_step += (abs(stepLength[1]/log(2) - pkt_HW.l.value()) - Input_intersection_step) / intersection_count;

				Input_intersection_pos_error_sum[0] += abs(position[0] - (pkt_HW.pos[0].value()*scale + shift[0]));
				Input_intersection_pos_error_sum[1] += abs(position[1] - (pkt_HW.pos[1].value()*scale + shift[1]));
				Input_intersection_pos_error_sum[2] += abs(position[2] - (pkt_HW.pos[2].value()*scale + shift[2]));
				Input_intersection_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - pkt_HW.dir.d[0].value());
				Input_intersection_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - pkt_HW.dir.d[1].value());
				Input_intersection_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - pkt_HW.dir.d[2].value());
				Input_intersection_a_error_sum[0] += abs(pkt.dir.a.array()[0] - pkt_HW.dir.a[0].value());
				Input_intersection_a_error_sum[1] += abs(pkt.dir.a.array()[1] - pkt_HW.dir.a[1].value());
				Input_intersection_a_error_sum[2] += abs(pkt.dir.a.array()[2] - pkt_HW.dir.a[2].value());
				Input_intersection_b_error_sum[0] += abs(pkt.dir.b.array()[0] - pkt_HW.dir.b[0].value());
				Input_intersection_b_error_sum[1] += abs(pkt.dir.b.array()[1] - pkt_HW.dir.b[1].value());
				Input_intersection_b_error_sum[2] += abs(pkt.dir.b.array()[2] - pkt_HW.dir.b[2].value());
				Input_intersection_weight_error_sum += abs(pkt.w - pkt_HW.w.value());
				Input_intersection_step_error_sum += abs(stepLength[1]/log(2) - pkt_HW.l.value());

				Input_intersection_pos_sum[0] += abs(position[0]);
				Input_intersection_pos_sum[1] += abs(position[1]);
				Input_intersection_pos_sum[2] += abs(position[2]);
				Input_intersection_dir_sum[0] += abs(pkt.dir.d.array()[0]);
				Input_intersection_dir_sum[1] += abs(pkt.dir.d.array()[1]);
				Input_intersection_dir_sum[2] += abs(pkt.dir.d.array()[2]);
				Input_intersection_a_sum[0] += abs(pkt.dir.a.array()[0]);
				Input_intersection_a_sum[1] += abs(pkt.dir.a.array()[1]);
				Input_intersection_a_sum[2] += abs(pkt.dir.a.array()[2]);
				Input_intersection_b_sum[0] += abs(pkt.dir.b.array()[0]);
				Input_intersection_b_sum[1] += abs(pkt.dir.b.array()[1]);
				Input_intersection_b_sum[2] += abs(pkt.dir.b.array()[2]);
				Input_intersection_weight_sum += abs(pkt.w);
				Input_intersection_step_sum += abs(stepLength[1]/log(2));

	//////////////////////////////////////////////// Hardware: Intersection check //////////////////////////////////////////////////////////////////
				if(m_fullPipeline || m_intersection)
				{
					// set all elements of the serialized data array to 0
					pkt_serialized[0][0] = 0x0;
					pkt_serialized[0][1] = 0x0;
					pkt_serialized[0][2] = 0x0;
					pkt_serialized[0][3] = 0x0;
					pkt_serialized[0][4] = 0x0;
					pkt_serialized[0][5] = 0x0;
					
					// Packer for serializing the Packet struct
					Packer<std::array<uint64_t,6>> packetPackerII(FullMonteHW::Packet::bits_,pkt_serialized[0]);
					packetPackerII.reset(pkt_serialized[0]);
					packetPackerII & pkt_HW;
					// Transfer packet data from host to HW and draw step size for comparison with SW														
					// mkIntersection module requires IntersectionRequest and Packet as input																
					// IntersectionRequest: TetraDef; Packet::dir; Packet::pos; Packet::l																																														

					// serialized FullMonteHW::Packet																														
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][0]);  
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][1]); 
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][2]); 				
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][3]); 		 		
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][4]);
					afu.mmio_write64(0, (uint64_t)pkt_serialized[0][5]); 

					// serialized FullMonteHW::TetraDef		
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][0]);																								
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][1]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][2]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][3]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][4]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][5]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][6]);																																				
					afu.mmio_write64(1<<3, (uint64_t)m_tetras_serialized[IDt][7]);

					// Once all required data is transferred to HW, we can signal the HW to start the Intersection module																																																																		
					afu.mmio_write64(2<<3,0x0ULL);

					counter = 0;
					while(( state=afu.mmio_read64(0) ) != INTERSECTION_DONE_STATE)																			
					{	
						if (counter > 100)
						{
							LOG_INFO << "  Waiting for HW state being 'INTERSECTION_DONE_STATE'. Current state (state=" << state << " looking for " << INTERSECTION_DONE_STATE << ")" << endl;		
						}																																																																								
						usleep(200);
						counter++;																														
					}

					// Unpacker for de-serializing the data into the StepResult struct
					Unpacker<std::array<uint64_t,2>> stepResultUnpacker(FullMonteHW::StepResult::bits, outputStepResult[0]);
				
					outputStepResult[0][0] = afu.mmio_read64(1<<3);
					outputStepResult[0][1] = afu.mmio_read64(1<<3);

					// De-serialization of HW output data back to a software type FullMonteHW::Output
					stepResultUnpacker & outputStepResultIntersection;
				}

//////////////////////////////////////////////// Intersection Calculation SW ////////////////////////////////////////////////////////////////////
#ifdef USE_SSE
				// Calculate: currTetra.getIntersection(pkt.p,__m128(pkt.direction()),pkt.s);
				d = __m128(pkt.direction());
				stepResult.idx=-1;

				// s[i] = s[0]
				// just use the physical step length for calculations 
				s = _mm_shuffle_ps(pkt.s,pkt.s,_MM_SHUFFLE(0,0,0,0));
				
				// dot[i] = nx[i] * d[0]
				dot = _mm_mul_ps(currTetra.nx,_mm_shuffle_ps(d,d,_MM_SHUFFLE(0,0,0,0)));
				// dot[i] = dot[i] + ny[i] * d[1]
				dot = _mm_add_ps(dot,_mm_mul_ps(currTetra.ny,_mm_shuffle_ps(d,d,_MM_SHUFFLE(1,1,1,1))));
				// dot[i] = dot[i] + nz[i] * d[2]
				dot = _mm_add_ps(dot,_mm_mul_ps(currTetra.nz,_mm_shuffle_ps(d,d,_MM_SHUFFLE(2,2,2,2))));
				
				// h1[i] = nx[i] * p[0]
				h1 = _mm_mul_ps(currTetra.nx,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(0,0,0,0)));
				// h1[i] = h1[i] + ny[i] * p[1]
				h1 = _mm_add_ps(h1,_mm_mul_ps(currTetra.ny,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(1,1,1,1))));
				// h1[i] = h1[i] + nz[i] * p[2]
				h1 = _mm_add_ps(h1,_mm_mul_ps(currTetra.nz,_mm_shuffle_ps(pkt.p,pkt.p,_MM_SHUFFLE(2,2,2,2))));
				// h1[i] = C[i] - h1[i]
				h1 = _mm_sub_ps(currTetra.C,h1);

				// dist[i] = h1[i]/dot[i]
				dist = _mm_div_ps(h1,dot);

				// (if dot[i] < 0) --> dist[i] = dist[i]
				// (else dot[i] >= 0) --> dist[i] = inf
				dist = _mm_blendv_ps(
							_mm_set1_ps(std::numeric_limits<float>::infinity()),
							dist,
							dot);

				//     -height  dot     h/dot   meaning
				//      -       +       -       OK: inside, facing away (no possible intersection)
				//      -       -       +       OK: inside, facing towards (intersection possible)
				//      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
				//      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

				// require C - n dot p < 0 (above face) and d dot n < 0
				// at most three of dist[i] should be negative
				/// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
				min_idx_val = getMinIndex4p(dist);

				// if (min_idx_val[0] < s[0]) -> return true
				// else resturn false
				stepResult.hit = _mm_ucomilt_ss(min_idx_val.second,s);

				
				// choose the corresponding faceID to the nearest distance -> indices can only be between 0 and 2
				stepResult.IDfe = currTetra.IDfds[min_idx_val.first&3];
				
				// choose the corresponding tetraID of the adjacent tetra with the closest distance 
				stepResult.IDte = currTetra.adjTetras[min_idx_val.first&3];
				
				// will be 4 if no min found... how can we not find a minimum in 4 numbers? Are all inf?
				stepResult.idx = min_idx_val.first;

				// compare the smallest distance min_idx_val.second with the random step length
				// and store the smaller value in result.distance
				stepResult.distance=_mm_min_ps(min_idx_val.second,s);
				
				// compare the result of the last command with 0 and store the larger value in result.distance
				stepResult.distance=_mm_max_ps(_mm_setzero_ps(),stepResult.distance);
				
				// Pe[i] = p[i] + d[i] * distance[i]
				stepResult.Pe = _mm_add_ps(pkt.p,_mm_mul_ps(d,stepResult.distance));

				// End of getIntersection
				
				_mm_store_ps(negative_height,h1);
				_mm_store_ps(cos_theta,dot);		

#else
            	// Calculate: stepResult=currTetra.getIntersection(pkt.p,m128(pkt.direction()),pkt.s);
				d = m128(pkt.direction());
				stepResult.idx=-1;
			
				/**
				 * @brief use the first entry as random step length
				 * s[i] = s[0]
				 * 
				 */
				m128 s = shuffle128(pkt.s,pkt.s,0,0,0,0);

				/**
				 * @brief calculate dot = n (dot) d
				 * calculate the entries of the dot product between n and d for all faces of a tetra
				 * 
				 * dot[i] = nx[i] * d[0]
				 */
				m128 dot = mult128(currTetra.nx,shuffle128(d,d,0,0,0,0));

				/// dot[i] = dot[i] + ny[i] * d[1]
				dot = add128(dot,mult128(currTetra.ny,shuffle128(d,d,1,1,1,1)));

				/// dot[i] = dot[i] + nz[i] * d[2]
				dot = add128(dot,mult128(currTetra.nz,shuffle128(d,d,2,2,2,2)));
				
				/**
				 * @brief calculate constraint if a point lies within the tetra. This also
				 * represents the height which the photon position is over the face of the respective tetra
				 * height = C - n (dot) p
				 * 
				 * h1[i] = nx[i] * p[0]
				 */
				m128 h1 = mult128(currTetra.nx,shuffle128(pkt.p,pkt.p,0,0,0,0));

				/// h1[i] = h1[i] + ny[i] * p[1]
				h1 = add128(h1,mult128(currTetra.ny,shuffle128(pkt.p,pkt.p,1,1,1,1)));

				/// h1[i] = h1[i] + nz[i] * p[2]
				h1 = add128(h1,mult128(currTetra.nz,shuffle128(pkt.p,pkt.p,2,2,2,2)));

				/**
				 * @brief height (= C - n dot p) should be negative if inside tetra, 
				 * may occasionally be (small) positive due to numerical error dot 
				 * negative means facing outwards. Subtract packed single-precision (32-bit) 
				 * floating-point elements in h1 from packed single-precision (32-bit) floating-point 
				 * elements in C, and store the results in h1.
				 * 
				 * h1[i] = C[i] - h1[i]
				 */
				h1 = sub128(currTetra.C,h1);


				// 
				/**
				 * @brief Divide packed single-precision (32-bit) floating-point elements 
				 * in h1 by packed elements in dot, and store the results in dist. dist is the distance 
				 * of the photon to the 4 faces of the tetra.
				 * 
				 * 
				 * dist[i] = h1[i]/dot[i]
				 */
				m128 dist = div128(h1,dot);

				
				/**
				 * @brief Checks if dot is positive or negative (_mm_blendv_ps). If it is negative, use
				 * the value of dist otherwise use infinity (which is apparently s --> )
				 * 
				 * selects dist where dist>0 and dot<0 (facing outwards), s otherwise
				 * 
				 * (if dot[i] < 0) --> dist[i] = dist[i]
				 * (else dot[i] > 0) --> dist[i] = inf
				 */
				m128 inf = set1_ps128(std::numeric_limits<float>::infinity());
				dist = blend128(
							inf,
							dist,
							dot);

				// at most three of the dot products should be negative

				//     -height  dot     h/dot   meaning
				//      -       +       -       OK: inside, facing away (no possible intersection)
				//      -       -       +       OK: inside, facing towards (intersection possible)
				//      +       +       +       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
				//      +       -       -       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)

				// require C - n dot p < 0 (above face) and d dot n < 0

				/// Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
				pair<unsigned,m128> min_idx_val = getMinIndex128(dist);

				/**
				 * @brief _mm_ucomilt_ss --> Compare the lower single-precision (32-bit) floating-point element  
				 * in min_idx_val.second and s for less-than, and return the boolean result (0 or 1). This instruction will not  
				 * signal an exception for QNaNs.
				 * Is the smallest distance of the photon smaller than the random step length s?
				 * If yes, return true. This means that the photon intersects with one of the tetras
				 * faces within the random step length 
				 * 
				 * if min_idx_val[0] < s[0] 
				 * 
				 */
				stepResult.hit = complo128(min_idx_val.second,s);

				
				/**
				 * @brief Choose the face with the smallest distance to the photons position and store it in
				 * IDfe. Bitwise AND of the index in min_idx_val because the index can also have the 
				 * value 4 which would result in bad memory access (size of IDfds is 4 [0-3])
				 * 
				 */
				stepResult.IDfe = currTetra.IDfds[min_idx_val.first&3];
				
				/**
				 * @brief Choose the adjacent tetra of the nearest face to the photons position and store it in
				 * IDte. Bitwise AND of the index in min_idx_val because the index can also have the 
				 * value 4 which would result in bad memory access (size of IDte is 4 [0-3])
				 * 
				 */
				stepResult.IDte = currTetra.adjTetras[min_idx_val.first&3];
				
				/// will be 4 if no min found
				stepResult.idx = min_idx_val.first;

				/// compare the smallest distance min_idx_val.second with the random step length
				/// and store the smaller value in result.distance
				stepResult.distance=min128(min_idx_val.second,s);
				
				/// compare the result of the last command with 0 and store the larger value in result.distance
				stepResult.distance=max128(set_zero128(),stepResult.distance);
				
				/**
				 * @brief Determine the new position of the photon after the step length. result.distance
				 * has the same value in each float element
				 * 
				 * Pe[i] = p[i] + d[i] * result.distance[i]
				 * 
				 */
				stepResult.Pe = add128(pkt.p,mult128(d,stepResult.distance));
				
				negative_height[0] = h1[0];
				negative_height[1] = h1[1];
				negative_height[2] = h1[2];
				negative_height[3] = h1[3];

				cos_theta[0] = dot[0];
				cos_theta[1] = dot[1];
				cos_theta[2] = dot[2];
				cos_theta[3] = dot[3];
#endif
/////////////////////////////////////// Intersection Results Comparison: HW vs. SW ////////////////////////////////////////////////////////////////

				if(m_fullPipeline || m_intersection)
				{
					if(m_verbose)
					{
						LOG_INFO << "Intersection Output - Adjacent TetraID" << endl
								 << "HW: " << outputStepResultIntersection.adj.tetID.value() << " <-> SW: " << stepResult.IDte << endl << endl
								 << "Intersection Output - Index (which of the 4 tetras):" << endl
								 << "HW: " << outputStepResultIntersection.idx.value() << " vs. SW: " << stepResult.idx << endl << endl
								 << "Intersection Output - Height between photon and nearest tetra:" << endl
								 << "HW: " << outputStepResultIntersection.height.value() << " vs. SW: " << abs(negative_height[stepResult.idx]) << endl 
								 << "Error: " << abs(negative_height[stepResult.idx]) - outputStepResultIntersection.height.value() << endl << endl
								 << "Intersection Output - cos(theta) between photon direction and normal of tetra face nearest to the photon:" << endl
								 << "HW: " << outputStepResultIntersection.costheta.value() << " vs. SW: " << abs(cos_theta[stepResult.idx]) << endl 
								 << "Error: " << abs(cos_theta[stepResult.idx]) - outputStepResultIntersection.costheta.value() << endl << endl
								 << "Intersection Output - Will the photon hit a tetra face or will it stay in the same tetra for this step?:" << endl
								 << "HW: 0->no Hit; 1-> Hit but no refractive index change; 2-> Hit with refractive index change" << endl
								 << "SW: 0->no Hit; 1-> Hit (software does not differentiate between the two possibilities here)" << endl
								 << "HW: " << outputStepResultIntersection.hit.value() << " vs. SW: " << stepResult.hit << endl << endl;
					}

					Output_intersection_height += (abs(abs(negative_height[stepResult.idx]) - (outputStepResultIntersection.height.value()*scale) - Output_intersection_height) / intersection_count);
					Output_intersection_costheta += (abs(abs(cos_theta[stepResult.idx]) - outputStepResultIntersection.costheta.value()) - Output_intersection_costheta) / intersection_count;

					Output_intersection_height_error_sum += abs(abs(negative_height[stepResult.idx]) - (outputStepResultIntersection.height.value()*scale));
					Output_intersection_costheta_error_sum += abs(abs(cos_theta[stepResult.idx]) - outputStepResultIntersection.costheta.value());

					Output_intersection_height_sum += abs(negative_height[stepResult.idx]);
					Output_intersection_costheta_sum += abs(cos_theta[stepResult.idx]);

					intersection_count++;
				}

				
				pkt.p   = stepResult.Pe;
			} // End of hit loop: for(Nhit=0; stepResult.hit && Nhit < Nhit_max_; ++Nhit)

			// Check if tetra does uncharacteristically many hits of tetra faces in one step
			// if it does, then something might be wrong. However, The number of hits a photon
			// is able to perform without interaction is highly dependant on the material properties of the mesh
			if (Nhit >= Nhit_max_)
			{
				cerr << "Terminated due to unusual number of interface hits at tetra " << IDt
						<< "  Nhit=" << Nhit << " dimensionless step remaining=" << pkt.s[0]  << " w=" << pkt.w << endl;
				killPacket = true;
			}
			if (killPacket == true)
			{
				killPacket = false;
				break;
			}

		// m_rng->hg(currTetra.matID): 
		// deflection_azimuth[0] = cos(theta) -> scatterDefl
		// deflection_azimuth[1] = sin(theta) -> scatterDefl
		// deflection_azimuth[2] = cos(psi)   -> scatterAz
		// deflection_azimuth[3] = sin(psi)   -> scatterAz
		deflection_azimuth = *(const std::array<float, 4>*) m_rng->hg(currTetra.matID);

/////////////////////////////////// Hardware: Absorption, Roulette and Scattering  //////////////////////////////////////////////////////////////
		
		// Data required for HW module mkStepFinish:
		// module [ModuleContext#(ctxT)] mkStepFinish#( Weight wmin, Vector#(n,PipeOut#(UVect2D_18)) hgQs, PipeOut#(UVect2D_18) azQ, PipeOut#(Bit#(4)) bernoulli, PipeOut#(Packet) pktin)
		// wmin (x); 36
		// hgQs (?); 2x18 -> deflection cos/sin(theta) replicated in HW by the amount of materials in the the mesh 
		// azQ  (?); 2x18 -> azimuth cos/sin(psi)
		// bernoulli (?); 4 -> not translatable from SW to HW
		// pkt_in (x); 318

		deflection[0] = deflection_azimuth[0];
		deflection[1] = deflection_azimuth[1];
		azimuth[0] = deflection_azimuth[2];
		azimuth[1] = deflection_azimuth[3];

		FullMonteHW::StepFinish stepFinish;
		stepFinish.wmin = FullMonteHW::Weight(wmin_);
		stepFinish.hg = FullMonteHW::UVect2D_18(deflection);
		stepFinish.az = FullMonteHW::UVect2D_18(azimuth);

		if(m_verbose && m_stepFinish)
		{
			LOG_INFO << "StepFinish Input - Random deflection angle:" << endl
						<< "HW: {cos(theta), sin(theta)} - {" << (int) stepFinish.hg[0].integer_value() << " (" << stepFinish.hg[0].integer_value()/pow(2,17) << "), " 
															<< (int) stepFinish.hg[1].integer_value() << " (" << stepFinish.hg[1].integer_value()/pow(2,17) <<  "}" 
						<< " vs. SW: {cos(theta), sin(theta)} - {" << deflection[0] << ", " << deflection[1] <<  "}" << endl
						<< "Error: {" << deflection[0] - stepFinish.hg[0].value() << ", " << deflection[1] - stepFinish.hg[1].value() << "}"<< endl << endl
						<< "StepFinish Input - Random azimuth angle:" << endl
						<< "HW: {cos(psi), sin(psi)} - {" << (int) stepFinish.az[0].integer_value() << " (" << stepFinish.az[0].integer_value()/pow(2,17) << "), " 
														<< (int) stepFinish.az[1].integer_value() << " (" << stepFinish.az[1].integer_value()/pow(2,17) <<  "}" 
						<< " vs. SW: {cos(psi), sin(psi)} - {" << azimuth[0] << ", " << azimuth[1] <<  "}" << endl
						<< "Error: {" << azimuth[0] - stepFinish.az[0].value() << ", " << azimuth[1] - stepFinish.az[1].value() << "}" << endl << endl
						<< "StepFinish Input - Minimum weight:" << endl
						<< "HW: " << stepFinish.wmin.integer_value() << " (" << stepFinish.wmin.integer_value()/pow(2,36) << "); "<< " vs. SW: " << wmin_ << endl
						<< "Error: " << wmin_ - stepFinish.wmin.value() << endl << endl;
		}		 

		Input_stepFinish_hg[0] += (abs(deflection[0] - stepFinish.hg[0].value()) - Input_stepFinish_hg[0]) / stepFinish_count;
		Input_stepFinish_hg[1] += (abs(deflection[1] - stepFinish.hg[1].value()) - Input_stepFinish_hg[1]) / stepFinish_count;
		Input_stepFinish_az[0] += (abs(azimuth[0] - stepFinish.az[0].value()) - Input_stepFinish_az[0]) / stepFinish_count;
		Input_stepFinish_az[1] += (abs(azimuth[1] - stepFinish.az[1].value()) - Input_stepFinish_az[1]) / stepFinish_count;

		Input_stepFinish_hg_error_sum[0] += abs(deflection[0] - stepFinish.hg[0].value());
		Input_stepFinish_hg_error_sum[1] += abs(deflection[1] - stepFinish.hg[1].value());
		Input_stepFinish_az_error_sum[0] += abs(azimuth[0] - stepFinish.az[0].value());
		Input_stepFinish_az_error_sum[1] += abs(azimuth[1] - stepFinish.az[1].value());

		Input_stepFinish_hg_sum[0] += abs(deflection[0]);
		Input_stepFinish_hg_sum[1] += abs(deflection[1]);
		Input_stepFinish_az_sum[0] += abs(azimuth[0]);
		Input_stepFinish_az_sum[1] += abs(azimuth[1]);

		if (m_fullPipeline || m_stepFinish)
		{

			vector<std::array<uint64_t,2>,boost::alignment::aligned_allocator<std::array<uint64_t,2>,128>> stepFinish_serialized; // serialized StepFinish input for HW transfer
			stepFinish_serialized.resize(1); // we only need the vector to contain 1 element because we will reuse the same element again
			// StepFinish is 108 bits large and is defined in CXXFullMonteTypes.hpp
			Packer<std::array<uint64_t,2>> stepFinishPacker(FullMonteHW::StepFinish::bits_,stepFinish_serialized[0]); // Packer for serializing StepFinish input
			
			stepFinish_serialized[0][0] = 0x0;
			stepFinish_serialized[0][1] = 0x0;

			stepFinishPacker.reset(stepFinish_serialized[0]);
			stepFinishPacker & stepFinish;
			
			// serialized FullMonteHW::StepFinish
			afu.mmio_write64(3<<3, (uint64_t)stepFinish_serialized[0][0]);  
			afu.mmio_write64(3<<3, (uint64_t)stepFinish_serialized[0][1]);

			pkt_serialized[0][0] = 0x0;
			pkt_serialized[0][1] = 0x0;
			pkt_serialized[0][2] = 0x0;
			pkt_serialized[0][3] = 0x0;
			pkt_serialized[0][4] = 0x0;
			pkt_serialized[0][5] = 0x0;
		
			// Packer for serializing the Packet struct
			Packer<std::array<uint64_t,6>> packetPackerIII(FullMonteHW::Packet::bits_,pkt_serialized[0]);

			packetPackerIII.reset(pkt_serialized[0]);
			packetPackerIII & pkt_HW;
			
			// serialized FullMonteHW::Packet																														
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][0]); 
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][1]); 
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][2]); 		 		
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][3]); 				
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][4]);
			afu.mmio_write64(0, (uint64_t)pkt_serialized[0][5]);
			
			// Signal HW to start the stepFinish module
			afu.mmio_write64(4<<3,0x0ULL);

			counter = 0;
			state=afu.mmio_read64(0);
			while(state != STEPFINISH_LIVE_STATE && state != STEPFINISH_DEAD_STATE)																																					
				{
					if (counter >100)	
					{																																																																		
						LOG_INFO << "  Waiting for HW state being 'STEPFINISH_LIVE_STATE' (4) or 'STEPFINISHDEAD_STATE' (5). Current state (state="  
							<< state << " looking for " << STEPFINISH_LIVE_STATE << " or "<< STEPFINISH_DEAD_STATE <<")" << endl;
					}														
					usleep(200);
					state=afu.mmio_read64(0);
					counter++;																													
				}
			
			if(state == STEPFINISH_LIVE_STATE)
			{
				// Unpacker for de-serializing the data into the Packet struct
				Unpacker<std::array<uint64_t,6>> LivePacketUnpacker(FullMonteHW::Packet::bits_, outputLivePacket[0]);

				outputLivePacket[0][0] = afu.mmio_read64(1<<3);
				outputLivePacket[0][1] = afu.mmio_read64(1<<3);
				outputLivePacket[0][2] = afu.mmio_read64(1<<3);
				outputLivePacket[0][3] = afu.mmio_read64(1<<3);
				outputLivePacket[0][4] = afu.mmio_read64(1<<3);
				outputLivePacket[0][5] = afu.mmio_read64(1<<3);

				LivePacketUnpacker & stepFinishPacket;
			}
			else if (state == STEPFINISH_DEAD_STATE)
			{
				// Unpacker for de-serializing the data into the Weight type
				Unpacker<std::array<uint64_t,1>> DeadPacketUnpacker(FullMonteHW::Weight::bits_, outputDeadPacket[0]);

				outputDeadPacket[0][0] = afu.mmio_read64(1<<3);

				DeadPacketUnpacker & weightRemaining;
			}

			// Unpacker for de-serializing the data into the AbsorptionInfo struct
			Unpacker<std::array<uint64_t,2>> absorptionInfoUnpacker(FullMonteHW::AbsorptionInfo::bits, outputAbsorptionInfo[0]);

			outputAbsorptionInfo[0][0] = afu.mmio_read64(1<<3); 
			outputAbsorptionInfo[0][1] = afu.mmio_read64(1<<3);
			
			// De-serialization of HW StepFinish results back to the software type FullMonteHW::AbsorptionInfo
			absorptionInfoUnpacker & absorptionInfo;
		}

////////////////////////////////////////// Absorption and Roulette //////////////////////////////////////////////////////////////////////////////

        // tie(pkt.w,dw) = absorb(pkt,currMat,currTetra);
		float w0 = pkt.w;
		float dw = w0 * currMat.absfrac;
		pkt.w = w0-dw;
        
		// Termination logic
        TerminationResult term;

        // tie(term,dw)=terminationCheck(wmin_,prwin_,m_rng,pkt,currMat,currTetra);
		// do roulette
		float roulette_dw;
		w0=pkt.w;
		if (pkt.w < wmin_)
		{
			if (*m_rng->floatU01() < prwin_)
			{
				pkt.w /= prwin_;
				// If the packet wins roulette, increase the weight by the propability of winning roulette, and continue the simulation
				tie(term,roulette_dw) = make_pair(RouletteWin,pkt.w-w0);
				LOG_INFO << "Roulette won. Increasing packet weight" << endl;
			}
			else
			{
				// Packet dies and this packets simulation ends
				tie(term,roulette_dw) = make_pair(RouletteLose,-pkt.w);
				LOG_INFO << "Roulette lost. Terminating packet" << endl;
				killPacket = true;
			}
		}
		else
		{
			// Continue simulation
			tie(term,roulette_dw) = make_pair(Continue,0.0);
		}
//////////////////////////////////////////////// Scattering /////////////////////////////////////////////////////////////////////////////////////
		
#ifdef USE_SSE
		if(killPacket == false)
		{
			// scatter(pkt,currMat,currTetra))	
			__m128 spinmatrix = _mm_load_ps(deflection_azimuth.data());
			pkt.dir = pkt.dir.scatter(SSE::Vector<4>(SSE::Vector<4>(spinmatrix)));
		}
    	
    	_mm_store_ps(position,pkt.p);
		_mm_store_ps(stepLength,pkt.s);	
#else
        // scatter(pkt,currMat,currTetra)
		m128 spinmatrix = deflection_azimuth;
    	pkt.dir = pkt.dir.scatter(SSE::Vector<4>(spinmatrix));

		position[0] = pkt.p[0];
		position[1] = pkt.p[1];
		position[2] = pkt.p[2];
		position[3] = pkt.p[3]; // not used
#endif

////////////////////////////////////////////// Comparison HW vs. SW /////////////////////////////////////////////////////////////////////////
		
		if (m_fullPipeline || m_stepFinish)
		{
			if(m_verbose)
			{
				LOG_INFO << "StepFinish Output - Absorption TetraID" << endl
						 << "HW: " << absorptionInfo.tetID << " vs. SW: " << IDt << endl << endl
						 << "StepFinish Output - Absorbed weight:" << endl
						 << "HW: " << absorptionInfo.w.value() << " vs. SW: " << dw << endl
						 << "Error: " << dw - absorptionInfo.w.value() << endl << endl;
			}
					 
			Output_stepFinish_absorbed += (abs(dw - absorptionInfo.w.value()) - Output_stepFinish_absorbed) / stepFinish_absorbed_count;

			stepFinish_absorbed_count++;

			if(state == STEPFINISH_LIVE_STATE)
			{
				if(m_verbose)
				{
					LOG_INFO << "StepFinish Output - New photon direction (after scattering)" << endl
							 << "HW: " << stepFinishPacket.dir.d[0].value() << " <-> SW: " << pkt.dir.d.array()[0] << endl
							 << "HW: " << stepFinishPacket.dir.d[1].value() << " <-> SW: " << pkt.dir.d.array()[1] << endl
							 << "HW: " << stepFinishPacket.dir.d[2].value() << " <-> SW: " << pkt.dir.d.array()[2] << endl
							 << "Error: " << pkt.dir.d.array()[0] - stepFinishPacket.dir.d[0].value() << endl 
									<< pkt.dir.d.array()[1] - stepFinishPacket.dir.d[1].value() << endl 
									<< pkt.dir.d.array()[2] - stepFinishPacket.dir.d[2].value() << endl << endl
							 << "StepFinish Output - Photon position:" << endl
							 << "HW: " << stepFinishPacket.pos[0].value()*scale + shift[0] << " <-> SW: " << position[0] << endl
							 << "HW: " << stepFinishPacket.pos[1].value()*scale + shift[1] << " <-> SW: " << position[1] << endl
							 << "HW: " << stepFinishPacket.pos[2].value()*scale + shift[2] << " <-> SW: " << position[2] << endl
							 << "Error: " << position[0] - (stepFinishPacket.pos[0].value()*scale + shift[0]) << " " 
										  << position[1] - (stepFinishPacket.pos[1].value()*scale + shift[1]) << " " 
						 				<< position[1] - (stepFinishPacket.pos[1].value()*scale + shift[1]) << " " 
										  << position[1] - (stepFinishPacket.pos[1].value()*scale + shift[1]) << " " 
										  << position[2] - (stepFinishPacket.pos[2].value()*scale + shift[2]) << endl
							 << "StepFinish Output - Remaining weight of photon packet:" << endl
							 << "HW: " << stepFinishPacket.w.value() << " vs. SW: " << pkt.w << endl 
							 << "Error: " << pkt.w - stepFinishPacket.w.value() << endl << endl;
				}	 
				if (killPacket == true)
				{
					LOG_INFO << "SW estimates packet lost Roulette, but HW did not." << endl;
				}
				else
				{
					Output_stepFinish_dir[0] += (abs(pkt.dir.d.array()[0] - stepFinishPacket.dir.d[0].value()) - Output_stepFinish_dir[0]) / stepFinish_count;
					Output_stepFinish_dir[1] += (abs(pkt.dir.d.array()[1] - stepFinishPacket.dir.d[1].value()) - Output_stepFinish_dir[1]) / stepFinish_count;
					Output_stepFinish_dir[2] += (abs(pkt.dir.d.array()[2] - stepFinishPacket.dir.d[2].value()) - Output_stepFinish_dir[2]) / stepFinish_count;
					Output_stepFinish_pos[0] += (abs(position[0] - (stepFinishPacket.pos[0].value()*scale + shift[0])) - Output_stepFinish_pos[0]) / stepFinish_count;
					Output_stepFinish_pos[1] += (abs(position[1] - (stepFinishPacket.pos[1].value()*scale + shift[1])) - Output_stepFinish_pos[1]) / stepFinish_count;
					Output_stepFinish_pos[2] += (abs(position[2] - (stepFinishPacket.pos[2].value()*scale + shift[2])) - Output_stepFinish_pos[2]) / stepFinish_count;
					Output_stepFinish_weight += (abs(pkt.w - stepFinishPacket.w.value()) - Output_stepFinish_weight) / stepFinish_count;
					Output_stepFinish_step 	 += (abs(stepLength[1]/log(2) - stepFinishPacket.l.value()) - Output_stepFinish_step)  / stepFinish_count;

					Output_stepFinish_dir_error_sum[0] += abs(pkt.dir.d.array()[0] - stepFinishPacket.dir.d[0].value());
					Output_stepFinish_dir_error_sum[1] += abs(pkt.dir.d.array()[1] - stepFinishPacket.dir.d[1].value());
					Output_stepFinish_dir_error_sum[2] += abs(pkt.dir.d.array()[2] - stepFinishPacket.dir.d[2].value());
					Output_stepFinish_pos_error_sum[0] += abs(position[0] - (stepFinishPacket.pos[0].value()*scale + shift[0]));
					Output_stepFinish_pos_error_sum[1] += abs(position[1] - (stepFinishPacket.pos[1].value()*scale + shift[1]));
					Output_stepFinish_pos_error_sum[2] += abs(position[2] - (stepFinishPacket.pos[2].value()*scale + shift[2]));
					Output_stepFinish_weight_error_sum += abs(pkt.w - stepFinishPacket.w.value());
					Output_stepFinish_step_error_sum   += abs(stepLength[1]/log(2) - stepFinishPacket.l.value());

					Output_stepFinish_dir_sum[0] += abs(pkt.dir.d.array()[0]);
					Output_stepFinish_dir_sum[1] += abs(pkt.dir.d.array()[1]);
					Output_stepFinish_dir_sum[2] += abs(pkt.dir.d.array()[2]);
					Output_stepFinish_pos_sum[0] += abs(position[0]);
					Output_stepFinish_pos_sum[1] += abs(position[1]);
					Output_stepFinish_pos_sum[2] += abs(position[2]);
					Output_stepFinish_weight_sum += pkt.w;
					Output_stepFinish_step_sum 	 += stepLength[1]/log(2);

					stepFinish_count++;
				}
			}
			if(state == STEPFINISH_DEAD_STATE)
			{
				if(m_verbose)
				{
					LOG_INFO << "StepFinish Output - Remaining photon weight at moment of death" << endl
							 << "HW: " << weightRemaining.value() << " vs. SW: " << roulette_dw << endl 
							 << "Error: " << roulette_dw - weightRemaining.value() << endl << endl;
				}
				
				if (killPacket == true)
				{
					Output_stepFinish_deadweight += (abs(roulette_dw - weightRemaining.value()) - Output_stepFinish_deadweight) / stepFinish_dead_count;

					Output_stepFinish_deadweight_error_sum += abs(roulette_dw - weightRemaining.value());

					Output_stepFinish_deadweight_sum += abs(roulette_dw);
				}
				else
				{
					LOG_INFO << "HW estimates packet lost Roulette, but SW did not." << endl;
				}
				stepFinish_dead_count++;
			}
		}
		if (killPacket == true)
		{
			killPacket = false;
			break;
		}
#ifdef CHRONOSTREAM
			//CHRONO
			auto timeStream1 = chrono::high_resolution_clock::now();
#endif

#ifdef CHRONOSTREAM
			//chrono
			auto timeStream2 = chrono::high_resolution_clock::now();
			printf("chrono TOTAL::duration in ns : %ld \n", chrono::duration_cast<chrono::nanoseconds>(timeStream2 - timeStream1).count() );
#endif

		} // End of step loop: for(Nstep=0; Nstep < Nstep_max_; ++Nstep)

		// We should never get to this code
		// The photon makes more steps than expected and thus does not die during roullette
		if (Nstep >= Nstep_max_)
		{
			AutoStreamBuffer b(cerr);
			b << "Terminated due to unusual number of steps at tetra " << IDt
			  << " dimensionless step remaining=" << pkt.s[0] << " w=" << pkt.w << '\n';
			
			break;
    	}

	} // End of photon launch loop: for(unsigned i=0;i<Npkt_;i++)

	if(m_fullPipeline || m_intersection)
	{
		LOG_INFO << "Mean error values [mean(SW-HW)] of Intersection Input:" << endl
				<< "        Position: " << Input_intersection_pos[0] << ", " << Input_intersection_pos[1] << ", " << Input_intersection_pos[2] << endl
				<< "       Direction: " << Input_intersection_dir[0] << ", " << Input_intersection_dir[1] << ", " << Input_intersection_dir[2] << endl
				<< "               a: " << Input_intersection_a[0] << ", " << Input_intersection_a[1] << ", " << Input_intersection_a[2] << endl
				<< "               b: " << Input_intersection_b[0] << ", " << Input_intersection_b[1] << ", " << Input_intersection_b[2] << endl
				<< "          Weight: " << Input_intersection_weight << endl
				<< "     Step length: " << Input_intersection_step << endl << endl;
		LOG_INFO << "Mean error values [mean(SW-HW)] of Intersection Output:" << endl
				<< "          Height: " << Output_intersection_height << endl
				<< "      cos(theta): " << Output_intersection_costheta << endl << endl;
	}
	if(m_fullPipeline || m_boundary)
	{
		LOG_INFO << "Mean error values [mean(SW-HW)] of Boundary Input:" << endl
				<< "          Height: " << Input_boundary_height << endl
				<< "      cos(theta): " << Input_boundary_costheta << endl << endl;
		LOG_INFO << "Mean error values [mean(SW-HW)] of Boundary Output:" << endl
				<< "     Step length: " << Output_boundary_step << endl
				<< "          Weight: " << Output_boundary_weight << endl
				<< "      cos(theta): " << Output_boundary_costheta << endl
				<< "        Position: " << Output_boundary_pos[0] << ", " << Output_boundary_pos[1] << ", " << Output_boundary_pos[2] << endl
				<< "       Direction: " << Output_boundary_dir[0] << ", " << Output_boundary_dir[1] << ", " << Output_boundary_dir[2] << endl << endl;
	}
	if(m_fullPipeline || m_reflectRefract)
	{
		LOG_INFO << "Mean error values [mean(SW-HW)] of ReflectRefract Input:" << endl
				<< "  Surface normal: " << Input_reflectRefract_normal[0] << ", " << Input_reflectRefract_normal[1] << ", " << Input_reflectRefract_normal[2] << endl
				<< "  Random Fresnel: " << Input_reflectRefract_rnd << endl
				<< "       Direction: " << Input_reflectRefract_dir[0] << ", " << Input_reflectRefract_dir[1] << ", " << Input_reflectRefract_dir[2] << endl
				<< "      cos(theta): " << Input_reflectRefract_costheta << endl << endl;
		LOG_INFO << "Mean error values [mean(SW-HW)] of ReflectRefract Output:" << endl
				<< "       Direction: " << Output_reflectRefract_dir[0] << ", " << Output_reflectRefract_dir[1] << ", " << Output_reflectRefract_dir[2] << endl
				<< "               a: " << Output_reflectRefract_a[0] << ", " << Output_reflectRefract_a[1] << ", " << Output_reflectRefract_a[2] << endl
				<< "               b: " << Output_reflectRefract_b[0] << ", " << Output_reflectRefract_b[1] << ", " << Output_reflectRefract_b[2] << endl << endl;
	}
	if(m_fullPipeline || m_stepFinish)
	{
		LOG_INFO << "Mean error values [mean(SW-HW)] of StepFinish Input:" << endl
				<< "       Random hg: " << Input_stepFinish_hg[0] << ", " << Input_stepFinish_hg[1] << endl
				<< "       Random az: " << Input_stepFinish_az[0] << ", " << Input_stepFinish_az[1] << endl << endl;
		LOG_INFO << "Mean error values [mean(SW-HW)] of StepFinish Output:" << endl
				<< " Absorbed weight: " << Output_stepFinish_absorbed << endl
				<< "     Step length: " << Output_stepFinish_step << endl
				<< "        Position: " << Output_stepFinish_pos[0] << ", " << Output_stepFinish_pos[1] << ", " << Output_stepFinish_pos[2] << endl
				<< "       Direction: " << Output_stepFinish_dir[0] << ", " << Output_stepFinish_dir[1] << ", " << Output_stepFinish_dir[2] << endl
				<< "          Weight: " << Output_stepFinish_weight << endl
				<< "    Death weight: " << Output_stepFinish_deadweight << endl << endl;
	}
	
	if(m_fullPipeline || m_intersection)
	{
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of Intersection Input:" << endl
				<< "        Position: " << Input_intersection_pos_error_sum[0] / Input_intersection_pos_sum[0] << ", " << Input_intersection_pos_error_sum[1] / Input_intersection_pos_sum[1] << ", " << Input_intersection_pos_error_sum[2] / Input_intersection_pos_sum[2] << endl
				<< "       Direction: " << Input_intersection_dir_error_sum[0] / Input_intersection_dir_sum[0] << ", " << Input_intersection_dir_error_sum[1] / Input_intersection_dir_sum[1] << ", " << Input_intersection_dir_error_sum[2] / Input_intersection_dir_sum[2] << endl
				<< "               a: " << Input_intersection_a_error_sum[0] / Input_intersection_a_sum[0] << ", " << Input_intersection_a_error_sum[1] / Input_intersection_a_sum[1] << ", " << Input_intersection_a_error_sum[2] / Input_intersection_a_sum[2] << endl
				<< "               b: " << Input_intersection_b_error_sum[0] / Input_intersection_b_sum[0] << ", " << Input_intersection_b_error_sum[1] / Input_intersection_b_sum[1] << ", " << Input_intersection_b_error_sum[2] / Input_intersection_b_sum[2] << endl
				<< "          Weight: " << Input_intersection_weight_error_sum / Input_intersection_weight_sum << endl
				<< "     Step length: " << Input_intersection_step_error_sum / Input_intersection_step_sum << endl << endl;
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of Intersection Output:" << endl
				<< "          Height: " << Output_intersection_height_error_sum / Output_intersection_height_sum << endl
				<< "      cos(theta): " << Output_intersection_costheta_error_sum / Output_intersection_costheta_sum << endl << endl;
	}
	if(m_fullPipeline || m_boundary)
	{
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of Boundary Input:" << endl
				<< "          Height: " << Input_boundary_height_error_sum / Input_boundary_height_sum << endl
				<< "      cos(theta): " << Input_boundary_costheta_error_sum / Input_boundary_costheta_sum << endl << endl;
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of Boundary Output:" << endl
				<< "     Step length: " << Output_boundary_step_error_sum / Output_boundary_step_sum << endl
				<< "          Weight: " << Output_boundary_weight_error_sum / Output_boundary_weight_sum << endl
				<< "      cos(theta): " << Output_boundary_costheta_error_sum / Output_boundary_costheta_sum << endl
				<< "        Position: " << Output_boundary_pos_error_sum[0] / Output_boundary_pos_sum[0] << ", " << Output_boundary_pos_error_sum[1] / Output_boundary_pos_sum[1] << ", " << Output_boundary_pos_error_sum[2] / Output_boundary_pos_sum[2] << endl
				<< "       Direction: " << Output_boundary_dir_error_sum[0] / Output_boundary_dir_sum[0] << ", " << Output_boundary_dir_error_sum[1] / Output_boundary_dir_sum[1] << ", " << Output_boundary_dir_error_sum[2] / Output_boundary_dir_sum[2] << endl << endl;
	}
	if(m_fullPipeline || m_reflectRefract)
	{
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of ReflectRefract Input:" << endl
				<< "  Surface normal: " << Input_reflectRefract_normal_error_sum[0] / Output_reflectRefract_dir_sum[0] << ", " << Input_reflectRefract_normal_error_sum[1] / Output_reflectRefract_dir_sum[1] << ", " << Input_reflectRefract_normal_error_sum[2] / Output_reflectRefract_dir_sum[2]<< endl
				<< "  Random Fresnel: " << Input_reflectRefract_rnd_error_sum / Input_reflectRefract_rnd_sum << endl
				<< "       Direction: " << Input_reflectRefract_dir_error_sum[0] / Input_reflectRefract_dir_sum[0] << ", " << Input_reflectRefract_dir_error_sum[1] / Input_reflectRefract_dir_sum[1] << ", " << Input_reflectRefract_dir_error_sum[2] / Input_reflectRefract_dir_sum[2] << endl
				<< "      cos(theta): " << Input_reflectRefract_costheta_error_sum / Input_reflectRefract_costheta_sum << endl << endl;
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of ReflectRefract Output:" << endl
				<< "       Direction: " << Output_reflectRefract_dir_error_sum[0] / Output_reflectRefract_dir_sum[0] << ", " << Output_reflectRefract_dir_error_sum[1] / Output_reflectRefract_dir_sum[1] << ", " << Output_reflectRefract_dir_error_sum[2] / Output_reflectRefract_dir_sum[2] << endl
				<< "               a: " << Output_reflectRefract_a_error_sum[0] / Output_reflectRefract_a_sum[0] << ", " << Output_reflectRefract_a_error_sum[1] / Output_reflectRefract_a_sum[1] << ", " << Output_reflectRefract_a_error_sum[2] / Output_reflectRefract_a_sum[2] << endl
				<< "               b: " << Output_reflectRefract_b_error_sum[0] / Output_reflectRefract_b_sum[0] << ", " << Output_reflectRefract_b_error_sum[1] / Output_reflectRefract_b_sum[1] << ", " << Output_reflectRefract_b_error_sum[2] / Output_reflectRefract_b_sum[2] << endl << endl;
	}
	if(m_fullPipeline || m_stepFinish)
	{
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of StepFinish Input:" << endl
				<< "       Random hg: " << Input_stepFinish_hg_error_sum[0] / Input_stepFinish_hg_sum[0] << ", " << Input_stepFinish_hg_error_sum[1] / Input_stepFinish_hg_sum[1] << endl
				<< "       Random az: " << Input_stepFinish_az_error_sum[0] / Input_stepFinish_az_sum[0] << ", " << Input_stepFinish_az_error_sum[1] / Input_stepFinish_az_sum[1] << endl << endl;
		LOG_INFO << "Error ratio [sum(SW-HW)/sum(SW)] of StepFinish Output:" << endl
				<< " Absorbed weight: " << Output_stepFinish_absorbed_error_sum / Output_stepFinish_absorbed_sum << endl
				<< "     Step length: " << Output_stepFinish_step_error_sum / Output_stepFinish_step_sum  << endl
				<< "        Position: " << Output_stepFinish_pos_error_sum[0] / Output_stepFinish_pos_sum[0] << ", " << Output_stepFinish_pos_error_sum[1] / Output_stepFinish_pos_sum[1] << ", " << Output_stepFinish_pos_error_sum[2] / Output_stepFinish_pos_sum[2] << endl
				<< "       Direction: " << Output_stepFinish_dir_error_sum[0] / Output_stepFinish_dir_sum[0] << ", " << Output_stepFinish_dir_error_sum[1] / Output_stepFinish_dir_sum[1] << ", " << Output_stepFinish_dir_error_sum[2] / Output_stepFinish_dir_sum[2] << endl
				<< "          Weight: " << Output_stepFinish_weight_error_sum / Output_stepFinish_weight_sum << endl
				<< "    Death weight: " << Output_stepFinish_deadweight_error_sum / Output_stepFinish_deadweight_sum << endl << endl;
	}

} // End of function call

template<class RNGT>void TetraMCP8DebugKernel<RNGT>::parentResults()
{
	// Needs to be populated later in time
	// Currently is a dummy function becasue we need it in order for SWIG to generate the constructor for this class
}


#endif /* KERNELS_P8_TETRAMCP8DEBUGKERNEL_HPP_ */


