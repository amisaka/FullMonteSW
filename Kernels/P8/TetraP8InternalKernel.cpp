/*
 * TetraP8InternalKernel.cpp
 *
 *  Created on: Jan 22, 2019
 *      Author: fynns
 */

#include "TetraP8InternalKernel.hpp"



void TetraP8InternalKernel::parentPrepare()
{
	TetraMCP8Kernel<RNG_SFMT_AVX>::parentPrepare();
}

void TetraP8InternalKernel::parentStart()
{
	TetraMCP8Kernel<RNG_SFMT_AVX>::parentStart();
}
