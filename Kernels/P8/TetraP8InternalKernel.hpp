/*
 * TetraMCP8Kernel.hpp
 *
 *  Created on: Jan 22, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_TETRAP8INTERNALKERNEL_HPP_
#define KERNELS_P8_TETRAP8INTERNALKERNEL_HPP_

#include "TetraMCP8Kernel.hpp"


class TetraP8InternalKernel : public TetraMCP8Kernel<RNG_SFMT_AVX>
{
public:
	TetraP8InternalKernel(){}

	~TetraP8InternalKernel(){}

	typedef RNG_SFMT_AVX RNG;

	virtual void parentPrepare() override;

	virtual void parentStart() override;
};



#endif /* KERNELS_P8_TETRAP8INTERNALKERNEL_HPP_ */
