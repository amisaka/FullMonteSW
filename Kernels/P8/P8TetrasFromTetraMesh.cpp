/*
 * P8TetrasFromTetraMesh.cpp
 *
 *  Created on: Mar 7, 2019
 *      Author: fynns
 */

#include "P8TetrasFromTetraMesh.hpp"

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <functional>

P8TetrasFromTetraMesh::P8TetrasFromTetraMesh()
{

}

P8TetrasFromTetraMesh::~P8TetrasFromTetraMesh()
{

}

std::pair<std::array<double,3>,double> P8TetrasFromTetraMesh::mesh(const TetraMesh* M)
{
	m_mesh=M;
	
	/** The code below is for HW scaling purposes and is not required by the software simulator
	 * Problem: The Hardware currently only supports position ranges from -8 to +8.
	 * Meshes can have significantly larger coordinate values.
	 * 
	 * Solution: We need to shift the x,y,z coordinates so that they revolve around the origin point (0,0,0)
	 * in order to maintain as much nuber accuracy as possible when we scale the coordinates to the 
	 * range of -8 to +8
	 * 
	 * Steps: 
	 * 1. Get the max and min values for x,y,z and calculate the translation vector we need to let everything revolve around the orgigin point (0,0,0)
	 * 2. Get the new max values of the x,y,z after the translation operation and determin the largest value. 
	 * (It doesn't matter if we use the max or the min values becasue both should be the same if we are looking at the absolute value)
	 * 3. Check if the largest new max value is greater or lesser than 8: 
	 *       if >8  -> calculate scaling parameter
	 *       if <=8 -> set scaling parameter to 1 
	 */
	// Get an iterator object to go thorugh all points of the mesh
	boost::iterator_range<TetraMesh::PointIterator> iter = m_mesh->pointRange();
	double x_max = 0, x_min = 0, x_maxNew  = 0;
	double y_max = 0, y_min = 0, y_maxNew = 0;
	double z_max = 0, z_min = 0, z_maxNew = 0;
	double max_coordinateValue = 0;

	boost::iterator_range<TetraMesh::DirectedFaceIterator> c_iter = m_mesh->directedFaces();
	double c_max = 0, c_min = 0;
	double max_constantValue = 0;

	double maxValue = 0;
	
	for (auto i : iter) // Do this loop for all points in the mesh
	{
		
		Point<3,double> Ps = get(coords,*M,i);
		// Determine maximum value of x
		if (x_max < Ps[0])
			x_max = Ps[0];
		// Determine minimum vlaue of x
		if (x_min > Ps[0])
			x_min = Ps[0];
		// Determine maximum value of y
		if (y_max < Ps[1])
			y_max = Ps[1];
		// Determine minimum value of y
		if (y_min > Ps[1])
			y_min = Ps[1];
		// Determine maximum value of z
		if (z_max < Ps[2])
			z_max = Ps[2];		
		// Determine minimum value of z
		if (z_min > Ps[2])
			z_min = Ps[2];
		
	}

	// Caclculate the translation vector for x,y,z
	m_translation_vector[0] = (x_max + x_min) / 2;
	m_translation_vector[1] = (y_max + y_min) / 2;
	m_translation_vector[2] = (z_max + z_min) / 2;

	// Determine the new maximum values for the shifted x,y,z coordinates
	x_maxNew = x_max - m_translation_vector[0];
	y_maxNew = y_max - m_translation_vector[1];
	z_maxNew = z_max - m_translation_vector[2];

	// Determine the new overall maximum value of x,y,z coordinates
	if (x_maxNew <= y_maxNew)
		if(y_maxNew <= z_maxNew)
		{
			max_coordinateValue = z_maxNew;
		}
		else
		{
			max_coordinateValue = y_maxNew;
		}
	else
	{
		if(x_maxNew <= z_maxNew)
		{
			max_coordinateValue = z_maxNew;
		}
		else
		{
			max_coordinateValue = x_maxNew;
		}
	}
	
	for (auto i : c_iter) // Do this loop for all points in the mesh
	{
		array<double,3> n;
		float c;

		// This calculates the normal and the constant c for the given tetra face (quite non descriptive get method...)
		tie(n,c) = get(face_plane,*M,i);

		// Determine maximum value of c
		if (c_max < c)
			c_max = c;
		// Determine minimum vlaue of c
		if (c_min > c)
			c_min = c;
	}
	if (c_max < abs(c_min))
	{
		max_constantValue = abs(c_min);
	}
	else
	{
		max_constantValue = c_max;
	}
	
	if (max_constantValue < max_coordinateValue)
	{
		maxValue = max_coordinateValue;
	}
	else
	{
		maxValue = max_constantValue;
	}
	

	// The Hardware only supports positions in a mesh with a range of -8 to +8
	// We need to check if we actually need to scale the mesh after the translation process.
	if(maxValue <= 8.0)
	{
		m_scaling_parameter = 1;
	}
	else
	{
		m_scaling_parameter = maxValue / 8;
	}
	
	LOG_INFO << "Shifting and Scaling of input mesh: " << endl
			 << "Vector to let the mesh be centered around the origin point (0,0,0):" << endl
			 << "x: " << m_translation_vector[0] << endl
			 << "y: " << m_translation_vector[1] << endl
			 << "z: " << m_translation_vector[2] << endl
			 << "Scaling factor to keep the (0,0,0)-mesh in the range of +/-8 in x,y,z:" << endl 
			 << "scale: " << m_scaling_parameter << endl;

	return std::make_pair(m_translation_vector, m_scaling_parameter);
}

void P8TetrasFromTetraMesh::update(const MaterialSet* Mat)
{
	// build the kernel tetras (halfspace representation) from the provided mesh
	makeKernelTetras(Mat);

	checkKernelFaces();
}


/** Validates the orientation of faces within the kernel tetra structure.
 *
 */

bool P8TetrasFromTetraMesh::checkKernelFaces() const
{
	bool status_ok=true;
	unsigned int shift = (1 << 26);
	cout << "INFO: Checking faces on " << m_tetras.size() << " tetras" << endl;

	// iterate over all tetras
	for(unsigned t=1; t < m_tetras.size(); ++t)
	{
		FullMonteHW::TetraDef T = m_tetras[t];

		// face reciprocity: each face points to either zero tetra or a tetra that points back to this one
		for(unsigned f=0;f<4;++f)
		{
			FullMonteHW::TetraDef adjTetra;

			if ((unsigned) T.adj[f].tetID >= m_tetras.size())
			{
				unsigned tetraID = (unsigned) T.adj[f].tetID & (~shift);
				if (tetraID != (unsigned) T.idfds[f])
				{
					status_ok=false;
					cout << "  Tetra " << t << " face " << f << " connects to a tetra out of array range" << endl;
				}
				else
					adjTetra = m_tetras[0];
			}
			else
				adjTetra = m_tetras[(unsigned) T.adj[f].tetID];
			
			unsigned tetID[4] = {(unsigned) adjTetra.adj[0].tetID, (unsigned) adjTetra.adj[1].tetID, (unsigned) adjTetra.adj[2].tetID, (unsigned) adjTetra.adj[3].tetID};
			unsigned tetra_conversion = (unsigned) T.idfds[f] | shift;
			if (T.adj[f].tetID != tetra_conversion  && boost::count_if(tetID, [t](unsigned i){ return i==t; }) != 1)
			{
				cout << "  Tetra " << t << " face " << f << " connects to tetra " << T.adj[f].tetID << " but that tetra connects to ";
				for(unsigned i=0;i<4;++i)
					cout << m_tetras[t].adj[i].tetID << ' ';
				cout << endl;

				status_ok = false;
			}
		}
	}

	return status_ok;
}

	unsigned int P8TetrasFromTetraMesh::face_numbers() const
	{
		return get(num_faces,*m_mesh);
	}

	unsigned int P8TetrasFromTetraMesh::getInterfaceID(unsigned matID1, unsigned matID2)
	{	
		return m_interfaceMap.find(make_pair(matID1,matID2))->second;	
	}


/** Converts the TetraMesh representation to the packed data structures used by the kernel.
 *
 * 
 * Produces: m_tetras
 *
 */
FullMonteHW::TetraDef P8TetrasFromTetraMesh::convert(const TetraMesh& M,TetraMesh::TetraDescriptor t, const MaterialSet* Mat, const double scale, const std::array<double,3> translation_vector, const std::unordered_map<std::pair<unsigned,unsigned>, unsigned, boost::hash<std::pair<unsigned,unsigned>>> interfaceMap)
{
	FullMonteHW::TetraDef T;
	::Material* m_current; // current material properties of the tetra. This is required to populate the ifcID field for the HW
	::Material* m_adjacent; // material properties of one of the adjacent tetras. This is for comparing if we have a refractive interface or not
	
	//Get all directed faces F of the tetra which is described by the TetraDescriptor t from the tetra mesh M
	const auto DirectedFaces = get(directed_faces,M,t);
	const auto UndirectedFaces = get(faces,M,t);
	
	// Get the material ID for the tetra
	unsigned matID = get(region,M,t);
	// Get the material properties of the current tetra
	m_current = Mat->get(matID);
	// get all tetra IDs and face IDs linking to this tetra
	const auto L = M.tetraFaceLinks()->get(t.value());
	
	T.tetID = t.value();
	// LOG_INFO << "TetID: " << T.tetID << " or " << t.value() << endl;

	for(unsigned i=0;i<4;++i)
	{
		array<double,3> n;
		float c;

		// This calculates the normal and the constant c for the given tetra face (quite non descriptive get method...)
		tie(n,c) = get(face_plane_scaled,M,DirectedFaces[i],scale,translation_vector);

		// Fill the HW Tetra type with the values of the SW Tetra
		unsigned adjMatID=get(region,M,L[i].tetraID);
		// Get the material properties of one of the adjacent tetras
		m_adjacent = Mat->get(adjMatID);

		// This checks if the refractive indices between the current tetra and the adjacent tetras differ
		if (m_adjacent->refractiveIndex() != m_current->refractiveIndex()) // if the refractive index differs, we will create a hash with the materialID combinations which will then be written to ifcID
		{
			const auto p = interfaceMap.find(make_pair(matID,adjMatID));
			T.adj[i].ifcID = p->second;
		}
		else // no refractive index difference -> set ifcID to 0
		{
			T.adj[i].ifcID = 0;
		}
		// Check if one of the adjacent tetras is actually the exterior of the mesh
		if ( L[i].tetraID.value() == 0)
		{
			unsigned int shift = 1 << 26;
			// in HW, we aren't assigning 0 to exterior tetras, but rather taking the faceID value and setting the MSB to 1
			// This way, we do not have to lookup the faceID when exiting the mesh but just need to ignore the MSB to get
			// the correct face ID from  which the photon packet exited the mesh
			T.adj[i].tetID 	= (UndirectedFaces[i].value() | shift); 
		}
		else // assign the SW tetraID to the HW tetraID since we are dealing with tetras inside the mesh 
		{
			T.adj[i].tetID = L[i].tetraID.value();
		}
		
		T.face[i].n = FullMonteHW::UVect3D_18(n);
		T.face[i].c = FixedPoint<int32_t,4,14>(c);
		T.idfds[i] = UndirectedFaces[i].value();
	}

	T.matID = matID;

	return T;
}

void P8TetrasFromTetraMesh::makeKernelTetras(const MaterialSet* Mat)
{
	unsigned material_number = Mat->size();
	for (unsigned i=0; i<material_number; i++)
	{
		for(unsigned j=0; j<material_number; j++)
		{
			m_interfaceMap.insert(make_pair(make_pair(i,j),m_interfaceMap.size()+1));
		}
	}

	m_tetras.resize(get(num_tetras,*m_mesh));
	static double scale = m_scaling_parameter;
	static std::array<double,3> translationVector = m_translation_vector;
	static std::unordered_map<std::pair<unsigned,unsigned>, unsigned, boost::hash<std::pair<unsigned,unsigned>>> interfaceMap = m_interfaceMap;
	boost::transform(
		m_mesh->tetras(),
		m_tetras.begin(),
		std::bind(convert,*m_mesh,std::placeholders::_1,Mat, scale, translationVector, interfaceMap));
	// for (unsigned i=0; i<m_tetras.size();i++)// : m_mesh->tetras())
	// {
	// 	m_tetras.at(i) = convert(*m_mesh,m_mesh->tetras()[i],Mat); 
	// }
}

const std::vector<FullMonteHW::TetraDef>& P8TetrasFromTetraMesh::tetras() const
{
	return m_tetras;
}

