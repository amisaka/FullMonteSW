/*
 * TetrasFromLayers.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#include "P8TetrasFromLayered.hpp"
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>

#include <limits>

using namespace std;
/**
 * @brief Construct a new TetrasFromLayered:: TetrasFromLayered object
 * 
 */
P8TetrasFromLayered::P8TetrasFromLayered()
{

}

P8TetrasFromLayered::~P8TetrasFromLayered()
{

}

void P8TetrasFromLayered::layers(const Layered *L)
{
	m_layers=L;
}

void P8TetrasFromLayered::update(const MaterialSet* Mat)
{
	unsigned Nl = m_layers->layerCount();
	m_tetras.clear();
	m_tetras.resize(Nl+2);

	// layer edges.
	// layer i (1..N) spans (edges[i-1], edges[i]) and contains material i
	// material[0] is top exterior, material[N+1] is bottom exterior

	// current limitation: top & bottom exterior must be same material

	vector<float> edges(Nl+1);

	edges[0] = 0.0f;
	for(unsigned i=1;i<=Nl;++i)
		edges[i] = edges[i-1] + m_layers->layer(i)->thickness();

	/**
	 * FullMonteHW TetraDef has a different definition of its Tetra components
	 * adj[0-3].tetID -> is only one tetraID value; not a vector of values as opposed to the software
	 * adj[0-3].ifcID -> is not used in SW; it indicates if the material IDs change between the tetra and its neighbors
	 * face[0-3].c -> is only one value and not a vector of 4 values for each face of the tetra
	 * face[0-3].n -> is a vector to describe the normal vector of the face
	 * idfds[0-3] -> similar to SW version IDfds
	 * matID -> similar to SW version
	 */
	array<double,3> n_zero = {0, 0 ,0};
	array<double,3> n_plus1 = {0, 0 ,1.0f};
	array<double,3> n_minus1 = {0, 0 ,-1.0f};
	::Material* m_current; // current material properties of the tetra. This is required to populate the ifcID field for the HW
	::Material* m_adjacent; // material properties of one of the adjacent tetras. This is for comparing if we have a refractive interface or not
	// top exterior tetra
	m_tetras[0].matID = 0;
	for(unsigned i=1; i<4; ++i)
	{
		m_tetras[0].adj[i].tetID = 0U;
		m_tetras[0].adj[i].ifcID = 1U;
		m_tetras[0].idfds[i] = 0U;

		m_tetras[0].face[i].c = 0.0f;
		m_tetras[0].face[i].n = FullMonteHW::UVect3D_18(n_zero);
	}
	for(unsigned i=1;i<=Nl;++i)
	{
		m_tetras[i].matID = i;
		
		m_tetras[i].adj[0].tetID = i-1;
		m_tetras[i].adj[1].tetID = i+1;
		m_tetras[i].adj[2].tetID = 0U; 
		m_tetras[i].adj[3].tetID = 0U;
		
		// Checking if the refractive index is different in the materials  which the adjacent tetras are assigned to
		m_current = Mat->get(i);
		m_adjacent = Mat->get(i-1);
		if (m_current->refractiveIndex() != m_adjacent->refractiveIndex()) 
		{ // True -> different refractive interfaces
			const auto p = m_interfaceMap.insert(make_pair(make_pair(i,i-1),m_interfaceMap.size()+1));
			m_tetras[i].adj[0].ifcID = p.second;
		}
		else // same refractive interfaces
			m_tetras[i].adj[0].ifcID = 0;
		
		m_adjacent = Mat->get(i+1);
		if (m_current->refractiveIndex() != m_adjacent->refractiveIndex())
		{ // True -> different refractive interfaces
			const auto p = m_interfaceMap.insert(make_pair(make_pair(i,i+1),m_interfaceMap.size()+1));
			m_tetras[i].adj[1].ifcID = p.second;
		}
		else // same refractive interfaces
			m_tetras[i].adj[1].ifcID = 0;

		m_adjacent = Mat->get(0);
		if (m_current->refractiveIndex() != m_adjacent->refractiveIndex())
		{ // True -> different refractive interfaces
			const auto p = m_interfaceMap.insert(make_pair(make_pair(i,0),m_interfaceMap.size()+1));
			m_tetras[i].adj[2].ifcID = p.second;
			m_tetras[i].adj[3].ifcID = p.second;
		}
		else // same refractive interfaces
		{
			m_tetras[i].adj[2].ifcID = 0;
			m_tetras[i].adj[3].ifcID = 0;
		} 

		m_tetras[i].idfds[0] = 2*i;
		m_tetras[i].idfds[1] = 2*i+3;
		m_tetras[i].idfds[2] = 0U;
		m_tetras[i].idfds[3] = 0U;
		
		m_tetras[i].face[0].n = FullMonteHW::UVect3D_18(n_plus1);
		m_tetras[i].face[1].n = FullMonteHW::UVect3D_18(n_minus1);
		m_tetras[i].face[2].n = FullMonteHW::UVect3D_18(n_zero);
		m_tetras[i].face[3].n = FullMonteHW::UVect3D_18(n_zero);
		
		m_tetras[i].face[0].c = edges[i-1];
		m_tetras[i].face[1].c = -edges[i];
		m_tetras[i].face[2].c = -numeric_limits<float>::infinity();
		m_tetras[i].face[3].c = -numeric_limits<float>::infinity();

		cout << "Tetra [" << i << "] ranges (" << edges[i-1] << ',' << edges[i] << ")" << endl;
	}

	// bottom exterior tetra
	m_tetras[Nl].adj[1].tetID = 0U;		// bottom tetra links down to tetra 0 (exterior) -- ideally would be a separate tetra
	m_tetras[Nl+1] = m_tetras[0];

	for(unsigned i=0;i<m_tetras.size();++i)
	{
		cout << "Tetra [" << i << "] links to " << m_tetras[i].adj[0].tetID << " and " << m_tetras[i].adj[1].tetID << endl;
	}
}

const vector<FullMonteHW::TetraDef>& P8TetrasFromLayered::tetras() const
{
	return m_tetras;
}
