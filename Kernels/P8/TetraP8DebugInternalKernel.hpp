/*
 * TetraP8DebugInternalKernel.hpp
 *
 *  Created on: Jan 22, 2019
 *      Author: fynns
 */

#ifndef KERNELS_P8_TETRAP8DEBUGINTERNALKERNEL_HPP_
#define KERNELS_P8_TETRAP8DEBUGINTERNALKERNEL_HPP_

#include "TetraMCP8DebugKernel.hpp"


class TetraP8DebugInternalKernel : public TetraMCP8DebugKernel<RNG_SFMT_AVX>
{
public:
	TetraP8DebugInternalKernel(){}

	~TetraP8DebugInternalKernel(){}

	typedef RNG_SFMT_AVX RNG;

	virtual void parentPrepare() override;

	virtual void parentStart() override;
};



#endif /* KERNELS_P8_TETRAP8DEBUGINTERNALKERNEL_HPP_ */
