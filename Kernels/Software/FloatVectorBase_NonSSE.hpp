/*
 * FloatVectorBase_NonSSE.hpp
 *
 *  Created on: Oct 30, 2018
 *      Author: fynns
 */

#ifndef KERNELS_SOFTWARE_FLOATVECTORBASE_NONSSE_HPP_
#define KERNELS_SOFTWARE_FLOATVECTORBASE_NONSSE_HPP_

#include <array>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/math/constants/constants.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <iostream>
//#include <fstream>

union long2int{
    long long l;
    unsigned int i2[2];
};

inline std::array<long long, 4> _NonSSE_mm256_srli_epi32(std::array<long long, 4> x,unsigned N)
{
	long2int h0;
    h0.l = x[2];
    long2int h1;
    h1.l = x[3];
	
    long2int l0;
    l0.l = x[0];
    long2int l1;
    l1.l = x[1];
    //std::cout << "Long longs: " << h0.l << " " << h1.l << " " << l0.l << " " << l1.l << std::endl;
    //std::cout << "2 ints: " << h0.i2[0] << "," << h0.i2[1] << " " << h1.i2[0] << "," << h1.i2[1] << " " << l0.i2[0] << "," << l0.i2[1] << " " << l1.i2[0] << "," << l1.i2[1] << std::endl;
    for(int i=0; i<2; i++)
    {    
        if (N > 31)
        {
            h0.i2[i] = 0;
            h1.i2[i] = 0;
            l0.i2[i] = 0;
            l1.i2[i] = 0;
        }
        else
        {
            h0.i2[i] = h0.i2[i] >> N;
            h1.i2[i] = h1.i2[i] >> N;

            l0.i2[i] = l0.i2[i] >> N;
            l1.i2[i] = l1.i2[i] >> N;
        }
    }
    std::array<long long, 4> result = {l0.l, l1.l, h0.l, h1.l};

    return result;
}

struct FloatVectorBase_NonSSE
{
private:
    static constexpr uint32_t  	exp_float24 = 0x40000000;		// exponent for float in [2,4)
    static constexpr uint32_t  	exp_float12	= 0x3f800000;		// exponent for float in [1,2)

    static constexpr uint32_t	float_signmask	= 0x80000000;	// 1b  sign
    static constexpr uint32_t	float_expmask	= 0x7f800000;	// 8b  excess-127 exponent
    static constexpr uint32_t	float_mantmask	= 0x007fffff;	// 23b mantissa (implicit leading 1)

public:

    /// Assign a scalar to all elements of the vector
    static inline std::array<float, 8> broadcast(float x)       { return {x, x, x, x, x, x, x, x}; }
    static inline std::array<float, 8> broadcastBits(uint32_t u)
    { 
        //std::array<unsigned int, 8> all_u = {u, u, u, u, u, u, u, u};
        float float_cast = 0;//*reinterpret_cast<float*>(&u);
        std::memcpy(&float_cast, &u, sizeof(float_cast));
        std::array<float, 8> result = {float_cast,float_cast,float_cast,float_cast,float_cast,float_cast,float_cast,float_cast};
        //for(int i=0; i<8; i++)
            //result[i] = float_cast[i];

        return result;
    }

    static inline std::array<float, 8> one()		{ return broadcast(1.0f);									}
    static inline std::array<float, 8> twopi()		{ return broadcast(boost::math::constants::two_pi<float>());}
    static inline std::array<float, 8> pi()			{ return broadcast(boost::math::constants::pi<float>());	}
    static inline std::array<float, 8> nan()		{ return broadcast(std::numeric_limits<float>::quiet_NaN());}
    static inline std::array<float, 8> infinity()	{ return broadcast(std::numeric_limits<float>::infinity());	}
    static inline std::array<float, 8> zero()		{ return {0, 0, 0, 0, 0, 0, 0, 0};							}

    /// Take absolute value by bitmask
    static inline std::array<float, 8> abs(std::array<float, 8> x)
    {
        std::array<float, 8> bitmask = broadcastBits(float_signmask);
        std::array<float, 8> *result;
    	
        unsigned char *c1 = reinterpret_cast<unsigned char *>(&x);
        unsigned char *c2 = reinterpret_cast<unsigned char *>(&bitmask);
        std::array<unsigned char,4*8> c;
        for(int j=0; j<4*8; j++)
        {
            c[j] = (~c1[j]) & c2[j];
        }
        result = (std::array<float,8>*) &c;

        return *result;			
    }

protected:

    /// Masks and inverse masks for IEEE754 float
    static inline std::array<float, 8> expmask()		{ return broadcastBits(float_expmask);			}
    static inline std::array<float, 8> signmask()		{ return broadcastBits(float_signmask);			}
    static inline std::array<float, 8> mantmask()		{ return broadcastBits(float_mantmask);			}

    /// Returns a vector filled with 2^p
    static inline std::array<float, 8> ldexp(int p)		{ return broadcastBits(((p+127)&0xff) << 23);	}

    /// Convert a vector of uint32_t to a vector of [0,1)
    static inline std::array<float, 8> ui32ToU01(std::array<long long, 4> u)
    {
        std::array<long long, 4> rand_mantissa = _NonSSE_mm256_srli_epi32(u,9); // random mantissa [1,2) due to implicit leading 1
        std::array<float,8> *float_cast = (std::array<float,8>*) &rand_mantissa;
        std::array<float,8> result_ldexp = ldexp(0); // set exp s.t. [1,2)
        std::array<float,8> result_one = one();
        std::array<float,8> *result_or;
        std::array<float,8> result = zero();

        unsigned char *c1 = reinterpret_cast<unsigned char *>(float_cast);
        unsigned char *c2 = reinterpret_cast<unsigned char *>(&result_ldexp);
        std::array<unsigned char,4*8> c{};
        for(int j=0; j<4*8; j++)
        {
            c[j] = c1[j] | c2[j];
        }
        result_or = (std::array<float,8>*) &c;
    
        for(int i=0; i<8; i++)
        {
            result[i] = result_or->data()[i] - result_one[i];
        }

        return result;
    }

    /// Convert a vector of uint32_t to [-1,1)
    static inline std::array<float, 8> ui32ToPM1(std::array<long long, 4> u)
    {
		std::array<long long, 4> rand_mantissa = _NonSSE_mm256_srli_epi32(u,9); // random mantissa [1,2) due to implicit leading 1
        std::array<float,8> *float_cast = (std::array<float,8>*) &rand_mantissa;
        std::array<float,8> result_ldexp = ldexp(1); // set exp s.t. [2,4)
        std::array<float,8> result_three = broadcast(3.0f);
        std::array<float,8> *result_or;
        std::array<float,8> result = zero();

        unsigned char *c1 = reinterpret_cast<unsigned char *>(float_cast);
        unsigned char *c2 = reinterpret_cast<unsigned char *>(&result_ldexp);
        std::array<unsigned char,4*8> c{};
        for(int j=0; j<4*8; j++)
        {
            c[j] = c1[j] | c2[j];
        }
        result_or = (std::array<float,8>*) &c;
    
        for(int i=0; i<8; i++)
        {
            result[i] = result_or->data()[i] - result_three[i];
        }

        //std::ofstream outfile;
        //outfile.open("FloatVectorBaseNonSSE.txt", std::ios_base::app);
	    //outfile << "Result PM1: "<< result[0]<< " " <<    result[1] << " "<<    result[2] <<" "<<   result[3]<< " "<<   result[4]<< " " <<    result[5] << " "<<   result[6] <<" "<<   result[7]<<std::endl;

        return result;
    }
};

std::ostream& operator<<(std::ostream& os,std::array<float, 8> x);

#endif /* KERNELS_SOFTWARE_FLOATVECTORBASE_NONSSE_HPP_ */