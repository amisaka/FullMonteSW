/*
 * FloatUnitExpDistribution.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_FLOATUNITEXPDISTRIBUTION_HPP_
#define KERNELS_SOFTWARE_FLOATUNITEXPDISTRIBUTION_HPP_

#include "FullMonteSW/Config.h"
//#include <iostream>
//#include <fstream>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include "FloatVectorBase.hpp"
#include "avx_mathfun.h"
#else
#include "FloatVectorBase_NonSSE.hpp"
#endif





/** Floating-point unit exponential distribution (mean = 1/lambda = 1.0, var = 1/lambda^2 = 1.0)
 *
 */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
class FloatUnitExpDistribution : public FloatVectorBase
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=1;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* rp = rng.getBlock();
		__m256i r = _mm256_load_si256((const __m256i*)rp);

		__m256 u10 = _mm256_mul_ps(
						broadcast(0.9999999f),	_mm256_sub_ps(	// rescale [1.0, 0) to [0.99999, 0] to eliminate the 0 value in the following log calculation		
												broadcast(2.0f), _mm256_or_ps( // r = 2.0 - [1,2) -> [1.0,0) avoiding zero for log(x)
													_mm256_castsi256_ps(_mm256_srli_epi32(r,9)), ldexp(0))));	
													// uniform random mantissa                 //ldexp(0) -> exponent 2^0 -> [1,2) due to implicit leading 1
		__m256 exprnd = _mm256_sub_ps(
							_mm256_setzero_ps(),
							log_ps(u10));							// -log(X) is unit exponential

		_mm256_store_ps(dst, exprnd);
		
		// DEBUG code to check for 0 values in the RNG
		// if(dst[0] == 0 || dst[1] == 0 || dst[2] == 0 || dst[3] == 0 || dst[4] == 0 || dst[5] == 0 || dst[6] == 0 || dst[7] == 0)
		// {
		// 	std::cout <<  "0 value detected in FloatUnitExpDistribution" << std::endl;
		// }
	}
};

#else
class FloatUnitExpDistribution : public FloatVectorBase_NonSSE
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=1;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* rp = rng.getBlock();

		std::array<long long, 4> r;
		std::copy((long long*) rp, (long long*) rp+4, r.data());

		std::array<long long, 4> rand_mantissa = _NonSSE_mm256_srli_epi32(r,9); // random mantissa [1,2) due to implicit leading 1
        std::array<float,8> *float_cast = (std::array<float,8>*) &rand_mantissa[0];
        std::array<float,8> result_ldexp = ldexp(0); // set exp s.t. [1,2)
        std::array<float,8> *result_or; 
		std::array<float,8> u10 = zero();

        unsigned char *c1 = reinterpret_cast<unsigned char *>(float_cast);
        unsigned char *c2 = reinterpret_cast<unsigned char *>(&result_ldexp);
        std::array<unsigned char,4*8> c{};
        for(int j=0; j<4*8; j++)
        {
            c[j] = c1[j] | c2[j];
        }
        result_or = (std::array<float,8>*) &c;

		std::array<float,8> _log_ps{};
        for(int i=0; i<8; i++)
        {
            u10[i] = 0.9999999f * (2.0f - result_or->data()[i]);
        }

		for(int i=0; i<8; i++)
        {
			_log_ps[i] = log(u10[i]);
        }

		
		std::array<float,8> exprnd{};
		for(int i=0; i<8; i++)
		{
			exprnd[i] = 0.0f - _log_ps[i]; 
		}
		std::copy(exprnd.begin(), exprnd.end(), dst);
	}
};
#endif



#endif /* KERNELS_SOFTWARE_FLOATUNITEXPDISTRIBUTION_HPP_ */
