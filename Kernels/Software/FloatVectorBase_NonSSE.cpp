/*
 * FloatVectorBase_NonSSE.cpp
 *
 *  Created on: Oct 30, 2018
 *      Author: fynns
 */

#include "FloatVectorBase_NonSSE.hpp"
#include <iostream>

std::ostream& operator<<(std::ostream& os,std::array<float, 8> x)
{
	const auto w = os.width();

	for(unsigned i=0;i<8;++i)
		std::cout << std::setw(w) << x.data()[i] << ' ';
	return os;
}
