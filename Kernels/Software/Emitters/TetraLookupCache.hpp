
/*
 ** TetraLookupCache.hpp
 **
 **  Created on: October 22th , 2018
 **      Author: Yasmin Afsharnejad
 */

// Cache designed for fiber emitter to store the tetra elements enclosing position of launch packets. The cache is not limited in size, but it works based on LRU policy and reorders the last recently seen tetra elements to the front. 
// TYS: To clean stuff up, I made this cache NOT THREAD SAFE. I repeat NOT THREAD SAFE.
// If the user of the cache wants thread safety, they should use their own locks around update() and the use of the list returned by getList()

#ifndef TETRALOOKUPCACHE_HPP
#define TETRALOOKUPCACHE_HPP

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <list>
#include <unordered_map>

 using namespace std;

class TetraLookupCache
{
public:
    TetraLookupCache(void): cachedIds{}, hashIds{}
    {} 

    /* Refers key x with in the cache */
    void update(TetraMesh::TetraDescriptor elTetra) 
    { 
        int elId = elTetra.value();

        if (hashIds.find(elId) == hashIds.end()) 
        {
            // not present in cache 
        } 
        else
        {
            // present in cache 
            //reorder the element id to the front of the cache (LRU policy) 
            cachedIds.erase(hashIds[elId]);
        }

        // update the cache and the address in hash table
        cachedIds.push_front(elTetra); 
        hashIds[elId] = cachedIds.begin();
    } 

    //access the list of tetra ids to tarverse in the cache
    list<TetraMesh::TetraDescriptor>* getList()
    {
        return &cachedIds; 
    }  

    //check the current size of cache (number of element ids existing in the cache)
    unsigned int getSize()
    {
        return cachedIds.size();
    }

private:
    //cache tetradescriptor of recently seen element ids 
    list <TetraMesh::TetraDescriptor> cachedIds;

    //To search the element ids in O(n) time complexity, use a hash table. The key is the tetra id number and the value is the address of tetra id descriptor in my cachedIds Cache.
    unordered_map<int, list<TetraMesh::TetraDescriptor>::iterator> hashIds;
};

#endif
