/*
 * Composite.hpp
 *
 *  Created on: Jan 27, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_EMITTERS_COMPOSITE_HPP_
#define KERNELS_SOFTWARE_EMITTERS_COMPOSITE_HPP_

#include <vector>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

namespace Emitter
{

/** Emitter composed of a vector of sub-emitters and detectors (owned by pointer) */

template<class RNG>class Composite : public EmitterBase<RNG>
{
public:

	virtual ~Composite()
	{
		// delete the emitters
		for(const auto e : m_emitters)
			delete e;
		// delete the emitter powers
		for(const auto e : m_emitters)
			delete e;

        // delete the detectors
        for (const auto d : m_detectors) 
            delete d; 
	}


	/** Provided a range that dereferences to pair<float,EmitterBase*>, constructs a Composite emitter.
	 * The Composite owns the pointers provided to it, deleting them when it is deleted.
	 */

	template<class ConstRange>Composite(ConstRange R, std::vector<DetectorBase<RNG>*> detectors)
	{
		std::vector<float> power;

		for(const std::pair<float,EmitterBase<RNG>*> p : R)
		{
			power.push_back(p.first);
			m_emitters.push_back(p.second);
			// store the combination of emitter and power in a vector to be able to sort them 
			// based on their power (highest->lowest)
			m_powerEmitters.push_back(std::make_tuple(p.first,p.second));
		}
		// Descending sort the {power, emitter} tuple based on the power entry
		// sort sorts the elements in ascending order by default -> rbegin(), rend() reverse that order 
		std::sort(m_powerEmitters.rbegin(), m_powerEmitters.rend());

		// Calculate the incremental powers based on the sorted emitters to be able to
		// create a range in which this emitter should be chosen as source for the next photon 
		m_incrementalPower.push_back(std::get<0>(m_powerEmitters[0]));
		for (unsigned i=1; i<m_powerEmitters.size(); i++)
		{
			m_incrementalPower.push_back(std::get<0>(m_powerEmitters[i]) + m_incrementalPower[i-1]);
		}
		m_sourceDistribution = boost::random::discrete_distribution<unsigned>(power);
		//std::cout << "  New Emitter::Composite with " << m_emitters.size() << " elements" << std::endl;
		m_index = 0;

        m_detectors = detectors;
	}

	LaunchPacket emit(RNG& rng, unsigned long long nPktReq, unsigned long long nPktDone) const
	{
		if (m_use_random)
		{
			// Returns a value distributed according to the parameters of the discrete_distribution.
			// operator()--> m_sourceDistribution(rng) is overloaded in the boost class
			return m_emitters[m_sourceDistribution(rng)]->emit(rng,0,0);
		}
		else
		{
			// upperBound is the number of packets that are launched from the source that is indexed by m_counter
			// from the current source
			unsigned size = m_incrementalPower.size();
			unsigned long long upperBound;
			if (m_index == size-1)
			{
				upperBound = nPktReq;
			}
			else
			{
				upperBound = (unsigned long long) ceil(m_incrementalPower[m_index]/m_incrementalPower[size-1] * nPktReq);
			}

			// if nPktDone is not smaller than the current upperBound, we will increase the emitter index by 1
			// and proceed to the next emitter and emit from it.
			if (nPktDone > upperBound)
			{
				m_index++;
			}
			if (nPktReq-1 == nPktDone && m_index != m_incrementalPower.size()-1)
			{
				std::cout << std::endl << std::endl;
				LOG_INFO << "WARNING: The number of simulated photons is not large enough to accurately model the light emission of the defined source!" << std::endl
						 << "Source size: " << m_incrementalPower.size() << std::endl
						 << "Photon number:" << nPktReq << std::endl
						 << "Source elements reached: " << m_index << std::endl << std::endl;
			}
			if( m_index >= m_incrementalPower.size() )
			{
				LOG_ERROR << "The index to ensure the correct emission distribution (in this thread) is higher than the number of available sources!" << std::endl
						  << "Upper Bound: " << upperBound << std::endl 
						  << "Current packet: " << nPktDone << std::endl 
						  << "Total packets: " << nPktReq << std::endl
						  << "Source size: " << m_incrementalPower.size() << std::endl 
						  << "Source index: " << m_index << std::endl;
				return std::get<1>(m_powerEmitters[m_incrementalPower.size()-1])->emit(rng,0,0);
			}
			else
			{
				return std::get<1>(m_powerEmitters[m_index])->emit(rng,0,0);
			}
		}
	}


private:
	std::vector<const EmitterBase<RNG>*> 			m_emitters;				///< Owning pointer to sub-emitters
	std::vector<Emitter::DetectorBase<RNG>*>           m_detectors;            ///< Owning pointer to detectors
    boost::random::discrete_distribution<unsigned> 	m_sourceDistribution;	///< Distribution for power
	std::vector<std::tuple<float, const EmitterBase<RNG>*>>			m_powerEmitters;		///< Tuple of Power + Owning pointer to sub-emitters
	std::vector<float>												m_incrementalPower;		///< Sum of power values to determine the correct photon distribution for each source.
	bool															m_use_random = false;	///< Defines if we are using the random distribution or a sequential approach for emitting photons
	mutable unsigned												m_index = 0;			///< Defines if the index of the emitter we are currently emitting from in the sequential approach
};

};


#endif /* KERNELS_SOFTWARE_EMITTERS_COMPOSITE_HPP_ */
