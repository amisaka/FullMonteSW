/**
 * \file CylDetector.hpp
 * Contains functionality implementation of a cylindrical detector
 *
 * \author Abed Yassine
 * \data June 10th, 2020
 *
 */

#ifndef KERNELS_SOFTWARE_EMITTERS_CYLDETECTOR_HPP_
#define KERNELS_SOFTWARE_EMITTERS_CYLDETECTOR_HPP_

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/SpinMatrix.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Geometry/Sources/CylDetector.hpp>

#include <FullMonteSW/Kernels/Software/Tetra.hpp>
#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/Kernels/Software/SSEMath.hpp>
#include <FullMonteSW/Kernels/Software/SSEConvert.hpp>

#include <iostream>
#include <vector>
#include <mutex>

namespace Emitter
{

static std::mutex detMutex;
// static unsigned bla = 0;

/**
 * Cylinder diffuser detector by checking incident photons and modeling detection profile
 */
template<class RNG> class CylDetector {
public:

    /** 
     * This enum class is used to check if a point is in the cylinder, on the surface of the cylinder or outside the cylinder
     */
    enum onCylinder { SURFACE, INSIDE, OUTSIDE}; 

    /**
     * Constructor
     */
	CylDetector(
        const TetraMesh* mesh,
        TetraMesh::TetraMeshRTree* rtree,
        KernelTetra* tetras,
        std::array<float,3> p0,
        std::array<float,3> p1,
        float radius,
        float numericalAperture,
        unsigned detectorID=-1U) :
            m_mesh(mesh),
            m_rtree(rtree),
            m_tetras(tetras),
		    m_endpoint({p0,p1}),
		    m_radius(radius),
            m_numericalAperture(numericalAperture),
            m_detectorID(detectorID)
    {
        assert(m_mesh != nullptr);

        // compute direction vector and normal
        std::array<float,3> dir = {
            p1[0] - p0[0],
            p1[1] - p0[1],
            p1[2] - p0[2]
        };

        // compute the midpoint of the cylinder
        m_midpoint = {
            (p1[0] + p0[0]) / 2.0f,
            (p1[1] + p0[1]) / 2.0f,
            (p1[2] + p0[2]) / 2.0f
        };
        
        // compute normal for cylinder direction and height of cylinder
        m_dir = normalize(dir);
        m_height = norm(dir);

        LOG_DEBUG << "Emitter::CylDetector direction vector=(" << m_dir[0] << "," << m_dir[1] << "," << m_dir[2] << ")" << 
                                       " midpoint=(" << m_midpoint[0] << "," << m_midpoint[1] << "," << m_midpoint[2] << ")" <<
                                       " height="<< m_height << endl;

        // spin matrix for z-axis and cylinder direction (must be set after setting m_dir)
        m_Q = SpinMatrix::k(m_dir);

        m_sse_end0 = SSE::Point3(p0);
        m_sse_end1 = SSE::Point3(p1); 
        m_sse_dir = SSE::Vector3(m_dir); 

        m_end0_q[0] = m_Q[0][0]*p0[0] + m_Q[0][1]*p0[1] + m_Q[0][2]*p0[2];
        m_end0_q[1] = m_Q[1][0]*p0[0] + m_Q[1][1]*p0[1] + m_Q[1][2]*p0[2];
        m_end0_q[2] = m_Q[2][0]*p0[0] + m_Q[2][1]*p0[1] + m_Q[2][2]*p0[2];
        m_end1_q[0] = m_Q[0][0]*p1[0] + m_Q[0][1]*p1[1] + m_Q[0][2]*p1[2];
        m_end1_q[1] = m_Q[1][0]*p1[0] + m_Q[1][1]*p1[1] + m_Q[1][2]*p1[2];
        m_end1_q[2] = m_Q[2][0]*p1[0] + m_Q[2][1]*p1[1] + m_Q[2][2]*p1[2];

        // Tag enclosing tetra with detectorID using rtree
        if (m_tetras)
            tagEnclosingTetraRtree(); 
    }

    ~CylDetector()
    {
    }

    /**
     * Determines if a point is within this cylinder
     */
    onCylinder isPointInside(const SSE::Point3& q) {
        
        SSE::Point3 endPt0 = m_sse_end0;
        SSE::Point3 endPt1 = m_sse_end1;
        SSE::Vector3 dir = m_sse_dir; 

        // first check if q lies between the circular planes of the cylinder ends
        SSE::Vector3 q_e0 = q - endPt0;
        SSE::Vector3 q_e1 = q - endPt1;

        bool betweenEnds = (float(dot(q_e0, dir)) >= -1e-3) && (float(dot(q_e1, dir)) <= 1e-3);

        if(betweenEnds) {
            // point lines between the ends of the cylinder
            // now ensure that the (shortest) distance from mid-line of the cylinder to the point is less than the radius
            float dist = float(norm(cross(q_e0, dir)));
            if (abs(dist - m_radius) < 1e-5) {
                return onCylinder::SURFACE;
            } else if (dist < m_radius) {
                return onCylinder::INSIDE;
            } else {
                return onCylinder::OUTSIDE;
            }
        } else {
            return onCylinder::OUTSIDE;
        }
    }

    /**
     * Determines if a line segment (ray) intersects any of the cylinder caps. If yes, it returns intersection point
     * We know that at least end0 of the line segment is outside the cylinder as the photon packet terminates once it intersects with the detector
     * @param end0 The first endpoint of the line segment 
     * @param end1 The second endpoint of the line segment (ray goes from end0 to end1) 
     * @param intPt [output] Intersection point
     * @return true if there is an intersection 
     */
    bool isLineIntersectsCaps(const SSE::Point3& end0, const SSE::Point3& end1, SSE::Point3& intPt)
    {
        /*
         * IDEA:
         * 1- Rotate cylinder to be centerd around origin and parallel to z access
         * 2- Rotate line segment same way. 
         * 3- Check that one point of line is outside and the other inside (Otherwise return false)
         * 4- Check that x^2 + y^2 of the line at z = z_min || z_max within the radius of the circle  (Otherwise return false)
         * 5- Intersection point is: (x_line, y_line, z), where z is either z_min of cylinder or z_max
         */ 
    
        std::array<float, 3> end0_q, end1_q, end0_orig = end0.array(), end1_orig = end1.array();
        end0_q[0] = m_Q[0][0]*end0_orig[0] + m_Q[0][1]*end0_orig[1] + m_Q[0][2]*end0_orig[2];
        end0_q[1] = m_Q[1][0]*end0_orig[0] + m_Q[1][1]*end0_orig[1] + m_Q[1][2]*end0_orig[2];
        end0_q[2] = m_Q[2][0]*end0_orig[0] + m_Q[2][1]*end0_orig[1] + m_Q[2][2]*end0_orig[2];
        end1_q[0] = m_Q[0][0]*end1_orig[0] + m_Q[0][1]*end1_orig[1] + m_Q[0][2]*end1_orig[2];
        end1_q[1] = m_Q[1][0]*end1_orig[0] + m_Q[1][1]*end1_orig[1] + m_Q[1][2]*end1_orig[2];
        end1_q[2] = m_Q[2][0]*end1_orig[0] + m_Q[2][1]*end1_orig[1] + m_Q[2][2]*end1_orig[2];


        float min_z = min(m_end0_q[2], m_end1_q[2]);
        float max_z = max(m_end0_q[2], m_end1_q[2]); 
        float test_z = 0.0;
        if ((end0_q[2] > max_z && end1_q[2] > max_z) ||
            (end0_q[2] < min_z && end1_q[2] < min_z) ||
            ((end0_q[2] > min_z && end1_q[2] > min_z && 
              end0_q[2] < max_z && end1_q[2] < max_z))) { // both outside or both within caps 
            return false;
        }
        else if (end0_q[2] < min_z + 1e-5 && end1_q[2] > min_z + 1e-5) {
            test_z = min_z;
        }
        else if (end0_q[2] > max_z - 1e-5 && end1_q[2] < max_z) {
            test_z = max_z;
        }

        // Figure out x and y

        SSE::Vector3 line_segment = SSE::Point3(end1_q) - SSE::Point3(end0_q);
        SSE::Vector3 line_dir = normalize(line_segment);
        float t = (test_z - end0_q[2])/float(line_dir.component<2>()); 
        float x_int = end0_q[0] + t*float(line_dir.component<0>());
        float y_int = end0_q[1] + t*float(line_dir.component<1>());

        std::array<float, 3> temp_int_pt = {x_int, y_int, test_z}; 

        float inside_circle = fabs(sqrtf(x_int*x_int + y_int*y_int)); 
        if (inside_circle > m_radius + 1e-5) { // outside
            return false;
        }

        // Rotate back 
        std::array<float, 3> temp_int_pt_orig = {
            m_Q[0][0]*temp_int_pt[0] + m_Q[1][0]*temp_int_pt[1] + m_Q[2][0]*temp_int_pt[2], 
            m_Q[0][1]*temp_int_pt[0] + m_Q[1][1]*temp_int_pt[1] + m_Q[2][1]*temp_int_pt[2], 
            m_Q[0][2]*temp_int_pt[0] + m_Q[1][2]*temp_int_pt[1] + m_Q[2][2]*temp_int_pt[2] }; 
     
        intPt = SSE::Point3(temp_int_pt_orig);
        return true; 

    }


    /**
     * Determines if a line segment (ray) intersects with the cylinder and then returns the intersection point 
     * We know that at least end0 of the ray is outside the cylinder as the photon packet terminates once it intersects with the detector 
     * @param end0 The first endpoint of the line segment 
     * @param end1 The second endpoint of the line segment (ray goes from end0 to end1)
     * @param intPt [output] Intersection point
     * @return true if there is an intersection
     */
     //*
    bool isLineIntersects(const SSE::Point3& end0, const SSE::Point3& end1, SSE::Point3& intPt) { 
        
        
        SSE::Vector3 line_segment = end1 - end0;
        if (float(norm(line_segment)) < 1e-5) { // photon packet didn't move
            return false;
        }

        // Check first if the line intersects any of the caps 
        if (isLineIntersectsCaps(end0, end1, intPt)) {
            return true;
        }

        SSE::Vector3 line_dir = normalize(line_segment); 
        SSE::Vector3 cyl_axis = m_sse_end1 - m_sse_end0; 

        // LOOK at stackoverflow.com/questions/4078401/trying-to-optimize-line-vs-cylinder-intersection
        // THIS MAY BE INEFFICIENT    

        SSE::Point3 cyl_mid(m_midpoint); 
        SSE::Vector3 end0_cyl_mid = end0 - cyl_mid;
        
        SSE::Vector3 normal = cross(line_dir, cyl_axis); 

        SSE::Scalar l_normal = norm(normal); 
        if (float(l_normal) < 1e-5) { // line segment is parallel to cylinder 
            return false; 
        }

        normal = normalize(normal);

        float end0_cyl_mid_dot_normal = abs(float(dot(end0_cyl_mid, normal)));

        if (end0_cyl_mid_dot_normal > m_radius) { // does not intersect
            return false; 
        }

        SSE::Vector3 end0_cyl_mid_axis = cross(end0_cyl_mid, cyl_axis); 
        float t = -1.0*float(dot(end0_cyl_mid_axis, normal))/float(l_normal); 
        SSE::Vector3 temp = cross(normal, cyl_axis); 
        temp = normalize(temp); 

        float s = float(dot(line_dir, temp));
        float d = end0_cyl_mid_dot_normal; 
        s = fabs(sqrtf(m_radius*m_radius - d*d)/s); 

        float in = t - s; 
        float out = t + s; 

        float lambda = 0.0; // parameter along the ray where intersection happens 
        if (in < -1e-5) {
            if (out < -1e-5) {
                return false; 
            } else {
                lambda = out;
            }
        }
        else if (out < -1e-5) {
            lambda = in;
        } else if (in < out) {
            lambda = in; 
        } else { 
            lambda = out;
        }
        
        assert (lambda > -1e-5);  

        if (lambda > float(norm(line_segment)) + 1e-3) { // ray can intersect but at a point longer than input segment
            return false;
        }

        // Calculate the intersection point 
        SSE::Point3 tempIntPt = end0 + line_dir*SSE::Scalar(lambda); 

        // Check it is inside the cylinder 
        if (isPointInside(tempIntPt) == onCylinder::OUTSIDE) {
            return false;
        }

        intPt = tempIntPt;

        return true;
    }


    // */
    /**
     * Returns the closest point on the cylinder axis to a random point
     * @param p The randome point to find the closest point to 
     * @return the closest point to p on the cylinder axis
     */
    std::array<float,3> closestPointOnLine(SSE::Point3 p) const {
        //// STEP 1: find the normal of the cylinder at random point 'p'
        // the vector formed by the first endpoint of the cylinder and the random point
        std::array<float,3> vec = {
            p[0] - m_endpoint[0][0],
            p[1] - m_endpoint[0][1],
            p[2] - m_endpoint[0][2],
        };

        // the projected distance from the random point p onto the line formed by the direction normal of the cylinder
        float dist = dot(vec, m_dir);

        // ensure the distance makes sense
        assert(dist>= -1e-3 && dist<=m_height+1e-3);

        // create a vector out of the closest point on the center line of the cylinder and the random point we are emitting from
        // this is the normal of the cylinder surface at the point 'p'
        std::array<float,3> closestPoint = {
            m_endpoint[0][0] + m_dir[0]*dist,
            m_endpoint[0][1] + m_dir[1]*dist,
            m_endpoint[0][2] + m_dir[2]*dist
        };

        return closestPoint;
    }

    /**
     * Returns the surface normal goint towards a point
     * 
     * @param p the random point to find the incident surface normal from
     * @return the unit normal vector
     */
     std::array<float, 3> direction(SSE::Point3 p) const {
        std::array<float, 3> closestPoint = closestPointOnLine(p); 

        // the vector from the closest point on the cylinder center line to the random point
        std::array<float,3> pointVec = {
            p[0] - closestPoint[0],
            p[1] - closestPoint[1],
            p[2] - closestPoint[2]
        };

        // the normal vector going from center line of cylinder to random point (surface normal at random point)
        std::array<float,3> normalAtP = normalize(pointVec);

        return normalAtP;
     }


    /**
     *
     * Implements the detection functionality of a cylinder
     * 
     * @param rng       Random number generator to use
     * @param packet    Packet to be tested if detected or not
     * @param n         refractive index of the material the detector is in
     */
     void detect(RNG& /*rng*/, Packet& packet, float n) 
     {
         SSE::Point3 pktP(packet.p); 
        SSE::Point3 intPt; 
        
        bool packetIntersects = isLineIntersects(SSE::Point3(packet.oldP), SSE::Point3(packet.p), intPt);

        if (!packetIntersects) {
            return; // if packet weight stays the same, kernel won't kill it
        }

        // Measure the angle of incidence 
        SSE::Vector3 dir(m_dir);
        dir = dir*SSE::Scalar(-1); // -1 as we want to measure the incidence angle going towards the proximal end
        float angle = std::acos(float(dot(dir, packet.direction()))); // both are unit vector

        // Check if angle is less than theta_max from 
        double NA_div_n = m_numericalAperture/n;
        if (NA_div_n > 1.0) NA_div_n = 1.0;
        
        if (angle <= std::asin(NA_div_n)) 
        {
            // For now assume that everything is detected. 
            if (m_detectionType == Source::CylDetector::DetectionType::FULL) 
            { 
                detMutex.lock(); 
                m_detectedWeight += packet.weight();  
                detMutex.unlock();

                packet.w = 0.0;
            }
            else if (m_detectionType == Source::CylDetector::DetectionType::PROBABILITY) 
            {
                // generate a probability (portion of the packet to be detected) that increases exponentially closer to proximal end
                // 1- Compute projection of current packet position on cylinder axis. 
                SSE::Point3 closestPoint(closestPointOnLine(intPt));//SSE::Point3(packet.p)));

                // 2- Find distance from endpoint[0] 
                SSE::Vector3 vec = SSE::Point3(m_endpoint[0]) - closestPoint; 
                float dist = float(norm(vec)); 

                // 3- call probability function with that distance 
                float expVal = expProbFast(dist, m_height); 
                float detWeight = packet.w*expVal; 
                
                detMutex.lock(); 
                m_detectedWeight += detWeight; 
    //                 if (bla < 20) {
    //                     std::array<float, 3> packet_pos = intPt.array(); 
    //                     std::cout << bla++ << " " << dist << " " << expVal << " "<< packet.w << " (" << packet_pos[0] << ", " << packet_pos[1] << ", " << packet_pos[2] << ")" << std::endl;
    //                 }
                detMutex.unlock(); 

                packet.w = detWeight < 1e-4 ? 0.0 : packet.w - detWeight; // remaining weight is the portion scattered and not detected (packet will die anyway)
            }
            else if (m_detectionType == Source::CylDetector::DetectionType::ODE) 
            {
                // generate a probability (portion of the packet to be detected) that increases exponentially closer to proximal end
                // 1- Compute projection of current packet position on cylinder axis. 
                SSE::Point3 closestPoint(closestPointOnLine(intPt));//SSE::Point3(packet.p)));

                // 2- Find distance from endpoint[0] 
                SSE::Vector3 vec = SSE::Point3(m_endpoint[0]) - closestPoint; 
                float dist = float(norm(vec)); 

                // 3- call probability function with that distance 
                float expVal = odeProb(dist, m_height); 
                float detWeight = packet.w*expVal; 
                
                detMutex.lock(); 
                m_detectedWeight += detWeight; 
    //             if (bla < 20) {
    // //                     std::array<float,4> packet_pos = as_array(packet.p);
    //                 std::array<float, 3> packet_pos = intPt.array(); 
    //                 std::cout << bla++ << " " << dist << " " << expVal << " "<< packet.w << " (" << packet_pos[0] << ", " << packet_pos[1] << ", " << packet_pos[2] << ")" << std::endl;
    //             }
                detMutex.unlock(); 

                packet.w = detWeight < 1e-4 ? 0.0 : packet.w - detWeight; // remaining weight is the portion scattered and not detected (packet will die anyway)
            }
        }
        else {
            packet.w = 0.0; // So the kernel kills it 
        }
     }


    // query some of the cylinder properties
    std::array<float,3> endpoint(int i) const { return m_endpoint[i]; }
    float radius() const { return m_radius; }
    float height() const { return m_height; }
    std::array<float,3> dirNormal() const { return m_dir; }
    float numericalAperture() const { return m_numericalAperture; }
    unsigned detectorID() const { return m_detectorID; }
    float detectedWeight() const { return m_detectedWeight; } 
    Source::CylDetector::DetectionType detectionType() { return m_detectionType; }

    // Setter of the detection type 
    void setDetectionType(Source::CylDetector::DetectionType dType) { m_detectionType = dType; }
    void resetDetectedWeight() { m_detectedWeight = 0.0f; }

    private:
    // the tetra mesh being emit from
    const TetraMesh* m_mesh;

    // the RTree for queries
    TetraMesh::TetraMeshRTree* m_rtree;

    KernelTetra* m_tetras;

    // the two endpoints of the cylinder 
    // Assume endpoint[0] is the proximal end (TODO: generalize this)
    std::array<float,3> m_endpoint[2];
    SSE::Point3 m_sse_end0;
    SSE::Point3 m_sse_end1;
    std::array<float,3> m_end0_q; // rotated around z access 
    std::array<float,3> m_end1_q; 

    // the midpoint of the cylinder
    std::array<float,3> m_midpoint;

    // the unit direction vector of the cylinder
    std::array<float,3> m_dir;
    SSE::Vector3 m_sse_dir;

    // the radius of the cylinder
    float m_radius=1.0f;

    // the height of the cylinder
    float m_height=0.0f;

    // the numerical aperture for the detection of the cylinder 
    float m_numericalAperture = 0.22f;

    // The over all weight of detected photons 
    float m_detectedWeight = 0.0f; 

    // the ID of the detector > 0. 
    unsigned m_detectorID = -1U;

    // the spin matrix for the direction vector of the cylinder (aligns cylinder direction vector to z-axis)
    std::array<std::array<float,3>,3> m_Q;

    // Controls how much of the packet weight to be detected 
    Source::CylDetector::DetectionType m_detectionType = Source::CylDetector::DetectionType::ODE;


    /** 
     * This method splits the cylinder into N points and find the tetra enclosing that point and tag it with the detector ID
     * @param spacing length of each split element of the detector. The higher the resolution of the mesh, the smaller this should be
     */
    void tagEnclosingTetra(float spacing) 
    {
        std::vector<unsigned> enclosingTetra; 
        unsigned numPts = static_cast<unsigned>(std::ceil(m_height/spacing) + 1); // +1 to include end point 

        // generate numPerp perpendicular vectors in random directions and discretize on those as well
        unsigned numRandPerp = 100;
        std::vector<std::array<float, 3>> randVecs(numRandPerp); 
        for (unsigned i = 0; i < numRandPerp; i++) 
        {
            randVecs[i] = getRandPerpendicularVector(); 
        }
        unsigned numPtsPerp = static_cast<unsigned>(std::ceil(m_radius/spacing) + 1); 

        // Discretize, find tetra and add
        for (unsigned i = 0; i < numPts; i++)
        {
            float mag = i*spacing;
            std::array<float, 3> currPt = {
                m_endpoint[0][0] + m_dir[0]*mag,
                m_endpoint[0][1] + m_dir[1]*mag,
                m_endpoint[0][2] + m_dir[2]*mag }; 
           
            auto tetra = m_rtree->search(currPt); 

            if (tetra.value() > 0 && std::find(enclosingTetra.begin(), enclosingTetra.end(), tetra.value()) == enclosingTetra.end()) {
                enclosingTetra.push_back(tetra.value());
                // Add its adjacent tetra
                for (unsigned ad = 0; ad < 4; ad++) {
                    unsigned tet_adjacent = m_tetras[tetra.value()].adjTetras[ad];
                    if (tet_adjacent && std::find(enclosingTetra.begin(), enclosingTetra.end(), tet_adjacent) == enclosingTetra.end()) {
                        enclosingTetra.push_back(tet_adjacent);
                    }
                }
               
               // loop over random vectors
               for (unsigned j = 0; j < numRandPerp; j++) 
               {
                   for (unsigned k = 0; k < numPtsPerp; k++)
                   {
                       float magPerp = spacing*k;
                       std::array<float, 3> currPtPerp = {
                           currPt[0] + magPerp*randVecs[j][0],
                           currPt[1] + magPerp*randVecs[j][1],
                           currPt[2] + magPerp*randVecs[j][2] };
            
                        auto tetra2 = m_rtree->search(currPtPerp); 

                        if (tetra2.value() > 0 && std::find(enclosingTetra.begin(), enclosingTetra.end(), tetra2.value()) == enclosingTetra.end()) {
                            enclosingTetra.push_back(tetra2.value());
                            // Add its adjacent tetra
                            for (unsigned ad = 0; ad < 4; ad++) {
                                unsigned tet_adjacent = m_tetras[tetra2.value()].adjTetras[ad];
                                if (tet_adjacent && std::find(enclosingTetra.begin(), enclosingTetra.end(), tet_adjacent) == enclosingTetra.end()) {
                                    enclosingTetra.push_back(tet_adjacent);
                                }
                            }
                        }
                   }
               }
            }
        }
       
        // Tag the tetra
        for (auto t_id : enclosingTetra) 
        {
            m_tetras[t_id].detectorEnclosedIDs.push_back(m_detectorID);
        }
    }

    /** 
     * This method creates a bounding box around the cylinder and then call rtree functionalities to find the tetra enclosed within
     * Note: This overestimates the number of tetra included as it takes the bounding box of the bounding boxes of both caps of the cylinder
     */
    void tagEnclosingTetraRtree() 
    { 
        // 1- create a bounding box of the cylinding. A bounding box is defined by the bottom left and top right points 
        std::array<float, 3> normal_vec = { 
            m_radius*sqrtf(1 - m_dir[0]*m_dir[0]), 
            m_radius*sqrtf(1 - m_dir[1]*m_dir[1]), 
            m_radius*sqrtf(1 - m_dir[2]*m_dir[2]) };

        std::array<float, 3> bb_min, bb_max; 
        bb_min[0] = std::fmin(m_endpoint[0][0] - normal_vec[0], m_endpoint[1][0] - normal_vec[0]);
        bb_min[1] = std::fmin(m_endpoint[0][1] - normal_vec[1], m_endpoint[1][1] - normal_vec[1]);
        bb_min[2] = std::fmin(m_endpoint[0][2] - normal_vec[2], m_endpoint[1][2] - normal_vec[2]);
        bb_max[0] = std::fmax(m_endpoint[0][0] + normal_vec[0], m_endpoint[1][0] + normal_vec[0]);
        bb_max[1] = std::fmax(m_endpoint[0][1] + normal_vec[1], m_endpoint[1][1] + normal_vec[1]);
        bb_max[2] = std::fmax(m_endpoint[0][2] + normal_vec[2], m_endpoint[1][2] + normal_vec[2]);

        std::vector<TetraMesh::TetraDescriptor> enclosed_tetra = m_rtree->getTetraInBoundingBox(bb_min, bb_max);

        std::unordered_set<unsigned> enclosed_tetra_set; 
        for (unsigned i = 0; i < enclosed_tetra.size(); i++) {
            enclosed_tetra_set.insert(enclosed_tetra[i].value()); 
        }
    
        // Tag the tetra
        for (auto t_id : enclosed_tetra_set) 
        {
            m_tetras[t_id].detectorEnclosedIDs.push_back(m_detectorID);
        }
    }

    /**
     * This method returns a unit vector in a random direction such that it is perpendicular to the cylinder
     */
    std::array<float, 3> getRandPerpendicularVector() 
    {
        // generate a vector p normal to m_dir and normalize it
        std::array<float, 3> p = { m_dir[1] - m_dir[2], m_dir[2] - m_dir[0], m_dir[0] - m_dir[1] };
        float p_length = norm(p); 

        if (p_length < 1e-5) // regenerate another vector
        {
            if (std::abs(m_dir[0]) > 1e-5) 
            {
                p = {m_dir[1], -m_dir[0], 0.0f}; 
            }
            else if (std::abs(m_dir[1]) > 1e-5) 
            {
                p = {0.0f, m_dir[2], -m_dir[1]};
            } 
            else 
            {
                p = {m_dir[2], 0.0f, -m_dir[0]}; 
            }
        }

        std::array<float, 3> p_n = normalize(p); 

        // take the cross product of m_dir and p and normalize it
        std::array<float, 3> v = cross(m_dir, p);
        v = normalize(v); 

        // generate a random polar angle between 0 and 2*PI
        float theta = (2*M_PI*rand())/RAND_MAX;

        // compute a random unit vector from p_n and v
        std::array<float, 3> rand_dir = { std::cos(theta)*p_n[0] + std::sin(theta)*v[0],
                                          std::cos(theta)*p_n[1] + std::sin(theta)*v[1],
                                          std::cos(theta)*p_n[2] + std::sin(theta)*v[2] };
                                          
        return normalize(rand_dir); 
    }

    /**
     * This function returns a probability of detection based on distance from proximal end 
     * The shape of the function is: prob(x) = exp(-(gamma/mu_0)*(exp(mu_0(x - x0)) - exp(-mu_0*x_0)), 
     * This is an ode solution for an exponential scattering profile  
     * 
     * We choose gamma = 10, mu_0 = 7 and x_0 = 0.8*height
     */
     std::function<float(float, float)> odeProb = [&](float t, float height)->float { 
        float gamma = 10.0, mu_0 = 7.0, delta_0 = (-1*gamma/mu_0), x_0 = 0.9*height;
        return std::exp(delta_0*(std::exp(mu_0*(t-x_0)) - std::exp(-mu_0*x_0)));
     };

    /**
     * This function returns a probability of detection based on distance from proximal end (FASTER DECAY)
     * The shape of the function is: prob(x) = exp(alpha*x), where alpha = log(0.01)/0.9*m_height 
     * This evaluates to 0.01 at 90% of length of the diffuser. 
     *
     */
     std::function<float(float, float)> expProbFast = [&](float t, float height)->float { 
         return std::exp((log(0.01f)/(0.9*height))*t); 
    //     float mu_0 = 7.0; 
    //      return std::exp(-mu_0*t); 

     };

};
}

#endif
