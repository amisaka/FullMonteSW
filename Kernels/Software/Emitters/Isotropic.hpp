/**
 * IsotropicEmitter.hpp
 *
 *  Created on: Jan 27, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_ISOTROPIC_HPP_
#define KERNELS_SOFTWARE_ISOTROPIC_HPP_

#include <FullMonteSW/Config.h>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE   
#include <mmintrin.h>
#else
#include "FullMonteSW/Kernels/Software/SSEMath.hpp"
#endif

#include <iostream>
#include <fstream>


namespace Emitter
{

/** Isotropic emitter - 3D unit vector */

template<class RNG>class Isotropic
{
public:

	PacketDirection direction(RNG& rng, SSE::Point3 /*p*/) const
	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE   
		__m128 one = _mm_set_ss(1.0f);				// [ 1 0 0 0]
		__m128 zero = _mm_setzero_ps();				// [ 0 0 0 0]

		//_mm_loadu_ps--> Load 4 rng values from the buffer stream of rng.uvect2D()
		//rng.uvect2D() and all similar methods pop the first rng vlaue of the stream,
		//meaning that calling the function multiple times always yields different values
		__m128 azuv = _mm_loadu_ps(rng.uvect2D());

		__m128 b = _mm_shuffle_ps(_mm_addsub_ps(zero,azuv),zero,_MM_SHUFFLE(0,1,0,1));

		azuv = _mm_movelh_ps(azuv,azuv);			// [cos(theta), sin(theta), cos(theta), sin(theta)]
		
		//_mm_load1_ps--> Load the first rng element from the buffer stream of rng.floatPM1()
		__m128 sinphi = _mm_load1_ps(rng.floatPM1());								// sin(phi) = [-1,1)
		__m128 cosphi = _mm_sqrt_ss(_mm_sub_ss(one,_mm_mul_ss(sinphi,sinphi)));		// cos(phi) = 1-sin2(phi)

		__m128 cpcpspsp = _mm_shuffle_ps(cosphi,sinphi,_MM_SHUFFLE(0,0,0,0));		// [cos(phi) cos(phi) sin(phi) sin(phi)]

		azuv = _mm_mul_ps(azuv,cpcpspsp);											// [ct*cp st*cp ct*sp st*sp]

		/**
    	 * @brief Explanation of
    	 * _mm_shuffle_ps(azuv,sinphi,_MM_SHUFFLE(1,0,1,0))
    	 * d[0] = azuv[0]
    	 * d[1] = azuv[1]
    	 * d[2] = sinphi[0]
    	 * d[3] = sinphi[1]
    	 * 
    	 */
		__m128 d = _mm_shuffle_ps(azuv,sinphi,_MM_SHUFFLE(1,0,1,0));
		
		/**
    	 * @brief Explanation of
    	 * _mm_shuffle_ps(azuv,_mm_sub_ps(zero,cosphi),_MM_SHUFFLE(1,0,3,2))
    	 * d[0] = azuv[2]
    	 * d[1] = azuv[3]
    	 * d[2] = 0-cosphi[0]
    	 * d[3] = 0-cosphi[1]
    	 * 
    	 */
		__m128 a = _mm_shuffle_ps(azuv,_mm_sub_ps(zero,cosphi),_MM_SHUFFLE(1,0,3,2));
#else
		m128 zero = set_zero128();
		
		m128 azuv;
		const float* rng_base = rng.uvect2D();
        std::copy(rng_base, rng_base+4, begin(azuv));
		
		
		

		m128 b = shuffle128(addsub128(zero,azuv),zero,0,1,0,1);

		azuv[2] = azuv[0];
		azuv[3] = azuv[1];			

		
		const float* rng_float_base = rng.floatPM1();
		m128 sinphi = {*rng_float_base, *rng_float_base, *rng_float_base, *rng_float_base};      					
		
		m128 cosphi = {1.0f, 0.0f, 0.0f, 0.0f};
		cosphi[0] = sqrtf(1.0-sinphi[0] * sinphi[0]);		
	
	
	

		m128 cpcpspsp = shuffle128(cosphi,sinphi,0,0,0,0);		

		azuv = mult128(azuv,cpcpspsp);											

		
		m128 d = shuffle128(azuv,sinphi,1,0,1,0);
		
	
		m128 a = shuffle128(azuv,sub128(zero,cosphi),1,0,3,2);
#endif

		return PacketDirection(
				SSE::UnitVector3(SSE::Vector3(d),SSE::NoCheck),
				SSE::UnitVector3(SSE::Vector3(a),SSE::NoCheck),
				SSE::UnitVector3(SSE::Vector3(b),SSE::NoCheck));

	}
};

};


#endif /* KERNELS_SOFTWARE_ISOTROPIC_HPP_ */
