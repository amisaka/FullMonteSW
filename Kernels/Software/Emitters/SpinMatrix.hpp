#ifndef KERNELS_SOFTWARE_EMITTERS_SPINMATRIX_HPP_
#define KERNELS_SOFTWARE_EMITTERS_SPINMATRIX_HPP_

#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Geometry/UnitVector.hpp>

#include <array>

/**
 * Spin matrix calculation for generic case and special case for i,j,k vectors
 * See: https://math.stackexchange.com/questions/542801/rotate-3d-coordinate-system-such-that-z-axis-is-parallel-to-a-given-vector
 */
class SpinMatrix {
protected:
    /**
     * Constructor for static class
     */
    SpinMatrix() {}

public:

    /**
     * Computes the rotation matrix to center vector 'v' on 'n'.
     *
     * @param v the vector to align to n
     * @param n the vector to align v to
     *
     * @return the spin matrix
     */
    static std::array<std::array<float,3>,3> compute(std::array<float,3> v, std::array<float,3> n) {
        // normalize the input vectors
        const Vector<3,float> v_norm = normalize(Vector<3,float>(v));
        const Vector<3,float> n_norm = normalize(Vector<3,float>(n));
        
        // special case if the normal vectors are parallel (or anti-parallel)
        const float parallelCheck = dot(v_norm, n_norm);
        if(parallelCheck == 1.0f) {
            std::array<std::array<float,3>,3> Q;
            
            // identity matrix does no rotation
            Q[0] = {1.0f, 0.0f, 0.0f};
            Q[1] = {0.0f, 1.0f, 0.0f};
            Q[2] = {0.0f, 0.0f, 1.0f};

            return Q;
        }
        else if (parallelCheck == -1.0f) {
            std::array<std::array<float,3>,3> Q;
            
            // identity matrix inverts direction
            Q[0] = {-1.0f, 0.0f, 0.0f};
            Q[1] = {0.0f, -1.0f, 0.0f};
            Q[2] = {0.0f, 0.0f, -1.0f};

            return Q;

        }


        const float theta = acos(dot(n_norm, v_norm));
        const Vector<3,float> b = normalize(cross(Vector<3,float>(n_norm), Vector<3,float>(v_norm)));

        const float q0 = cos(theta/2.0f);
        const float q1 = sin(theta/2.0f) * b[0];
        const float q2 = sin(theta/2.0f) * b[1];
        const float q3 = sin(theta/2.0f) * b[2];

        // set the rotation matrix
        std::array<std::array<float,3>,3> Q;
        Q[0] = {q0*q0 + q1*q1 - q2*q2 - q3*q3,  2.0f*(q1*q2 - q0*q3),           2.0f*(q1*q3 + q0*q2)};
	    Q[1] = {2.0f*(q2*q1 + q0*q3),           q0*q0 - q1*q1 + q2*q2 - q3*q3,  2.0f*(q2*q3 - q0*q1)};
	    Q[2] = {2.0f*(q3*q1 - q0*q2),           2.0f*(q3*q2 + q0*q1),           q0*q0 - q1*q1 - q2*q2 + q3*q3};

        return Q;
    }
    
    /**
     * Special case of compute to center v on the x-axis
     */
    static std::array<std::array<float,3>,3> i(std::array<float,3> v) {
        return SpinMatrix::compute(v, {1.0f, 0.0f, 0.0f});
    }
    
    /**
     * Special case of compute to center v on the y-axis
     */
    static std::array<std::array<float,3>,3> j(std::array<float,3> v) {
        return SpinMatrix::compute(v, {0.0f, 1.0f, 0.0f});
    }

    /**
     * Special case of compute to center v on the z-axis
     */
    static std::array<std::array<float,3>,3> k(std::array<float,3> v) {
        return SpinMatrix::compute(v, {0.0f, 0.0f, 1.0f});
    }
};

#endif

