#ifndef KERNELS_SOFTWARE_HEMISPHERE_HPP_
#define KERNELS_SOFTWARE_HEMISPHERE_HPP_

#include <FullMonteSW/Config.h>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Kernels/Software/SSEMath.hpp>
#include <FullMonteSW/Geometry/UnitVector.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/SpinMatrix.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/ThetaDistribution.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <iomanip>
#include <iostream>
#include <array>
#include <math.h>
#include <cmath>
#include <stdio.h>

/**
 * An emitter Direction that emits in the hemisphere oriented about a specific vector
 * This is a simpler case of the Fiber cone with 'full' aperture (a cone with an angle of 90 degrees)
 */
namespace Emitter {
    template <class RNG> class HemiSphere {
        public:
            HemiSphere(unsigned tetraID, bool emitNormal, unsigned faceID, float NA, std::string thetaDist, const MaterialSet* materialset, const TetraMesh* mesh) : 
                        m_NA(NA), m_materialset(materialset), m_mesh(mesh)  {
                //// track normal ////
                // get 'normal' vector based on the choice of normal or anti-normal
                UnitVector<3,double> n;
                if(emitNormal) {
                    n = get(face_normal, *m_mesh, TetraMesh::FaceDescriptor(faceID));
                } else {
                    n = get(face_anti_normal, *m_mesh, TetraMesh::FaceDescriptor(faceID));
                }
                m_norm = { (float)n[0], (float)n[1], (float)n[2] };

                m_thetaDistribution = ThetaDistributionFunc::fromString(thetaDist);
                
                //// compute rotation matrix to center z-axis on normal ////
                Q = SpinMatrix::k(m_norm);

                //// determine refractive index of the tetra we are emitting into
                unsigned region_index = get(region,*m_mesh, TetraMesh::TetraDescriptor(tetraID));
                m_refractiveIndex = new float(m_materialset->get(region_index)->refractiveIndex());
            }

            PacketDirection direction(RNG& rng, SSE::Point3 /*p*/) const {
                // generate random angles
                const float rnd0 = *rng.floatU01();
                const float rnd1 = *rng.floatU01();

                // compute phi angle (in x-y plane)
                const float phi = 2.0f*M_PI*rnd0;

                // compute the theta angle (angle of elevation towards z-axis) based on chosen distribution
                float theta;
                if(m_thetaDistribution == UNIFORM) {
                    // this results in values between 0 and PI
                    theta = acos(2.0f*rnd1 - 1.0f);
                } else if(m_thetaDistribution == LAMBERT) {
                    theta = asin(rnd1);
                } else if(m_thetaDistribution == CUSTOM)
                {
                    if (m_NA/m_refractiveIndex[0] <= 1.0f)
                        theta = rnd1 * asin(m_NA/m_refractiveIndex[0]);
                    else
                        theta = rnd1 * asin(1.0f);
                    
                } else {
                    LOG_ERROR << "HemiSphere::direction unknown emission distribution, defaulting to UNIFORM" << endl;
                    theta = acos(2.0f*rnd1 - 1.0f);
                }
                
                // generate unit vector on hemi-sphere oriented about the z-axis
                const std::array<float,3> p_ijk {
                    sin(theta)*sin(phi),
                    sin(theta)*cos(phi),
                    abs(cos(theta))  // abs() makes it a hemisphere, rather than a sphere
                };

                // rotate to orient around the tetra face normal using the rotation matrix
                const array<float,3> p_uvw {
                    p_ijk[0]*Q[0][0] + p_ijk[1]*Q[0][1] + p_ijk[2]*Q[0][2],
                    p_ijk[0]*Q[1][0] + p_ijk[1]*Q[1][1] + p_ijk[2]*Q[1][2],
                    p_ijk[0]*Q[2][0] + p_ijk[1]*Q[2][1] + p_ijk[2]*Q[2][2]
                };

                // create a PacketDirection from the vector we just created
                SSE::UnitVector3 d = SSE::UnitVector3::normalize(SSE::Vector3(p_uvw));
                SSE::UnitVector3 a,b;
                tie(a,b) = SSE::normalsTo(d);
                // LOG_INFO << "theta: " << theta << "; p_ijk: " << p_ijk[0] << ' ' << p_ijk[1] << ' ' << p_ijk[2] << "; direction: "<< d[0] << ' ' << d[1] << ' ' << d[2] << endl;
                return PacketDirection(d,a,b);
            }

            /**
             * Set the theta distribution
             * @param the distribution enum to use
             */
            void setThetaDistribution(ThetaDistribution d) {
                m_thetaDistribution = d;
            }
            
            /**
             * Set the theta distribution by a string
             * @param the distribution in string form (currently only: "UNIFORM", "LAMBERT" or "CUSTOM")
             */
            void setThetaDistribution(std::string dStr) {
                m_thetaDistribution = ThetaDistributionFunc::fromString(dStr); 
            }

        private:

            float m_NA=1.0;

            //pointer to the material set
            const MaterialSet* m_materialset;

            // the tetra mesh being emitted from
            const TetraMesh* m_mesh;
            
            // refractive index of the material the photon is launched in
            float* m_refractiveIndex=nullptr; 

            // the normal vector the sphere is oriented on
            std::array<float,3> m_norm;

            // the rotation matrix
	        std::array<std::array<float,3>,3> Q;

            // the type of emittion distribution for the theta angle:
            //   UNIFORM: theta randomly distributed from -90 to 90
            //   LAMBERT: theta distributed based on Lambert's cosine law (https://www.particleincell.com/2015/cosine-distribution/)
            //   CUSTOM: theta is defined by the nuremical aperture theta = asin(NA/n)
            ThetaDistribution m_thetaDistribution=UNIFORM;
    };
};

#endif

