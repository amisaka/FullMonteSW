/*
 * TetraInternalKernel.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include "TetraInternalKernel.hpp"

#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>

// in addition to the normal kernel preparations, also mark faceFlags in Tetra structure for faces that need scoring

void TetraInternalKernel::parentPrepare()
{
	TetraMCKernel<RNG_SFMT_AVX,InternalScorer>::parentPrepare();

	const TetraMesh* M = dynamic_cast<const TetraMesh*>(geometry());
	if (!M)
	{
		throw std::logic_error("TetraInternalKernel::parentPrepare() given invalid geometry (can't cast to TetraMesh*)");
	}
}

