/*
 * PacketPostmortem.hpp
 *
 *  Created on: Sep 26, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_LOGGER_PACKETPOSTMORTEM_HPP_
#define KERNELS_SOFTWARE_LOGGER_PACKETPOSTMORTEM_HPP_

#include <FullMonteSW/Kernels/Event.hpp>

#include <mutex>
#include <vector>

#include <FullMonteSW/Kernels/Software/PacketDirection.hpp>

#include "BaseLogger.hpp"
#include "AbstractScorer.hpp"

using namespace std;

class PacketPostmortem : public AbstractScorer
{
public:
	PacketPostmortem();
	virtual ~PacketPostmortem();

	virtual void prepare(Kernel* K) override;
	virtual void clear() override;
	virtual void postResults(OutputDataCollection* C) const override;

	class Logger;

	Logger createLogger();


	// accept a commit from a logger
	void commit(Logger& L);
};


/** Traces the photon's path into a list of vectors in memory.
 *
 * Registers a chance in tetra when it is absorbed.
 */

class PacketPostmortem::Logger : public BaseLogger
{
public:
	Logger();
	Logger(Logger&& lm_) = default;
	Logger(const Logger& lm_)=delete;

	~Logger();

    void eventDie(AbstractScorer& S,double w);
    void eventNewTetra(AbstractScorer&,const Packet& pkt,const Tetra& T0,const unsigned tetraFaceIndex);

    /// Commit is a no-op
    inline void eventCommit(AbstractScorer&){}

    void eventClear(AbstractScorer&);

private:
    PacketDirection		m_direction;
    friend class PacketPostmortem;
};



#endif /* KERNELS_SOFTWARE_LOGGER_PACKETPOSTMORTEM_HPP_ */
