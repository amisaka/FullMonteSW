#include "AbstractScorer.hpp"
#include "BaseLogger.hpp"

#include <list>
#include <vector>
#include <mutex>
#include <string>

/** Scorer to record memory traces
 *
 * Derived classes must:
 * 		Define class Logger meeting the appropriate concept
 * 		Provide Logger createLogger();
 * 		override traceName() to specify the name of the trace provided
 */

class Kernel;
class OutputDataCollection;

class AccumulationEventScorer : public AbstractScorer
{
public:
	AccumulationEventScorer();
	virtual ~AccumulationEventScorer();

	virtual void clear()									override final;
	virtual void prepare(Kernel* K)						override final;
	virtual void postResults(OutputDataCollection* C) 	const override final;

// Required for Scorer concept, but provided by derived classes
//	class Logger;
//	Logger createLogger();


protected:
	/// Base class for logger functions - manages a memory trace and provides means to merge it
	/// Concrete derived classes must overload the appropriate event methods
	class LoggerBase;

	typedef struct {
	    int 		Tet_or_FaceID;
	    double 		weight_increment;
	} EventRecord;

	virtual const std::string& traceName() const=0;		///< Define the name assigned to the output data
	void merge(const std::vector<EventRecord>& v);		///< Copy the provided vector into the list (thread-safe, uses mutex)

private:
	std::mutex								m_traceListMutex;
	std::list<std::vector<EventRecord>>		m_traceList;
};




/** Incomplete base class for memory trace logger.
 * Provides the means to accumulate a memory trace, and to merge it back to a parent AccumulationEventScorer.
 *
 * Derived classes use the merge(S) and record(address,weight) functions to report events up to the parent class.
 * They need to overload the logger event methods to customize them for a particular purpose.
 */

class AccumulationEventScorer::LoggerBase : public BaseLogger
{
public:
	LoggerBase();
	~LoggerBase();


protected:
    void merge(AccumulationEventScorer& S);					///< Merge the trace to the master trace set
    void record(int ID,double w);		///< Record increment weight to a given address

private:
    std::vector<EventRecord>		m_trace;
};




/** Concrete derived class that scores geometry hits.
 *
 * Scores the absoprtion and exit event of the photons in the tetrahedral mesh.
 *
 * Contributes a TetraAccumulationEventSet result named "TetraAccumulationEvent"
 *
 */


class TetraAccumulationEventScorer : public AccumulationEventScorer
{
public:
	TetraAccumulationEventScorer();
	virtual ~TetraAccumulationEventScorer();

	class Logger : public AccumulationEventScorer::LoggerBase
	{
	public:
		inline void eventExit(AbstractScorer&,const Ray3,int IDf,double w)
		{ record(-abs(IDf>>1),w); };

		inline void eventAbsorb(AbstractScorer&,const Point3,unsigned IDt,double, double absorbed_weight)
			{ record(IDt,absorbed_weight); };

		inline void eventTerminate(AbstractScorer& S,const Packet&)
			{ merge(static_cast<AccumulationEventScorer&>(S)); }
	};

	Logger createLogger();

protected:
	static std::string s_traceName;
	const std::string& traceName() const { return s_traceName; }
};

