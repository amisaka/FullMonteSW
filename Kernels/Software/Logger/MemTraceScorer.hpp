#include "AbstractScorer.hpp"
#include "BaseLogger.hpp"

#include <list>
#include <vector>
#include <mutex>
#include <string>

/** Scorer to record memory traces
 *
 * Derived classes must:
 * 		Define class Logger meeting the appropriate concept
 * 		Provide Logger createLogger();
 * 		override traceName() to specify the name of the trace provided
 */

class Kernel;
class OutputDataCollection;

class MemTraceScorer : public AbstractScorer
{
public:
	MemTraceScorer();
	virtual ~MemTraceScorer();

	virtual void clear()									override final;
	virtual void prepare(Kernel* K)						override final;
	virtual void postResults(OutputDataCollection* C) 	const override final;

// Required for Scorer concept, but provided by derived classes
//	class Logger;
//	Logger createLogger();


protected:
	/// Base class for logger functions - manages a memory trace and provides means to merge it
	/// Concrete derived classes must overload the appropriate event methods
	class LoggerBase;

	typedef struct {
	    unsigned 	address;
	    unsigned 	count;
	} RLERecord;

	virtual const std::string& traceName() const=0;		///< Define the name assigned to the output data
	void merge(const std::vector<RLERecord>& v);		///< Copy the provided vector into the list (thread-safe, uses mutex)

private:
	std::mutex								m_traceListMutex;
	std::list<std::vector<RLERecord>>		m_traceList;
};




/** Incomplete base class for memory trace logger.
 * Provides the means to accumulate a memory trace, and to merge it back to a parent MemTraceScorer.
 *
 * Derived classes use the merge(S) and record(address,count) functions to report events up to the parent class.
 * They need to overload the logger event methods to customize them for a particular purpose.
 */

class MemTraceScorer::LoggerBase : public BaseLogger
{
public:
	LoggerBase();
	~LoggerBase();


protected:
    void merge(MemTraceScorer& S);					///< Merge the trace to the master trace set
    void record(unsigned address,unsigned N=1);		///< Record some number of accesses to a given address

private:
    std::vector<RLERecord>		m_trace;
};




/** Concrete derived class that scores geometry hits.
 *
 * Scores the memory access patterns of the photons in the tetrahedral mesh. The memory trace's repeat count is the number of
 * absorption accesses. If it is zero, it means the photon passed through without absorption.
 *
 * Contributes a MemTraceSet result named "TetraMemTrace"
 *
 */


class TetraMemTraceScorer : public MemTraceScorer
{
public:
	TetraMemTraceScorer();
	virtual ~TetraMemTraceScorer();

	class Logger : public MemTraceScorer::LoggerBase
	{
	public:
		inline void eventLaunch(AbstractScorer&,const Ray3,unsigned IDt,double)
			{ record(IDt,0); };

		inline void eventAbsorb(AbstractScorer&,const Point3,unsigned IDt,double,double)
			{ record(IDt,1); };

		inline void eventNewTetra(AbstractScorer&,const Packet&,const Tetra&,const unsigned,unsigned IDt_new,const x86Kernel::Material&)
			{ record(IDt_new,0); };

		inline void eventTerminate(AbstractScorer& S,const Packet&)
			{ merge(static_cast<MemTraceScorer&>(S)); }
	};

	Logger createLogger();

protected:
	static std::string s_traceName;
	const std::string& traceName() const { return s_traceName; }
};

