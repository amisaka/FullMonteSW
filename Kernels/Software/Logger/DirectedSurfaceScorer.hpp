/*
 * InternalSurfaceScorer.hpp
 *
 *  Created on: Sep 28, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_LOGGER_DIRECTEDSURFACESCORER_HPP_
#define KERNELS_SOFTWARE_LOGGER_DIRECTEDSURFACESCORER_HPP_

#include "AbstractScorer.hpp"
#include "BaseLogger.hpp"

#include "../Tetra.hpp"
#include "../Material.hpp"

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>

#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/Kernels/Kernel.hpp>
#include <vector>

#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

class Permutation;
class GeometryPredicate;
class VolumeCellPredicate;
class SurfaceCellPredicate;

/** Marshal the common non-templated functions for concrete DirectedSurfaceScorer
 */

class AbstractDirectedSurfaceScorer : public AbstractScorer
{
public:
	AbstractDirectedSurfaceScorer();
	virtual ~AbstractDirectedSurfaceScorer();

	// DirectedSurface-specific items
	void addScoringRegionBoundary(VolumeCellPredicate* vol);		///< Adds a region whose boundary will be scored
	void addScoringSurface(SurfaceCellPredicate* surf);			///< Adds a surface to score

	void removeScoringRegionBoundary(VolumeCellPredicate* vol);	///< Remove a region from boundary scoring
	void removeScoringSurface(SurfaceCellPredicate* surf);		///< Remove a surface from scoring

	void numericalAperture(float NA);
	float numericalAperture() const;

	Permutation*			getOutputSurface(unsigned i) const;
	unsigned				getNumberOfOutputSurfaces() const;

protected:
	void createOutputSurfaces(Kernel* K);						///< Prepare output surfaces for scoring
	float m_NA=-2;
private:
	std::vector<GeometryPredicate*>	m_outputCellPredicates;

	void clearSurfaces();
	void addGeometryPredicate(GeometryPredicate*);
	void removeGeometryPredicate(GeometryPredicate*);

	/** Permutation mapping from all directed faces to output exit undirected faces.
	 * Low bit of directed face index indicates if entering direction is inverted with respect to the undirected face.
	 */
	std::vector<Permutation*>		m_outputSurfaces;
};



/** Scores the directed fluence passing through an interior surface.
 *
 * Relies on a per-face flag set in the kernel Tetra; for the i'th face of the tetra, fluence will be scored if (flags>>(i<<3)) & 0x01.
 * (ie. for tetra i's j'th face, m_tetras[i].faceFlags bit number 8*j)
 *
 * Posts one DirectedSurface result per input face-set spec.
 */

template<class AccumulatorT>class DirectedSurfaceScorer : public AbstractDirectedSurfaceScorer
{
public:
	DirectedSurfaceScorer();
	~DirectedSurfaceScorer();

	typedef AccumulatorT Accumulator;

	/// Scorer required overrides
	virtual void prepare(Kernel* K) override;
	virtual void clear() override;
	virtual void postResults(OutputDataCollection* C) const override;

	Accumulator& accumulator(){ return m_accumulator; }

#ifndef SWIG
	class Logger : public BaseLogger
	{
	public:
		Logger(typename Accumulator::ThreadHandle&& h, float NA) : m_handle(std::move(h)), m_NA(NA){}
		inline void eventNewTetra(AbstractScorer& S,const Packet& pkt,const Tetra& tet,const unsigned idx,const unsigned /*IDt_new*/,const x86Kernel::Material& currMat)
		{
			auto& DS = static_cast<DirectedSurfaceScorer&>(S);

			unsigned IDfd=tet.IDfds[idx];
			
			if ((tet.faceFlags >> (idx << 3)) & 0x1)		// check if LSB of faceFlags is set for this face
			{
				if (m_NA != -2)
				{
					std::array<float,3> face_normal = tet.face_normal(idx);
					// Calculate the if the angle of incidence is less than the maximum angle where the incoming packet should be detected
					SSE::Vector3 normal(face_normal);
					//normal = normal*SSE::Scalar(-1); // -1 as we want to measure the incidence angle going towards the proximal end
					float angle = std::acos(abs(float(dot(normal, pkt.direction())))); // both are unit vectors

					// Check if angle is less than theta_max from NA
					if (m_NA/currMat.n <= 1 && angle <= std::asin(m_NA/currMat.n))
						m_handle.accumulate(DS.m_accumulator,IDfd,pkt.weight());
				}
				else
				{
					m_handle.accumulate(DS.m_accumulator,IDfd,pkt.weight());
				}
				
				
			}
		}

		void eventCommit(AbstractScorer& S)
			{ m_handle.commit(static_cast<DirectedSurfaceScorer&>(S).m_accumulator); }

		void eventClear(AbstractScorer&)
			{ m_handle.clear(); }
	private:
		typename Accumulator::ThreadHandle m_handle;
		float m_NA;
	};


	Logger createLogger();
#endif

private:

	Accumulator m_accumulator;
};



template<class AccumulatorT>DirectedSurfaceScorer<AccumulatorT>::DirectedSurfaceScorer()
{
}

template<class AccumulatorT>DirectedSurfaceScorer<AccumulatorT>::~DirectedSurfaceScorer()
{
}



/** Post a DirectedSurface result for each of the surfaces listed.
 *
 */

template<class AccumulatorT>void DirectedSurfaceScorer<AccumulatorT>::postResults(OutputDataCollection *C) const
{
	for(unsigned i=0;i<getNumberOfOutputSurfaces();++i)
	{
		Permutation* perm = getOutputSurface(i);

		// set up the outputs
		SpatialMap<float>* exiting  = new SpatialMap<float>(perm->dim(),AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);

		SpatialMap<float>* entering = exiting->clone();

		DirectedSurface* ds = new DirectedSurface();
		ds->outputType(AbstractSpatialMap::PhotonWeight);
		ds->entering(entering);
		ds->exiting(exiting);
		ds->permutation(perm);

		switch (getUnit())
		{
		case Kernel::Joule:
			switch (getDimension())
			{
				case DimensionUnit::m:
					ds->unitType(AbstractSpatialMap::J_m);
					break;
				case DimensionUnit::cm:
					ds->unitType(AbstractSpatialMap::J_cm);
					break;
				case DimensionUnit::mm:
					ds->unitType(AbstractSpatialMap::J_mm);
					break;
				default:
					throw std::logic_error("DirectedSurfaceScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Joule' but dimension unit is set to 'NONE'");
					break;
			}
			break;
		case Kernel::Watt:
			switch (getDimension())
			{
				case DimensionUnit::m:
					ds->unitType(AbstractSpatialMap::W_m);
					break;
				case DimensionUnit::cm:
					ds->unitType(AbstractSpatialMap::W_cm);
					break;
				case DimensionUnit::mm:
					ds->unitType(AbstractSpatialMap::W_mm);
					break;
				default:
					throw std::logic_error("DirectedSurfaceScorer::postResults(C) Impossible unit combination - Photon weight should be interpreted as 'Watt' but dimension unit is set to 'NONE'");
					break;
			}
			break;
		
		default:
			switch (getDimension())
			{
				case DimensionUnit::m:
					throw std::logic_error("DirectedSurfaceScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'm'");
					break;
				case DimensionUnit::cm:
					throw std::logic_error("DirectedSurfaceScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'cm'");
					break;
				case DimensionUnit::mm:
					throw std::logic_error("DirectedSurfaceScorer::postResults(C) Impossible unit combination - Raw photon weight should be used (unit is set to 'NONE') but dimension unit is set to 'mm'");
					break;
				default:
					break;
			}
			break;
		}

		for(unsigned i=0;i<perm->dim(); ++i)
		{
			unsigned exitIdx = perm->get(i);

			// Low-bit of directed face index is surface direction (0=with normal, 1=against normal)
			// Face normals point inside the tetra, so if tetra is inside region then normal points inside region
			// Permutation low-bit indicates whether to invert or not for entering
			exiting->set(i,m_accumulator[exitIdx^1]);
			entering->set(i,m_accumulator[exitIdx]);
		}

		
		ds->name("DirectedSurfaceEnergy");
		C->add(ds);
	}
}

// Clear accumulator, ensure correct size (one entry per directed face), and create output surfaces
template<class AccumulatorT>void DirectedSurfaceScorer<AccumulatorT>::prepare(Kernel* K)
{
	const TetraMesh* M = dynamic_cast<const TetraMesh*>(K->geometry());

	if(!M)
		throw std::logic_error("DirectedSurfaceScorer::prepare() invalid cast to TetraMesh");

	m_accumulator.resize(get(num_directed_faces,*M));
	m_accumulator.clear();

	createOutputSurfaces(K);

	setDimension(K->geometry()->getUnit());
	setUnit(K->getUnit());
}

template<class AccumulatorT>void DirectedSurfaceScorer<AccumulatorT>::clear()
{
	m_accumulator.clear();
}

template<class AccumulatorT>typename DirectedSurfaceScorer<AccumulatorT>::Logger DirectedSurfaceScorer<AccumulatorT>::createLogger()
{
	return Logger(m_accumulator.createThreadHandle(), m_NA);
}



#endif /* KERNELS_SOFTWARE_LOGGER_DIRECTEDSURFACESCORER_HPP_ */
