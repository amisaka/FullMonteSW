/*
 * AbstractScorer.hpp
 *
 *  Created on: Sep 29, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_LOGGER_ABSTRACTSCORER_HPP_
#define KERNELS_SOFTWARE_LOGGER_ABSTRACTSCORER_HPP_

/** Scorer Concept
 *
 * A Scorer manages the scoring process, including the following responsibilities:
 *
 * 	1) Acting as a Logger factory when the Kernel starts up
 * 	2) Conversion of the Logger information into a finalized OutputData*
 *
 *
 *
 *
 * Concrete class overrides
 *
 * 		virtual postResults(OutputDataCollection*)				Posts its results to the specified collection
 * 		virtual void prepare(Kernel* K)							Prepares to score the results of specified kernel
 * 		virtual void clear()									Resets all scores to a default state
 *
 *
 * Concept requirements
 *
 * 		Default-constructible
 * 		typedef Logger											The type of logger created
 * 		Logger createLogger();									Logger factory
 */

#include <FullMonteSW/Kernels/Kernel.hpp>
#include <FullMonteSW/Geometry/Geometry.hpp>

class OutputDataCollection;

class AbstractScorer
{
public:
	virtual ~AbstractScorer();

	virtual void clear()=0;
	virtual void prepare(Kernel* K)=0;
	virtual void postResults(OutputDataCollection* o) const=0;

	void setUnit(Kernel::EnergyPower unit) { m_unit = unit; }
	Kernel::EnergyPower getUnit() const { return m_unit; }

	void setDimension(DimensionUnit dimension) { m_dim = dimension; }
	DimensionUnit getDimension() const {return m_dim; }

protected:
	AbstractScorer();

private:
	Kernel::EnergyPower m_unit=Kernel::EnergyPower::NONE;
	DimensionUnit m_dim=DimensionUnit::NONE;
};

#endif /* KERNELS_SOFTWARE_LOGGER_ABSTRACTSCORER_HPP_ */
