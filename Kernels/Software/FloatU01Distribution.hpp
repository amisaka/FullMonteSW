/*
 * FloatU01Distribution.hpp
 *
 *  Created on: Feb 17, 2016
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_FLOATU01DISTRIBUTION_HPP_
#define KERNELS_SOFTWARE_FLOATU01DISTRIBUTION_HPP_

#include "FullMonteSW/Config.h"
//#include <iostream>
//#include <fstream>

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
#include "FloatVectorBase.hpp"
#else
#include "FloatVectorBase_NonSSE.hpp"
#endif

/** Returns 8 floats in U [0,1) distribution */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
class FloatU01Distribution : public FloatVectorBase
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=1;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* ip = rng.getBlock();

		__m256i i = _mm256_load_si256((const __m256i*)ip);
		_mm256_store_ps(dst, ui32ToU01(i));
		
		//std::ofstream outfile;
    	//outfile.open("FloatU01SSE.txt", std::ios_base::app);
		//outfile << "Result exprnd: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
	}
};
#else
class FloatU01Distribution : public FloatVectorBase_NonSSE
{
public:
	typedef float					result_type;
	static constexpr std::size_t	OutputsPerInputBlock=8;		///< Outputs generated per invocation
	static constexpr std::size_t	OutputElementSize=1;		///< Number of output elements per input

	typedef uint32_t				input_type;
	static constexpr std::size_t	InputBlockSize=8;			///< Inputs consumed per invocation

	template<class RNG>void calculate(RNG& rng,float *dst)
	{
		const uint32_t* ip = rng.getBlock();

		std::array<long long, 4> i;
		std::copy((long long*) ip, (long long*) ip+4, i.data());
		std::array<float, 8> result = ui32ToU01(i);
		std::copy(result.begin(), result.end(), dst);

		//std::ofstream outfile;
    	//outfile.open("FloatU01NonSSE.txt", std::ios_base::app);
		//outfile << "Result exprnd: "<< dst[0]<< " " << dst[1] << " "<< dst[2] <<" "<< dst[3]<< " "<< dst[4]<< " " << dst[5] << " "<< dst[6] <<" "<< dst[7]<<std::endl;
	}
};
#endif



#endif /* KERNELS_SOFTWARE_FLOATU01DISTRIBUTION_HPP_ */
