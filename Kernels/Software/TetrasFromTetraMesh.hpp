/*
 * TetrasFromTetraMesh.hpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_TETRASFROMTETRAMESH_HPP_
#define KERNELS_SOFTWARE_TETRASFROMTETRAMESH_HPP_


/** Creates kernel tetras from a TetraMesh description
 *
 *
 */

#include "Tetra.hpp"
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <vector>

class Partition;
class TetraMesh;

class TetrasFromTetraMesh
{
public:
	TetrasFromTetraMesh();
	~TetrasFromTetraMesh();

	void mesh(const TetraMesh* M);
	void update();

	/// Checks that the kernel faces make sense
	bool checkKernelFaces() const;

	const std::vector<Tetra>& tetras() const;

private:

	void makeKernelTetras();

	static Tetra convert(const TetraMesh& M,TetraMesh::TetraDescriptor T);

	/** Packed and aligned tetra representation for the kernel, holding face normals, constants, adjacent tetras,
	 * bounding faces, material ID, and face flags (for logging).
	 */

	const TetraMesh*				m_mesh=nullptr;
    std::vector<Tetra>       	m_tetras;
};

#endif /* KERNELS_SOFTWARE_TETRASFROMTETRAMESH_HPP_ */
