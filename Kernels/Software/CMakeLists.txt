# create FullMonteSWKernel shared library from the source files
ADD_LIBRARY(FullMonteSWKernel SHARED
    SSEMath.cpp
    ThreadedMCKernelBase.cpp
    PacketDirection.cpp
    HenyeyGreenstein.cpp
    TetraMCKernel.cpp
	Material.cpp
	MCMLKernel.cpp
	TetraInternalKernel.cpp
	Logger/VolumeAbsorptionScorer.cpp
    Logger/SurfaceExitScorer.cpp
    Logger/AbstractScorer.cpp
	Logger/DirectedSurfaceScorer.cpp
	Logger/PathScorer.cpp
    Logger/EventScorer.cpp
    Logger/ConservationScorer.cpp
    Logger/PacketPostmortem.cpp
    Logger/MemTraceScorer.cpp
    Logger/AccumulationEventScorer.cpp
	Tetra.cpp
    TetrasFromTetraMesh.cpp
    TetrasFromLayered.cpp
)

TARGET_LINK_LIBRARIES(FullMonteSWKernel
    SFMT
    FullMonteLogging
    FullMonteGeometry
    FullMonteKernelBase
    FullMonteData
    FullMonteMCMLFile
    FullMonteGeometryPredicates
    FullMonteLogging
)

# build FullMonteSWKernel
INSTALL(TARGETS FullMonteSWKernel
    DESTINATION lib)

INSTALL(FILES avx_mathfun.h sse_mathfun.h
    DESTINATION include/Kernels/Software)

IF(WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteSWKernel.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMonteSWKernelTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMonteSWKernel.i
    )
    SWIG_LINK_LIBRARIES(FullMonteSWKernelTCL 
        ${TCL_LIBRARY} 
        FullMonteGeometry 
        FullMonteSWKernel 
        FullMonteData
    )
    INSTALL(TARGETS FullMonteSWKernelTCL LIBRARY 
        DESTINATION lib
    )
ENDIF()

IF(WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMonteSWKernel.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(SWKernel 
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMonteSWKernel.i
    )
    SWIG_LINK_LIBRARIES(SWKernel 
        ${Python3_LIBRARIES} 
        FullMonteGeometry 
        FullMonteSWKernel 
        FullMonteData
    )
    SET_TARGET_PROPERTIES(_SWKernel PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()