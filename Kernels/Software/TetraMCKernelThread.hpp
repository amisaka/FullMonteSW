#ifndef MAINLOOP_HPP_INCLUDED_
#define MAINLOOP_HPP_INCLUDED_

#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <iomanip>
#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernel.hpp>

#include <FullMonteSW/Kernels/Software/Emitters/CylDetector.hpp>

#include "BlockRandomDistribution.hpp"
#include "HenyeyGreenstein.hpp"
#include "FloatUnitExpDistribution.hpp"
#include "FloatU01Distribution.hpp"

#include "Material.hpp"

#include "AutoStreamBuffer.hpp"
#include "Logger/BaseLogger.hpp"
#include "Tetra.hpp"

// Removing MAX_MATERIALS constraint
//#define MAX_MATERIALS 32

//class template
template<class RNGType,class Scorer>class alignas(32) TetraMCKernel<RNGType,Scorer>::Thread : public ThreadedMCKernelBase::Thread
{
public:
	typedef RNGType RNG;

	~Thread(){ }

    // move-constructs the logger and gets thread ready to run but does not actually start it (call doWork())
	Thread(TetraMCKernel<RNGType,Scorer>& K,Logger&& logger_);

    /// Main body which does all the work
    void doWork(ThreadedMCKernelBase* kernel);
    int doOnePacket(TetraMCKernel<RNGType,Scorer>* kernel,LaunchPacket pkt);

    /// Seed the RNG
    void seed(unsigned s)
    {
    	m_rng.seed(s);
    }

    inline bool scatter(Packet& pkt,const x86Kernel::Material& mat,const Tetra& region);

private:
	Logger 	logger;											///< Logger object
    RNG 	m_rng;											///< Random-number generator
};


// move-constructs the logger and gets thread ready to run but does not actually start it (call start())
template<class RNG,class Scorer>TetraMCKernel<RNG,Scorer>::Thread::Thread(TetraMCKernel<RNG,Scorer>& K,Logger&& logger_) :
	logger(std::move(logger_))
{
	m_rng.hgSetSize(K.materials()->size());

	for(unsigned i=0;i<K.materials()->size(); ++i)
		m_rng.gParamSet(i,K.materials()->get(i)->anisotropy());
}

template<class RNG,class Scorer>void TetraMCKernel<RNG,Scorer>::Thread::doWork(ThreadedMCKernelBase* kernel)
{
	LaunchPacket lpkt;
	if (!kernel)
		throw std::logic_error("No kernel provided");

	TetraMCKernel<RNG,Scorer>* K = static_cast<TetraMCKernel<RNG,Scorer>*>(kernel);

	if (!K->m_emitter)
		throw std::logic_error("Missing emitter");

	for( ; m_nPktDone < m_nPktReq; ++m_nPktDone)
	{
		lpkt = K->m_emitter->emit(m_rng, m_nPktReq, m_nPktDone);
		doOnePacket(K,lpkt);
	}

    K->updateDetectedWeight(); // update the Source::CylDetector structure to output the detected weight by each detector

	log_event(logger,K->m_scorer,Events::Commit);
}

enum TerminationResult { Continue=0, RouletteWin, RouletteLose, TimeGate, Detected, Other=-1  };

    /**
     * Conducts termination check (including roulette).
     *
     * @param rng Random-number generator
     * @param mat Material
     *
     * @returns pair<RouletteResult,double> = < result, dw> where dw is change in packet weight due to roulette
     *
     * @tparam RNG
     */

template<class RNG>pair<TerminationResult,double> terminationCheck(const double wmin,const double prwin,RNG& rng,Packet& pkt,const x86Kernel::Material& /*mat*/,const Tetra& /*region*/)
{
    // do roulette
	double w0=pkt.w;
    if (pkt.w < wmin)
    {
    	if (*rng.floatU01() < prwin)
    	{
    		pkt.w /= prwin;
    		return make_pair(RouletteWin,pkt.w-w0);
    	}
    	else
    		return make_pair(RouletteLose,-pkt.w);
    }
    else
    	return make_pair(Continue,0.0);
}

template<class RNGType,class Scorer>inline bool TetraMCKernel<RNGType,Scorer>::Thread::scatter(Packet& pkt,const x86Kernel::Material& /*mat*/,const Tetra& region)
{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
	__m128 spinmatrix = _mm_load_ps(m_rng.hg(region.matID));
    
#else
    m128 spinmatrix = *(const m128*) m_rng.hg(region.matID);

#endif

    pkt.dir = pkt.dir.scatter(SSE::Vector<4>(spinmatrix));
	return true;
}

inline pair<float,float> absorb(const Packet& pkt,const x86Kernel::Material& mat,const Tetra& /*tet*/)
{
	float w0 = pkt.weight();
	float dw = w0 * mat.absfrac;
	return make_pair(w0-dw,dw);
}

#ifdef USE_SSE
inline bool vec_isnan(__m128 x)
{
	return _mm_movemask_ps(x != x);
}
#else
inline bool vec_isnan(m128 x)
{
#warning "Suboptimal performance due to non-vectorized vec_isnan(x)"''
	for(unsigned i=0;i<3;++i)
		if (isnan(x[i]))
			return true;
	return false;
}
#endif

// Check for unit vector

#ifdef USE_SSE
template<std::size_t N>bool vec_isunit(__m128 x,float tol2)
{
	// upper 4 bits: inclusion mask (include bottom N elements)
	// lower 4 bits: result-destination mask (send to result[0] only)
	const int mask = (((1<<N)-1)<<4) | 0x1;
	__m128 res = _mm_sub_ps(_mm_dp_ps(x,x,mask),_mm_set1_ps(1.0));
	__m128 tol2v = _mm_set1_ps(tol2);

	return _mm_movemask_ps(res < tol2v) & _mm_movemask_ps(-res < tol2v) & 0x1;
}
#else
#warning "Suboptimal performance due to non-vectorized vec_isunit(x,tol2)"
#warning "NonSSE implementation of vec_isunit(x,tol2) needs checking"
template<std::size_t N>bool vec_isunit(m128 x,float tol2)
{
	return abs((dp_ps128(x,x,(0x7<<4)|0x1))[0]-1.0) < tol2;
}
#endif


template<class RNG,class Scorer>int TetraMCKernel<RNG,Scorer>::Thread::doOnePacket(TetraMCKernel<RNG,Scorer>* kernel,LaunchPacket lpkt)
{
    unsigned Nhit,Nstep;
    StepResult stepResult;
    // Select the current Tetra from which the photons should be launched
    // The tetraID is stored in lpkt.element
    Tetra currTetra = kernel->m_tetras[lpkt.element];

    // Get the material of the current Tetra
    x86Kernel::Material currMat = kernel->m_mats[currTetra.matID];

    unsigned IDt=lpkt.element;

    // f_tmp[4] represents the refractive indices of neighboring tetras and the ratio of them
	// It is only required for reflect and refract calculations
    float f_tmp[4] __attribute__((aligned(16)));
    float &n1 = f_tmp[0];
    float &n2 = f_tmp[1];
    float &ratio = f_tmp[2];
    // Currently, we assume that the next step will be in the same tetra meaning we will not change the tetra during the hop stage of the simulation
    // This implies that the next material ID is queal to the current material ID.
    unsigned IDt_next=IDt, IDm=currTetra.matID, IDm_next=IDm, IDm_bound;

    
    Packet pkt(lpkt);
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
    log_event(logger,kernel->m_scorer,Events::Launch,std::make_pair(pkt.p,__m128(pkt.direction())),IDt,1.0);
#else
    log_event(logger,kernel->m_scorer,Events::Launch,std::make_pair(pkt.p,m128(pkt.direction())),IDt,1.0);
#endif
    // start another hop
    for(Nstep=0; Nstep < kernel->Nstep_max_; ++Nstep)
    {
    	TerminationResult term = Other;
    	if (pkt.dir.d.isnan() | pkt.dir.a.isnan() | pkt.dir.b.isnan() | vec_isnan(pkt.p) |
    			!pkt.dir.d.check(SSE::Silent,1e-4) | !pkt.dir.a.check(SSE::Silent,1e-4) | !pkt.dir.b.check(SSE::Silent,1e-4))
		{
        	{
        		AutoStreamBuffer b(cerr);
        		b << "Abnormal condition: position or direction is NaN at tetra " << IDt << "  Terminating packet\n";
        	}
            log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
            log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
			return 1;
		}

        assert(pkt.dir.d.check(SSE::Silent,1e-4) && pkt.dir.a.check(SSE::Silent,1e-4) && pkt.dir.b.check(SSE::Silent,1e-4));

//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
        // draw a hop length; pkt.s = { physical distance, Mean Free Paths (MFPs) to go, time, 0 }
        pkt.s = _mm_mul_ps(
        			_mm_load1_ps(m_rng.floatExp()), // generate one random number and store it 4 times into a m128 register
					currMat.m_init); // initial propagation vector (1/muT physical step remaining, 1 dimensionless step remaining, 0 time, 0 X)

        assert(!vec_isnan(pkt.s));

        // assert only is active when debugging, so leaving it commented for now
        assert(0 <= _mm_cvtss_f32(pkt.s));
        stepResult = currTetra.getIntersection(pkt.p,__m128(pkt.direction()),pkt.s);
        // Assumption: This assertion checks if the source placement actually makes sense. 
        // Why would you place a source in the air?
        assert(_mm_cvtss_f32(pkt.s) < 1e4);
#else
        const float* rng = m_rng.floatExp();
        m128 floatExp = {*rng, *rng, *rng, *rng};      

        pkt.s = mult128(
        			floatExp, // generate one random number and store it 4 times into a m128 register
					currMat.m_init); // initial propagation vector (1/muT physical step remaining, 1 dimensionless step remaining, 0 time, 0 X)

    	assert(0 <= pkt.s[0]);
        stepResult = currTetra.getIntersection(pkt.p,m128(pkt.direction()),pkt.s);
        

        // Assumption: This assertion checks if the source placement actually makes sense. 
        // Why would you place a source in the air?
        assert(pkt.s[0] < 1e3);
#endif
        
        // attempt hop
        pkt.oldP   = pkt.p;
        pkt.p      = stepResult.Pe;
//         if (!stepResult.hit) { // Call detection functionality
            // Detection logic: before termination, check if photon can be detected. 
            if (currTetra.detectorEnclosedIDs.size() > 0) // There is at least one detector enclosed in this tetra
            {
                for (auto detID : currTetra.detectorEnclosedIDs)
                {
                    // check if packet position intersects with the detector 
                    Emitter::Detector<RNG, Emitter::CylDetector<RNG>>* currDetector = dynamic_cast<Emitter::Detector<RNG, Emitter::CylDetector<RNG>>*>(kernel->m_detectors[detID - 1]);

                    if (currDetector == nullptr) {
                        AutoStreamBuffer b(cerr);
                        b << "Only cylindrical detector types are supported\n";
                    }
                    else {
                        float currWeight = pkt.w; 
                        currDetector->detect(m_rng, pkt, currMat.n);  
                        if (currWeight > pkt.w) // portion of it got detected, terminate
                        {
                            term = Detected; 
                            break;
                        }
                    }
                } // end detectors loop
            } // End checking for detectors
//         }

        // loop while hitting a face in current step. On every hit, we also check for intersection with detectors. 
        for(Nhit=0; term != Detected && stepResult.hit && Nhit < kernel->Nhit_max_; ++Nhit)
        {
        	enum Disposition { Undetermined, Transmit, SpecialTerminate, SpecialReflect, SpecialTransmit, TIR, FresnelReflect, Refract }
        		disposition=Undetermined;

            	if (pkt.dir.d.isnan() | pkt.dir.a.isnan() | pkt.dir.b.isnan() | vec_isnan(pkt.p) |
            			!pkt.dir.d.check(SSE::Silent,1e-4) | !pkt.dir.a.check(SSE::Silent,1e-4) | !pkt.dir.b.check(SSE::Silent,1e-4))
        		{
                	{
                		AutoStreamBuffer b(cerr);
                		b << "Abnormal condition: position or direction is NaN at tetra " << IDt << "  Terminating packet\n";
                	}
                    log_event(logger,kernel->m_scorer,Events::NoHit,pkt,currTetra);
                    log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
        			return 1;
        		}
            //cout << "  hit face " << stepResult.idx << "/4 into tetra " << stepResult.IDte << endl;
            // extremely rarely, this can be a problem; we get no match in the getIntersection routine
            if(stepResult.idx > 3)
            {
            	{
            		AutoStreamBuffer b(cerr);
            		b << "Abnormal condition: stepResult.idx=" << stepResult.idx << " (no tetra intersection), IDte=" << stepResult.IDte <<
            			"  Terminating packet\n" << "\tThis may happen occasionally but if it's more than a few ppm of packets, or consistently the same tetra that would be a concern.\n";
            	}
                log_event(logger,kernel->m_scorer,Events::NoHit,pkt,currTetra);
                log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                return -1;
            }
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here

            // Advance packet to boundary face
#ifdef USE_SSE
            pkt.s = _mm_add_ps(
            			pkt.s,
						_mm_mul_ps(
								stepResult.distance,
								currMat.m_prop));
#else
            pkt.s = add128(
            			pkt.s,
						mult128(
								stepResult.distance,
								currMat.m_prop));
#endif

            assert(!vec_isnan(pkt.s));

            IDm_bound = kernel->m_tetras[stepResult.IDte].matID;

            if (IDm == IDm_bound) { // no material change
            	log_event(logger,kernel->m_scorer,Events::Boundary,pkt.p,stepResult.IDfe,IDt,stepResult.IDte);
                IDt_next = stepResult.IDte;
            }
            else // boundary with material change
            {
            	// check face being crossed for reflective surface handling
            	if ((kernel->m_tetras[IDt].faceFlags >> ((stepResult.idx << 3)+Tetra::SpecialInterface)) & 0x1)
            	{
            		float reflective_WeightAdjustment=kernel->m_reflectiveFaceTable[IDm][IDm_bound].weight_multiplier;
            		float reflective_ReflectProbability=kernel->m_reflectiveFaceTable[IDm][IDm_bound].prob_reflect;

            		if (reflective_WeightAdjustment == 0)			// perfect absorber -> terminate
            		{
            			disposition = SpecialTerminate;
            			log_event(logger,kernel->m_scorer,Events::SpecialAbsorb,pkt.p,stepResult.IDfe,pkt.w);
        				log_event(logger,kernel->m_scorer,Events::SpecialTerminate,pkt,pkt.p,stepResult.IDfe);
        	        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
        	        	return 1;
            		}
            		else
            		{
            			if (reflective_ReflectProbability != 0 && (reflective_ReflectProbability == 1 || (*m_rng.floatU01() < reflective_ReflectProbability)))	// reflect
            			{
            				disposition = SpecialReflect;
#ifdef USE_SSE
                            log_event(logger,kernel->m_scorer,Events::SpecialReflect,pkt.p,__m128(pkt.direction()));
#else
                            log_event(logger,kernel->m_scorer,Events::SpecialReflect,pkt.p,m128(pkt.direction()));
#endif
            			}
            			else {
            				log_event(logger,kernel->m_scorer,Events::SpecialTransmit,pkt.p,stepResult.IDfe,IDt,stepResult.IDte);
            				disposition = SpecialTransmit;
            			}

            			if (reflective_WeightAdjustment != 1)
            			{
            				log_event(logger,kernel->m_scorer,Events::SpecialAbsorb,pkt.p,stepResult.IDfe,pkt.w*(1-reflective_WeightAdjustment));
            				pkt.w *= reflective_WeightAdjustment;
            			}
            		}
            	}

                n2 = kernel->m_mats[IDm_bound].n;
                n1 = currMat.n;

            	if (disposition == SpecialTerminate)
            		// do no further work on a packet that is being terminated
            	{
            	}
                else if (disposition != SpecialReflect && n1 == n2)
                	// no refractive index difference and not being forced to reflect
                {
                	log_event(logger,kernel->m_scorer,Events::Boundary,pkt.p,stepResult.IDfe,IDt,stepResult.IDte);
                    IDt_next = stepResult.IDte;
                }
                else 
                	// normal case with a refractive mismatch
                	// or being forced to reflect (shares many of the same calculations as refraction)
                {
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
                	// Report encountering a refractive interface
                	log_event(logger,kernel->m_scorer,Events::Interface,make_pair(pkt.p,__m128(pkt.direction())),stepResult.IDfe,stepResult.IDte);
                    __m128 Fn[4];

                    // Compute normals and incidence angle for reflection/refraction
                    Fn[0] = currTetra.nx;
                    Fn[1] = currTetra.ny;
                    Fn[2] = currTetra.nz;
                    Fn[3] = _mm_setzero_ps();
                    _MM_TRANSPOSE4_PS(Fn[0],Fn[1],Fn[2],Fn[3]);
                    __m128 normal = Fn[stepResult.idx];
                    __m128 costheta = _mm_min_ps(
                    						_mm_set1_ps(1.0),
											_mm_sub_ps(
													_mm_setzero_ps(),
													__m128(dot(SSE::Vector3(normal),pkt.direction()))));

                    ratio = n1/n2;
					__m128 n1_n2_ratio = _mm_load_ps(f_tmp);

                    // sin(t) & cos(t) might be invalid here, since they are computed as if refraction proceeds
                    // in the case of TIR, it will not and the values are meaningless (sin(t) > 0, cos(t) == 0)
                    __m128 sini_cosi_sint_cost = RefractSSE(n1_n2_ratio,costheta);

                	__m128 newdir = _mm_set1_ps(std::numeric_limits<float>::quiet_NaN());

                    // check if reflective surface is forcing reflection or total internal reflection criterion met
                    if (disposition == SpecialReflect || (disposition == Undetermined && (_mm_movemask_ps(_mm_cmplt_ss(_mm_set_ss(1.0),_mm_movehl_ps(sini_cosi_sint_cost,sini_cosi_sint_cost)))&1)))
                    {
                        bool lambertianReflection=kernel->m_reflectiveFaceTable[IDm][IDm_bound].lambertianReflection;
                    	// if reflective surface is requested and lambertian reflection is desired,  we need to generate a random reflection angle
                        if (lambertianReflection == true && disposition == SpecialReflect)
                        {
                            // generate random angles
                            const float rnd0 = *m_rng.floatU01();
                            const float rnd1 = *m_rng.floatU01();

                            newdir = lambertian_reflect(normal, _mm256_set1_ps(rnd0), _mm_set_ss(rnd1));
                        }
                        else // else default to specular reflection
                        {
                            newdir = reflect(__m128(pkt.direction()),normal,sini_cosi_sint_cost);
                        }
                        if (vec_isnan(newdir))
                        {
                        	AutoStreamBuffer b(cerr);
                        	b << "Abnormal condition: new direction is NaN at tetra " << IDt <<
                        			"  Terminating packet\n";
                        	log_event(logger,kernel->m_scorer,Events::NoHit,pkt,currTetra);
                        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                        	return 1;
                        }

                        if (disposition == Undetermined)
                        {
                        	// If SpecialReflect, then logger has already been called
                        	disposition = TIR;
                        	log_event(logger,kernel->m_scorer,Events::ReflectInternal,pkt.p,__m128(pkt.direction()));
                        }
                    }
                    else {
                    	// Not forcing reflection and not total internal reflection: either forced transmission (with refraction)
                    	// or possible Fresnel reflection

                        if (disposition != SpecialTransmit && (_mm_movemask_ps(_mm_cmplt_ss(_mm_load_ss(m_rng.floatU01()),FresnelSSE(n1_n2_ratio,sini_cosi_sint_cost)))&1))
                        {
                        	// not forcing transmission and Fresnel reflection proceeds by random variable
                            newdir = reflect(__m128(pkt.direction()),normal,sini_cosi_sint_cost);

                            assert(!vec_isnan(newdir));

                            assert(vec_isunit<3>(newdir,1e-3));

                            log_event(logger,kernel->m_scorer,Events::ReflectFresnel,pkt.p,__m128(pkt.direction()));
                        }
                        else 
                        {
                        	if (n1 != n2 && disposition != SpecialTransmit)
                        	{
                            	// forcing transmission or Fresnel reflection random draw said no reflection

                        		// d_p: direction in interface plane (removes component along normal)
                            	__m128 d_p = _mm_add_ps(
        									__m128(pkt.direction()),
        									_mm_mul_ps(
        										normal,
        										costheta));

                            	assert(!vec_isnan(d_p));
                            	assert(!vec_isnan(n1_n2_ratio));
                            	assert(!vec_isnan(normal));
                            	assert(!vec_isnan(sini_cosi_sint_cost));

                            	// (d-dot(d,n))*sin(theta_t) - n*cos(theta_t)
                            	// = in-plane component + out-of-plan component

                        		newdir = _mm_sub_ps(
                        				_mm_mul_ps(
                        						d_p,
                        						_mm_shuffle_ps(n1_n2_ratio,n1_n2_ratio,_MM_SHUFFLE(2,2,2,2))),
                        				_mm_mul_ps(
                        						normal,
                        						_mm_shuffle_ps(sini_cosi_sint_cost,sini_cosi_sint_cost,_MM_SHUFFLE(3,3,3,3))));

                        		assert(vec_isunit<3>(newdir,1e-3));
                        	}
                        	else
                        		newdir = __m128(pkt.dir.d);

                        	if (vec_isnan(newdir))
                            {
                                AutoStreamBuffer b(cerr);
                                b << "Abnormal condition: new direction is NaN during transmission at tetra " << IDt << "  Terminating packet\n";
                                log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
                                log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                                return 1;
                            }
						    log_event(logger,kernel->m_scorer,Events::Refract,pkt.p,__m128(pkt.direction()));
                            IDt_next = stepResult.IDte;
                        } // if: fresnel reflection
                    }
#else
                	log_event(logger,kernel->m_scorer,Events::Interface,make_pair(pkt.p,m128(pkt.direction())),stepResult.IDfe,stepResult.IDte);
                    m128 Fn[4];

                    Fn[0] = currTetra.nx;
                    Fn[1] = currTetra.ny;
                    Fn[2] = currTetra.nz;
                    Fn[3] = set_zero128();
                    
                    //Replaces _MM_TRANSPOSE4_PS(Fn[0],Fn[1],Fn[2],Fn[3]);
                    array<float,4> tmp3, tmp2, tmp1, tmp0;
                    tmp0 = {Fn[0][0], Fn[1][0], Fn[2][0], Fn[3][0]};
                    tmp1 = {Fn[0][1], Fn[1][1], Fn[2][1], Fn[3][1]};
                    tmp2 = {Fn[0][2], Fn[1][2], Fn[2][2], Fn[3][2]};
                    tmp3 = {Fn[0][3], Fn[1][3], Fn[2][3], Fn[3][3]};
                    
                    Fn[0] = tmp0;
                    Fn[1] = tmp1;
                    Fn[2] = tmp2;
                    Fn[3] = tmp3;
                    m128 normal = Fn[stepResult.idx];
                    m128 costheta = min128(
                    						set1_ps128(1.0),
											sub128(
													set_zero128(),
													m128(dot(SSE::Vector3(normal),pkt.direction()))));

                    ratio = n1/n2;
					m128 n1_n2_ratio = load_ps128(f_tmp);

                    m128 sini_cosi_sint_cost = RefractSSE(n1_n2_ratio,costheta);


                	m128 newdir;
                    /**
                     * @brief Check for Total Internal Reflection (TIR)?
                     * 
                     */
                    if (disposition == SpecialReflect || (disposition == Undetermined && 1.0 < sini_cosi_sint_cost[2]))
                    {
                        bool lambertianReflection=kernel->m_reflectiveFaceTable[IDm][IDm_bound].lambertianReflection;
                    	// if reflective surface is requested and lambertian reflection is desired,  we need to generate a random reflection angle
                        if (lambertianReflection == true && disposition == SpecialReflect)
                        {
                            // generate random angles
                            const float rnd0 = *m_rng.floatU01();
                            const float rnd1 = *m_rng.floatU01();

                            newdir = lambertian_reflect(normal, rnd0, rnd1);
                        }
                        else // else default to specular reflection
                        {
                            newdir = reflect(m128(pkt.direction()),normal,sini_cosi_sint_cost);
                        }
                        if (vec_isnan(newdir))
                        {
                        	AutoStreamBuffer b(cerr);
                        	b << "Abnormal condition: new direction is NaN at tetra " << IDt <<
                        			"  Terminating packet\n";
                        	log_event(logger,kernel->m_scorer,Events::NoHit,pkt,currTetra);
                        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                        	return 1;
                        }

                        if (disposition == Undetermined)
                        {
                        	// If SpecialReflect, then logger has already been called
                        	disposition = TIR;
                        	log_event(logger,kernel->m_scorer,Events::ReflectInternal,pkt.p,m128(pkt.direction()));
                        }
                    }
                    else 
                    {
					    m128 pr = FresnelSSE(n1_n2_ratio,sini_cosi_sint_cost);
                        
                        if (disposition != SpecialTransmit && *m_rng.floatU01() < pr[0])
                        {
                            // not forcing transmission and Fresnel reflection proceeds by random variable
                            newdir = reflect(m128(pkt.direction()),normal,sini_cosi_sint_cost);

                            assert(!vec_isnan(newdir));
                            assert(vec_isunit<3>(newdir,1e-3));

                            log_event(logger,kernel->m_scorer,Events::ReflectFresnel,pkt.p,m128(pkt.direction()));
                        }
                        else 
                        {
                            if (n1 != n2 && disposition != SpecialTransmit)
                            {
                                // forcing transmission or Fresnel reflection random draw said no reflection

                                // d_p: direction in interface plane (removes component along normal)
                                m128 d_p = add128(
                                            m128(pkt.direction()),
                                            mult128(normal,costheta));

                                assert(!vec_isnan(d_p));
                                assert(!vec_isnan(n1_n2_ratio));
                                assert(!vec_isnan(normal));
                                assert(!vec_isnan(sini_cosi_sint_cost));

                                // (d-dot(d,n))*sin(theta_t) - n*cos(theta_t)
                                // = in-plane component + out-of-plan component

                                newdir = sub128(
                                    mult128(
                                        d_p,
                                        shuffle128(n1_n2_ratio,n1_n2_ratio,2,2,2,2)),
                                    mult128(
                                        normal,
                                        shuffle128(sini_cosi_sint_cost,sini_cosi_sint_cost,3,3,3,3)));

                                assert(vec_isunit<3>(newdir,1e-3));
                            }
                            else
                                newdir = m128(pkt.dir.d);
                            if (vec_isnan(newdir))
                            {
                                AutoStreamBuffer b(cerr);
                                b << "Abnormal condition: new direction is NaN during transmission at tetra " << IDt << "  Terminating packet\n";
                                log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
                                log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                                return 1;
                            }
						    log_event(logger,kernel->m_scorer,Events::Refract,pkt.p,m128(pkt.direction()));
                            IDt_next = stepResult.IDte;
                        } // if: fresnel reflection
                    }
#endif
                    assert(vec_isunit<3>(newdir,1e-3));

                    pkt.dir = PacketDirection(SSE::UnitVector3(SSE::Vector3(newdir),SSE::Assert));

                    if(!(pkt.dir.d.check(SSE::Silent,1e-4) & pkt.dir.a.check(SSE::Silent,1e-4) & pkt.dir.b.check(SSE::Silent,1e-4) & !vec_isnan(pkt.p)))
                    {
                        AutoStreamBuffer b(cerr);
                    	b << "Abnormal condition: packet direction is not normal " << IDt << "  Terminating packet\n";
                    	log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
                    	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
                    	return 1;
                    }
                } // if: refractive index difference
            } // if: material change

            // changing tetras
            if (IDt_next != IDt)
            {
            	if (IDt_next == 0)				// exiting mesh
            	{
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
            		log_event(logger,kernel->m_scorer,Events::Exit,make_pair(pkt.p,__m128(pkt.direction())),stepResult.IDfe,pkt.w);
#else
                    log_event(logger,kernel->m_scorer,Events::Exit,make_pair(pkt.p,m128(pkt.direction())),stepResult.IDfe,pkt.w);
#endif
                    log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
            		return 0;
            	}
            	else							// not exiting mesh
            	{
                	log_event(logger,kernel->m_scorer,Events::NewTetra,pkt,currTetra,stepResult.idx,IDt_next,currMat);
            		IDt = IDt_next;
            		IDm_next = IDm_bound;
            		currTetra=kernel->m_tetras[IDt];
                }
            }

            // if material ID changed, fetch new material and update step length remaining
            if (IDm != IDm_next)
            {
                IDm = IDm_next;
                currMat = kernel->m_mats[IDm];

                // TODO: Probable error in t caused here
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
                // movehdup_ps(pkt.s) does [a b c d] -> [b b d d], ie. [s l t X] -> [l l X X]
                // div_ss(a,b) does [a0 a1 a2 a3] [b0 X X X] -> [a0/b0 a1 a2 a3]
                pkt.s = _mm_div_ss(_mm_movehdup_ps(pkt.s), _mm_set_ss(currMat.muT));
#else
                pkt.s[0] = pkt.s[1] / currMat.muT;
                pkt.s[2] = pkt.s[3];
#endif
            }
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
            // get the next intersection
            stepResult=currTetra.getIntersection(pkt.p,__m128(pkt.direction()),pkt.s);
#else
            stepResult=currTetra.getIntersection(pkt.p,m128(pkt.direction()),pkt.s);
#endif
            pkt.oldP = pkt.p;
            pkt.p    = stepResult.Pe;

            // Detection logic: before termination, check if photon can be detected. 
            bool detected = false;
            if (currTetra.detectorEnclosedIDs.size() > 0) // There is at least one detector enclosed in this tetra
            {
                for (auto detID : currTetra.detectorEnclosedIDs)
                {
                    // check if packet position intersects with the detector 
                    Emitter::Detector<RNG, Emitter::CylDetector<RNG>>* currDetector = dynamic_cast<Emitter::Detector<RNG, Emitter::CylDetector<RNG>>*>(kernel->m_detectors[detID - 1]);

                    if (currDetector == nullptr) {
                        AutoStreamBuffer b(cerr);
                        b << "Only cylindrical detector types are supported\n";
                    }
                    else {
                        float currWeight = pkt.w; 
                        currDetector->detect(m_rng, pkt, currMat.n);  
                        if (currWeight > pkt.w) // portion of it got detected, terminate
                        {
                            term = Detected; 
                            detected = true;
                            break;
                        }
                    }
                } // end detectors loop
            } // End checking for detectors
            if (detected)
                break;

            assert(pkt.dir.d.check(SSE::Silent,1e-4) & pkt.dir.a.check(SSE::Silent,1e-4) & pkt.dir.b.check(SSE::Silent,1e-4) & !vec_isnan(pkt.p));
        } // End of hit loop: for(Nhit=0; stepResult.hit && Nhit < kernel->Nhit_max_; ++Nhit)

        //std::cout << "TetraID: " << IDt_next << std::endl;
        /**
         * @brief This is the Drop Phase of the Packet, where it is decided whether 
         * the packet is allowed to continue or will be terminated
         * 
         */
        if (Nhit >= kernel->Nhit_max_)
        {
            AutoStreamBuffer b(cerr);
        	b << "Terminated due to unusual number of interface hits at tetra " << IDt
        			<< "  Nhit=" << Nhit << " physical step remaining=" << pkt.s[0]  << " w=" << pkt.w << '\n';
        	log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
        	return -2;
        }

        // Absorption process
        float w0=pkt.w;
        if (term != Detected) {
            float dw;
            tie(pkt.w,dw) = absorb(pkt,currMat,currTetra);
            if (isnan(dw))
            {
                AutoStreamBuffer b(cerr);
                b << "Terminated due to NAN value for absorbed weight. This should never happen and needs investigation\n";
                return -2;
            }
            if (dw != 0.0)
                log_event(logger,kernel->m_scorer,Events::Absorb,pkt.p,IDt,pkt.w,dw);

            // Termination logic
            w0=pkt.w;

            tie(term,dw)=terminationCheck(kernel->wmin_,kernel->prwin_,m_rng,pkt,currMat,currTetra);
        }

       switch(term){
        case Continue:								// Continues, no roulette
        	break;

        case RouletteWin:							// Wins at roulette, dw > 0
        	log_event(logger,kernel->m_scorer,Events::RouletteWin,w0,pkt.w);
        	break;

        case RouletteLose:							// Loses at roulette, dw < 0
        	log_event(logger,kernel->m_scorer,Events::Die,pkt.w);
        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
        	return 0;
        	break;

        case TimeGate:								// Expires due to time gate
        	log_event(logger,kernel->m_scorer,Events::TimeGate,pkt);
        	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
        	return 1;
        	break;

        case Detected:
            // TODO log_event(logger, kernel->m_scorer, Events::Detected, pkt);
            log_event(logger,kernel->m_scorer, Events::Terminate,pkt);
            return 2;
            break;
        
        case Other:
        default:
        	break;
        }
        /**
         * @brief The scatter function resembles the Spin phase in the simulation
         * 
         */
//#ifdef __ALTIVEC__
    // optimized vector code for P8 goes here
#ifdef USE_SSE
    	if (scatter(pkt,currMat,currTetra))
    		log_event(logger,kernel->m_scorer,Events::Scatter,__m128(pkt.direction()),__m128(pkt.direction()),std::numeric_limits<float>::quiet_NaN());
#else
        if (scatter(pkt,currMat,currTetra))
    		log_event(logger,kernel->m_scorer,Events::Scatter,m128(pkt.direction()),m128(pkt.direction()),std::numeric_limits<float>::quiet_NaN());
#endif

    } // End of step loop: for(Nstep=0; Nstep < kernel->Nstep_max_; ++Nstep) 

    {
    	AutoStreamBuffer b(cerr);
    	b << "Terminated due to unusual number of steps at tetra " << IDt
		  << " physical step remaining=" << pkt.s[0] << " w=" << pkt.w << '\n';
    }

    log_event(logger,kernel->m_scorer,Events::Abnormal,pkt,Nstep,Nhit);
	log_event(logger,kernel->m_scorer,Events::Terminate,pkt);
    return -1;
}


#endif