/*
 * TetraInternalKernel.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef KERNELS_SOFTWARE_TETRAINTERNALKERNEL_HPP_
#define KERNELS_SOFTWARE_TETRAINTERNALKERNEL_HPP_

#include "TetraMCKernel.hpp"
#include "TetraMCKernelThread.hpp"

#include <FullMonteSW/Kernels/Software/Logger/LoggerTuple.hpp>

#include <list>

#include "Logger/BaseLogger.hpp"

#include "Logger/VolumeAbsorptionScorer.hpp"
#include "Logger/SurfaceExitScorer.hpp"
#include "Logger/ConservationScorer.hpp"
#include "Logger/EventScorer.hpp"
#include "Logger/DirectedSurfaceScorer.hpp"


typedef std::tuple<
		EventScorer,
		ConservationScorer,
		VolumeAbsorptionScorer,
		SurfaceExitScorer,
		DirectedSurfaceScorer<QueuedMultiThreadAccumulator<double,double>>>
		InternalScorer;

class SurfaceCellPredicate;
class VolumeCellPredicate;
class GeometryPredicate;

class TetraInternalKernel : public TetraMCKernel<RNG_SFMT_AVX,InternalScorer>
{
public:
	TetraInternalKernel(){}

	typedef RNG_SFMT_AVX RNG;

	EventScorer& 														eventScorer() 				{ return get<0>(m_scorer); }
	ConservationScorer& 												conservationScorer() 		{ return get<1>(m_scorer); }
	VolumeAbsorptionScorer& 											volumeScorer() 				{ return get<2>(m_scorer); }
	SurfaceExitScorer& 													surfaceScorer() 			{ return get<3>(m_scorer); }
	DirectedSurfaceScorer<QueuedMultiThreadAccumulator<double,double>>&	directedSurfaceScorer() 	{ return get<4>(m_scorer); }


	virtual void parentPrepare() override;
};



#endif /* KERNELS_SOFTWARE_TETRAINTERNALKERNEL_HPP_ */
