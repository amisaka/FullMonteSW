/*
 * Kernel.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: jcassidy
 */

#include "Kernel.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Logging/FullMonteTimer.hpp>

#include <FullMonteSW/Geometry/Geometry.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Sources/Abstract.hpp>

#include <FullMonteSW/Kernels/KernelObserver.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <thread>
#include <algorithm>

void Kernel::runSync()
{
	if (m_status != Idle && m_status != Finished)
		throw std::logic_error("Kernel::runSync called while status not Idle || Finished");

    // Layered geometry does not use m_src as sources, it direcly defines
	// a pencil beam in the code. The check for m_src is a special case for the Layered geometry.
	// Ensure at least one Source has power
    if (m_src != NULL && m_src->totalPower() <= 0.0f)
        throw std::logic_error("Emitters must have a total power greater than 0");

	DimensionUnit geo_unit = m_geometry->getUnit();
	for(unsigned i=0; i<m_materialSet->size(); i++)
	{
		DimensionUnit mat_unit = m_materialSet->get(i)->getUnit();
		if(mat_unit != geo_unit)
			throw std::logic_error("Kernel::runSync called while dimension units of materials and mesh do not match");
	}

	if (m_unit == EnergyPower::NONE && geo_unit != DimensionUnit::NONE)
		throw std::logic_error("Kernel::runSync called while the dimension unit is defined but the energy unit is set to 'NONE'.");

	if (m_unit != EnergyPower::NONE && geo_unit == DimensionUnit::NONE)
		throw std::logic_error("Kernel::runSync called while the dimension unit is set to 'NONE' but the energy unit is defined.");
	
    updateStatus(Preparing);

	results()->clear();


	// notify observers we're preparing
	for(const auto o : m_observers)
		o->notify_prepare(*this);

    auto start_prep = std::chrono::high_resolution_clock::now();
	prepare_();
    auto end_prep = std::chrono::high_resolution_clock::now();
    LOG_INFO << std::fixed << std::setprecision(3) << "Kernel preparation took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_prep-start_prep).count() << " ms\n";

	// notify observers we're starting
	for(const auto o: m_observers)
		o->notify_start(*this);

	updateStatus(Running);

	FullMonteTimer timer;
    auto start_kernel = std::chrono::high_resolution_clock::now();
	start_();
	awaitFinish();
    auto end_kernel = std::chrono::high_resolution_clock::now();
    LOG_INFO << std::fixed << std::setprecision(3) << "Kernel execution took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_kernel-start_kernel).count() << " ms\n";
	LOG_INFO << std::fixed << std::setprecision(3) << "Peak memory usage up to this point: " << timer.max_rss_mib() << " Mib\n";

    gatherResults();

	updateStatus(Finished);

	// notify observers we're done
	for(const auto o : m_observers)
		o->notify_finish(*this);

	// share the results
	OutputDataCollection* res = results();
	for(unsigned i=0;i<res->size();++i)
	{
		OutputData* d = res->getByIndex(i);
		for(const auto o : m_observers)
			o->notify_result(*this,d);
	}
}

void Kernel::startAsync()
{
	if (m_status != Idle && m_status != Finished)
		throw std::logic_error("Kernel::runSync called while status not Idle || Finished");

    // Layered geometry does not use m_src as sources, it direcly defines
	// a pencil beam in the code. The check for m_src is a special case for the Layered geometry.
	// Ensure at least one Source has power
    if (m_src != NULL && m_src->totalPower() <= 0.0)
        throw std::logic_error("Emitters must have a total power greater than 0");
	
    results()->clear();

	// launch a thread that runs the normal synchronous routine
	m_parentThread = std::thread(std::mem_fn(&Kernel::runSync),this);

	// return as soon as it's running
	awaitStatus(Running);
}

void Kernel::finishAsync()
{
	m_parentThread.join();
}

void Kernel::updateStatus(Status status)
{
	std::unique_lock<std::mutex> lk(m_statusMutex);
	m_status=status;
	m_statusCV.notify_all();
}

Kernel::Status Kernel::status() const
{
	return m_status;
}

void Kernel::awaitStatus(Status status)
{
	std::unique_lock<std::mutex> lk(m_statusMutex);
	m_statusCV.wait(lk, [this,status]{ return m_status==status; });
}

MaterialSet* Kernel::materials() const
{
	return m_materialSet;
}

void Kernel::geometry(const Geometry* G)
{
	m_geometry=G;
	m_dirtyGeometry=true;
}

const Geometry* Kernel::geometry() const
{
	return m_geometry;
}

void Kernel::materials(MaterialSet* ms)
{
	m_materialSet=ms;
}

OutputDataCollection* Kernel::results()
{
	if (!m_results)
		m_results = new OutputDataCollection();
	return m_results;
}

void Kernel::isEnergy_or_Power(std::string str)
{
	//transfrom to lower case
	std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c){ return std::tolower(c); });
	if(str == "w" || str == "watt") 
	{
		m_unit = EnergyPower::Watt;
	} else if(str == "j" || str == "joule") 
	{
		m_unit = EnergyPower::Joule;
	}else {
		LOG_ERROR << "MCKernelBase::isEnergy_or_Power unknown string " << str << " - defaulting to NONE" << std::endl;
		m_unit = EnergyPower::NONE;
	}
}

Kernel::EnergyPower Kernel::getUnit() const
{
	return m_unit;
}

std::string Kernel::isEnergy_or_Power() const
{
	if(m_unit == EnergyPower::Watt) 
	{
		return "Watt";
	} else if (m_unit == EnergyPower::Joule) 
	{
		return "Joule";
	} else
	{
		return "NONE";
	}
}

void Kernel::energyPowerValue(float e)
{
	m_energy_power = e;
}

float Kernel::energyPowerValue() const
{
	return m_energy_power;
}
