#if defined(SWIGTCL)
%module FullMonteKernelsTCL
#elif defined(SWIGPYTHON)
%module Kernels
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
//#include <FullMonteSW/Warnings/Boost.hpp>
%}

%include "std_vector.i"
%include "std_string.i"

%include "../Geometry/FullMonteGeometry_types.i"

%{
//#include "Kernel.hpp" 
#include "KernelObserver.hpp"
#include "OStreamObserver.hpp"
//#include "MCKernelBase.hpp"
#include "SeedSweep.hpp"
//#include <FullMonteSW/Geometry/TetraMesh.hpp>
%}

//%include "Kernel.hpp"
%include "KernelObserver.hpp"
%include "OStreamObserver.hpp"
//%include "MCKernelBase.hpp"
%include "SeedSweep.hpp"
