package require FullMonte

set ifn [lindex $argv 0]
set ofn [lindex $argv 1]

puts "Converting TIM-OS mesh $ifn to labeled VTK mesh $ofn"

TIMOSMeshReader R
    R filename $ifn
    R read
    set M [R mesh]

VTKMeshWriter W
    W filename $ofn
    W mesh $M
    W write

exit
