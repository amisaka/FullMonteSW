/*
 * cache_accumulationevents.cpp
 *
 *  Created on: Nov 9, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernel.hpp>

#include <FullMonteSW/Kernels/Software/Logger/LoggerTuple.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernelThread.hpp>

#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/OutputTypes/AccumulationEventSet.hpp>
#include <FullMonteSW/OutputTypes/AccumulationEvent.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputDataSummarize.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Kernels/Software/Logger/AccumulationEventScorer.hpp>
#include <FullMonteSW/Kernels/Software/Logger/EventScorer.hpp>


#include <fstream>
#include <string>
#include <tuple>
#include <map>
#include <unordered_map>
#include <boost/functional/hash.hpp>

// Define the kernel that just scores memory traces

typedef tuple<
		EventScorer,
		TetraAccumulationEventScorer> AccumulationEventKernelScorer;

class AccumulationEventKernel : public TetraMCKernel<RNG_SFMT_AVX,AccumulationEventKernelScorer>
{
public:
	/**
	 * @brief Construct a new AccumulationEvent Kernel object. This is just a test class for accumulation events when using FullMonte
	 * and doesn't belong to the normal Kernel types of FullMonte <-- Can be ignored for TCL 
	 * 
	 */
	AccumulationEventKernel(){}
	virtual ~AccumulationEventKernel(){}

	typedef RNG_SFMT_AVX RNG;
};

using namespace std;

/**
 * @brief LRU cache replacement strategy. Updates the cache based on the LRU strategy and adapts the
 * LRU sequence accordingly. Also handles the hit and miss counts
 * 
 * @param cacheHit - Are we dealing with a cache hit or a cache miss
 * @param set - The set which the current TetraID is assigned to
 * @param tetraID - The current TetraID
 * @param hitPosition - If cacheHit == true, at which position did we encounter the TetraID
 * @param os - Reference to output file stream
 * @param misses - Reference to counter for overall misses
 * @param capacityMisses - Reference to counter for capacity misses
 * @param conflictMisses - Reference to counter for conflict misses
 * @param compulsoryMisses - Reference to counter for compulsory misses
 * @param hits - Reference to counter of overall hits
 * @param cache - Reference to the cache
 * @param cacheReplacement - Reference to the replacement sequence of the cache
 * @param visitedCache - Reference to the vector storing all TetraID that have already been in the cache
 */
void LeastRecentlyUsed(bool cacheHit, unsigned set, int tetraID, int hitPosition,
         unsigned &misses, unsigned &/*capacityMisses*/, unsigned &/*conflictMisses*/, unsigned &/*compulsoryMisses*/, unsigned &hits, unsigned &accessesUntilEviction,
		 unsigned &misses_1run, unsigned &/*capacityMisses_1run*/, unsigned &/*conflictMisses_1run*/, unsigned &/*compulsoryMisses_1run*/, unsigned &hits_1run,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &/*visitedCacheMap*/, vector<pair<int,unsigned>> &cacheEviction)
{
    if (cacheHit == false)
        {
			unsigned currentEntry = cache[set][cacheReplacement[set][0]];
            // Replace an element of the cache with the new requested element (LRU)
			if (currentEntry != 0 || tetraID < 0)
			{
				pair<int, unsigned> evictionInput;
				// Store the evicted tetraID and record the number of accesses it took for the element to be evicted
				if ( tetraID < 0)
				{
					evictionInput = make_pair(tetraID, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
					return;
				}
				else
				{
					evictionInput = make_pair(currentEntry, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
				}
				
			}
			if (tetraID > 0)
			{
				misses ++;
				misses_1run++;
				cache[set][cacheReplacement[set][0]] = tetraID;
				unsigned accessedEntry = cacheReplacement[set][0];
				for(unsigned j=0; j < cache[set].size()-1; j++)
				{
					cacheReplacement[set][j] = cacheReplacement[set][j+1];
				}
				cacheReplacement[set][cache[set].size()-1] = accessedEntry;
			}
			/*
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
					capacityMisses_1run++;
                }
                else
                {
                    conflictMisses++;
					conflictMisses_1run++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
				compulsoryMisses_1run++;
				visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }
			*/
        }
        else // cacheHit == true
        {
            hits++;
			hits_1run++;
            vector<unsigned>::iterator iter = find(cacheReplacement[set].begin(), cacheReplacement[set].end(), hitPosition);
            unsigned index = distance(cacheReplacement[set].begin(), iter);
            // since this cache entry has been used last, we will place it to the back of the least recently used sequence
            for(unsigned j=index; j < cache[set].size()-1; j++)
            {
                cacheReplacement[set][j] = cacheReplacement[set][j+1];
            }
            cacheReplacement[set][cache[set].size()-1] = hitPosition;
        }
}

void FirstInFirstOut(bool cacheHit, unsigned set, int tetraID,
         unsigned &misses, unsigned &/*capacityMisses*/, unsigned &/*conflictMisses*/, unsigned &/*compulsoryMisses*/, unsigned &hits, unsigned &accessesUntilEviction,
		 unsigned &misses_1run, unsigned &/*capacityMisses_1run*/, unsigned &/*conflictMisses_1run*/, unsigned &/*compulsoryMisses_1run*/, unsigned &hits_1run,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &/*visitedCacheMap*/, vector<pair<int,unsigned>> &cacheEviction)
{
    if (cacheHit == false)
        {
			unsigned currentEntry = cache[set][cacheReplacement[set][0]];
			if (currentEntry != 0 || tetraID < 0)
			{
				pair<int, unsigned> evictionInput;
				// Store the evicted tetraID and record the number of accesses it took for the element to be evicted
				if ( tetraID < 0)
				{
					evictionInput = make_pair(tetraID, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
					return;
				}
				else
				{
					evictionInput = make_pair(currentEntry, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
				}
				
			}
            if (tetraID > 0)
			{
				misses ++;
				misses_1run++;
				// Replace an element of the cache with the new requested element (FIFO)
				cache[set][cacheReplacement[set][0]] = tetraID;
				
				cacheReplacement[set][0] = (cacheReplacement[set][0]+1)%cache[set].size();
			}
			/*
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
					capacityMisses_1run++;
                }
                else
                {
                    conflictMisses++;
					conflictMisses_1run++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
				compulsoryMisses_1run++;
                visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }
			*/
        }
        else // cacheHit == true
        {
            hits++;
			hits_1run++;
            // with a FIFO replacement strategy, a hit does not influence the replacement of the block
            // --> Do nothing
        }
}

void MostRecentlyUsed(bool cacheHit, unsigned set, int tetraID, int hitPosition,
         unsigned &misses, unsigned &/*capacityMisses*/, unsigned &/*conflictMisses*/, unsigned &/*compulsoryMisses*/, unsigned &hits, unsigned &accessesUntilEviction,
		 unsigned &misses_1run, unsigned &/*capacityMisses_1run*/, unsigned &/*conflictMisses_1run*/, unsigned &/*compulsoryMisses_1run*/, unsigned &hits_1run,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &/*visitedCacheMap*/, vector<pair<int,unsigned>> &cacheEviction)
{
    if (cacheHit == false)
        {
			unsigned currentEntry = cache[set][cacheReplacement[set][0]];
			if (currentEntry != 0 || tetraID < 0)
			{
				pair<int, unsigned> evictionInput;
				// Store the evicted tetraID and record the number of accesses it took for the element to be evicted
				if ( tetraID < 0)
				{
					evictionInput = make_pair(tetraID, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
					return;
				}
				else
				{
					evictionInput = make_pair(currentEntry, accessesUntilEviction);
					cacheEviction.push_back(evictionInput);
					accessesUntilEviction = 0;
				}
				
			}
			if (tetraID > 0)
			{
				misses ++;
				misses_1run++;
				// Replace an element of the cache with the new requested element (MRU)
				vector<unsigned>::iterator it;
				it = find(cache[set].begin(),cache[set].end(), 0);
				if(it != cache[set].end()) // if we have space in this set of the cache
				{
					unsigned index = distance(cache[set].begin(), it);
					cache[set][index] = tetraID;

					for(unsigned j=0; j < cache[set].size()-1; j++)
					{
						cacheReplacement[set][j] = cacheReplacement[set][j+1];
					}
					cacheReplacement[set][cache[set].size()-1] = index;
				}
				else // if this set of the cache is full
				{
					// Replace the most recently used element with the current element
					cache[set][cacheReplacement[set][cache[set].size()-1]] = tetraID;
				}
			}
			/*
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
					capacityMisses_1run++;
                }
                else
                {
                    conflictMisses++;
					conflictMisses_1run++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
				compulsoryMisses_1run++;
                visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }*/
        }
        else // cacheHit == true
        {
            hits++;
			hits_1run++;
            vector<unsigned>::iterator iter = find(cacheReplacement[set].begin(), cacheReplacement[set].end(), hitPosition);
            unsigned index = distance(cacheReplacement[set].begin(), iter);
            // since this cache entry has been used last, we will place it to the back of the least recently used sequence
            for(unsigned j=index; j < cache[set].size()-1; j++)
            {
                cacheReplacement[set][j] = cacheReplacement[set][j+1];
            }
            cacheReplacement[set][cache[set].size()-1] = hitPosition;
        }
}

enum ReplacementStrategy{LRU, FIFO, MRU};

enum MeshString {Unknown, HeadNeck, Colin, Bladder, Cube};
static map<string, MeshString> mapMeshString;

static void initializeStringMapping()
{
	mapMeshString["HEADNECK"] = HeadNeck;
	mapMeshString["COLIN"] = Colin;
	mapMeshString["BLADDER"] = Bladder;
	mapMeshString["CUBE"] = Cube;
}


int main(int argc,char **argv)
{
	initializeStringMapping();
	boost::program_options::options_description cmdline;

	string fnMesh, fnOut, replacementStrategy, fnPermutation;

	vector<float> point, pencilbeam, ball, line, fiber;
    unsigned source_volume;


	unsigned long long Npkt;
	float wmin = 1e-5, prwin = 0.1;
    unsigned Nthreads=MAX_THREAD_COUNT, maxHits = 10000, maxSteps = 10000, seed = 0;
	unsigned inflight = 1;
	bool useZipf = false;
    unsigned cacheSize = 32, ways = 32, zipfSize = 100;
	unsigned output_buffer = 64;
	int writeLatency = 210;

	cmdline.add_options()
			("cacheSize,c",boost::program_options::value<unsigned>(&cacheSize)->default_value(32),"How many elements can the cache store?")
			("ways,w",boost::program_options::value<unsigned>(&ways)->default_value(32),"What degree of associativity does the cache have?")
            ("strategy,s",boost::program_options::value<string>(&replacementStrategy),"What replacement strategy should the caceh use? (LRU/FIFO)")
            ("zipf,z",boost::program_options::value<bool>(&useZipf)->default_value(false),"Should we also use pre-loaded memory based on the Zipf distribution? (1->True/0->False)")
            ("zipfSize,k",boost::program_options::value<unsigned>(&zipfSize)->default_value(1000),"How many pre-loaded Tetras should the memory contain?")
			("mesh,m",boost::program_options::value<string>(&fnMesh),"Which mesh should be used for simulation? Options: {HeadNeck, Colin, Bladder, Cube}")
			("point",boost::program_options::value<vector<float>>(&point)->multitoken(),"Define position of a point source (syntax {1.0,2.0,3.0})")
            ("pencil",boost::program_options::value<vector<float>>(&pencilbeam)->multitoken(),"Define position (1st 3 values) and direction (next 3 values) of a pencilbeam source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("line",boost::program_options::value<vector<float>>(&line)->multitoken(),"Define start position (1st 3 values) and end position (next 3 values) of a line source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("ball",boost::program_options::value<vector<float>>(&ball)->multitoken(),"Define position (1st 3 values) and radius (4th value) of a ball source (syntax {1.0,2.0,3.0,4.0})")
            ("srcvolume",boost::program_options::value<unsigned>(&source_volume)->default_value(0),"Define the element ID of a volume source (syntax 123456")
            ("fiber",boost::program_options::value<vector<float>>(&fiber)->multitoken(),"Define position (1st 3 values), direction (next 3 values), radius (7th value), and numerical aperture (8th value) of a fibercone source (syntax 123456")
            ("threads",boost::program_options::value<unsigned>(&Nthreads)->default_value(MAX_THREAD_COUNT),"Number of threads to use")
			("packets,N",boost::program_options::value<unsigned long long>(&Npkt),"Packet count")
			("wmin",boost::program_options::value<float>(&wmin)->default_value(1e-5),"Roulette threshold (default: 1e-5)")
            ("prwin",boost::program_options::value<float>(&prwin)->default_value(0.1),"Roulette threshold (default: 0.1)")
            ("hits",boost::program_options::value<unsigned>(&maxHits)->default_value(10000),"Number of maximum hits of a photon (default: 10000)")
            ("steps",boost::program_options::value<unsigned>(&maxSteps)->default_value(10000),"Number of maximum steps of a photon (default: 10000)")
			("rng",boost::program_options::value<unsigned>(&seed)->default_value(0),"Seed for RNG (default: no seed)")
			("inflight",boost::program_options::value<unsigned>(&inflight)->default_value(1),"How many packets-in-flight should be handled by the cache? (default: 1)")
			("permutation",boost::program_options::value<string>(&fnPermutation),"Input file for memory access permuation")
			("outputFile",boost::program_options::value<string>(&fnOut),"Output file prefix")
			("writeBuffer",boost::program_options::value<unsigned>(&output_buffer)->default_value(64),"The number of elements that are buffered before writing the data to the host (default: 64)")
			("writeLatency",boost::program_options::value<int>(&writeLatency)->default_value(210),"The number of cycles that are required to complete a write cycle (default: 210)")
			("help,h","Get help")
			;

	boost::program_options::variables_map vm;

	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	vm.notify();

	if (vm.count("help"))
	{
		cout << cmdline << endl;
		return -1;
	}
	else if (fnMesh.empty())
	{
		cout << "Missing mesh definition in command line" << endl;
		cout << cmdline << endl;
		return -1;
	}

	ReplacementStrategy strategy;
    if (replacementStrategy.compare("LRU") == 0)
    {
        strategy = LRU;
    }
    else if (replacementStrategy.compare("FIFO") == 0)
    {
        strategy = FIFO;
    }
    else if (replacementStrategy.compare("MRU") == 0)
    {
        strategy = MRU;
    }
    else
    {
        cout << "Could not recognize the desired replacement strategy. Defaulting to LRU." << endl;
        strategy = LRU;
    }

	string filename = FULLMONTE_DATA_DIR;
	
	VTKMeshReader VTK_MR;
	TIMOSMeshReader TIMOS_MR;

	TetraMesh* M;
	//Define MaterialSet to hold all materials for the supported meshes
    MaterialSet MS;

	/**     Material definitions for all mesh types
	 * @brief Material(float muA,float muS,float g,float n);
	 * muA -> absorption coefficient
	 * muS -> scattering coefficient
	 * g   -> anisotropy
	 * n   -> refractive index
	 */
    Material air(0.0,0.0,0.0,1.0);
	MS.exterior(&air);
    
	// HeadNeck Materials
    Material tongue(0.95,83.3,0.926,1.37);
    Material larynx(0.55,15.0,0.9,1.36);
    Material tumour(0.13,9.35,0.92,1.39);
    Material teeth(0.99,60.0,0.95,1.48);
    Material bone(0.3,100.0,0.9,1.56);
    Material surroundingtissues(1.49,10.0,0.9,1.35);
    Material subcutaneousfat(0.2,30.0,0.78,1.32);
    Material skin(2.0,187.0,0.93,1.38);
    
	// Bladder Materials
    Material surround(0.5,100.0,0.9,1.39);
    Material _void(0.01,0.1,0.9,1.37);

	// Colin Materials
	Material skalp(0.019,7.8,0.89,1.37);
	Material skull(0.004,0.009,0.89,1.37);
	Material greymatter(0.02,9.0,0.89,1.37);
	Material whitematter(0.08,40.9,0.84,1.37);

	// Cube Materials
	Material bigcube(0.05,20.0,0.9,1.3);
	Material smallcube1(0.1,10.0,0.7,1.1);
	Material smallcube2(0.2,20.0,0.8,1.2);
	Material smallcube3(0.1,10.0,0.9,1.4);
	Material smallcube4(0.2,20.0,0.9,1.5); 
	
	std::transform(fnMesh.begin(), fnMesh.end(), fnMesh.begin(), ::toupper);
	switch(mapMeshString[fnMesh])
	{
		case HeadNeck:
			filename.append("/HeadNeck/HeadNeck.mesh.vtk");
			VTK_MR.filename(filename);
			VTK_MR.read();
			M = VTK_MR.mesh();

			MS.append(&tongue);
			MS.append(&tumour);
			MS.append(&larynx);
			MS.append(&teeth);
			MS.append(&bone);
			MS.append(&surroundingtissues);
			MS.append(&subcutaneousfat);
			MS.append(&skin);
			break;
		case Bladder:
			filename.append("/Bladder/July2017BladderWaterMesh1.mesh.vtk");
			VTK_MR.filename(filename);
			VTK_MR.read();
			M = VTK_MR.mesh();
			
        	MS.append(&surround);
        	MS.append(&_void);
			break;
		case Colin:
        	filename.append("/Colin27/Colin27.mesh");
			TIMOS_MR.filename(filename);
			TIMOS_MR.read();
			M = TIMOS_MR.mesh();

			MS.append(&skalp);
			MS.append(&skull);
			MS.append(&greymatter);
			MS.append(&whitematter);
			break;
		case Cube:
			filename.append("/TIM-OS/cube_5med/cube_5med.mesh");
			TIMOS_MR.filename(filename);
			TIMOS_MR.read();
			M = TIMOS_MR.mesh();

			MS.append(&bigcube);
			MS.append(&smallcube1);
			MS.append(&smallcube2);
			MS.append(&smallcube3);
			MS.append(&smallcube4);
			break;
		default:
			cout << "No valid mesh specified! Exiting..." << endl;
			cout << cmdline << endl;
			return -1;
	}

	unsigned blocks = Npkt / inflight;
	if ((blocks * inflight) != Npkt)
	{
		cout << "The defined packets-in-flight and the number of simulated packets do not fit exactly." << endl
			<< "The number of simulated packets will be set to " << blocks * inflight << endl;
		Npkt = blocks * inflight;
	}
	cout << "Packets: " << Npkt << endl;
	cout << "Threads: " << Nthreads << endl;

	Source::Composite C;

    Source::Point P;
    if(!point.empty() && point.size() == 3)
    {
        P.position({point[0], point[1], point[2]});
        C.add(&P);
    }
    
    Source::PencilBeam PB;
    if(!pencilbeam.empty() && pencilbeam.size() == 6)
    {
        PB.position({pencilbeam[0], pencilbeam[1], pencilbeam[2]});
        PB.direction({pencilbeam[3], pencilbeam[4], pencilbeam[5]});
        C.add(&PB);
    }

    Source::Volume V;
    if(source_volume != 0)
    {
        V.elementID(source_volume);
        C.add(&V);  
    }

    Source::Ball B;
    if (!ball.empty() && ball.size() == 4)
    {
        B.position({ball[0], ball[1], ball[2]});
        B.radius(ball[3]);
        C.add(&B);
    }

    Source::Line L;
    if(!line.empty() && line.size() == 6)
    {
        L.endpoint(0, {line[0], line[1], line[2]});
        L.endpoint(1, {line[3], line[4], line[5]});
        C.add(&L);
    }

    Source::Fiber F;
    if(!fiber.empty() && fiber.size() == 8)
    {
        F.fiberPos({fiber[0], fiber[1], fiber[2]});
        F.fiberDir({fiber[3], fiber[4], fiber[5]});
        F.radius(fiber[6]);
        F.numericalAperture(fiber[7]);
        C.add(&F);
    }
	if(C.elements().empty())
	{
		cout << "Missing at least one source definition in command line" << endl;
		cout << cmdline << endl;
		return -1;
	}

	AccumulationEventKernel K;

	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.geometry(M);
	K.source(&C);
	K.materials(&MS);

	K.runSync();
	
	OutputDataCollection* res = K.results();

	const auto eventTrace = static_cast<const AccumulationEventSet*>			(res->getByName("TetraAccumulationEvent"));

	const auto events = static_cast<MCEventCountsOutput*>		(res->getByName("EventCounts"));

	if (events)
	{
		OutputDataSummarize OS;
		OS.visit(events);
	}
	else
		cout << "Event counts missing" << endl;

	if (eventTrace)
	{
		// Write raw traces, and accumulate total access counts
		vector<unsigned> accessCounts;
		vector<vector<double>> weightIncrements;
		weightIncrements.resize(eventTrace->size()/inflight);
		vector<vector<int>> accessTraces;
		accessTraces.resize(eventTrace->size()/inflight);
		vector<vector<int>> inflightTraces;
		inflightTraces.resize(inflight);
		vector<vector<double>> inflightWeightIncrements;
		inflightWeightIncrements.resize(inflight);
		
		ofstream os;

		// cout << "Accumulation event set found with " << eventTrace->size() << " entries" << endl;
		unsigned Ntr=0,Nacc=0;
		for(unsigned i=0;i<eventTrace->size();i+=inflight)
		{
			for(unsigned m=0; m<inflight; m++)
			{
				inflightTraces[m].resize(0);
				inflightWeightIncrements[m].resize(0);	
				//os.open(fnOut+".traces_photon"+to_string(i)+".txt");
				AccumulationEvent* tr = eventTrace->get(i+m);
				Ntr += tr->size();
				unsigned Nacc_tr=0;
				for(unsigned j=0;j<tr->size();++j)
				{
					int addr=tr->get(j).address;
					double w=tr->get(j).weight;
					Nacc_tr++;
					
					if (addr < 0)
					{
						// Do nothing
					}
					else if (addr >= (int) accessCounts.size())
					{
						accessCounts.resize(addr+1,0);
						accessCounts[addr]++;
					}
					else
					{
						accessCounts[addr]++;
					}
					
					inflightTraces[m].push_back(addr);
					inflightWeightIncrements[m].push_back(w);
					
				}
				Nacc += Nacc_tr;
			}
			unsigned largestVectorSize = 0;
			for(unsigned m=0; m<inflight; m++)
			{
				if (largestVectorSize < inflightTraces[m].size())
					largestVectorSize = inflightTraces[m].size();
			}
			for(unsigned j=0; j<largestVectorSize; j++)
			{
				for(unsigned m=0; m<inflight; m++)
				{
					if(j < inflightTraces[m].size())
					{
						accessTraces[i/inflight].push_back(inflightTraces[m][j]);
						weightIncrements[i/inflight].push_back(inflightWeightIncrements[m][j]);
					}
				}
			}
				
			//cout << "  [" << setw(6) << i << "] trace with " << tr->size() << " entries covering " << Nacc_tr << " accesses" << endl;
			//os.close();
		}
		//cout << " Total " << Ntr << " addresses, " << Nacc << " accesses with max address " << accessCounts.size()-1 << endl;


		// write total access counts
		// os.open(fnOut+".frequency.txt");

		// unsigned check=0;
		// for(unsigned i=0;i<accessCounts.size();++i)
		// {
		// 	os << i << ' ' << accessCounts[i] << endl;
		// 	check += accessCounts[i];
		// }
		// //cout << "Access checksum: " << check << endl;

		// os.close();

		// Create a permutation into descending access frequency
		// Write into file with 3 columns
		os.open(fnOut+fnMesh+to_string(Npkt)+"-"+to_string(C.count())+"Sources_RankedOccurrence_AccumulationCache.txt");

		os << "# First column: address (TetraID/FaceID)" << endl;
		os << "# Second column: access frequency (Number of access)" << endl;
		os << "# Third column: cumulative access probability (What kind of hit rate would we get when including all tetras up to this line?)" << endl;

		vector<unsigned> perm(accessCounts.size());
		for(unsigned i=0;i<accessCounts.size();++i)
			perm[i]=i;


		boost::sort(perm,[&accessCounts](unsigned l,unsigned r){ return accessCounts[l] > accessCounts[r]; });
		
		// inverse permutation
		vector<unsigned> invperm(accessCounts.size());
		for(unsigned i=0;i<perm.size();++i)
			invperm[perm[i]]=i;

		unsigned nnz=0,csum=0;
		for(unsigned i=0;i<perm.size();++i)
		{
			if (accessCounts[perm[i]] > 0)
			{
				csum += accessCounts[perm[i]];
				os << perm[i] << ' ' << accessCounts[perm[i]] << ' ' << fixed << setprecision(5) << double(csum)/double(Nacc) << endl;
				++nnz;
			}
		}
		os.close();

		/**
		 * @brief Cache Architecture definitions
		 * 
		 * Parameters:
		 * Size of the cache: Number of entries we want to allow the cache to hold
		 * Associativity of the cache: Number of entries(ways), each set able to store
		 * Size of the cache has to be dividable by the number of ways
		 * 
		 * 1-way set associative cache -> direct mapped cache
		 * (size of the cache)-way set associative cache -> fully associative cache
		 */
		unsigned setNumber = cacheSize / ways;
		if ((ways * setNumber) != cacheSize)
		{
			cout << "The cache size and the number of ways do not create a valid cache." << endl
				<< "The cache size will be set to " << ways * setNumber << endl
				<< "New cache parameters:" << endl
				<< "Cache size: " << ways * setNumber << endl
				<< "      Ways:" << ways << endl
				<< "      Sets:" << setNumber << endl;
			cacheSize = ways * setNumber;
		}
		std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> zipfMap;
		std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> visitedCacheMap;
		// open a file in read mode.
    	ifstream infile;
		char tetraData[150];
		//vector<unsigned> cacheZipf;
		if(fnPermutation.empty())
        {
            // cacheZipf = perm;
			// cacheZipf.resize(zipfSize);
			for(unsigned i=0; i<zipfSize; i++)
			{
				if (i < perm.size())
				{
					zipfMap.insert(make_pair(perm[i],zipfMap.size()));
				}
				else
				{
					break;
				}
				
			}
        }
		else
		{
			infile.open(fnPermutation);
			infile.getline(tetraData, 150);
			infile.getline(tetraData, 150);
			infile.getline(tetraData, 150);
            for(unsigned i=0; i < zipfSize; i++)
            {
				if(infile.eof())
					break;
                infile >> tetraData;
                //cacheZipf.push_back(atoi(tetraData));
				zipfMap.insert(make_pair(atoi(tetraData),zipfMap.size()));
                infile >> tetraData; // we do not need the next two numbers of the file
                infile >> tetraData;
            }
		}

		vector<vector<unsigned>> cache;
		vector<vector<unsigned>> cacheReplacement;
		// Evicted tetraID and the number of accesses since the last eviction
		vector<pair<int, unsigned>> cacheEviction;
		//vector<unsigned> visitedCache;
		cache.resize(setNumber);
		cacheReplacement.resize(setNumber);
		for(unsigned i=0; i < cache.size(); i++)
		{
			cache[i].resize(ways);
			cacheReplacement[i].resize(ways);
			for(unsigned j=0; j < cacheReplacement[i].size(); j++)
			{
				cacheReplacement[i][j] = j;
				cache[i][j] = 0;
			}
		}
		//os.open(fnOut+fnMesh+to_string(Npkt)+"-"+to_string(C.count())+"Sources_CacheStatistics.txt");
		unsigned hits = 0, zipfHits = 0, misses = 0, compulsoryMisses = 0, capacityMisses = 0, conflictMisses = 0, totalAccesses = 0, accessesUntilEviction = 0;
		float progress = ceil(float(accessTraces.size()) / 100.0);
		cout << endl;
		for(unsigned m=0; m<accessTraces.size(); m++)
		{
			unsigned hits_1run = 0, zipfHits_1run = 0, misses_1run = 0, compulsoryMisses_1run = 0, capacityMisses_1run = 0, conflictMisses_1run = 0;
			 
			if((m % unsigned(progress) ) == 0)
			{
				cout << "Progress: " << double(m)/double(accessTraces.size())*100 << "\r" << flush;
			}
			for(unsigned i=0; i<accessTraces[m].size(); i++)
			{
				int tetraID = accessTraces[m][i];
				unsigned set = tetraID % setNumber;

				int hitPosition = -1; 
				bool cacheHit = false;
				//os << "Cache after requesting TetraID: " << tetraID << endl;
				vector<unsigned>::iterator iter;
				iter = find(cache[set].begin(),cache[set].end(), tetraID);

				if(useZipf && zipfMap.find(tetraID) != zipfMap.end())
				{
					zipfHits++;
					zipfHits_1run++;
					accessesUntilEviction++;
				}
				else
				{
					if(iter != cache[set].end() && tetraID > 0)
					{
						cacheHit = true;
						hitPosition = distance(cache[set].begin(), iter);
						accessesUntilEviction++;
						//os << "Cache hit: " << tetraID << " found in set " << set << " at position " << hitPosition << endl;
					}
					else
					{
						cacheHit = false;   
					}
					switch(strategy)
					{
						case LRU:   
							LeastRecentlyUsed(cacheHit, set, tetraID, hitPosition,
											misses, capacityMisses, conflictMisses, compulsoryMisses, hits, accessesUntilEviction,
											misses_1run, capacityMisses_1run, conflictMisses_1run, compulsoryMisses_1run, hits_1run,
											cache, cacheReplacement, visitedCacheMap, cacheEviction);
							break;
						case FIFO:  
							FirstInFirstOut(cacheHit, set, tetraID,
											misses, capacityMisses, conflictMisses, compulsoryMisses, hits, accessesUntilEviction,
											misses_1run, capacityMisses_1run, conflictMisses_1run, compulsoryMisses_1run, hits_1run,
											cache, cacheReplacement, visitedCacheMap, cacheEviction);
							break;
						case MRU:
							MostRecentlyUsed(cacheHit, set, tetraID, hitPosition,
											misses, capacityMisses, conflictMisses, compulsoryMisses, hits, accessesUntilEviction,
											misses_1run, capacityMisses_1run, conflictMisses_1run, compulsoryMisses_1run, hits_1run,
											cache, cacheReplacement, visitedCacheMap, cacheEviction);											
							break;
						default:
							break;
					}
				}
			
			}
			totalAccesses += accessTraces[m].size();
			// os 	<< "Cache statistics: " << endl
				// << setw(40) << "              Total accesses; " << totalAccesses <<													setw(40) << "; Incremental accesses; " << accessTraces[m].size() << endl
				// << setw(40) << "                  Total Hits; " << hits << 															setw(40) << "               ; Hits; " << hits_1run << endl
				// << setw(40) << "             Total Zipf Hits; " << zipfHits << 														setw(40) << "          ; Zipf Hits; " << zipfHits_1run << endl
				// << setw(40) << "                Total Misses; " << misses << 														setw(40) << "             ; Misses; " << misses_1run << endl
				// << setw(40) << "            Hit/Miss Balance; " << totalAccesses - hits - zipfHits - misses <<						setw(40) << "   ; Hit/Miss Balance; " << accessTraces[m].size() - hits_1run - zipfHits_1run - misses_1run << endl
				// << setw(40) << "       Total Capacity Misses; " << capacityMisses <<												setw(40) << "    ; Capacity Misses; " << capacityMisses_1run << endl
				// << setw(40) << "       Total Conflict Misses; " << conflictMisses <<												setw(40) << "    ; Conflict Misses; " << conflictMisses_1run << endl
				// << setw(40) << "     Total Compulsory Misses; " << compulsoryMisses <<												setw(40) << "  ; Compulsory Misses; " << compulsoryMisses_1run << endl
				// << setw(40) << "                Miss Balance; " << misses - capacityMisses - conflictMisses - compulsoryMisses << 	setw(40) << "       ; Miss Balance; " << misses_1run - capacityMisses_1run - conflictMisses_1run - compulsoryMisses_1run << endl
				// << setw(40) << "      Overall Cache Hit Rate; " << double(hits)/double(totalAccesses-zipfHits) << 					setw(40) << "     ; Cache Hit Rate; " << double(hits_1run)/double(accessTraces[m].size()-zipfHits_1run) << endl
				// << setw(40) << "       Overall Zipf Hit Rate; " << double(zipfHits)/double(totalAccesses) << 						setw(40) << "      ; Zipf Hit Rate; " << double(zipfHits_1run)/double(accessTraces[m].size()) << endl
				// << setw(40) << "   Overall Combined Hit Rate; " << double(zipfHits+hits)/double(totalAccesses) <<					setw(40) << "  ; Combined Hit Rate; " << double(zipfHits_1run+hits_1run)/double(accessTraces[m].size()) << endl
				// << setw(40) << "Overall Upper Bound Hit Rate; " << double(totalAccesses-compulsoryMisses)/double(totalAccesses) <<	setw(40) << "; Upper Bound Hit Rate; " << double(accessTraces[m].size()-compulsoryMisses_1run)/double(accessTraces[m].size()) << endl << endl;
				

		}
		//os.close();
		
		// Write into file with 2 columns
		os.open(fnOut+fnMesh+to_string(Npkt)+"-"+to_string(C.count())+"Sources_Evictions_AccumulationCache.txt");

		os << "# First column: address (TetraID/FaceID)" << endl;
		os << "# Second column: Number of accesses since last eviction" << endl;
		vector<pair<int, unsigned>> evictionEvents = cacheEviction;
		float averageAcessesUntilEviction = 0;
		for(unsigned i=0; i<cacheEviction.size(); i++)
		{
			averageAcessesUntilEviction += cacheEviction[i].second;
		}
		// write output definitions
		unsigned output_buffer1 = output_buffer;
		unsigned output_buffer2 = output_buffer;
		int writeComplete = 0;
		bool writeDone = true;
		int accessCounterBuf1 = 0;
		int accessCounterBuf2 = 0;
		unsigned stalls = 0;
		unsigned useBuffer = 1;
		for(unsigned i=0; i<cacheEviction.size(); ++i)
		{
			if (cacheEviction[i].first != 0)
			{
				os << cacheEviction[i].first << ' ' << cacheEviction[i].second << endl;

				// Start of output simulation
				// cout << endl << "Write Statistics:" << endl
				// 	 << "Writing elements to Buffer " << useBuffer << " (0 and 3 meaning stalls)" << endl
				// 	 << "Elements in Buffer 1: " << accessCounterBuf1 << endl
				// 	 << "Elements in Buffer 2: " << accessCounterBuf2 << endl
				// 	 << "Cycles until write completes: " << writeComplete << endl
				// 	 << "Number of Stalls due to full buffers: " << stalls << endl;

				writeComplete -= evictionEvents[i].second+1;
				if(writeComplete <= 0)
					writeDone = true;
				if (useBuffer == 1)
				{
					if (accessCounterBuf2 > 0 && (writeDone == true || writeComplete >= writeLatency) )
						accessCounterBuf2 -= evictionEvents[i].second+1; //+1 because consecutive evictions will have 0 accesses since the last eviction 
					if (accessCounterBuf2 < 0 && writeComplete <= 0)
					{
						accessCounterBuf2 = 0;
						writeComplete = 0;
					}
					if(accessCounterBuf1 < (int) output_buffer1)
					{
						accessCounterBuf1++;
					}
					if(accessCounterBuf1 == (int) output_buffer1) // if the first buffer is full, switch to 2nd buffer
					{
						if(accessCounterBuf2 == 0) // if Buffer2 is empty
						{
							useBuffer = 2; // use Buffer2 to store events
							if(writeDone == true)
							{
								writeComplete = writeLatency + output_buffer;
								writeDone = false;
							}

						}
						else
							useBuffer = 0; // both buffers are filled with data -> need to stall until one buffer can store data again
					}
					
				}
				else if (useBuffer == 2)
				{
					if (accessCounterBuf1 > 0 && (writeDone == true || writeComplete >= writeLatency) )
						accessCounterBuf1 -= evictionEvents[i].second+1; //+1 because consecutive evictions will have 0 accesses since the last eviction 
					if (accessCounterBuf1 < 0 && writeComplete <= 0)
					{
						accessCounterBuf1 = 0;
						writeComplete = 0;
					}
					if(accessCounterBuf2 < (int) output_buffer2)
					{
						accessCounterBuf2++;
					}
					if(accessCounterBuf2 == (int) output_buffer2)// if the second buffer is full, switch to the first buffer
					{
						
						if(accessCounterBuf1 == 0)
						{
							useBuffer = 1;
							if(writeDone == true)
							{
								writeComplete = writeLatency + output_buffer;
								writeDone = false;
							}
						}
						else
							useBuffer = 3;
					}
				}
				else if (useBuffer == 0)
				{
					
					if(writeDone == false)
					{
						stalls+=writeComplete;
						writeDone = true;
						writeComplete = 0;
					}
					else
					{
						stalls+=accessCounterBuf2;
						writeDone = false;
						writeComplete = writeLatency;
						accessCounterBuf2 = 0;
						useBuffer = 2;
					}
					evictionEvents[i].second = 0;
					i--;
				}
				else
				{
					
					if(writeDone == false)
					{
						stalls+=writeComplete;
						writeDone = true;
						writeComplete = 0;
					}
					else
					{
						stalls+=accessCounterBuf1;
						writeDone = false;
						writeComplete = writeLatency;
						accessCounterBuf1 = 0;
						useBuffer = 1;
					}
					evictionEvents[i].second = 0;
					i--;
				}
			}
		}
		os.close();
		
		cout << endl << "Average accesses until eviction: " << (float) averageAcessesUntilEviction/cacheEviction.size() << endl
			 << "Number of Stalls due to full buffers: " << stalls << endl;
		cout << endl;
		cout << "Final Cache statistics" << endl
			 << "   Total Accumulation Events; " << totalAccesses << endl
			 << "             Total Zipf Hits; " << zipfHits << endl											
			 << "                  Total Hits; " << hits << endl																													
			 << "                Total Misses; " << misses << endl														
			 << "                 Exit Events; " << totalAccesses - hits - zipfHits - misses << endl																		
			 << "      Overall Cache Hit Rate; " << double(hits)/double(totalAccesses-zipfHits) << endl					
			 << "       Overall Zipf Hit Rate; " << double(zipfHits)/double(totalAccesses) << endl						
			 << "   Overall Combined Hit Rate; " << double(zipfHits+hits)/double(totalAccesses) << endl
			 << "              Tetra coverage; " << nnz/(double) M->tetraCells()->size() << endl;
	}
	else
		cout << "Accumulation events missing" << endl;

	cout << "Done" << endl;
}
