/*
 * cacheDSE_memtrace.cpp
 *
 *  Created on: Dec 10, 2019
 *      Author: fynns
 */

#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernel.hpp>

#include <FullMonteSW/Kernels/Software/Logger/LoggerTuple.hpp>
#include <FullMonteSW/Kernels/Software/TetraMCKernelThread.hpp>

#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/OutputTypes/MemTraceSet.hpp>
#include <FullMonteSW/OutputTypes/MemTrace.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/OutputDataSummarize.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Kernels/Software/Logger/MemTraceScorer.hpp>
#include <FullMonteSW/Kernels/Software/Logger/EventScorer.hpp>


#include <fstream>
#include <string>
#include <tuple>
#include <map>
#include <chrono>
#include <unordered_map>
#include <boost/functional/hash.hpp>

// Define the kernel that just scores memory traces

typedef tuple<
		EventScorer,
		TetraMemTraceScorer> MemTraceKernelScorer;

class MemTraceKernel : public TetraMCKernel<RNG_SFMT_AVX,MemTraceKernelScorer>
{
public:
	/**
	 * @brief Construct a new Mem Trace Kernel object. This is just a test class for memory traces when using FullMonte
	 * and doesn't belong to the normal Kernel types of FullMonte <-- Can be ignored for TCL 
	 * 
	 */
	MemTraceKernel(){}
	virtual ~MemTraceKernel(){}

	typedef RNG_SFMT_AVX RNG;
};

using namespace std;

/**
 * @brief LRU cache replacement strategy. Updates the cache based on the LRU strategy and adapts the
 * LRU sequence accordingly. Also handles the hit and miss counts
 * 
 * @param cacheHit - Are we dealing with a cache hit or a cahce miss
 * @param set - The set which the current TetraID is assigned to
 * @param tetraID - The current TetraID
 * @param hitPosition - If cacheHit == true, at which position did we encounter the TetraID
 * @param os - Reference to output file stream
 * @param misses - Reference to counter for overall misses
 * @param capacityMisses - Reference to counter for capacity misses
 * @param conflictMisses - Reference to counter for conflict misses
 * @param compulsoryMisses - Reference to counter for compulsory misses
 * @param hits - Reference to counter of overall hits
 * @param cache - Reference to the cache
 * @param cacheReplacement - Reference to the replacement sequence of the cache
 * @param visitedCache - Reference to the vector storing all TetraID that have already been in the cache
 */
void LeastRecentlyUsed(bool cacheHit, unsigned ways, unsigned set, unsigned tetraID, int hitPosition, bool prefetch, tuple<unsigned, unsigned> adjacentTetras[4],
         unsigned &misses, unsigned &capacityMisses, unsigned &conflictMisses, unsigned &compulsoryMisses, unsigned &hits,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &visitedCacheMap)
{
    if (cacheHit == false)
        {
            misses ++;
            // Replace an element of the cache with the new requested element (LRU)
            cache[cacheReplacement[0][set]][set] = tetraID;
            unsigned accessedEntry = cacheReplacement[0][set];
            for(unsigned j=0; j < ways-1; j++)
            {
                cacheReplacement[j][set] = cacheReplacement[j+1][set];
            }
            cacheReplacement[ways-1][set] = accessedEntry;
			if (prefetch == true)
			{
				for (unsigned i=0; i < 4; i++)
				{
					unsigned prefetch_set = get<1>(adjacentTetras[i]);
					unsigned prefetch_tetraID = get<0>(adjacentTetras[i]);
					cache[cacheReplacement[0][prefetch_set]][prefetch_set] = prefetch_tetraID;
					unsigned prefetch_accessedEntry = cacheReplacement[0][prefetch_set];
					for(unsigned j=0; j < ways-1; j++)
					{
						cacheReplacement[j][prefetch_set] = cacheReplacement[j+1][prefetch_set];
					}
					cacheReplacement[ways-1][prefetch_set] = prefetch_accessedEntry;
					// check if the prefetched tetraIDs have never been in the cache before 
					// if yes -> record them as have been in the cache now
					if (visitedCacheMap.find(prefetch_tetraID) == visitedCacheMap.end())
					{
						visitedCacheMap.insert(make_pair(prefetch_tetraID, visitedCacheMap.size()));
					}
				}
			}
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
                }
                else
                {
                    conflictMisses++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
				visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }
        }
        else // cacheHit == true
        {
            hits++;

            int idx = -1;
			for (unsigned i=0; i < ways; i++)
			{
				if (cacheReplacement[i][set] == unsigned(hitPosition))
				{
					idx = i;
					break;
				}
			}
			if (idx == -1)
			{
				std::cout << "Fatal Error: Cache Replacement has not found an entry but it should exist. Aborting..." << endl;
				return;
			}
			unsigned index = unsigned(idx); 
            // since this cache entry has been used last, we will place it to the back of the least recently used sequence
            for(unsigned j=index; j < ways-1; j++)
            {
                cacheReplacement[j][set] = cacheReplacement[j+1][set];
            }
            cacheReplacement[ways-1][set] = unsigned(hitPosition);
        }
}

void FirstInFirstOut(bool cacheHit, unsigned ways, unsigned set, unsigned tetraID, bool prefetch, tuple<unsigned, unsigned> adjacentTetras[4],
         unsigned &misses, unsigned &capacityMisses, unsigned &conflictMisses, unsigned &compulsoryMisses, unsigned &hits,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &visitedCacheMap)
{
    if (cacheHit == false)
        {
            misses ++;
            // Replace an element of the cache with the new requested element (FIFO)
            cache[cacheReplacement[0][set]][set] = tetraID;
            
            cacheReplacement[0][set] = (cacheReplacement[0][set]+1)%ways;
			if (prefetch == true)
			{
				for (unsigned i=0; i < 4; i++)
				{
					unsigned prefetch_set = get<1>(adjacentTetras[i]);
					unsigned prefetch_tetraID = get<0>(adjacentTetras[i]);
					cache[cacheReplacement[0][prefetch_set]][prefetch_set] = prefetch_tetraID;
					cacheReplacement[0][prefetch_set] = (cacheReplacement[0][prefetch_set]+1)%ways;
		
					// check if the prefetched tetraIDs have never been in the cache before 
					// if yes -> record them as have been in the cache now
					if (visitedCacheMap.find(prefetch_tetraID) == visitedCacheMap.end())
					{
						visitedCacheMap.insert(make_pair(prefetch_tetraID, visitedCacheMap.size()));
					}
				}
			}
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
                }
                else
                {
                    conflictMisses++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
                visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }
        }
        else // cacheHit == true
        {
            hits++;
            // with a FIFO replacement strategy, a hit does not influence the replacement of the block
            // --> Do nothing
        }
}

void MostRecentlyUsed(bool cacheHit, unsigned ways, unsigned set, unsigned tetraID, int hitPosition, bool prefetch, tuple<unsigned, unsigned> adjacentTetras[4],
         unsigned &misses, unsigned &capacityMisses, unsigned &conflictMisses, unsigned &compulsoryMisses, unsigned &hits,
         vector<vector<unsigned>> &cache, vector<vector<unsigned>> &cacheReplacement, std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> &visitedCacheMap)
{
    if (cacheHit == false)
        {
            misses ++;
            // Replace an element of the cache with the new requested element (MRU)
			int idx = -1;
			for (unsigned i=0; i < ways; i++)
			{
				if (cache[i][set] == 0)
				{
					idx = i;
					break;
				}
			}
            if(idx != -1) // if we have space in this set of the cache
            {
                unsigned index = unsigned(idx);
                cache[set][index] = tetraID;

                for(unsigned j=0; j < ways-1; j++)
                {
                    cacheReplacement[j][set] = cacheReplacement[j+1][set];
                }
                cacheReplacement[ways-1][set] = index;
            }
            else // if this set of the cache is full
            {
                // Replace the most recently used element with the current element
                cache[cacheReplacement[ways-1][set]][set] = tetraID;
            }
			if (prefetch == true)
			{
				for (unsigned i=0; i < 4; i++)
				{
					unsigned prefetch_set = get<1>(adjacentTetras[i]);
					unsigned prefetch_tetraID = get<0>(adjacentTetras[i]);
					
					int idx_prefetch = -1;
					for (unsigned j=0; j < ways; j++)
					{
						if (cache[j][prefetch_set] == 0)
						{
							idx_prefetch = j;
							break;
						}
					}
					if(idx_prefetch != -1) // if we have space in this set of the cache
					{
						unsigned index = unsigned(idx_prefetch);
						cache[index][prefetch_set] = prefetch_tetraID;

						for(unsigned j=0; j < ways-1; j++)
						{
							cacheReplacement[j][prefetch_set] = cacheReplacement[j+1][prefetch_set];
						}
						cacheReplacement[ways-1][prefetch_set] = index;
					}
					else // if this set of the cache is full
					{
						// Replace the most recently used element with the current element
						cache[cacheReplacement[ways-1][prefetch_set]][prefetch_set] = prefetch_tetraID;
					}
					// check if the prefetched tetraIDs have never been in the cache before 
					// if yes -> record them as have been in the cache now
					if (visitedCacheMap.find(prefetch_tetraID) == visitedCacheMap.end())
					{
						visitedCacheMap.insert(make_pair(prefetch_tetraID, visitedCacheMap.size()));
					}
				}
			}
            
            // check if the element has already been in the cache
            if (visitedCacheMap.find(tetraID) != visitedCacheMap.end())
            {
                bool fullCache = true;
                // check if Cache is full -> Capacity miss, otherwise a conflict miss
                for(auto &s: cache)
                {
                    if (find(s.begin(), s.end(), 0) != s.end())
                    {
                        fullCache = false;
                        break;
                    }
                }
                if(fullCache == true) // we have a capacity miss
                {
                    capacityMisses++;
                }
                else
                {
                    conflictMisses++;
                }
            }
            else // element has never been in the cache before
            {
                compulsoryMisses++;
                visitedCacheMap.insert(make_pair(tetraID, visitedCacheMap.size()));
            }
        }
        else // cacheHit == true
        {
            hits++;
            vector<unsigned>::iterator iter = find(cacheReplacement[set].begin(), cacheReplacement[set].end(), hitPosition);
            unsigned index = distance(cacheReplacement[set].begin(), iter);
            // since this cache entry has been used last, we will place it to the back of the least recently used sequence
            for(unsigned j=index; j < cache[set].size()-1; j++)
            {
                cacheReplacement[set][j] = cacheReplacement[set][j+1];
            }
            cacheReplacement[set][cache[set].size()-1] = hitPosition;
        }
}

enum ReplacementStrategy{LRU, FIFO, MRU};

enum MeshString {Unknown, HeadNeck, Colin, Bladder, Cube};
static map<string, MeshString> mapMeshString;

static void initializeStringMapping()
{
	mapMeshString["HEADNECK"] = HeadNeck;
	mapMeshString["COLIN"] = Colin;
	mapMeshString["BLADDER"] = Bladder;
	mapMeshString["CUBE"] = Cube;
}


int main(int argc,char **argv)
{
	initializeStringMapping();
	boost::program_options::options_description cmdline;

	string fnMesh, fnOut, replacementStrategy, fnPermutation;

	vector<float> point, pencilbeam, ball, line, fiber;
    unsigned source_volume;


	unsigned long long Npkt;
	float wmin = 1e-5, prwin = 0.1;
    unsigned Nthreads=MAX_THREAD_COUNT, maxHits = 10000, maxSteps = 10000, seed = 0;
	unsigned inflight = 1;
	bool useZipf = false, prefetch = false, useCache = true;
    unsigned cacheSize, ways, zipfSize;

	cmdline.add_options()
            ("strategy,s",boost::program_options::value<string>(&replacementStrategy),"What replacement strategy should the cache use? (LRU/FIFO/MRU)")
			("mesh,m",boost::program_options::value<string>(&fnMesh),"Which mesh should be used for simulation? Options: {HeadNeck, Colin, Bladder, Cube}")
			("point",boost::program_options::value<vector<float>>(&point)->multitoken(),"Define position of a point source (syntax {1.0,2.0,3.0})")
            ("pencil",boost::program_options::value<vector<float>>(&pencilbeam)->multitoken(),"Define position (1st 3 values) and direction (next 3 values) of a pencilbeam source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("line",boost::program_options::value<vector<float>>(&line)->multitoken(),"Define start position (1st 3 values) and end position (next 3 values) of a line source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("ball",boost::program_options::value<vector<float>>(&ball)->multitoken(),"Define position (1st 3 values) and radius (4th value) of a ball source (syntax {1.0,2.0,3.0,4.0})")
            ("srcvolume",boost::program_options::value<unsigned>(&source_volume)->default_value(0),"Define the element ID of a volume source (syntax 123456")
            ("fiber",boost::program_options::value<vector<float>>(&fiber)->multitoken(),"Define position (1st 3 values), direction (next 3 values), radius (7th value), and numerical aperture (8th value) of a fibercone source (syntax 123456")
            ("threads",boost::program_options::value<unsigned>(&Nthreads)->default_value(MAX_THREAD_COUNT),"Number of threads to use")
			("packets,N",boost::program_options::value<unsigned long long>(&Npkt),"Packet count")
			("wmin",boost::program_options::value<float>(&wmin)->default_value(1e-5),"Roulette threshold (default: 1e-5)")
            ("prwin",boost::program_options::value<float>(&prwin)->default_value(0.1),"Roulette threshold (default: 0.1)")
            ("hits",boost::program_options::value<unsigned>(&maxHits)->default_value(10000),"Number of maximum hits of a photon (default: 10000)")
            ("steps",boost::program_options::value<unsigned>(&maxSteps)->default_value(10000),"Number of maximum steps of a photon (default: 10000)")
			("rng",boost::program_options::value<unsigned>(&seed)->default_value(0),"Seed for RNG (default: no seed)")
			("inflight",boost::program_options::value<unsigned>(&inflight)->default_value(1),"How many packets-in-flight should be handled by the cache? (default: 1)")
			("permutation",boost::program_options::value<string>(&fnPermutation),"Input file for memory access permutation")
			("outputFile",boost::program_options::value<string>(&fnOut),"Output file prefix")
			("help,h","Get help")
			;

	boost::program_options::variables_map vm;

	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	vm.notify();

	if (vm.count("help"))
	{
		std::cout << cmdline << endl;
		return -1;
	}
	else if (fnMesh.empty())
	{
		std::cout << "Missing mesh definition in command line" << endl;
		std::cout << cmdline << endl;
		return -1;
	}

	ReplacementStrategy strategy;
    if (replacementStrategy.compare("LRU") == 0)
    {
        strategy = LRU;
    }
    else if (replacementStrategy.compare("FIFO") == 0)
    {
        strategy = FIFO;
    }
    else if (replacementStrategy.compare("MRU") == 0)
    {
        strategy = MRU;
    }
    else
    {
        std::cout << "Could not recognize the desired replacement strategy. Defaulting to LRU." << endl;
        strategy = LRU;
    }

	string filename = FULLMONTE_DATA_DIR;
	
	VTKMeshReader VTK_MR;
	TIMOSMeshReader TIMOS_MR;

	TetraMesh* M;
	//Define MaterialSet to hold all materials for the supported meshes
    MaterialSet MS;

	/**     Material definitions for all mesh types
	 * @brief Material(float muA,float muS,float g,float n);
	 * muA -> absorption coefficient
	 * muS -> scattering coefficient
	 * g   -> anisotropy
	 * n   -> refractive index
	 */
    Material air(0.0,0.0,0.0,1.0);
	MS.exterior(&air);
    
	// HeadNeck Materials
    Material tongue(0.95,83.3,0.926,1.37);
    Material larynx(0.55,15.0,0.9,1.36);
    Material tumour(0.13,9.35,0.92,1.39);
    Material teeth(0.99,60.0,0.95,1.48);
    Material bone(0.3,100.0,0.9,1.56);
    Material surroundingtissues(1.49,10.0,0.9,1.35);
    Material subcutaneousfat(0.2,30.0,0.78,1.32);
    Material skin(2.0,187.0,0.93,1.38);
    
	// Bladder Materials
    Material surround(0.5,100.0,0.9,1.39);
    Material _void(0.01,0.1,0.9,1.37);

	// Colin Materials
	Material skalp(0.019,7.8,0.89,1.37);
	Material skull(0.004,0.009,0.89,1.37);
	Material greymatter(0.02,9.0,0.89,1.37);
	Material whitematter(0.08,40.9,0.84,1.37);

	// Cube Materials
	Material bigcube(0.05,20.0,0.9,1.3);
	Material smallcube1(0.1,10.0,0.7,1.1);
	Material smallcube2(0.2,20.0,0.8,1.2);
	Material smallcube3(0.1,10.0,0.9,1.4);
	Material smallcube4(0.2,20.0,0.9,1.5); 
	
	std::transform(fnMesh.begin(), fnMesh.end(), fnMesh.begin(), ::toupper);
	switch(mapMeshString[fnMesh])
	{
		case HeadNeck:
			filename.append("/HeadNeck/HeadNeck.mesh.vtk");
			VTK_MR.filename(filename);
			VTK_MR.read();
			M = VTK_MR.mesh();

			MS.append(&tongue);
			MS.append(&tumour);
			MS.append(&larynx);
			MS.append(&teeth);
			MS.append(&bone);
			MS.append(&surroundingtissues);
			MS.append(&subcutaneousfat);
			MS.append(&skin);
			break;
		case Bladder:
			filename.append("/Bladder/July2017BladderWaterMesh1.mesh.vtk");
			VTK_MR.filename(filename);
			VTK_MR.read();
			M = VTK_MR.mesh();
			
        	MS.append(&surround);
        	MS.append(&_void);
			break;
		case Colin:
        	filename.append("/Colin27/Colin27.mesh");
			TIMOS_MR.filename(filename);
			TIMOS_MR.read();
			M = TIMOS_MR.mesh();

			MS.append(&skalp);
			MS.append(&skull);
			MS.append(&greymatter);
			MS.append(&whitematter);
			break;
		case Cube:
			filename.append("/TIM-OS/cube_5med/cube_5med.mesh");
			TIMOS_MR.filename(filename);
			TIMOS_MR.read();
			M = TIMOS_MR.mesh();

			MS.append(&bigcube);
			MS.append(&smallcube1);
			MS.append(&smallcube2);
			MS.append(&smallcube3);
			MS.append(&smallcube4);
			break;
		default:
			std::cout << "No valid mesh specified! Exiting..." << endl;
			std::cout << cmdline << endl;
			return -1;
	}

	unsigned blocks = Npkt / inflight;
	if ((blocks * inflight) != Npkt)
	{
		std::cout << "The defined packets-in-flight and the number of simulated packets do not fit exactly." << endl
			<< "The number of simulated packets will be set to " << blocks * inflight << endl;
		Npkt = blocks * inflight;
	}
	std::cout << "Packets: " << Npkt << endl;
	std::cout << "Threads: " << Nthreads << endl;

	Source::Composite C;

    Source::Point P;
    if(!point.empty() && point.size() == 3)
    {
        P.position({point[0], point[1], point[2]});
        C.add(&P);
    }
    
    Source::PencilBeam PB;
    if(!pencilbeam.empty() && pencilbeam.size() == 6)
    {
        PB.position({pencilbeam[0], pencilbeam[1], pencilbeam[2]});
        PB.direction({pencilbeam[3], pencilbeam[4], pencilbeam[5]});
        C.add(&PB);
    }

    Source::Volume V;
    if(source_volume != 0)
    {
        V.elementID(source_volume);
        C.add(&V);  
    }

    Source::Ball B;
    if (!ball.empty() && ball.size() == 4)
    {
        B.position({ball[0], ball[1], ball[2]});
        B.radius(ball[3]);
        C.add(&B);
    }

    Source::Line L;
    if(!line.empty() && line.size() == 6)
    {
        L.endpoint(0, {line[0], line[1], line[2]});
        L.endpoint(1, {line[3], line[4], line[5]});
        C.add(&L);
    }

    Source::Fiber F;
    if(!fiber.empty() && fiber.size() == 8)
    {
        F.fiberPos({fiber[0], fiber[1], fiber[2]});
        F.fiberDir({fiber[3], fiber[4], fiber[5]});
        F.radius(fiber[6]);
        F.numericalAperture(fiber[7]);
        C.add(&F);
    }
	if(C.elements().empty())
	{
		std::cout << "Missing at least one source definition in command line" << endl;
		std::cout << cmdline << endl;
		return -1;
	}

	MemTraceKernel K;

	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.geometry(M);
	K.source(&C);
	K.materials(&MS);

	K.runSync();
	
	OutputDataCollection* res = K.results();

	const auto memtrace = static_cast<const MemTraceSet*>			(res->getByName("TetraMemTrace"));
	//const auto memcount = static_cast<const SpatialMap<unsigned>*>	(res->getByName("MemAccessCount"));

	const auto events = static_cast<MCEventCountsOutput*>		(res->getByName("EventCounts"));

	if (events)
	{
		OutputDataSummarize OS;
		OS.visit(events);
	}
	else
		std::cout << "Event counts missing" << endl;

	if (memtrace)
	{
		// Write raw traces, and accumulate total access counts
		vector<unsigned> accessCounts;
		vector<vector<unsigned>> accessTraces;
		accessTraces.resize(memtrace->size()/inflight);
		vector<vector<unsigned>> inflightTraces;
		inflightTraces.resize(inflight);

		//cout << "Memory trace set found with " << memtrace->size() << " entries" << endl;
		unsigned Ntr=0,Nacc=0;
		for(unsigned i=0;i<memtrace->size();i+=inflight)
		{
			for(unsigned m=0; m<inflight; m++)
			{
				inflightTraces[m].resize(0);	
				//os.open(fnOut+".traces_photon"+to_string(i)+".txt");
				MemTrace* tr = memtrace->get(i+m);
				Ntr += tr->size();
				unsigned Nacc_tr=0;
				for(unsigned j=0;j<tr->size();++j)
				{
					unsigned addr=tr->get(j).address;
					unsigned count=tr->get(j).count;
					Nacc_tr += count;

					if (addr >= accessCounts.size())
						accessCounts.resize(addr+1,0);

					accessCounts[addr] += count;
					//os << addr << ' ' << count << endl;
					for(unsigned n=0; n<count; n++)
					{
						inflightTraces[m].push_back(addr);
					}
					
				}
				Nacc += Nacc_tr;
			}
			unsigned largestVectorSize = 0;
			for(unsigned m=0; m<inflight; m++)
			{
				if (largestVectorSize < inflightTraces[m].size())
					largestVectorSize = inflightTraces[m].size();
			}
			for(unsigned j=0; j<largestVectorSize; j++)
			{
				for(unsigned m=0; m<inflight; m++)
				{
					if(j < inflightTraces[m].size())
						accessTraces[i/inflight].push_back(inflightTraces[m][j]);
				}
			}
		}



		vector<unsigned> perm(accessCounts.size());
		for(unsigned i=0;i<accessCounts.size();++i)
			perm[i]=i;


		boost::sort(perm,[&accessCounts](unsigned l,unsigned r){ return accessCounts[l] > accessCounts[r]; });
		
		// inverse permutation
		vector<unsigned> invperm(accessCounts.size());
		for(unsigned i=0;i<perm.size();++i)
			invperm[perm[i]]=i;

		unsigned nnz=0,csum=0;
		for(unsigned i=0;i<perm.size();++i)
		{
			if (accessCounts[perm[i]] > 0)
			{
				csum += accessCounts[perm[i]];
				++nnz;
			}
		}
		
		vector<unsigned> permFile;
		if(!fnPermutation.empty())
		{
			// Read file for that stored access permutations for a lesser amount of photon packet simulations
			// open a file in read mode.
			ifstream infile;
			char tetraData[150];
			
			infile.open(fnPermutation);
			infile.getline(tetraData, 150);
			infile.getline(tetraData, 150);
			infile.getline(tetraData, 150);
			while(!infile.eof())
			{
				infile >> tetraData;
				permFile.emplace_back(atoi(tetraData));
				infile >> tetraData; // we do not need the next two numbers of the file
				infile >> tetraData;								
			}
			infile.close();
		}

		/**
		 * @brief Cache Architecture definitions
		 * 
		 * Parameters:
		 * Size of the cache: Number of entries we want to allow the cache to hold
		 * Associativity of the cache: Number of entries(ways), each set able to store
		 * Size of the cache has to be dividable by the number of ways
		 * 
		 * 1-way set associative cache -> direct mapped cache
		 * (size of the cache)-way set associative cache -> fully associative cache
		 */
		
		// Possible simulation runs:
		// 0. Pure LRU Cache size 512 - 50.000
		// 0.1 Associativity 1 way -> 16 way
		// 1. Pure LRU + Prefetch Cache size 512 - 50.000
		// 1.1 Associativity 1 way -> 16 way
		// 2. LRU + Zipf Cache combined size 512 - 50.000
		// 2.1 Associativity 1 way -> 16 way
		// 3. LRU + Prefetch + Zipf Cache combined size 512 - 50.000
		// 3.1 Associativity 1 way -> 16 way
		// 4. Pure Zipf Cache size 512 - 50.000
		for (unsigned sims=0; sims < 5; sims++)
		{
			unsigned maxSizeCache, maxSizeZipf;
			switch(sims)
			{
				case 0:
					useCache = true;
					prefetch = false;
					useZipf = false;
					maxSizeZipf = 0;
					maxSizeCache = 51200;
					break;
				case 1:
					useCache = true;
					prefetch = true;
					useZipf = false;
					maxSizeZipf = 0;
					maxSizeCache = 51200;
					break;
				case 2:
					useCache = true;
					prefetch = false;
					useZipf = true;
					maxSizeZipf = 51200;
					maxSizeCache = 51200;
					break;
				case 3:
					useCache = true;
					prefetch = true;
					useZipf = true;
					maxSizeZipf = 51200;
					maxSizeCache = 51200;
					break;
				case 4:
					useCache = false;
					prefetch = false;
					useZipf = true;
					maxSizeZipf = 51200;
					maxSizeCache = 0;
					break;
				default:
					break;
			}
			ofstream os;
			os.open(fnOut+fnMesh+to_string(Npkt)+"-"+to_string(C.count())+"Sources_"+to_string(useCache)+"useCache_"+to_string(prefetch)+"prefetch_"+to_string(useZipf)+"useZipf"+"_CacheStatistics.txt");
			os << "CacheSize Ways Prefetching ZipfSize Accesses ZipfHits Hits Misses CapacityMisses ConflictMisses CompulsoryMisses CacheHitRate ZipfHitRate CombinedHitRate UpperBoundHitRate TetraCoverage" << endl;

			//for (cacheSize=2048; cacheSize <= maxSizeCache; cacheSize+=2048)
			cacheSize=2048;
			do
			{
				//for (ways = 1; ways <= 16; ways*=2)
				ways = 1;
				do
				{
					//for (zipfSize=2048; zipfSize <= maxSizeZipf-cacheSize; zipfSize+=2048)
					zipfSize=2048;
					unsigned currentCacheSize;
					do
					{
						unsigned setNumber = cacheSize / ways;
						if ((ways * setNumber) != cacheSize)
						{
							std::cout << "The cache size and the number of ways do not create a valid cache." << endl
								<< "The cache size will be set to " << ways * setNumber << endl
								<< "New cache parameters:" << endl
								<< "Cache size: " << ways * setNumber << endl
								<< "      Ways:" << ways << endl
								<< "      Sets:" << setNumber << endl;
							cacheSize = ways * setNumber;
						}
					
						std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> zipfMap;
						std::unordered_map<unsigned, unsigned, boost::hash<unsigned>> visitedCacheMap;

						if(fnPermutation.empty())
						{
							for(unsigned i=0; i<zipfSize; i++)
							{
								if (i < perm.size())
									zipfMap.insert(make_pair(perm[i],zipfMap.size()));
								else
									break;
							}
						}
						else
						{
							for(unsigned i=0; i<zipfSize; i++)
							{
								if (i < permFile.size())
									zipfMap.insert(make_pair(permFile[i],zipfMap.size()));
								else
									break;
							}
						}

						vector<vector<unsigned>> cache;
						vector<vector<unsigned>> cacheReplacement;
						cache.resize(ways);
						cacheReplacement.resize(ways);
						for(unsigned i=0; i < cache.size(); i++)
						{
							cache[i].resize(setNumber);
							cacheReplacement[i].resize(setNumber);
							for(unsigned j=0; j < cacheReplacement[i].size(); j++)
							{
								cacheReplacement[i][j] = i;
							}
						}
						
						unsigned hits = 0, zipfHits = 0, misses = 0, compulsoryMisses = 0, capacityMisses = 0, conflictMisses = 0, totalAccesses = 0;
						float progress = ceil(float(accessTraces.size()-1) / 100.0);
						std::cout << endl;
						// Record start time
						auto start = std::chrono::high_resolution_clock::now();
						for(unsigned m=0; m<accessTraces.size(); m++)
						{							
							if((m % unsigned(progress) ) == 0)
							{
								std::cout << "Progress: " << double(m)/double(accessTraces.size())*100 << "\r" << flush;
							}
							else if (m == accessTraces.size()-1)
							{
								// Record end time
								auto finish = std::chrono::high_resolution_clock::now();
								std::chrono::duration<double> elapsed = finish - start;

								std::cout << "Progress: 100% ...DONE -> Elapsed Time: " << elapsed.count() << " sec\r" << flush;
							}
							for(unsigned i=0; i<accessTraces[m].size(); i++)
							{
								unsigned tetraID = accessTraces[m][i];
								unsigned set = tetraID % setNumber;
								// store adjacent tetraID and the respective set it has to go into
								tuple<unsigned, unsigned> adjacentTetras[4] = {	make_tuple((unsigned) M->tetraFaceLinks()->get(tetraID)[0].tetraID, (unsigned) M->tetraFaceLinks()->get(tetraID)[0].tetraID % setNumber),
																				make_tuple((unsigned) M->tetraFaceLinks()->get(tetraID)[1].tetraID, (unsigned) M->tetraFaceLinks()->get(tetraID)[1].tetraID % setNumber),
																				make_tuple((unsigned) M->tetraFaceLinks()->get(tetraID)[2].tetraID, (unsigned) M->tetraFaceLinks()->get(tetraID)[2].tetraID % setNumber),
																				make_tuple((unsigned) M->tetraFaceLinks()->get(tetraID)[3].tetraID, (unsigned) M->tetraFaceLinks()->get(tetraID)[3].tetraID % setNumber)
																			};
								int hitPosition = -1; 
								bool cacheHit = false;
								//os << "Cache after requesting TetraID: " << tetraID << endl;
								for (unsigned j=0; j < ways; j++)
								{
									if (cache[j][set] == tetraID)
									{
										hitPosition = j;
										break;
									}
								}

								if(useZipf && zipfMap.find(tetraID) != zipfMap.end())
								{
									zipfHits++;
								}
								else if(useCache == true)
								{
									if(hitPosition != -1)
									{
										cacheHit = true;
										//os << "Cache hit: " << tetraID << " found in set " << set << " at position " << hitPosition << endl;
									}
									else
									{
										cacheHit = false;   
									}
									switch(strategy)
									{
										case LRU:   
											LeastRecentlyUsed(cacheHit, ways, set, tetraID, hitPosition, prefetch, adjacentTetras,
															misses, capacityMisses, conflictMisses, compulsoryMisses, hits,
															cache, cacheReplacement, visitedCacheMap);
											break;
										case FIFO:  
											FirstInFirstOut(cacheHit, ways, set, tetraID, prefetch, adjacentTetras,
															misses, capacityMisses, conflictMisses, compulsoryMisses, hits,
															cache, cacheReplacement, visitedCacheMap);
											break;
										case MRU:
											MostRecentlyUsed(cacheHit, ways, set, tetraID, hitPosition, prefetch, adjacentTetras,
															misses, capacityMisses, conflictMisses, compulsoryMisses, hits,
															cache, cacheReplacement, visitedCacheMap);											
											break;
										default:
											break;
									}
								}
								else
								{
									misses++;
								}
							
							}
							totalAccesses += accessTraces[m].size();

						}
						if (useCache == true)
						{
							os << cacheSize << " " << ways << " " << prefetch;
						}
						else
						{
							os << 0 << " " << 0 << " " << prefetch;
						}
						if (useZipf == true)
						{
							os << " " << zipfSize << " ";
						}
						else
						{
							os << " " << 0 << " ";
						}
						
						double cacheHitRate;
						if ((totalAccesses-zipfHits) == 0)
						{
							cacheHitRate = 0;
						}
						else
						{
							cacheHitRate = double(hits)/double(totalAccesses-zipfHits);
						}
 						os  << totalAccesses << " " << zipfHits << " " << hits << " " << misses << " " << capacityMisses << " " 
						 	<< conflictMisses << " " << compulsoryMisses << " " << cacheHitRate << " "					
							<< double(zipfHits)/double(totalAccesses) << " " << double(zipfHits+hits)/double(totalAccesses) << " " 
							<< double(totalAccesses-compulsoryMisses)/double(totalAccesses) << " " << nnz/(double) M->tetraCells()->size() << endl;

						if (useZipf == false)
							break;

						zipfSize+=2048;
						
						if (useCache == false)
							currentCacheSize = 0;
						else
							currentCacheSize = cacheSize;
					} while (zipfSize <= maxSizeZipf-currentCacheSize);
					if (useCache == false)
						break;
					
					ways*=2;
				} while(ways <= 16);
				if (useCache == false)
					break;
				
				cacheSize+=2048;
			} while(cacheSize <= maxSizeCache);
			os.close();
		}
		
	}
	else
		std::cout << "Memory trace missing" << endl;

	std::cout << "Done" << endl;
	
}
