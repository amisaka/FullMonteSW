#include <boost/timer/timer.hpp>
#include <fstream>

#include <string>

#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>
#include <FullMonteSW/Storage/VTK/VTKMeshWriter.hpp>
#include <FullMonteSW/Storage/VTK/VTKSurfaceWriter.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>

#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Ball.hpp>
#include <FullMonteSW/Geometry/Sources/Line.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>
#include <FullMonteSW/Geometry/Sources/TetraFace.hpp>
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>
#include <FullMonteSW/Geometry/Sources/CylDetector.hpp> 
#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>

#include <FullMonteSW/Kernels/Software/TetraInternalKernel.hpp>
#include <FullMonteSW/Kernels/Software/TetraSurfaceKernel.hpp>

#include <FullMonteSW/Kernels/SeedSweep.hpp>
#include <FullMonteSW/Queries/BasicStats.hpp>
#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Queries/EventCountComparison.hpp>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#ifdef CUDAACCEL_ENABLED
#include <FullMonteSW/Kernels/CUDA/TetraCUDAVolumeKernel.hpp>
#endif

/**
 * @brief Fullmonte program which is similar to the .tcl main file.
 * 
 * Example: call this program call from the binary folder 
 * bin/run_fullmonte --point={7.9683,108.2337,-45.4809} --input=0 --packets=10 --output=output_mesh --volume=true --threads=8
 * if you want to see all available options run
 * bin/run_fullmonte --help
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */

int main(int argc,char **argv)
{
	int input=0;				// choose between different input files

    string meshFile = "";
    string opticalPropsFile = "";
    bool isVTK = true;

	string oPrefix;

    vector<float> point;
    vector<float> pencilbeam;
    vector<float> ball;
    vector<float> line;
    unsigned source_volume = 0;
    vector<float>fiber;
    vector<float> cyl_detector;
    std::string cylDetectorType = "";

	unsigned Nthreads=MAX_THREAD_COUNT;
	unsigned long long Npkt;
	float energy;
	float wmin;
    float prwin;
    unsigned maxHits;
    unsigned maxSteps;
    unsigned seed;

    bool volume;
    bool surface;
    unsigned region;


	boost::program_options::options_description cmdline("Allowed options");

    // Explanation of the available command line options to make this program as flexible as possible
	cmdline.add_options()
			("help","Show help")
			("input",boost::program_options::value<int>(&input)->default_value(0),"Which mesh should be used as input file? 0->HeadNeck (default); 1->Bladder; 2->Mesh file should be supplied with option --meshFile and optical properties should be supplied with --opticalPropsFile")
            ("isVTK", boost::program_options::value<bool>(&isVTK)->default_value(true), "If input == 2, you can specify if the mesh supplied is in .vtk format (1) or .mesh format (0) (default: 1)")
            ("meshFile", boost::program_options::value<string>(&meshFile)->default_value(""), "VTK mesh file to read in case input = 2")
            ("opticalPropsFile", boost::program_options::value<string>(&opticalPropsFile)->default_value(""), "Optical properties file to read in case input = 2")
			("point",boost::program_options::value<vector<float>>(&point)->multitoken(),"Define position of a point source (syntax {1.0,2.0,3.0})")
            ("pencil",boost::program_options::value<vector<float>>(&pencilbeam)->multitoken(),"Define position (1st 3 values) and direction (next 3 values) of a pencilbeam source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("line",boost::program_options::value<vector<float>>(&line)->multitoken(),"Define start position (1st 3 values) and end position (next 3 values) of a line source (syntax {1.0,2.0,3.0,1.0,2.0,3.0})")
            ("ball",boost::program_options::value<vector<float>>(&ball)->multitoken(),"Define position (1st 3 values) and radius (4th value) of a ball source (syntax {1.0,2.0,3.0,4.0})")
            ("srcvolume",boost::program_options::value<unsigned>(&source_volume)->default_value(0),"Define the element ID of a volume source (syntax 123456")
            ("fiber",boost::program_options::value<vector<float>>(&fiber)->multitoken(),"Define position (1st 3 values), direction (next 3 values), radius (7th value), and numerical aperture (8th value) of a fibercone source (syntax 123456")
            ("CylDetector", boost::program_options::value<vector<float>>(&cyl_detector)->multitoken(), "Define start position (1st 3 values), end position (2nd 3 values), radius (7th value) and numerical aperture (8th value) of a cylindrical detector. (syntax {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0}). Must define at least one source of the above as well. You can specify the detection type to either 'Full', 'EXPONENTIAL' or 'ODE' by setting the option --detectionType (default is EXPONENTIAL).")
            ("detectionType", boost::program_options::value<std::string>(&cylDetectorType)->default_value("EXPONENTIAL"), "Detection type of the cylindrical detector. FULL detects every thing within NA of the detector. EXPONENTIAL detects portion of the packet weights based on a decaying exponential profile with respect to the packet position from the proximal end. ODE detects portion of the packet weight based on an decaying exponential of an exponential of the packet positin profile, which is the solution of the ODE that relates it to the scattering profile. (default: Exponential)") 
            ("threads",boost::program_options::value<unsigned>(&Nthreads)->default_value(MAX_THREAD_COUNT),"Number of threads to use")
			("output",boost::program_options::value<string>(&oPrefix),"Output file")
			("packets",boost::program_options::value<unsigned long long>(&Npkt)->default_value(10000),"Number of packets to run (default: 10000)")
			("energy",boost::program_options::value<float>(&energy)->default_value(10),"The energy value that the number of packets represent (default: 10)")
			("wmin",boost::program_options::value<float>(&wmin)->default_value(1e-5),"Roulette threshold (default: 1e-5)")
            ("prwin",boost::program_options::value<float>(&prwin)->default_value(0.1),"Roulette threshold (default: 0.1)")
            ("hits",boost::program_options::value<unsigned>(&maxHits)->default_value(10000),"Number of maximum hits of a photon (default: 10000)")
            ("steps",boost::program_options::value<unsigned>(&maxSteps)->default_value(10000),"Number of maximum steps of a photon (default: 10000)")
            ("volume",boost::program_options::value<bool>(&volume)->default_value(0),"Output the volume fluence to a mesh and textfile (default: false)")
            ("surface",boost::program_options::value<bool>(&surface)->default_value(0),"Output the exterior surface fluence to a mesh and textfile (default: false)")
            ("region",boost::program_options::value<unsigned>(&region)->default_value(0),"Set the region for outputting the surface fluence of that specific region to a mesh and textfile (default: 0)")
            ("rng",boost::program_options::value<unsigned>(&seed)->default_value(0),"Seed for RNG (default: no seed)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline, boost::program_options::command_line_style::unix_style ^ boost::program_options::command_line_style::allow_short),vm);
	boost::program_options::notify(vm);

	if (vm.count("help") || argc == 1)
	{
		cmdline.print(cout);
		return 0;
	}

    // Read the mesh from the specified file (either HeadNeck or Bladder mesh)
    VTKMeshReader R;
    TIMOSMeshReader TR;
    if (input == 0)
    {
        string filename = FULLMONTE_DATA_DIR;
        filename.append("/HeadNeck/HeadNeck.mesh.vtk");
        R.filename(filename);
    }
    else if (input == 1)
    {
        string filename = FULLMONTE_DATA_DIR;
        filename.append("/Bladder/July2017BladderWaterMesh1.mesh.vtk");
        R.filename(filename);
    }
    else if (input == 2)
    {
        if (meshFile == "") 
        {
            std::cout << "Mesh file not supplied with input = 2" << std::endl;
            return 1;
        }
        if (isVTK) 
            R.filename(meshFile); 
        else 
            TR.filename(meshFile);
    }
    else
    {
        std::cout << "Only '0' for the HeadNeck mesh, '1' for the Bladder mesh, or '2' for supplied mesh are accepted values for the input option" << std::endl;
        return 1;
    }
   
    TetraMesh* mesh;

    if (isVTK) {
        R.read();
        mesh = R.mesh();
    } else {
        TR.read(); 
        mesh = TR.mesh(); 
    }

    //Define Materials for the HeadNeck mesh or for the Bladder mesh
    MaterialSet MS;
    Material air;
    
    Material tongue;
    Material larynx;
    Material tumour;
    Material teeth;
    Material bone;
    Material surroundingtissues;
    Material subcutaneousfat;
    Material skin;
    
    Material surround;
    Material _void;
    
    if (input == 0)
    {
        
        air.scatteringCoeff(0);
        air.absorptionCoeff(0);
        air.anisotropy(0.0);
        air.refractiveIndex(1.0);

        
        tongue.scatteringCoeff(83.3);
        tongue.absorptionCoeff(0.95);
        tongue.anisotropy(0.926);
        tongue.refractiveIndex(1.37);

        
        larynx.scatteringCoeff(15);
        larynx.absorptionCoeff(0.55);
        larynx.anisotropy(0.9);
        larynx.refractiveIndex(1.36);

        
        tumour.scatteringCoeff(9.35);
        tumour.absorptionCoeff(0.13);
        tumour.anisotropy(0.92);
        tumour.refractiveIndex(1.39);

        
        teeth.scatteringCoeff(60);
        teeth.absorptionCoeff(0.99);
        teeth.anisotropy(0.95);
        teeth.refractiveIndex(1.48);

        
        bone.scatteringCoeff(100);
        bone.absorptionCoeff(0.3);
        bone.anisotropy(0.9);
        bone.refractiveIndex(1.56);

        
        surroundingtissues.scatteringCoeff(10);
        surroundingtissues.absorptionCoeff(1.49);
        surroundingtissues.anisotropy(0.9);
        surroundingtissues.refractiveIndex(1.35);

        
        subcutaneousfat.scatteringCoeff(30);
        subcutaneousfat.absorptionCoeff(0.2);
        subcutaneousfat.anisotropy(0.78);
        subcutaneousfat.refractiveIndex(1.32);

        
        skin.scatteringCoeff(187);
        skin.absorptionCoeff(2.0);
        skin.anisotropy(0.93);
        skin.refractiveIndex(1.38);

        MS.exterior(&air);
        MS.append(&tongue);
        MS.append(&tumour);
        MS.append(&larynx);
        MS.append(&teeth);
        MS.append(&bone);
        MS.append(&surroundingtissues);
        MS.append(&subcutaneousfat);
        MS.append(&skin);
    }
    else if (input == 1)
    {

        air.scatteringCoeff(0);
        air.absorptionCoeff(0);
        air.anisotropy(0.0);
        air.refractiveIndex(1.37);
        
        surround.scatteringCoeff(100.0);
        surround.absorptionCoeff(0.5);
        surround.anisotropy(0.9);
        surround.refractiveIndex(1.39);

        _void.scatteringCoeff(0.1);
        _void.absorptionCoeff(0.01);
        _void.anisotropy(0.9);
        _void.refractiveIndex(1.37);

        MS.exterior(&air);
        MS.append(&surround);
        MS.append(&_void);
    }
    else if (input == 2)
    {
        if (opticalPropsFile == "") {
            std::cout << "Optical properties file not supplied with input = 2" << std::endl;
            return 1;
        }

        TIMOSMaterialReader MR;
        MR.filename(opticalPropsFile);
        MR.read();
        MS = *(MR.materials()); 
    }

    /**
     * @brief Configure the sources; if they are defined in the arguments, we will add them to the composite source.
     * We will always call th
     * 
     */
 
	Source::Composite C;
	Source::Point P;
	if(!point.empty() && point.size() == 3)
	{
		P.position({point[0], point[1], point[2]});
		C.add(&P);
	}
	
	Source::PencilBeam PB;
	if(!pencilbeam.empty() && pencilbeam.size() == 6)
	{
		PB.position({pencilbeam[0], pencilbeam[1], pencilbeam[2]});
		PB.direction({pencilbeam[3], pencilbeam[4], pencilbeam[5]});
		C.add(&PB);
	}

	Source::Volume V;
	if(source_volume != 0)
	{
		V.elementID(source_volume);
		C.add(&V);  
	}

	Source::Ball B;
	if (!ball.empty() && ball.size() == 4)
	{
		B.position({ball[0], ball[1], ball[2]});
		B.radius(ball[3]);
		C.add(&B);
	}

	Source::Line L;
	if(!line.empty() && line.size() == 6)
	{
		L.endpoint(0, {line[0], line[1], line[2]});
		L.endpoint(1, {line[3], line[4], line[5]});
		C.add(&L);
	}

	Source::Fiber F;
	if(!fiber.empty() && fiber.size() == 8)
	{
		F.fiberPos({fiber[0], fiber[1], fiber[2]});
		F.fiberDir({fiber[3], fiber[4], fiber[5]});
		F.radius(fiber[6]);
		F.numericalAperture(fiber[7]);
		C.add(&F);
	}

	Source::CylDetector cylD; 
	if (!cyl_detector.empty() && cyl_detector.size() == 8) 
	{
		cylD.endpoint(0, {cyl_detector[0], cyl_detector[1], cyl_detector[2]});
		cylD.endpoint(1, {cyl_detector[3], cyl_detector[4], cyl_detector[5]});
		cylD.radius(cyl_detector[6]); 
		cylD.numericalAperture(cyl_detector[7]); 

		if (cylDetectorType == "") {
			cylD.detectionType("EXPONENTIAL");
		} else {
			cylD.detectionType(cylDetectorType); 
		}

		C.add(&cylD); 
	}

	VolumeCellInRegionPredicate vol;
	vol.setRegion(region);

	// CUDA option - if enabled
#ifdef CUDAACCEL_ENABLED
	TetraCUDAVolumeKernel cudaK;
	cudaK.packetCount(Npkt);
	cudaK.source(&C);
	cudaK.geometry(mesh);
	cudaK.materials(&MS);
	cudaK.roulettePrWin(prwin);
	cudaK.rouletteWMin(wmin);
	cudaK.maxHits(maxHits);
	cudaK.maxSteps(maxSteps);
	if (seed != 0)
	{
		cudaK.randSeed(seed);
	}
	cudaK.runSync();
	
	OutputDataCollection* cudaODC = cudaK.results();
	EnergyToFluence cudaEF;
	cudaEF.kernel(&cudaK);
	cudaEF.data(cudaODC->getByName("VolumeEnergy"));
	cudaEF.inputEnergy();
	cudaEF.outputFluence();
	cudaEF.update();

	TextFileMatrixWriter cudaTW;
	cudaTW.filename(oPrefix+"_volume_cuda.txt");
	cudaTW.source(cudaODC->getByName("VolumeEnergy"));
	cudaTW.write();
#endif

	TetraInternalKernel K;
	K.packetCount(Npkt);
	K.energyPowerValue(energy);
	K.source(&C);
	K.geometry(mesh);
	K.materials(&MS);
	if (region != 0)
	{
		K.directedSurfaceScorer().addScoringRegionBoundary(&vol);
	}
	// Additional parameters to set (optional);
	K.roulettePrWin(prwin);
	K.rouletteWMin(wmin);
	K.maxHits(maxHits);
	K.maxSteps(maxSteps);
	K.threadCount(Nthreads);
	if (seed != 0)
	{
		K.randSeed(seed);
	}
	

	K.runSync();

	OutputDataCollection* ODC = K.results();
	
	if (volume == true)
	{
		EnergyToFluence EF;
		EF.kernel(&K);
		EF.data(ODC->getByName("VolumeEnergy"));
		EF.outputFluence();
		EF.update();

		VTKMeshWriter W;
		W.filename(oPrefix+"_fluence.vtk");
		W.addData("Fluence", EF.result());
		W.mesh(mesh);
		W.write();

		TextFileMatrixWriter TW;
		TW.filename(oPrefix+"_fluence.txt");
		TW.source(ODC->getByName("VolumeEnergy"));
		TW.write();
	}

	if (surface == true)
	{
		EnergyToFluence EF;
		EF.kernel(&K);
		EF.data(ODC->getByName("SurfaceExitEnergy"));
		EF.outputFluence();
		EF.update();

		string filename = oPrefix;
		filename.append("_surface.vtk");

		VTKSurfaceWriter W;
		W.filename(filename.c_str());
		W.addData("Fluence", EF.result());
		W.mesh(mesh);
		W.write();

		TextFileMatrixWriter TW;
		TW.filename(oPrefix+"_surface.txt");
		TW.source(ODC->getByName("SurfaceExitEnergy"));
		TW.write();
	}
	if (region != 0)
	{
		EnergyToFluence EF;
		EF.kernel(&K);
		EF.data(ODC->getByName("DirectedSurface"));
		EF.outputFluence();
		EF.update();

		string filename = oPrefix;
		filename.append("_directedsurface.vtk");

		VTKSurfaceWriter W;
		W.filename(filename.c_str());
		W.addData("Fluence", EF.result());
		W.mesh(mesh);
		W.write();

		TextFileMatrixWriter TW;
		TW.filename(oPrefix+"_directedsurface.txt");
		TW.source(ODC->getByName("DirectedSurface"));
		TW.write();
	}

    if (!cyl_detector.empty() && cyl_detector.size() == 8) {
        std::cout << "Weight of photon packets captured by detector = " << cylD.detectedWeight() << std::endl;
    }

   return 0;
}
