/*
 * vis_mesh_compare.cpp
 *
 *  Created on: Nov 23, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSOutputReader.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceReader.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMatrixReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCResultReader.hpp>

#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/Tests/DummyKernel.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/VTK/vtkFullMonteTetraMeshWrapper.h>
#include <FullMonteSW/VTK/vtkFullMonteArrayAdaptor.h>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkFloatArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <iostream>
using namespace std;

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

int main(int argc,char **argv)
{
	boost::program_options::options_description opt;

	string fnGeom, fnOpt, fnRef, fnOut, fnTest;

	opt.add_options()
			("geometry",boost::program_options::value<string>(&fnGeom), "Geometry file (MMC, VTK, or TIM-OS format)")
			("opt",boost::program_options::value<string>(&fnOpt),"Optical-properties file (MMC or TIM-OS format)")
			("ref",boost::program_options::value<string>(&fnRef),"Reference data field")
			("test",boost::program_options::value<string>(&fnTest),"Test data field")
			("cv","Output coefficient of variation")
			("surface,s","Surface comparison")
			("volume,v","Volume comparison")
			("energy,e","Output energy")
			("fluence,f","Output fluence")
			("zscore,z","Output z scores")
			("ratio,r","Output ratio")
			("output",boost::program_options::value<string>(&fnOut),"Output VTK file name")
			;

	boost::program_options::variables_map vm;

	store(parse_command_line(argc,argv,opt),vm);
	notify(vm);

	TetraMesh* M = nullptr;

	if (vm.count("help"))
	{
		cout << opt;
		return 0;
	}
	else if (!vm.count("ref") || !vm.count("test"))
	{
		cout << "Test and reference datasets must be provided" << endl;
		cout << opt;
		return 0;
	}

	if (vm.count("geometry"))
	{
		TIMOSMeshReader MR;
		MR.filename(fnGeom);
		MR.read();

		M = MR.mesh();
	}

	MaterialSet* MS = nullptr;

	if (vm.count("opt"))
	{
		TIMOSMaterialReader OR;
		OR.filename(fnOpt);
		OR.read();

		MS = OR.materials();
	}

	DummyKernel k;
	k.geometry(M);
	k.materials(MS);

	TextFileMeanVarianceReader RR;
	RR.filename(fnRef);
	RR.read();

	MeanVarianceSet* ref = RR.result();

	SpatialMap<float>* E_test=nullptr;
	SpatialMap<float>* phi_test=nullptr;
	SpatialMap<float>* E_ref=nullptr;
	SpatialMap<float>* phi_ref=nullptr;

	boost::filesystem::path testPath = fnTest;

	if (testPath.extension() == ".dat")		// MMC
	{
		cout << "Test data is in MMC format" << endl;

		MMCResultReader R;
		R.filename(fnTest);
		R.read();

		E_test = R.output();
	}
	else if (testPath.extension() == ".txt")	// FullMonte
	{
		cout << "Test data is in FullMonte format" << endl;

		TextFileMatrixReader R;
		R.filename(fnTest);
		R.read();

		E_test = static_cast<SpatialMap<float>*>(R.output());
		E_test->spatialType(AbstractSpatialMap::Volume);
	}
	else if (testPath.extension() == ".timos")
	{
		cout << "Test data is in TIM-OS format" << endl;

		TIMOSOutputReader R;
		R.filename(fnTest);
		R.read();

		phi_test = static_cast<SpatialMap<float>*>(R.volumeFluence());

		EnergyToFluence EF;
		EF.inputFluence();
		EF.outputEnergy();

		EF.kernel(&k);
		EF.data(phi_test);
		EF.update();

		E_test = static_cast<SpatialMap<float>*>(EF.result());
	}


	if (!phi_test)
	{
		if (!M || !MS)
			cout << "Cannot output fluence for lack of geometry &/ materials infomation" << endl;
		if (!E_test)
			cout << "Cannot output fluence for lack of energy information" << endl;

		EnergyToFluence EF;
		EF.kernel(&k);
		EF.inputEnergy();
		EF.outputFluence();
		EF.data(E_test);
		EF.update();

		phi_test = static_cast<SpatialMap<float>*>(EF.result());
	}

	if(!M || !MS)
	{
		cout << "Cannot output fluence for lack of geometry &/ materials infomation" << endl;
	}
	else
	{
		EnergyToFluence EF;
		EF.inputEnergy();
		EF.outputFluence();
		EF.kernel(&k);
		EF.data(ref->mean());

		EF.update();

		phi_ref = static_cast<SpatialMap<float>*>(EF.result());
		E_ref = ref->mean();
	}


	vtkFullMonteTetraMeshWrapper *VTKM = vtkFullMonteTetraMeshWrapper::New();
	VTKM->mesh(M);
	VTKM->update();

	vtkUnstructuredGrid* ug = VTKM->regionMesh();

	unsigned Ne = M->tetraCells()->size();


	// Provide field of Z Scores

	if (!vm.count("zscore"))
	{}
	else if (!ref && !ref->variance())
		cout << "Z Scores skipped due to missing reference dataset" << endl;
	else if (!E_test)
		cout << "Z Scores skipped due to missing test set energy" << endl;
	else
	{
		vtkFloatArray* arr_z = vtkFloatArray::New();
		arr_z->SetNumberOfTuples(Ne);
		arr_z->SetName("Z Score");

		for(unsigned i=0;i<Ne;++i)
		{
			float mu = ref->mean()->get(i);
			float sigma = sqrt(ref->variance()->get(i));

			float x = E_test->get(i);

			if(sigma == 0 && x == 0)
				arr_z->SetValue(i,0);
			else if (sigma == 0 && x > 0)
				arr_z->SetValue(i,0);
			else
				arr_z->SetValue(i,(x-mu)/sigma);
		}

		ug->GetCellData()->AddArray(arr_z);
	}




	// If fluence is available, add a two-component array with the fluences (test scaled to match reference)
	//	0:	Reference set
	//	1:	Test set

	{
		vtkFloatArray* vtkPhi = vtkFloatArray::New();
		vtkPhi->SetNumberOfComponents(2);
		vtkPhi->SetNumberOfTuples(Ne);
		vtkPhi->SetName("Fluence");


		vtkPhi->SetComponentName(0,"Reference");
		vtkPhi->SetComponentName(1,"Test");

		for(unsigned i=0;i<Ne;++i)
		{
			vtkPhi->SetComponent(i,0,phi_ref->get(i));
			vtkPhi->SetComponent(i,1,phi_test->get(i));
		}

		ug->GetCellData()->AddArray(vtkPhi);
	}



	// If fluence is available, add a two-component array with the fluences (test scaled to match reference)
	//	0:	Reference set
	//	1:	Test set

	{
		vtkFloatArray* vtkCV = vtkFloatArray::New();
		vtkCV->SetNumberOfTuples(Ne);
		vtkCV->SetName("CV");

		for(unsigned i=0;i<Ne;++i)
		{
			float mu = ref->mean()->get(i);
			float sigma = sqrt(ref->variance()->get(i));
			//if (sigma == 0)
			//	vtkCV->SetValue(i,0);
			//else
				vtkCV->SetValue(i,sigma/mu);
		}

		ug->GetCellData()->AddArray(vtkCV);
	}



	// Compute ratio of the two fields (Test/Ref)
	if (!vm.count("ratio"))
	{
	}
	else if (!E_test)
		cout << "Skipping ratio output due to missing E_test" << endl;
	else if (!E_ref)
		cout << "Skipping ratio output due to missing E_ref" << endl;
	else
	{
		vtkFloatArray* vtkRatio = vtkFloatArray::New();
		vtkRatio->SetNumberOfTuples(Ne);
		vtkRatio->SetName("Ratio");

		ug->GetCellData()->AddArray(vtkRatio);

		for(unsigned i=0;i<Ne;++i)
		{
			float test = E_test->get(i);
			float ref = E_ref->get(i);

			if (ref != 0)
				vtkRatio->SetValue(i,test/ref);
			else if (test == 0)					// 0/0 => equal => ratio is 1
				vtkRatio->SetValue(i,1.0f);
			else								// ratio NaN since ref == 0, but no clean way to represent in text file
				vtkRatio->SetValue(i,1.0f);
		}
	}

	vtkUnstructuredGridWriter *VTKW = vtkUnstructuredGridWriter::New();
	VTKW->SetFileName(fnOut.c_str());
	VTKW->SetFileTypeToBinary();
	VTKW->SetInputData(ug);
	VTKW->Update();
}
