package require FullMonte

#### Sets up Colin27 for a point source
## 
##
## Objects
##
##      TP      TetraEnclosingPointByLinearSearch
##      P       A point source
##      $M      The mesh
##      MS      The material set
##
##
##
## Variables
#   meshfn
#   packets
#   rngseed
#   pos 
#   tetra


#### BASIC RUN PARAMS

set meshfn  "$FullMonte::datadir/Colin27/Colin27.mesh"
set packets 1000000
set rngseed 1234
set pos "71.2 149.8 127.5"

Point P
    P position $pos



#### LOAD MESH AND LOCATE TETRA (for TIMOS and MMC)

TIMOSMeshReader TR
    TR filename $meshfn
    TR read
    set M [TR mesh]

puts "Read mesh"

TetraEnclosingPointByLinearSearch TP
    TP point $pos
    TP mesh $M
    TP update

set tetra [TP tetraID]
puts "Point $pos is located in tetra $tetra"

P elementHint $tetra



#### OPTICAL PROPERTIES

Material air
    air scatteringCoeff 0.0
    air absorptionCoeff 0.0
    air refractiveIndex 1.0
    air anisotropy      0.0

# Fang2010 optical properties for 630nm
Material skull
    skull scatteringCoeff   7.8
    skull absorptionCoeff   0.019
    skull anisotropy        0.89
    skull refractiveIndex   1.37

Material csf
    csf scatteringCoeff   0.009
    csf absorptionCoeff   0.004
    csf anisotropy        0.89
    csf refractiveIndex   1.37

Material graymatter
    graymatter scatteringCoeff   9.0
    graymatter absorptionCoeff   0.02
    graymatter anisotropy        0.89
    graymatter refractiveIndex   1.37

Material whitematter
    whitematter scatteringCoeff   40.9
    whitematter absorptionCoeff   0.08
    whitematter anisotropy        0.84
    whitematter refractiveIndex   1.37

MaterialSet MS
    MS exterior air
    MS set 1 skull
    MS set 2 csf
    MS set 3 graymatter
    MS set 4 whitematter


#### Create MMC files

MMCOpticalWriter MMCW
    MMCW filename "prop_colin27-point0.mmc.dat"
    MMCW materials MS
    MMCW write

MMCJSONWriter MMCJ
    MMCJ filename       "colin27-point0.mmc.json"
    MMCJ packets $packets
    MMCJ seed $rngseed
    MMCJ sessionname    "colin27-point0.mmc"
    MMCJ meshfilename   "colin27-point0.mmc"
    MMCJ mmccommand     "$FullMonte::mmcpath"
    MMCJ initelem       $tetra
    MMCJ timestep       0 1 1
    MMCJ srcpos         $pos
    MMCJ write



#### Create TIM-OS files

#(mesh already exists)

TIMOSSourceWriter       TS
    TS filename         "colin27-point0.timos.source"
    TS source           P
    TS packets          $packets
    TS write

TIMOSMaterialWriter     TM
    TM materials        MS
    TM filename         "colin27.timos.opt" 
    TM write

## Write a perturbed version with 1% higher scattering in the gray matter
    graymatter scatteringCoeff [expr [graymatter scatteringCoeff]*1.01]
    TM filename         "colin27-perturb.timos.opt"
    TM write

exit 
