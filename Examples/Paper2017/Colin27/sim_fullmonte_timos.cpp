/*
 * sim_fullmonte_mmc.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */


#include <string>

#include <FullMonteSW/Kernels/Software/TetraSVKernel.hpp>
#include <FullMonteSW/Kernels/SeedSweep.hpp>

#include <FullMonteSW/Queries/BasicStats.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>

#include <boost/timer/timer.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

/** sim_fullmonte_timos
 *
 * Runs FullMonte's kernel with a single TIMOS test case
 *
 */

int main(int argc,char **argv)
{
	string fnMesh, fnOpt, fnSource;

	string oPrefix;
	string oFnFmt;

	unsigned Nthreads=MAX_THREAD_COUNT;
	unsigned Nr=1;
	float wmin=1e-5;
	unsigned long long Npkt=0;

	boost::program_options::options_description cmdline;

	cmdline.add_options()
			("help,h","Show help")
			("geometry,g",boost::program_options::value<string>(&fnMesh),"Geometry file")
			("source,s",boost::program_options::value<string>(&fnSource),"Source definition file")
			("threads,t",boost::program_options::value<unsigned>(&Nthreads),"Number of threads to use")
			("opt",boost::program_options::value<string>(&fnOpt),"Optical properties")
			("output,o",boost::program_options::value<string>(&oPrefix),"Output file prefix")
			("packets,p",boost::program_options::value<unsigned long long>(&Npkt),"Number of packets to run")
			("runs,r",boost::program_options::value<unsigned>(&Nr),"Number of independent runs")
			("details",boost::program_options::value<string>(&oFnFmt),"Provide per-run details (include sprintf-style %d for seed number")
			("wmin",boost::program_options::value<float>(&wmin),"Roulette threshold (default 1e-5)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	boost::program_options::notify(vm);

	if (vm.count("help"))
	{
		cmdline.print(cout);
		return 0;
	}

	// read specified test case
	TetraMesh* M = nullptr;
	MaterialSet* MS = nullptr;
	Source::Abstract* S = nullptr;

	if (vm.count("geometry"))
	{
		cout << "Reading mesh description from " << fnMesh << endl;
		TIMOSMeshReader R;
		R.filename(fnMesh);
		R.read();

		M = R.mesh();

		if (!M)
		{
			cout << "Failed to read " << fnMesh << endl;
			return -1;
		}
	}
	else
	{
		cout << "No geometry definition provided!" << endl;
		return -1;
	}

	if (vm.count("opt"))
	{
		TIMOSMaterialReader R;
		R.filename(fnOpt);
		R.read();

		MS = R.materials();
	}
	else
	{
		cout << "No optical properties specified" << endl;
		return -1;
	}

	if (vm.count("source"))
	{
		TIMOSSourceReader R;
		R.filename(fnSource);
		R.read();

		S = R.source();

		unsigned long long p = R.packets();

		cout << "Source file specifies " << p << " packets" << endl;

		if (Npkt)
			cout << "Overriding packet count with argument: " << Npkt << endl;
		else
			Npkt = p;
	}
	else
	{
		cout << "No sources specified" << endl;
	}


	// set up kernel

	cout << "Running " << Nr << "x" << Npkt << " simulations using "<< Nthreads << " threads" << endl;
	cout << "  wmin=" << wmin << endl;

	TetraSVKernel K;
	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.rouletteWMin(wmin);
	K.source(S);
	K.geometry(M);
	K.materials(MS);

	if (Nr > 1)
	{
		// set up seed sweep
		SeedSweep SS;
		SS.kernel(&K);
		SS.seedRange(1,Nr+1);
		SS.printProgress(true);
		SS.run();

		OutputDataCollection* EV = SS.resultsByName("VolumeEnergy");

		if (!oFnFmt.empty())
		{
			char oFn[1024];
			TextFileMatrixWriter W;

			cout << "Writing files: " << flush;

			for(unsigned i=0;i<EV->size();++i)
			{
				snprintf(oFn,1024,oFnFmt.c_str(),i);

				string fn = string(oFn)+".EV.txt";
				W.filename(fn);
				W.source(EV->getByIndex(i));
				W.write();
				cout << i << " " << flush;
			}
			cout << endl;
		}

		BasicStats BS;
		BS.input(EV);
		BS.update();

		SpatialMap<float>* mu = static_cast<SpatialMap<float>*>(BS.mean());
		SpatialMap<float>* sigma2 = static_cast<SpatialMap<float>*>(BS.variance());

		MeanVarianceSet MVS;
		MVS.mean(mu);
		MVS.variance(sigma2);
		MVS.packetsPerRun(Npkt);
		MVS.runs(Nr);

		cout << "Writing mean-variance volume energy" << endl;

		TextFileMeanVarianceWriter W;
		W.filename(oPrefix + ".EV.mve.out");
		W.input(&MVS);
		W.write();


		OutputDataCollection* ES = SS.resultsByName("SurfaceExitEnergy");

		if (!oFnFmt.empty())
		{
			char oFn[1024];
			TextFileMatrixWriter W;

			cout << "Writing files: " << flush;

			for(unsigned i=0;i<ES->size();++i)
			{
				snprintf(oFn,1024,oFnFmt.c_str(),i);

				string fn = string(oFn)+".ES.txt";

				W.filename(fn);
				W.source(ES->getByIndex(i));
				W.write();
				cout << i << " " << flush;
			}
			cout << endl;
		}

		BasicStats SBS;
		SBS.input(ES);
		SBS.update();

		SpatialMap<float>* muS = static_cast<SpatialMap<float>*>(SBS.mean());
		SpatialMap<float>* sigma2S = static_cast<SpatialMap<float>*>(SBS.variance());

		cout << "Writing mean-variance surface energy" << endl;

		MeanVarianceSet MVSS;
		MVSS.mean(muS);
		MVSS.variance(sigma2S);
		MVSS.packetsPerRun(Npkt);
		MVSS.runs(Nr);

		W.filename(oPrefix + ".ES.mve.out");
		W.input(&MVSS);
		W.write();

	}
	else
	{
		K.runSync();


		OutputDataCollection* res = K.results();

		SpatialMap<float>* EV = static_cast<SpatialMap<float>*>(res->getByName("VolumeEnergy"));

		SpatialMap<float>* ES = static_cast<SpatialMap<float>*>(res->getByName("SurfaceExitEnergy"));

		// write converted MCML output
		TextFileMatrixWriter W;
		W.filename(oPrefix + ".EV.txt");
		W.source(EV);
		W.write();

		W.filename(oPrefix + ".ES.txt");
		W.source(ES);
		W.write();
	}
}
