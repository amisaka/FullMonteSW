/*
 * sim_fullmonte_mmc.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */


#include <string>

#include <FullMonteSW/Kernels/Software/TetraSVKernel.hpp>
#include <FullMonteSW/Kernels/SeedSweep.hpp>
#include <FullMonteSW/Queries/BasicStats.hpp>

#include <FullMonteSW/Storage/MMC/MMCJSONCase.hpp>
#include <FullMonteSW/Storage/MMC/MMCJSONReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCTextMeshReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCOpticalReader.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>



#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/timer/timer.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

/** sim_fullmonte_mmc
 *
 * Runs FullMonte's kernel with multiple seeds and aggregates the output into a mean-variance dataset.
 *
 */

int main(int argc,char **argv)
{
	string iJSON;							// input filename

	string oPrefix;
	string oFnFmt;

	unsigned Nthreads=MAX_THREAD_COUNT;
	unsigned Nr=1;
	float wmin=1e-5;
	unsigned long long Npkt=0;

	boost::program_options::options_description cmdline;

	cmdline.add_options()
			("help,h","Show help")
			("json,j",boost::program_options::value<string>(&iJSON),"MMC .json input file")
			("threads,t",boost::program_options::value<unsigned>(&Nthreads),"Number of threads to use")
			("output,o",boost::program_options::value<string>(&oPrefix),"Output file prefix (defaults to session ID from .json file)")
			("packets,p",boost::program_options::value<unsigned long long>(&Npkt),"Number of packets to run, defaults to number in input file")
			("runs,r",boost::program_options::value<unsigned>(&Nr),"Number of runs")
			("details",boost::program_options::value<string>(&oFnFmt),"Output filename string with %d specifying sequence number")
			("wmin",boost::program_options::value<float>(&wmin),"Roulette threshold (default 1e-5)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	boost::program_options::notify(vm);

	if (vm.count("help"))
	{
		cmdline.print(cout);
		return 0;
	}

	// read specified test case
	MMCJSONCase* C = nullptr;
	TetraMesh* M = nullptr;
	MaterialSet* MS = nullptr;

	if (vm.count("json"))
	{
		cout << "Reading problem description from " << iJSON << endl;
		MMCJSONReader R;
		R.filename(iJSON);
		R.read();

		C = R.result();

		if (!C)
		{
			cout << "Failed to read " << iJSON << endl;
			return -1;
		}

		C->print(cout);

		string iFn = C->meshId();

		cout << "Reading MMC-format mesh from " << iFn << endl;


		MMCTextMeshReader MR;
		MR.path(".");
		MR.prefix(iFn);
		MR.read();

		M = MR.mesh();


		string matFn = "prop_" + C->meshId() + ".dat";
		cout << "Reading MMC-format materials from " << matFn << endl;

		MMCOpticalReader OR;
		OR.filename(matFn);
		OR.read();
		MS = OR.materials();
	}
	else
	{
		cout << "No problem definition provided!" << endl;
		return -1;
	}


	if (Npkt == 0)
		Npkt = C->photons();

	if (oPrefix.empty())
	{
		// session names typically have .mmc appended. Remove all the
		oPrefix = C->sessionId();
		oPrefix = oPrefix.substr(0,oPrefix.find_first_of("."));
	}

	// set up kernel

	cout << "Running " << Nr << "x" << Npkt << " simulations using "<< Nthreads << " threads" << endl;
	cout << "  Input " << iJSON << " output " << oPrefix << endl;
	cout << "  wmin=" << wmin << endl;

	TetraSVKernel K;
	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.rouletteWMin(wmin);
	K.source(C->createSource());
	K.geometry(M);
	K.materials(MS);

	if (vm.count("runs"))
	{
		// set up seed sweep
		SeedSweep SS;
		SS.kernel(&K);
		SS.seedRange(1,Nr+1);
		SS.printProgress(true);
		SS.run();

		OutputDataCollection* EV = SS.resultsByName("VolumeEnergy");

		if (!oFnFmt.empty())
		{
			char oFn[1024];
			TextFileMatrixWriter W;

			cout << "Writing files: " << flush;

			for(unsigned i=0;i<EV->size();++i)
			{
				snprintf(oFn,1024,oFnFmt.c_str(),i);

				string fn = string(oFn)+".EV.txt";
				W.filename(fn);
				W.source(EV->getByIndex(i));
				W.write();
				cout << i << " " << flush;
			}
			cout << endl;
		}

		BasicStats BS;
		BS.input(EV);
		BS.update();

		SpatialMap<float>* mu = static_cast<SpatialMap<float>*>(BS.mean());
		SpatialMap<float>* sigma2 = static_cast<SpatialMap<float>*>(BS.variance());

		MeanVarianceSet MVS;
		MVS.mean(mu);
		MVS.variance(sigma2);
		MVS.packetsPerRun(Npkt);
		MVS.runs(Nr);

		cout << "Writing mean-variance volume energy" << endl;

		TextFileMeanVarianceWriter W;
		W.filename(oPrefix + ".EV.mve.out");
		W.input(&MVS);
		W.write();


		OutputDataCollection* ES = SS.resultsByName("SurfaceExitEnergy");

		if (!oFnFmt.empty())
		{
			char oFn[1024];
			TextFileMatrixWriter W;

			cout << "Writing files: " << flush;

			for(unsigned i=0;i<ES->size();++i)
			{
				snprintf(oFn,1024,oFnFmt.c_str(),i);

				string fn = string(oFn)+".ES.txt";

				W.filename(fn);
				W.source(ES->getByIndex(i));
				W.write();
				cout << i << " " << flush;
			}
			cout << endl;
		}

		BasicStats SBS;
		SBS.input(ES);
		SBS.update();

		SpatialMap<float>* muS = static_cast<SpatialMap<float>*>(SBS.mean());
		SpatialMap<float>* sigma2S = static_cast<SpatialMap<float>*>(SBS.variance());

		cout << "Writing mean-variance surface energy" << endl;

		MeanVarianceSet MVSS;
		MVSS.mean(muS);
		MVSS.variance(sigma2S);
		MVSS.packetsPerRun(Npkt);
		MVSS.runs(Nr);

		W.filename(oPrefix + ".ES.mve.out");
		W.input(&MVSS);
		W.write();
	}
	else
	{
		K.runSync();

		OutputDataCollection* res = K.results();

		SpatialMap<float>* EV = static_cast<SpatialMap<float>*>(res->getByName("VolumeEnergy"));

		SpatialMap<float>* ES = static_cast<SpatialMap<float>*>(res->getByName("SurfaceExitEnergy"));

		// write converted MCML output
		TextFileMatrixWriter W;
		W.filename(oPrefix + ".EV.txt");
		W.source(EV);
		W.write();

		W.filename(oPrefix + ".ES.txt");
		W.source(ES);
		W.write();
	}
}
