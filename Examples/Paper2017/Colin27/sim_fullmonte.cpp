/*
 * sim_fullmonte_mmc.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */



#include <string>

#include <FullMonteSW/Kernels/Software/TetraSVKernel.hpp>
#include <FullMonteSW/Kernels/SeedSweep.hpp>
#include <FullMonteSW/Queries/BasicStats.hpp>

#include <FullMonteSW/Storage/MMC/MMCJSONCase.hpp>
#include <FullMonteSW/Storage/MMC/MMCJSONReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCTextMeshReader.hpp>
#include <FullMonteSW/Storage/MMC/MMCOpticalReader.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSSourceReader.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/timer/timer.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

/** sim_fullmonte
 *
 * Runs FullMonte's kernel with multiple seeds and aggregates the output into a mean-variance dataset.
 *
 */

int main(int argc,char **argv)
{
	string iJSON;							// input filename

	string oPrefix;

	unsigned Nthreads=MAX_THREAD_COUNT;
	unsigned Nr=1;
	float wmin=1e-5;
	unsigned long long Npkt=0;

	boost::program_options::options_description cmdline;

	cmdline.add_options()
			("help,h","Show help")
			("mmc-json,j",boost::program_options::value<string>(&iJSON),"MMC .json input file")
			("timos-mesh,m",boost::program_options::value<string>(),"TIM-OS mesh file")
			("timos-source,s",boost::program_options::value<string>(),"TIM-OS source definition")
			("timos-opt",boost::program_options::value<string>(),"TIM-OS optical properties file")
			("threads,t",boost::program_options::value<unsigned>(&Nthreads),"Number of threads to use")
			("output,o",boost::program_options::value<string>(&oPrefix),"Output file prefix (defaults to session ID from .json file)")
			("packets,p",boost::program_options::value<unsigned long long>(&Npkt),"Number of packets to run, defaults to number in input file")
			("runs,r",boost::program_options::value<unsigned>(&Nr),"Number of runs")
			("wmin",boost::program_options::value<float>(&wmin),"Roulette threshold (default 1e-5)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	boost::program_options::notify(vm);

	if (vm.count("help"))
	{
		cmdline.print(cout);
		return 0;
	}

	// read specified test case
	MMCJSONCase* C = nullptr;
	TetraMesh* M = nullptr;
	MaterialSet* MS = nullptr;
	Source::Abstract *S = nullptr;

	if (vm.count("timos-mesh"))
	{
		TIMOSMeshReader R;
		R.filename(vm["timos-mesh"].as<string>());
		R.read();

		M = R.mesh();
	}

	if (vm.count("timos-opt"))
	{
		TIMOSMaterialReader R;
		R.filename(vm["timos-opt"].as<string>());
		R.read();

		MS = R.materials();
	}

	if (vm.count("timos-source"))
	{
		TIMOSSourceReader R;
		R.filename(vm["timos-source"].as<string>());
		R.read();

		S = R.source();
	}

	if (vm.count("mmc-json"))
	{
		cout << "Reading problem description from " << iJSON << endl;
		MMCJSONReader R;
		R.filename(iJSON);
		R.read();

		C = R.result();

		if (!C)
		{
			cout << "Failed to read " << iJSON << endl;
			return -1;
		}

		C->print(cout);

		if (S)
		{
			cout << "Overriding source provided to MMC JSON with command-line option" << endl;
		}
		else
			S = C->createSource();


		if (M)
		{
			cout << "Overriding mesh provided to MMC JSON with command-line option" << endl;
		}
		else
		{
			string iFn = C->meshId();
			cout << "Reading MMC-format mesh from " << iFn << endl;
			MMCTextMeshReader MR;
			MR.path(".");
			MR.prefix(iFn);
			MR.read();

			M = MR.mesh();
		}

		if (MS)
		{
			cout << "Overriding materials provided to MMC JSON with command-line option" << endl;
		}
		else
		{
			string matFn = "prop_" + C->meshId() + ".dat";
			cout << "Reading MMC-format materials from " << matFn << endl;

			MMCOpticalReader OR;
			OR.filename(matFn);
			OR.read();
			MS = OR.materials();
		}
	}

	if(vm.count("packets"))
	{
		Npkt = vm["packets"].as<unsigned long long>();
	}
	else if (C)
	{
		Npkt = C->photons();
	}
	else
	{
		cout << "Packet count not provided" << endl;
		return -1;
	}

	if (!oPrefix.empty())
	{
	}
	else if (C)
	{
		oPrefix = C->sessionId();
	}
	else
	{
		cout << "No output prefix specified - using 'out'" << endl;
	}


	// set up kernel

	cout << "Running " << Nr << "x" << Npkt << " simulations using "<< Nthreads << " threads" << endl;
	cout << "  wmin=" << wmin << endl;

	if (!(Npkt && Nthreads && S && M && MS))
	{
		cout << "Problem definition missing" << endl;
		return -1;
	}

	TetraSVKernel K;
	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.rouletteWMin(wmin);
	K.source(S);
	K.geometry(M);
	K.materials(MS);

	if (vm.count("runs"))
	{
		// set up seed sweep
		SeedSweep SS;
		SS.kernel(&K);
		SS.seedRange(1,Nr+1);
		SS.printProgress(true);
		SS.run();

		OutputDataCollection* EV = SS.resultsByName("VolumeEnergy");

		BasicStats BS;
		BS.input(EV);
		BS.update();

		SpatialMap<float>* mu = static_cast<SpatialMap<float>*>(BS.mean());
		SpatialMap<float>* sigma2 = static_cast<SpatialMap<float>*>(BS.variance());

		MeanVarianceSet MVS;
		MVS.mean(mu);
		MVS.variance(sigma2);
		MVS.packetsPerRun(Npkt);
		MVS.runs(Nr);

		cout << "Writing mean-variance volume energy" << endl;

		TextFileMeanVarianceWriter W;
		W.filename(oPrefix + ".EV.mve.out");
		W.input(&MVS);
		W.write();


		OutputDataCollection* ES = SS.resultsByName("SurfaceExitEnergy");

		BasicStats SBS;
		SBS.input(ES);
		SBS.update();

		SpatialMap<float>* muS = static_cast<SpatialMap<float>*>(SBS.mean());
		SpatialMap<float>* sigma2S = static_cast<SpatialMap<float>*>(SBS.variance());

		cout << "Writing mean-variance surface energy" << endl;

		MeanVarianceSet MVSS;
		MVSS.mean(muS);
		MVSS.variance(sigma2S);
		MVSS.packetsPerRun(Npkt);
		MVSS.runs(Nr);

		W.filename(oPrefix + ".ES.mve.out");
		W.input(&MVSS);
		W.write();
	}
	else
	{
		K.runSync();

		OutputDataCollection* res = K.results();

		SpatialMap<float>* EV = static_cast<SpatialMap<float>*>(res->getByName("VolumeEnergy"));

		SpatialMap<float>* ES = static_cast<SpatialMap<float>*>(res->getByName("SurfaceExitEnergy"));

		// write converted MCML output
		TextFileMatrixWriter W;
		W.filename(oPrefix + ".EV.txt");
		W.source(EV);
		W.write();

		W.filename(oPrefix + ".ES.txt");
		W.source(ES);
		W.write();
	}
}
