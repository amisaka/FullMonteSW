/*
 * Test_MCML.cpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

/** Load the sample file provided with MCML and run it
 *
 */

#include <boost/timer/timer.hpp>

#include <string>

#include <FullMonteSW/Storage/MCML/MCMLInputReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLOutputReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLCase.hpp>

#include <FullMonteSW/Kernels/Software/MCMLKernel.hpp>
#include <FullMonteSW/Kernels/SeedSweep.hpp>
#include <FullMonteSW/Queries/BasicStats.hpp>
#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceWriter.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Queries/EventCountComparison.hpp>

//#include "Test_Fixture_MCML.hpp"

//BOOST_AUTO_TEST_CASE(MCMLSampleTraces)
//{
//	MCMLInputReader R;
//
//	R.filename(FULLMONTE_DATA_DIR "/MCML/sample/sample.mci");
//	R.read();
//
//	BOOST_REQUIRE_EQUAL( R.cases(), 2U );
//
//	// work with first case
//	R.start();
//
//	MCMLCase* C = R.current();
//
//	MCMLKernelWithTraces K;
//	K.packetCount(100);						// override input-file-provided test cases
//	K.threadCount(MAX_THREAD_COUNT);
//	K.rouletteWMin(C->rouletteThreshold());
//	K.geometry(C->geometry());
//	K.materials(C->materials());
//
//	K.runSync();
//
//	OutputDataCollection* DC = K.results();
//
//	BOOST_REQUIRE_EQUAL( DC->size(), 4U );
//
//	MCConservationCountsOutput* cons = nullptr;
//
//	BOOST_REQUIRE( cons = dynamic_cast<MCConservationCountsOutput*>(DC->getByName("ConservationCounts")) );
//	cout << "Absorbed: " << fixed << setprecision(6) << cons->w_absorb << endl;
//	cout << "Exited:   " << fixed << setprecision(6) << cons->w_exit << endl;
//	cout << "Roulette: " << fixed << setprecision(6) << cons->w_die << endl;
//	cout << "Launched: " << fixed << setprecision(6) << cons->w_launch << endl;
//
//	PacketPositionTraceSet* tr = nullptr;
//	BOOST_REQUIRE( (tr = dynamic_cast<PacketPositionTraceSet*>(DC->getByName("PacketTraces"))) );
//
//#ifdef WRAP_VTK
//	vtkFullMontePacketPositionTraceSetToPolyData* TR = vtkFullMontePacketPositionTraceSetToPolyData::New();
//	TR->includeTetra(true);
//	TR->includeMaterial(false);
//	TR->source(tr);
//	TR->update();
//
//	vtkPolyDataWriter* W = vtkPolyDataWriter::New();
//	W->SetFileName("mcmltraces.vtk");
//	W->SetInputData(TR->getPolyData());
//	W->Update();
//	W->Delete();
//#endif
//}

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

/** sim_fullmonte_mcml_sweep
 *
 * Runs FullMonte's MCML kernel with multiple seeds and aggregates the output into a mean-variance dataset.
 *
 */

int main(int argc,char **argv)
{
	string iFn;				// input filename
	unsigned iCase = 0; 	// case number to run

	string oPrefix,oFn;

	unsigned Nthreads=MAX_THREAD_COUNT;
	unsigned long long Npkt=0ULL;
	float wmin=1e-4;
	unsigned Nr=1;

	boost::program_options::options_description cmdline;

	cmdline.add_options()
			("help,h","Show help")
			("input,i",boost::program_options::value<string>(&iFn),"MCML input file")
			("case",boost::program_options::value<unsigned>(&iCase),"Case to use from input file (default 0)")
			("threads,t",boost::program_options::value<unsigned>(&Nthreads),"Number of threads to use")
			("output,o",boost::program_options::value<string>(&oPrefix),"Output file prefix")
			("packets,p",boost::program_options::value<unsigned long long>(&Npkt),"Number of packets to run, defaults to number in input file")
			("runs,r",boost::program_options::value<unsigned>(&Nr),"Number of runs")
			("wmin",boost::program_options::value<float>(&wmin),"Roulette threshold (default 1e-5)");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc,argv,cmdline),vm);
	boost::program_options::notify(vm);

	if (vm.count("help"))
	{
		cmdline.print(cout);
		return 0;
	}


	// read specified test case
	MCMLInputReader R;

	R.filename(iFn);
	R.verbose(true);
	R.read();

	MCMLCase* C = R.get(iCase);

	if (!C)
	{
		cout << "Failed to read case!" << endl;
		return -1;
	}



	if (Npkt == 0)
		Npkt = C->packets();

	if (oPrefix.empty())
		oPrefix = C->outputFilename();

	if (Nr == 1)
		oFn = oPrefix + ".Erz.out";
	else
		oFn = oPrefix + ".mve.out";

	// set up kernel

	cout << "Running " << Nr << "x" << Npkt << " simulations using "<< Nthreads << " threads" << endl;
	cout << "  Input " << iFn << "#" << iCase << " output " << oFn << endl;
	cout << "  wmin=" << wmin << endl;

	MCMLKernel K;
	K.packetCount(Npkt);
	K.threadCount(Nthreads);
	K.rouletteWMin(wmin);
	K.geometry(C->geometry());
	K.materials(C->materials());

	if (vm.count("runs"))
	{
		// set up seed sweep
		SeedSweep SS;
		SS.kernel(&K);
		SS.seedRange(1,Nr+1);
		SS.printProgress(true);
		SS.run();

		OutputDataCollection* ArzSet = SS.resultsByName("CylAbsorbedEnergy");

		BasicStats BS;
		BS.input(ArzSet);
		BS.update();

		SpatialMap2D<float>* mu = static_cast<SpatialMap2D<float>*>(BS.mean());
		SpatialMap2D<float>* sigma2 = static_cast<SpatialMap2D<float>*>(BS.variance());

		MeanVarianceSet MVS;
		MVS.mean(mu);
		MVS.variance(sigma2);
		MVS.packetsPerRun(Npkt);
		MVS.runs(Nr);

		TextFileMeanVarianceWriter W;
		W.filename(oFn);
		W.input(&MVS);
		W.write();
	}
	else
	{
		K.runSync();

		OutputDataCollection* res = K.results();
		SpatialMap<float>* Erz = static_cast<SpatialMap<float>*>(res->getByName("CylAbsorbedEnergy"));
		SpatialMap<float>* Erz_no_zeroTetra = new SpatialMap<float>(Erz->dim()-1);
		for (unsigned i=0; i<Erz_no_zeroTetra->dim(); ++i)
		{
			Erz_no_zeroTetra->set(i, Erz->get(i+1));
		}
		// write converted MCML output
		TextFileMatrixWriter W;
		W.filename(oFn);
		W.source(Erz_no_zeroTetra);
		W.write();
	}
}
