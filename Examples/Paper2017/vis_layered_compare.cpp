/*
 * vis_layered_compare.cpp
 *
 *  Created on: Nov 27, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceReader.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMatrixReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLOutputReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLInputReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLCase.hpp>

#include <FullMonteSW/Geometry/Layered.hpp>

#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <fstream>
#include <iostream>
using namespace std;

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Tests/DummyKernel.hpp>

int main(int argc,char **argv)
{
	boost::program_options::options_description opt;

	string fnGeom, fnRef, fnOut, fnTest;
	unsigned caseNumber=0;

	opt.add_options()
			("geometry",boost::program_options::value<string>(&fnGeom), "Geometry file (MMC, VTK, or TIM-OS format)")
			("case",boost::program_options::value<unsigned>(&caseNumber),"MCML case number")
			("ref",boost::program_options::value<string>(&fnRef),"Reference data field")
			("test",boost::program_options::value<string>(&fnTest),"Test data field")
			("output",boost::program_options::value<string>(&fnOut),"Output file name")
			;

	boost::program_options::variables_map vm;

	store(parse_command_line(argc,argv,opt),vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << opt;
		return 0;
	}
	else if (!vm.count("ref") || !vm.count("test"))
	{
		cout << "Test and reference datasets must be provided" << endl;
		cout << opt;
		return 0;
	}

	Layered* geom = nullptr;
	MaterialSet* MS = nullptr;
	MCKernelBase* K = new DummyKernel();

	if (!vm.count("case"))
	{
		cout << "Using default case=0" << endl;
	}

	if (vm.count("geometry"))
	{
		MCMLInputReader R;

		R.filename(fnGeom);
		R.read();

		MCMLCase* C = R.get(caseNumber);

		geom = C->geometry();
		MS = C->materials();
		K->geometry(geom);
		K->materials(MS);
	}

	TextFileMeanVarianceReader RR;
	RR.filename(fnRef);
	RR.read();

	MeanVarianceSet* ref = RR.result();


	TextFileMatrixReader MR;
	MR.filename(fnTest);
	MR.read();

	SpatialMap2D<float>* E_test = static_cast<SpatialMap2D<float>*>(MR.output());
	SpatialMap<float>* phi_test=nullptr;
	SpatialMap<float>* phi_ref=nullptr;

	boost::filesystem::path testPath = fnTest;

	if (!geom || !MS)
		cout << "Cannot output fluence for lack of geometry &/ materials infomation" << endl;
	if (!E_test)
		cout << "Cannot output fluence for lack of energy information" << endl;
	E_test->outputType(AbstractSpatialMap::EnergyPerVolume);

	EnergyToFluence EF_test;
	EF_test.kernel(K);
	EF_test.data(E_test);
	EF_test.outputFluence();

	EF_test.update();

	phi_test = static_cast<SpatialMap<float>*>(EF_test.result());

	ref->mean()->outputType(AbstractSpatialMap::Energy);
	EnergyToFluence EF_ref;
	EF_test.kernel(K);
	EF_ref.data(ref->mean());
	EF_ref.outputFluence();

	EF_ref.update();

	phi_ref = static_cast<SpatialMap2D<float>*>(EF_ref.result());

	ofstream os(fnOut);

	os << "% MCML-FullMonte comparison" << endl;
	os << "% Data is presented row-major A[1][1] A[1][2] ... " << endl;
	os << "% mu_ref sigma_ref phi_ref phi_test" << endl;
	os << geom->dims().ri << 'x' << geom->dims().zi << endl;

	for(unsigned i=1;i<ref->dim();++i)
	{
		float mu 	= ref->mean()->get(i);
		float sigma = sqrt(ref->variance()->get(i));

		float phi = phi_ref->get(i);
		float x = phi_test->get(i);

		os << mu << ' ' << sigma << ' ' << phi << ' ' << x << endl;

	}
}
