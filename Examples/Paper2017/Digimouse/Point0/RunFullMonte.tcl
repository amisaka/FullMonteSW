package require FullMonte
package require vtk

source $FullMonte::sourcedir/Examples/Paper2017/Digimouse/Point0/SetupPoint0.tcl

###

TetraVolumeKernel K
    K geometry $M
    K materials $MS
    K source P
    K packetCount $packets
    K threadCount $FullMonte::defaultThreadCount

set tStart [clock milliseconds]
K runSync
set tEnd [clock milliseconds]

puts "Kernel ran in [expr $tEnd-$tStart]"

set RES [K results]

set EV [$RES getByName "VolumeEnergy"]

EnergyToFluence EF
    EF inputEnergy
    EF outputFluence
    EF data $EV
    EF kernel K
    EF update

TextFileMatrixWriter W
    W filename "fluence.fullmonte.out"
    W source [EF result]
    W write

vtkFullMonteArrayAdaptor VTKA
    VTKA source [EF result]
    VTKA update

vtkFullMonteTetraMeshWrapper VTKM
    VTKM mesh $M
    VTKM update

set vtkM [VTKM blankMesh]
set vtkA [VTKA array]

$vtkA SetName "Fluence (au)"

[$vtkM GetCellData] AddArray $vtkA

vtkUnstructuredGridWriter VTKW
    VTKW SetFileName "new.fullmonte.vtk"
    VTKW SetInputData $vtkM
    VTKW Update

exit
