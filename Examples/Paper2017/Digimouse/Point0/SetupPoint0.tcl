package require FullMonte

#### Sets up Digimouse for a point source
## 
##
## Objects
##
##      TP      TetraEnclosingPointByLinearSearch
##      P       A point source
##      $M      The mesh
##      MS      The material set
##
##
##
## Variables
#   meshfn
#   packets
#   rngseed
#   pos 
#   tetra


#### BASIC RUN PARAMS

set meshfn  "$FullMonte::datadir/TIM-OS/mouse/mouse.mesh"
set optfn   "$FullMonte::datadir/TIM-OS/mouse/mouse.opt"
set packets 1000000
set rngseed 1234
set pos "16.3 42.2 7.9"

Point P
    P position $pos



#### LOAD MESH AND LOCATE TETRA (for TIMOS and MMC)

TIMOSMeshReader TR
    TR filename $meshfn
    TR read
    set M [TR mesh]

puts "Read mesh"

TetraEnclosingPointByLinearSearch TP
    TP point $pos
    TP mesh $M
    TP update

set tetra [TP tetraID]
puts "Point $pos is located in tetra $tetra"

P elementHint $tetra


TIMOSMaterialReader MR
    MR filename $optfn
    MR read
    set MS [MR materials]
