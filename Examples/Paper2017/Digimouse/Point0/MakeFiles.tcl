package require FullMonte

source $FullMonte::sourcedir/Examples/Paper2017/Digimouse/Point0/SetupPoint0.tcl

#### Create MMC files

set casename mouse-point0

MMCJSONWriter MMCJ
    MMCJ filename       "$casename.mmc.json"
    MMCJ packets        $packets
    MMCJ seed           $rngseed
    MMCJ sessionname    "$casename.mmc"
    MMCJ meshfilename   "$casename.mmc"
    MMCJ mmccommand     "$FullMonte::mmcpath"
    MMCJ initelem       $tetra
    MMCJ timestep       0 1 1
    MMCJ srcpos         $pos
    MMCJ write


#### Create TIM-OS files

# Mesh, optical files already exist
# Write the source file

TIMOSSourceWriter       TS
    TS filename         "$casename.timos.source"
    TS source           P
    TS packets          $packets
    TS write

exit 
