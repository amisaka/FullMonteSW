/*
 * Compare.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: jcassidy
 */

#include <string>
#include <fstream>


#include <FullMonteSW/Queries/Sort.hpp>
#include <FullMonteSW/Queries/Permute.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMatrixReader.hpp>

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <FullMonteSW/Warnings/Push.hpp>

#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceReader.hpp>
#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>

#include "Chi2Similarity.hpp"

int main(int argc,char **argv)
{
	string fnTestFormat;
	string fnRef;
	string oFn="chi2.out";

	float extra_cv_add=0.0f;
	float extra_sigma_mult=0.0f;

	unsigned long long nPktTest=0ULL;

	string runs;

	float pCrit=0.95f;

	boost::program_options::options_description opt;

	opt.add_options()
			("test",boost::program_options::value<string>(&fnTestFormat),"Test dataset (format using %d to specify number)")
			("ref",boost::program_options::value<string>(&fnRef),"Reference dataset")
			("runs",boost::program_options::value<string>(&runs),"Runs to analyze (eg. 1:N for inclusive range)")
			("test-packets",boost::program_options::value<unsigned long long>(&nPktTest),"Packet count in test set (not always required")
			("output",boost::program_options::value<string>(&oFn),"Output file name")
			("extra-stddev-mult",boost::program_options::value<float>(&extra_sigma_mult),"Extra fraction of standard deviation to add (sigma *= (1+extra-stddev-mult))")
			("extra-cv",boost::program_options::value<float>(&extra_cv_add),"Value to add to coefficient of variation (sigma += extra-cv*mean)")
			("checkpoint-CE",boost::program_options::value<vector<float>>()->multitoken(),"Energy values at which to test (0.95 -> test bins containing 95% of energy)")
			("checkpoint-CV",boost::program_options::value<vector<float>>()->multitoken(),"Coefficient of variation values at which to test (0.1 -> test all bins with a c.v. <= 10%)")
			("pcrit",boost::program_options::value<float>(&pCrit),"Critical value of p for automated testing")
			;

	boost::program_options::variables_map vm;

	store(parse_command_line(argc,argv,opt),vm);
	notify(vm);

	unsigned first=-1U,last=-1U,stride=1;

	if (!runs.empty())
	{
		stringstream ss(runs);

		char sep;
		ss >> first >> sep >> last ;

		if (ss.fail() || sep != ':')
		{
			cout << "Failed to parse range argument '" << runs << "'" << endl;
			return 0;
		}

		ss >> sep;

		if (ss.eof())
		{

		}
		else if (sep == ':')
		{
			stride=last;
			ss >> last;
		}
		else
		{
			cout << "Failed to parse range argument '" << runs << "'" << endl;
			return 0;
		}

		cout << "Running from " << first << " to " << last <<  " with stride " << stride << endl;
	}


	if (vm.count("ref") != 1)
	{
		cout << "No reference filename provided. Must have a --ref argument." << endl;
		return -1;
	}

	if (vm.count("test") != 1)
	{
		cout << "No test filename provided. Must have a --test argument." << endl;
		return -1;
	}

	vector<float> check_ce;
	vector<float> check_cv;

	if (vm.count("checkpoint-CE") > 0)
		check_ce = vm["checkpoint-CE"].as<vector<float>>();

	if (vm.count("checkpoint-CV") > 0)
		check_cv = vm["checkpoint-CV"].as<vector<float>>();



	////// READ REFERENCE DATA

	boost::filesystem::path pRef = fnRef;

	cout << "Reference dataset: " << pRef << endl;

	boost::filesystem::path pRefStem = pRef;
	boost::filesystem::path ext = pRef.extension();

	Geometry* 		geom = nullptr;
	MeanVarianceSet* E_ref=nullptr;


	SpatialMap<float>* E_test = nullptr;

	if (ext == ".out")
	{
		boost::filesystem::path pRefStem2 = pRefStem.stem();
		if (pRefStem2.extension() == ".mve")
		{
			TextFileMeanVarianceReader R;
			R.filename(fnRef);

			R.read();

			E_ref = R.result();

			cout << "  Mean-variance dataset with " << E_ref->dim() << " elements" << endl;
		}
		else
			cout << "  Unknown dataset type" << endl;
	}
	else
	{
		cout << "Unrecognized reference dataset type - extension '" << ext << "' from file '" << pRefStem << endl;
	}

	////// READ TEST DATA



	////// EMIT CHI2 RESULTS

	ofstream os(oFn);

	Chi2Similarity compare(os);

	compare.reference(E_ref);

	if (geom)
		compare.partition(geom->regions());
	else
		cout << "Geometry partition missing!" << endl;


	compare.testPacketCount(nPktTest);

	compare.extraCvToAdd(extra_cv_add);
	compare.extraStdDevMultiplier(extra_sigma_mult);

	unsigned r,Npass=0,Nfail=0;

	for(r=first;r <= last;r += stride)
	{
		char iFn[1024];
		snprintf(iFn,1024,fnTestFormat.c_str(),r);

		cout << iFn << endl;

		boost::filesystem::file_status st = boost::filesystem::status(iFn);

		if(!boost::filesystem::exists(st))
		{
			cout << "Did not find file " << iFn << " so terminating" << endl;
			break;
		}

			TextFileMatrixReader R;
			R.filename(iFn);
			R.read();

			E_test = static_cast<SpatialMap<float>*>(R.output());


		compare.test(E_test);
		compare.update();

		os << endl << endl;




		////// Automated checks based on command-line parameters
		// checkpoint-CE 0.xx checks the first N bins that cover at least xx% of the total energy absorption
		// checkpoint-CV y checks the first N bins having coefficient of variation (sigma/mu) <= y
		//
		// for all tests, a single value critical p-value pCrit is used

		bool ok=true;

		for(float E : check_ce)
		{
			double chi2crit;

			cout << "Check at csum(E) = " << E << endl;
			Chi2Similarity::Chi2Entry ret = compare.getByEnergyFraction(E);

			chi2crit = ret.criticalValue(pCrit);


			cout << "  E%=" << ret.Esum << " ID=" << ret.ID << " rank=" << ret.index << " cv=" << ret.cv << " chi2=" << ret.chi2 << " <= " << chi2crit << ' ';
			if (ret.chi2 < chi2crit)
				cout << "PASS";
			else
			{
				cout << "FAIL";
				ok = false;
			}
			cout << endl;
		}

		for(float cv: check_cv)
		{
			cout << "Check at cv <= " << cv << " ";
			Chi2Similarity::Chi2Entry ret = compare.getByCVThreshold(cv);
			double chi2crit = ret.criticalValue(pCrit);

			cout << "  E%=" << ret.Esum << " ID=" << ret.ID << " rank=" << ret.index << " cv=" << ret.cv << " chi2=" << ret.chi2 << " <= " << chi2crit << ' ';
			if (ret.chi2 < chi2crit)
				cout << "PASS";
			else
			{
				cout << "FAIL";
				ok = false;
			}
			cout << endl;
		}

		if (ok)
			Npass++;
		else
			Nfail++;
	}

	cout << Npass+Nfail << " tested: " << Npass << " pass, " << Nfail << " fail" << endl;

	return 0;
}
