/*
 * Compare.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: jcassidy
 */

#include <string>


#include <FullMonteSW/Queries/Sort.hpp>
#include <FullMonteSW/Queries/Permute.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Queries/EnergyToFluence.hpp>
#include <FullMonteSW/Queries/Rescale.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include <fstream>


#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/Storage/MMC/MMCResultReader.hpp>
#include <FullMonteSW/Storage/MCML/MCMLInputReader.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMatrixReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSOutputReader.hpp>

#include <FullMonteSW/Config.h>


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Storage/MCML/MCMLCase.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Storage/MCML/MCMLOutputReader.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMeanVarianceReader.hpp>
#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/Queries/Rescale.hpp>

#include <FullMonteSW/Tests/DummyKernel.hpp>

#include "Chi2Similarity.hpp"

int main(int argc,char **argv)
{
	string fnGeom;
	string fnTest;
	string fnRef;
	string fnOpt;
	string fnTestType;
	string oFn="chi2.out";

	float extra_cv_add=0.0f;
	float extra_sigma_mult=0.0f;

	unsigned long long nPktTest=0ULL;

	float pCrit=0.95f;
	unsigned caseNumber=0;

	boost::program_options::options_description opt;

	opt.add_options()
			("geometry",boost::program_options::value<string>(&fnGeom), "Geometry file (not required if TIM-OS or MCML output used")
			("opt",boost::program_options::value<string>(&fnOpt),"Optical-properties file (not required if MCML output used")
			("ref",boost::program_options::value<string>(&fnRef),"Reference dataset")
			("no-error-codes","Return error only on failure to read file. If files load OK, return 0 regardless of (useful to stop CMake from automatically deleting output on failure)")
			("surface,s","Compare surface data (TIM-OS only)")
			("volume,v","Compare volume data (TIM-OS only)")
			("case",boost::program_options::value<unsigned>(&caseNumber),"Case number (for MCML)")
			("test",boost::program_options::value<string>(&fnTest),"Test dataset")
			("test-type",boost::program_options::value<string>(&fnTestType),"Test dataset type (optional)")
			("test-packets",boost::program_options::value<unsigned long long>(&nPktTest),"Packet count in test set (not always required")
			("output",boost::program_options::value<string>(&oFn),"Output file name")
			("extra-stddev-mult",boost::program_options::value<float>(&extra_sigma_mult),"Extra fraction of standard deviation to add (sigma *= (1+extra-stddev-mult))")
			("extra-cv",boost::program_options::value<float>(&extra_cv_add),"Value to add to coefficient of variation (sigma += extra-cv*mean)")
			("checkpoint-CE",boost::program_options::value<vector<float>>()->multitoken(),"Energy values at which to test (0.95 -> test bins containing 95% of energy)")
			("checkpoint-CV",boost::program_options::value<vector<float>>()->multitoken(),"Coefficient of variation values at which to test (0.1 -> test all bins with a c.v. <= 10%)")
			("pcrit",boost::program_options::value<float>(&pCrit),"Critical value of p for automated testing")
			;

	boost::program_options::variables_map vm;

	store(parse_command_line(argc,argv,opt),vm);
	notify(vm);


	if (vm.count("ref") != 1)
	{
		cout << "No reference filename provided. Must have a --ref argument." << endl;
		return -1;
	}

	if (vm.count("test") != 1)
	{
		cout << "No test filename provided. Must have a --test argument." << endl;
		return -1;
	}

	vector<float> check_ce;
	vector<float> check_cv;

	if (vm.count("checkpoint-CE") > 0)
		check_ce = vm["checkpoint-CE"].as<vector<float>>();

	if (vm.count("checkpoint-CV") > 0)
		check_cv = vm["checkpoint-CV"].as<vector<float>>();



	////// READ REFERENCE DATA

	boost::filesystem::path pRef = fnRef;

	cout << "Reference dataset: " << pRef << endl;

	boost::filesystem::path pRefStem = pRef;
	boost::filesystem::path ext = pRef.extension();

	Geometry* 		geom = nullptr;
	MaterialSet*	mat = nullptr;
	MeanVarianceSet* E_ref=nullptr;
	MCKernelBase*	K = new DummyKernel();


	SpatialMap<float>* E_test = nullptr;

	if (ext.empty())
	{
		cout << "No extension provided, can't tell what kind of file" << endl;
	}
	else if (ext == ".xz" || ext == ".bz2" || ext == ".gz")
	{
		cout << "  Compresssed with " << ext << endl;
		pRefStem = pRef.stem();
		ext = pRefStem.extension();
	}

	cout << "  Extension: " << ext << endl;


	if (ext == ".out")
	{
		boost::filesystem::path pRefStem2 = pRefStem.stem();
		if (pRefStem2.extension() == ".mve")
		{
			TextFileMeanVarianceReader R;
			R.filename(fnRef);

			R.read();

			E_ref = R.result();

			cout << "  Mean-variance dataset with " << E_ref->dim() << " elements" << endl;
		}
		else
			cout << "  Unknown dataset type" << endl;
	}
	else
	{
		cout << "Unrecognized reference dataset type - extension '" << ext << "' from file '" << pRefStem << endl;
	}
//	else if (ext == "dat")
//	{
//	}
//	else if (ext == "mco")
//	{
//		cout << "  MCML output" << endl;
//
//		MCMLOutputReader R;
//		R.filename(fnRef);
//		R.read();
//
//		// MCML output embeds the geometry description, materials, and packet count
//		MCMLCase* C = R.runParams();
//		Layered* L = C->geometry();
//
//		mat = C->materials();
//		nPktTest = C->packets();
//		geom = L;
//	}
	////// READ PROBLEM SETUP IF REQUIRED

	if (vm.count("geometry"))
	{
		if (geom)
		{
			cout << "Redundant geometry specified with --geom option" << endl;
			//cout << "  MCML output includes the geometry" << endl;
			return -1;
		}

		boost::filesystem::path pGeom = fnGeom;

		boost::filesystem::path ext = pGeom.extension();

		if (ext == ".xz" || ext == ".bz2" || ext == ".gz")
		{
			cout << "  Compressed output not supported" << endl;
			return -1;
		}

		if (ext == ".mesh")
		{
			TIMOSMeshReader R;
			R.filename(fnGeom);
			R.read();

			geom = R.mesh();
		}
		else if (ext == ".mci")
		{
			MCMLInputReader R;
			R.filename(fnGeom);
			R.read();

			MCMLCase* C = R.get(caseNumber);

			if (!C)
			{
				cout << "Invalid case number specified" << endl;
				return -1;
			}

			nPktTest = C->packets();
			geom = C->geometry();
		}
	}
	else if (!geom)
	{
		//cout << "No geometry specified - must use a --geom option" << endl;
		//return -1;
	}



	boost::filesystem::path pOpt = fnOpt;

	if (vm.count("opt"))
	{
		if (mat)
		{
			cout << "Redundant --opt option supplied. Materials already provided" << endl;
			return -1;
		}

		cout << "Materials: " << fnOpt << endl;

		if (pOpt.extension() == ".opt")
		{
			cout << "  TIM-OS format" << endl;
			TIMOSMaterialReader OR;
			OR.filename(fnOpt);
			OR.read();

			mat = OR.materials();
		}
		else if (pOpt.extension() == ".dat")
		{
			cout << "  MMC format" << endl;
			cout << "  Not currently supported" << endl;
			return -1;
		}
	}


	////// READ TEST DATA

	boost::filesystem::path pTest = fnTest;

	cout << "Test dataset: " << pTest << endl;

	boost::filesystem::path pTestStem = pTest;
	ext = pTest.extension();

	if (ext.empty())
	{
		cout << "No extension provided, can't tell what kind of file" << endl;
	}
	else if (ext == ".xz" || ext == ".bz2" || ext == ".gz")
	{
		cout << "  Compresssed with " << ext << endl;
		pTestStem = pTest.stem();
		ext = pTestStem.extension();
	}

	cout << "  Extension: " << ext << endl;

	if (ext == ".timos" || (ext == ".out" && pTestStem.stem().extension() == ".timos"))
	{
		TIMOSOutputReader R;

		R.filename(fnTest);
		R.read();

		SpatialMap<float>* phi_test = nullptr;

		if (vm.count("surface"))
		{
			if (vm.count("volume"))
			{
				cout << "Error: can't specify both surface and volume options" << endl;
				return -1;
			}
			phi_test = static_cast<SpatialMap<float>*>(R.surfaceFluence());
			phi_test->outputType(AbstractSpatialMap::Fluence);

		}
		else if (vm.count("volume"))
		{
			phi_test = static_cast<SpatialMap<float>*>(R.volumeFluence());
			phi_test->outputType(AbstractSpatialMap::Fluence);
		}
		else
		{
			cout << "TIM-OS output requires that the type (surface, volume) be specified" << endl;
			return -1;
		}

		EnergyToFluence EF;
		EF.outputEnergy();

		EF.data(phi_test);

		if (vm.count("volume") && !mat)
		{
			cout << "Materials required for TIM-OS fluence->energy conversion" << endl;
			return -1;
		}
		K->materials(mat);

		if (!geom)
		{
			cout << "Geometry required for TIM-OS fluence->energy conversion" << endl;
			return -1;
		}
		K->geometry(geom);
		EF.kernel(K);
		EF.update();

		E_test = static_cast<SpatialMap<float>*>(EF.result());

	}
	else if (ext == ".out")
	{
		boost::filesystem::path pTestStem2 = pTestStem.stem();
		if (pTestStem2.extension() == ".mve")
		{
			cout << "  Mean-variance dataset unsupported for test set" << endl;
			return -1;
		}
		else if (pTestStem2.extension() == ".Erz")
		{
			cout << "  FullMonte layered output" << endl;
			TextFileMatrixReader R;
			R.filename(fnTest);
			R.read();

			E_test = static_cast<SpatialMap<float>*>(R.output());

			cout << "  E_test has " << E_test->dim() << " elements" << endl;
		}
		else
			cout << "  Unknown dataset type" << endl;
	}
	else if (ext == ".dat")
	{
		MMCResultReader R;
		R.filename(fnTest);
		R.read();

		E_test = R.output();
	}
	else if (ext == ".mco")
	{
		cout << "  MCML output" << endl;

		MCMLOutputReader R;
		R.filename(fnTest);
		R.read();

		// MCML output embeds the geometry description, materials, and packet count
		MCMLCase* C = R.runParams();
		Layered* L = C->geometry();

		CylIndex dims = L->dims();
		cout << "  " << dims.ri*dims.zi << " elements (" << dims.ri << " radial x" << dims.zi << " depth)" << endl;

		mat = C->materials();
		nPktTest = C->packets();
		geom = L;


		OutputDataCollection* res = R.result();

		SpatialMap<float>* Ev_test_raw = static_cast<SpatialMap<float>*>(res->getByName("Absorption_rz"));

		float reflectance = R.energyDisposition().specularReflectance;
		float ni = C->materials()->get(0)->refractiveIndex();
		float nt = C->materials()->get(1)->refractiveIndex();

		float Rsp = (ni-nt)/(ni+nt);
		Rsp *= Rsp;

		cout << "  Specular reflectance from MC: " << reflectance << " vs calculated " << Rsp << endl;

		Rescale S;
		S.factor(float(nPktTest) / (1.0f - reflectance));
		S.source(Ev_test_raw);
		S.update();

		EnergyToFluence EF;
		EF.inputEnergyPerVolume();
		EF.outputEnergy();
		K->geometry(L);

		EF.data(S.output());
		EF.update();
		E_test = static_cast<SpatialMap<float>*>(EF.result());

		cout << "  E_test has " << E_test->dim() << " elements" << endl;
	}
	else if (ext == ".txt")
	{
		cout << "Plain-text output" << endl;

		TextFileMatrixReader R;
		R.filename(fnTest);
		R.read();

		E_test = static_cast<SpatialMap<float>*>(R.output());
	}



	if (!nPktTest)
	{
		cout << "Error: test packet count undetermined by input file; must specify with --test-packets ####" << endl;
		return -1;
	}





	////// EMIT CHI2 RESULTS

	ofstream os(oFn);

	Chi2Similarity compare(os);

	compare.reference(E_ref);

	if(vm.count("surface"))
		cout << "Skipping surface partition for IDt column" << endl;
	else if (geom)
		compare.partition(geom->regions());
	else
		cout << "Geometry partition missing!" << endl;

	compare.test(E_test);
	compare.testPacketCount(nPktTest);

	compare.extraCvToAdd(extra_cv_add);
	compare.extraStdDevMultiplier(extra_sigma_mult);

	compare.update();

	os << endl << endl;


	////// Automated checks based on command-line parameters
	// checkpoint-CE 0.xx checks the first N bins that cover at least xx% of the total energy absorption
	// checkpoint-CV y checks the first N bins having coefficient of variation (sigma/mu) <= y
	//
	// for all tests, a single value critical p-value pCrit is used

	bool ok=true;

	for(float E : check_ce)
	{
		double chi2crit;

		cout << "Check at csum(E) = " << E << endl;
		Chi2Similarity::Chi2Entry ret = compare.getByEnergyFraction(E);

		chi2crit = ret.criticalValue(pCrit);


		cout << "  E%=" << ret.Esum << " ID=" << ret.ID << " rank=" << ret.index << " cv=" << ret.cv << " chi2=" << ret.chi2 << " <= " << chi2crit << ' ';
		if (ret.chi2 < chi2crit)
			cout << "PASS";
		else
		{
			cout << "FAIL";
			ok = false;
		}
		cout << endl;
	}

	for(float cv: check_cv)
	{
		cout << "Check at cv <= " << cv << " ";
		Chi2Similarity::Chi2Entry ret = compare.getByCVThreshold(cv);
		double chi2crit = ret.criticalValue(pCrit);

		cout << "  E%=" << ret.Esum << " ID=" << ret.ID << " rank=" << ret.index << " cv=" << ret.cv << " chi2=" << ret.chi2 << " <= " << chi2crit << ' ';
		if (ret.chi2 < chi2crit)
			cout << "PASS";
		else
		{
			cout << "FAIL";
			ok = false;
		}
		cout << endl;
	}

	return ok | vm.count("no-error-codes") ? 0 : -1;
}
