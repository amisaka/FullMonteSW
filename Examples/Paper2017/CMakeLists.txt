ADD_EXECUTABLE(Compare Compare.cpp Chi2Similarity.cpp)
TARGET_LINK_LIBRARIES(Compare FullMonteGeometry FullMonteData FullMonteQueries FullMonteTextFile FullMonteTIMOS FullMonteMMCFile ${Boost_PROGRAM_OPTIONS_LIBRARY} FullMonteMCMLFile)

ADD_EXECUTABLE(Chi2Check Chi2Check.cpp Chi2Similarity.cpp)
TARGET_LINK_LIBRARIES(Chi2Check FullMonteData FullMonteQueries FullMonteTextFile ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_FILESYSTEM_LIBRARY})

IF(WRAP_VTK)
    ADD_EXECUTABLE(vis_mesh_compare vis_mesh_compare.cpp)
    TARGET_LINK_LIBRARIES(vis_mesh_compare FullMonteGeometry FullMonteData FullMonteQueries FullMonteMMCFile FullMonteTIMOS FullMonteTextFile ${Boost_PROGRAM_OPTIONS_LIBRARIES} vtkFullMonte)
ENDIF()

ADD_EXECUTABLE(vis_layered_compare vis_layered_compare.cpp)
TARGET_LINK_LIBRARIES(vis_layered_compare FullMonteGeometry FullMonteData FullMonteQueries FullMonteMCMLFile FullMonteTextFile ${Boost_PROGRAM_OPTIONS_LIBRARY})

ADD_CUSTOM_TARGET(paper2017)

MESSAGE("${Blue}Entering subdirectory Examples/Paper2017/Digimouse${ColourReset}")
ADD_SUBDIRECTORY(Digimouse)
MESSAGE("${Blue}Entering subdirectory Examples/Paper2017/Colin27${ColourReset}")
ADD_SUBDIRECTORY(Colin27)
MESSAGE("${Blue}Entering subdirectory Examples/Paper2017/MCML${ColourReset}")
ADD_SUBDIRECTORY(MCML)