The data files in this directory are generated with the following input parameters:

(1) The total number of packets launched is 1e7.

(2) The source type is specified in the file name.
    Line source: endpoint0 = [132 63 97];
                 endpoint1 = [102 63 97].

(3) The data type is volume-based energy/fluence, as specified with the file name.

(4) The comparison picture shows the new simulation data on the left and golden result on the right. All the data have been adjusted to be unitized. The scale is set to logarithmic so the minimum data used is 1e-4 times of the maximum value.
