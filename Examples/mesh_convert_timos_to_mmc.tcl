package require FullMonte

set ifn [lindex $argv 0]
set ofn [lindex $argv 1]

puts "Converting TIM-OS format mesh $ifn to MMC mesh files {elem,velem,node,facenb}_$ofn"

# Load TIM-OS format mesh
TIMOSMeshReader R
R filename "$ifn"
R read
set M [R mesh]

# Write out the MMC mesh
MMCMeshWriter W
W filename "$ofn"
W mesh $M
W write

exit
