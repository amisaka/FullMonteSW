package require FullMonte

set ifn [lindex $argv 0]
set ofn [lindex $argv 1]

puts "Reading from $ifn"

TIMOSMaterialReader PR
PR filename "$ifn"
PR read
set MS [PR materials]

puts "Converting TIMOS-format materials $ifn to MMC-format $ofn"

MMCOpticalWriter MW
MW filename "$ofn"
MW materials $MS
MW write

exit
