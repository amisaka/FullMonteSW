#### Extract triangular-mesh nested surfaces from a labeled tetrahedral volume mesh
#
# With region labels 1..N it will emit N surfaces containing the outer surface of 1..N, 2..N, 3..N
#
# If R# is the set of tetrahedra labeled with region #, and u is the set union operator, then outputs
#
# Surface (R1 u R2 u R3 u ... RN)
# Surface (     R2 u R3 u ... RN)
# ...
# Surface (                   RN)
#
# In MeshTool, a tetrahedron located inside multiple regions is labeled as the most specific (highest-index) region.
# By forming the bounding surfaces as unions, we should be able to avoid bounding-box inclusions where thin gaps exist between
# surfaces. 
#
## Usage: vtk extract_surfaces.tcl <tetrahedral-mesh> <max surface ID>
#
# eg. vtk extract_surfaces.tcl Colin27 4
#
# will emit 4 files:
#       Colin27_1.vtk   with the triangular mesh surface of regions 1..4
#       Colin27_2.vtk                                    of regions 2..4

package require vtk

set pfx     [lindex $argv 0]
set max_id  [lindex $argv 1]

# Suppress GUI window
wm withdraw .

# Read mesh
vtkUnstructuredGridReader R
    R SetFileName $pfx.vtk

# Threshold by region ID
vtkThreshold th
    th SetInputConnection [R GetOutputPort]

# Extract boundary triangles from tetrahedral mesh subsets
vtkGeometryFilter triangles
    triangles SetInputConnection [th GetOutputPort]

# Write output polygons out
vtkPolyDataWriter W
    W SetInputConnection [triangles GetOutputPort]

for { set i 1 } { $i <= $max_id } { incr i } {
    th ThresholdBetween $i 4
    W SetFileName "$pfx_$i.vtk"
    W Update
}

exit
