
[![coverage report](https://gitlab.com/FullMonte/FullMonteSW/badges/master/coverage.svg)](https://gitlab.com/FullMonte/FullMonteSW/commits/master)
[![pipeline status](https://gitlab.com/FullMonte/FullMonteSW/badges/master/pipeline.svg)](https://gitlab.com/FullMonte/FullMonteSW/commits/master)

# FullMonte

|[Overview](https://gitlab.com/FullMonte/FullMonteSW/wikis/home) |[Installation Guide](https://gitlab.com/FullMonte/FullMonteSW/wikis/Install) | [User Guide](https://gitlab.com/FullMonte/FullMonteSW/wikis/Userguide) |
|:---: |:---:|:---:|
---

## Introduction

FullMonte is a flexible and accurate simulator for light propagation in complex media, such as tissue. It is also   the fastest  open-source tetrahedral mesh-based Monte Carlo software model available. It has the ability to model light propagation through turbid (highly-scattering) media such as living tissue. Our group has also created several tools that help prepare problems for FullMonte or leverage it to optimize light distributions for specific purposes, such as photodynamic therapy (PDT) planning. This wiki describes how this full suite of tools fits together and what you can do with them.

## System Requirements

- AVX instruction support minimum, AVX2 recommended for performance

## The FullMonte Suite of Tools consists of

* [FullMonteSW](https://gitlab.com/FullMonte/FullMonteSW): 
  * FullMonteSW is a fast and accurate simulator of light propagation which supports arbitrary media (tissue types) and geometries (3D structures).  

* [MeshTool](https://gitlab.com/FullMonte/MeshTool):
  * Meshtool generates tetrahedral volume meshes suitable for use with FullMonteSW from polyhedral 
        surfaces such as those emitted by ITK-Snap. ITK-Snap is often used to delineate or segment tissues in 3D medical images; MeshTool can convert these segmented images into meshes that FullMonteSW can operate upon.
* [PDT-space](https://gitlab.com/FullMonte/pdt-space): 
  * PDT-space is a tool used for treatment planning of photodynamic therapy (PDT). It optimizes the power allocation to a set of light probes to destroy a tumour while minimizing damage to the healthy organs-at-risk tissue surrounding the tumour. PDT-space can use FullMonteSW to simulate the light propagation for the various probe power allocations it considers.

* [PDT GUI](https://gitlab.com/FullMonte/PDT-fullmonte-gui#pdt-fullmonte-gui):
  * Under construction: The PDT GUI will integrate the above tools in a single user interface, along with visualization of their outputs.

## Process flow
  
* Note: The VTK procedures (blue box) have to be run natively (not through a docker container) on the computer

```mermaid
graph LR
    A[MeshTool] --> B[Mesh]
    B1[Tissue's<br>Optical<br>Properties] --> C
    B --> C[FullMonteSW]
    B2[Light<br>Source<br>Positions] --> C
    C --> D[VTK File]
    D --> D1[VTK]
    subgraph VTK
    D1--> D2[3D Visualization<br>of Meshes]
    end
    D --> E[PDT Space]
    E --> F1[Optimized<br>Power of Sources]
    E --> F2[Final<br>Fluence<br>Distriubtion]
```

## Building

FullMonte offers two options for building the project. You can use a docker container with either a precompiled image with FullMonteSW already installed or an image where you need to build FullMonte from source. The other option is a native install on your machine. This only work if your are running a UNIX distriubtion.

```mermaid
graph TD
    A[Install Options] --> B[Docker]
    A --> C[Native]
    B --> D[Image with<br>FullMonte already<br>installed] 
    B --> E[Image where<br>you need to<br>build FullMonte<br>from source]
    C --> F[Build FullMonte<br>from source]
    click D "https://gitlab.com/FullMonte/FullMonteSW/wikis/fullmontesw-container" "Link to user install wiki"
    click E "https://gitlab.com/FullMonte/FullMonteSW/wikis/Developer-Guide#docker-development-install" "Link to docker -dev install wiki"
    click F "https://gitlab.com/FullMonte/FullMonteSW/wikis/Developer-Guide#native-development-install-linux-only" "Link to native install wiki"
```

FullMonte uses the CMake (www.cmake.org) packaging system to support multiple OSes, compilers, IDEs, and build systems. It should
compile relatively easily on Unix-like operating systems with modern processors (AVX instruction set minimum).

Please see the `BUILDING.md` file for more information.

### Additional Useful Software

* [VTK](https://www.vtk.org) is an open-source, freely available software system for 3D computer graphics, image processing, and visualization; required version is 7.1.1 - needs to be built from source with CMake
* [Paraview](www.paraview.org) for result visualization (version 5.4 or later)
  * requires VTK

* Qt for PDT GUI (work in progress)
  * Download Qt 5.3 from [qt.io](http://www.qt.io/download-open-source) and follow their installation instructions

* [ITK-SNAP](http://www.itksnap.org/pmwiki/pmwiki.php) is a software application used to segment structures in 3D medical images

## License
The whole license agreement can be seen in the `LICENSE.md` file of this repository.

If your work with this software results in any academic publications, please give credit by citing:

* High-performance, robustly verified Monte Carlo simulation with FullMonte. http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=1 By: Cassidy, Jeffrey; Nouri, Ali; Betz, Vaughn; et al. Journal of biomedical optics  Volume: 23   Issue: 8   Pages: 1-11   Published: 2018-Aug

**Bibtex:**
```bibtex
@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Jeffrey  Cassidy,Ali  Nouri,Vaughn  Betz,Lothar  Lilge},
title = {High-performance, robustly verified Monte Carlo simulation with FullMonte},
journal = {Journal of Biomedical Optics},
volume = {23},
number = {},
pages = {23 - 23 - 11},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {https://doi.org/10.1117/1.JBO.23.8.085001},
eprint = {}
}
```

* Automatic interstitial photodynamic therapy planning via convex optimization http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=2 By: Yassine, Abdul-Amir; Kingsford, William; Xu, Yiwen; et al. BIOMEDICAL OPTICS EXPRESS  Volume: 9   Issue: 2   Pages: 898-920   Published: FEB 1 2018

**Bibtex:**
```bibtex
@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Yassine, A.-A., Kingsford, W., Xu, Y., Cassidy, J., Lilge, L., & Betz, V.},
title = {Automatic interstitial photodynamic therapy planning via convex optimization},
journal = {Biomedical Optics Express},
volume = {9},
number = {2},
pages = {989 - 920},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {http://doi.org/10.1364/BOE.9.000898},
eprint = {}
}
```

* Treatment plan evaluation for interstitial photodynamic therapy in a mouse model by Monte Carlo simulation with FullMonte
http://apps.webofknowledge.com.myaccess.library.utoronto.ca/full_record.do?product=UA&search_mode=GeneralSearch&qid=1&SID=7EYb5H7oXj2hSz2LnJp&page=1&doc=4 By: Cassidy, Jeffrey; Betz, Vaughn; Lilge, Lothar FRONTIERS IN PHYSICS  Volume: 3     Article Number: 6   Published: FEB 24 2015

**Bibtex:**
```bitex
@article{10.3389/fphy.2015.00006,
author={Cassidy, Jeffrey and Betz, Vaughn and Lilge, Lothar},
title={Treatment plan evaluation for interstitial photodynamic therapy in a mouse model by Monte Carlo simulation with FullMonte},
journal={Frontiers in Physics},
volume={3},
pages={6},
year={2015},
url={https://www.frontiersin.org/article/10.3389/fphy.2015.00006},
doi={10.3389/fphy.2015.00006},
issn={2296-424X}
}
```

## Contributing

The basic guidelines for contributing to the project are described in `CONTRIBUTING.md` as well as in the respective [wiki page](https://gitlab.com/FullMonte/FullMonteSW/wikis/Fixing-bugs-and-adding-features). In short, you need to follow these steps:

 1. Create an issue
 2. Assign the issue
 3. Create a branch
 4. Fix the bug
 5. Test the bugfix
 6. Create a merge request

## Testing

### Basic simulation kernel

Basic functionality can be checked by running `Kernels/Software/Test_Mouse` which is a basic bioluminescence simulation based on the
Digimouse test data released with TIM-OS by Shen and Wang (2010). It loads the mesh, optical properties, and source definitions, checks
that the data is loaded correctly (number of mesh elements, sources, etc), then runs the simulator and checks that the basic energy-conservation laws are followed, and that the event counts are approximately correct.

If FullMonte was built with `WRAP_VTK` enabled, then it will also output .vtk files showing the surface and volume fluence, which can
be loaded into Paraview (using the `VisualizeMouse.pvsm` state file).

### Unit Tests

Many folders contain a `Test/` subfolder with Boost Unit Test Framework unit tests for specific classes.
