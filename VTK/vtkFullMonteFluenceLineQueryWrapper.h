/*
 * vtkFullMonteFluenceLineQueryWrapper.h
 *
 *  Created on: Mar 6, 2016
 *      Author: jcassidy
 */

#ifndef VTK_VTKFULLMONTEFLUENCELINEQUERYWRAPPER_H_
#define VTK_VTKFULLMONTEFLUENCELINEQUERYWRAPPER_H_

#include <FullMonteSW/Config.h>
#include <vtkObject.h>

class FluenceLineQuery;
class vtkPolyData;

class vtkFullMonteFluenceLineQueryWrapper : public vtkObject
{
public:
	vtkTypeMacro(vtkFullMonteFluenceLineQueryWrapper,vtkObject)
	const FluenceLineQuery* fluenceLineQuery() const;
	void fluenceLineQuery(const FluenceLineQuery* q);

	void fluenceLineQuery(const char* swigFluenceLineQueryPtr);

	void update();

	vtkPolyData* getPolyData() const;

protected:
	vtkFullMonteFluenceLineQueryWrapper();

private:
	float 						m_minSegmentLength=1e-5f;
	const FluenceLineQuery* 	m_fluenceLineQuery=nullptr;
	vtkPolyData*				m_vtkPD=nullptr;
};

#endif /* VTK_VTKFULLMONTEFLUENCELINEQUERYWRAPPER_H_ */
