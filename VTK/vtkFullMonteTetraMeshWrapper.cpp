/*
 * vtkFullMonteTetraMeshWrapper.cpp
 *
 *  Created on: Mar 3, 2016
 *      Author: jcassidy
 */

#include "vtkFullMonteTetraMeshWrapper.h"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkUnsignedShortArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkObjectFactory.h>

#include <FullMonteSW/Warnings/Boost.hpp>

#include <boost/range/adaptor/indexed.hpp>

#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Point.hpp>
#include <FullMonteSW/Geometry/Points.hpp>

#include "vtkHelperFunctions.hpp"

using namespace std;

vtkStandardNewMacro(vtkFullMonteTetraMeshWrapper)

vtkFullMonteTetraMeshWrapper::vtkFullMonteTetraMeshWrapper()
{
	m_points=vtkPoints::New();

	m_tetras=vtkCellArray::New();

	m_regions=vtkUnsignedShortArray::New();
		m_regions->SetName("Region");

	m_faces=vtkCellArray::New();

	pd = vtkPolyData::New();

	ug = vtkUnstructuredGrid::New();

	//ids = vtkIdTypeArray::New();
}

/**
 * @brief Destroy the vtk FullMonteTetraMeshWrapper::vtkFullMonteTetraMeshWrapper object
 * All vtk objects are subject to the VTK memory management. There were some problems with
 * memory leaks and this is what currently works.
 * 
 * Initialize() restores the object back to its initial state and releases all memory allocated by it
 * if the reference count is 1.
 * 
 * Delete() deletes the object and frees the memory if the reference count is 1
 * 
 * Not sure if the objects have to get called in this specific sequence, but this is the sequence that 
 * does not hog memory when running several iteration of VTKMeshWriter, so going with it for now.
 * 
 */
vtkFullMonteTetraMeshWrapper::~vtkFullMonteTetraMeshWrapper()
{
	pd->Initialize();
	pd->Delete();

	m_regions->Initialize();
	m_regions->Delete();

	m_points->Initialize();
	m_points->Delete();	

	m_tetras->Initialize();
	m_tetras->Delete();

	m_faces->Initialize();
	m_faces->Delete();
	
	ug->Initialize();
	ug->Delete();
}

void vtkFullMonteTetraMeshWrapper::mesh(const TetraMesh* m)
{
	m_mesh = m;
	// update() can also be called separately, but in VTKMeshWriter.cpp, it
	// is actually not required to do so since we are already calling it here.
	update();
}



const TetraMesh* vtkFullMonteTetraMeshWrapper::mesh() const
{
	return m_mesh;
}

vtkPolyData* vtkFullMonteTetraMeshWrapper::faces() const
{
	pd->SetPoints(points());
	pd->SetPolys(m_faces);
	return pd;
}

vtkPoints* vtkFullMonteTetraMeshWrapper::points() const
{
	return m_points;
}

vtkUnsignedShortArray* vtkFullMonteTetraMeshWrapper::regions() const
{
	return m_regions;
}

void vtkFullMonteTetraMeshWrapper::update()
{
	if (!m_mesh)
	{
		LOG_WARNING << "vtkFullMonteTetraMeshWrapper::update() skipped due to null pointer" << endl;
		m_points->SetNumberOfPoints(0);
		m_tetras->SetNumberOfCells(0);
		m_regions->SetNumberOfTuples(0);
		m_faces->SetNumberOfCells(0);
		return;
	}

	bool zeroTetra_inserted = false;
	zeroTetra_inserted = getVTKTetraCells(*m_mesh,m_tetras);
	getVTKPoints(*m_mesh,m_points, zeroTetra_inserted);
	getVTKTetraRegions(*m_mesh,m_regions, zeroTetra_inserted);
	getVTKTriangleCells(*m_mesh,m_faces, zeroTetra_inserted);

	Modified();
}

vtkUnstructuredGrid* vtkFullMonteTetraMeshWrapper::regionMesh() const
{
	vtkUnstructuredGrid* regionMesh = blankMesh();
	regionMesh->GetCellData()->SetScalars(m_regions);
	return regionMesh;
}

vtkUnstructuredGrid* vtkFullMonteTetraMeshWrapper::blankMesh() const
{
	ug->SetPoints(m_points);
	ug->SetCells(VTK_TETRA,m_tetras);
	return ug;
}

#ifdef WRAP_VTK
#include "swig_traits.hpp"
extern template class swig_traits<TetraMesh*>;
#ifdef WRAP_TCL


void vtkFullMonteTetraMeshWrapper::mesh(const char* swigTetraMeshPtr)
{
	TetraMesh* m = decodeSwigPointer<TetraMesh*>(swigTetraMeshPtr);
	LOG_DEBUG << "m=" << m << endl;
	if (m)
	{
		if (m->points() && m->tetraCells()) {
		    LOG_DEBUG << "vtkFullMonteTetraMeshWrapper::mesh(swigTetraMeshPtr) updated to hold " << m << " " << m->points()->size() << " points and " << m->tetraCells()->size() << " tetras" << endl;
        } else {
		    LOG_DEBUG << "vtkFullMonteTetraMeshWrapper::mesh(swigTetraMeshPtr) updated to hold " << m << " " << " an empty mesh" << endl;
        }

	}
	mesh(m);
}
#endif

// TODO Support SWIG - VTK python interface
// #ifdef WRAP_PYTHON
// void vtkFullMonteTetraMeshWrapper::mesh(const void* swigTetraMeshPtr)
// {
// 	PyObject* pyTetraMeshPtr = (PyObject*) swigTetraMeshPtr;
// 	TetraMesh* m = decodeSwigPointer<TetraMesh*>(pyTetraMeshPtr);
// 	LOG_DEBUG << "m=" << m << endl;
// 	if (m)
// 	{
// 		if (m->points() && m->tetraCells()) {
// 		    LOG_DEBUG << "vtkFullMonteTetraMeshWrapper::mesh(swigTetraMeshPtr) updated to hold " << m << " " << m->points()->size() << " points and " << m->tetraCells()->size() << " tetras" << endl;
//         } else {
// 		    LOG_DEBUG << "vtkFullMonteTetraMeshWrapper::mesh(swigTetraMeshPtr) updated to hold " << m << " " << " an empty mesh" << endl;
//         }

// 	}
// 	mesh(m);
// }
// #endif
#endif
