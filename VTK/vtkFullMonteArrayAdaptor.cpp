/*
 * vtkFullMonteArrayAdaptor.cpp
 *
 *  Created on: Aug 12, 2016
 *      Author: jcassidy
 */

#include "vtkFullMonteArrayAdaptor.h"

#include <memory>

#include <vtkFloatArray.h>
#include <vtkAbstractArray.h>

#include <string>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/OutputTypes/AbstractSpatialMap.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <boost/range/adaptor/indexed.hpp>

#include <cmath>
#include <sstream>
#include <iostream>

using namespace std;

vtkFullMonteArrayAdaptor::vtkFullMonteArrayAdaptor()
{
	m_vtkArray = vtkFloatArray::New();
}

vtkFullMonteArrayAdaptor::~vtkFullMonteArrayAdaptor()
{
	m_vtkArray->Delete();
}


void vtkFullMonteArrayAdaptor::source(const OutputData* D)
{
	m_fullMonteArray = D;
	update();
}


void vtkFullMonteArrayAdaptor::update()
{
	LOG_DEBUG << "vtkFullMonteArrayAdaptor::update()" << flush << endl;

	// TODO: Inefficient if array is already the right size & type
	if (m_vtkArray)
		m_vtkArray->Delete();

	if (!m_fullMonteArray)
	{
		LOG_ERROR << "vtkFullMonteArrayAdaptor::update() has null m_fullMonteArray" << endl;
		return;
	}

	const AbstractSpatialMap* M = dynamic_cast<const AbstractSpatialMap*>(m_fullMonteArray);

	if (!M)
	{
		LOG_ERROR << "vtkFullMonteArrayAdaptor::update() cannot downcast m_fullMonteArray to AbstractSpatialMap" << endl;
		return;
	}


	if (const SpatialMap<double>* md = dynamic_cast<const SpatialMap<double>*>(m_fullMonteArray))
	{
		m_vtkArray = vtkFloatArray::New();
		m_vtkArray->SetNumberOfTuples(md->dim());

		for(const auto d : md->values() | boost::adaptors::indexed(0U))
		{
			// static_cast<vtkFloatArray*>(m_vtkArray)->SetValue(d.index(), std::isnan(d.value()) ? 0.0 : d.value());
			static_cast<vtkFloatArray*>(m_vtkArray)->SetValue(d.index(), d.value());
		}

	}
	else if (const SpatialMap<float>* mf = dynamic_cast<const SpatialMap<float>*>(m_fullMonteArray))
	{
		m_vtkArray = vtkFloatArray::New();
		m_vtkArray->SetNumberOfTuples(mf->dim());

		for(const auto f : mf->values() | boost::adaptors::indexed(0U))
		{
			// static_cast<vtkFloatArray*>(m_vtkArray)->SetValue(f.index(), isnan(f.value()) ? 0.0f : f.value());
			static_cast<vtkFloatArray*>(m_vtkArray)->SetValue(f.index(), f.value());
		}
	}

	else
	{
		LOG_ERROR << "Failed to cast result in vtkFullMonteArrayAdaptor" << endl;
		return;
	}

	stringstream ss;

	switch(M->spatialType())
	{
	case AbstractSpatialMap::Surface:
		ss << "Surface";
		break;

	case AbstractSpatialMap::Volume:
		ss << "Volume";
		break;

	case AbstractSpatialMap::Line:
		ss << "Line";
		break;

	case AbstractSpatialMap::Point:
		ss << "Point";
		break;

	default:
		ss << "(unknown-spatial-type)";
		LOG_WARNING << "unknown type in AbstractSpatialMap" << endl;
	}

	m_vtkArray->SetName(ss.str().c_str());

	Modified();
	m_vtkArray->Modified();
}

vtkAbstractArray* vtkFullMonteArrayAdaptor::array()
{
	return m_vtkArray;
}

vtkStandardNewMacro(vtkFullMonteArrayAdaptor)

#ifdef WRAP_VTK
#include "swig_traits.hpp"
#ifdef WRAP_TCL

void vtkFullMonteArrayAdaptor::source(const char *swigOutputDataPtr)
{
	OutputData* d = decodeSwigPointer<OutputData*>(swigOutputDataPtr);
	LOG_DEBUG << "vtkFullMonteArrayAdaptor::source(\"" << swigOutputDataPtr << "\") decoded to " << d << endl;
	source(d);
}
#endif

// TODO Support SWIG - VTK python interface
// #ifdef WRAP_PYTHON
// void vtkFullMonteArrayAdaptor::source(const void *swigOutputDataPtr)
// {
// 	PyObject* pyOutputDataPtr = (PyObject*) swigOutputDataPtr;
// 	OutputData* d = decodeSwigPointer<OutputData*>(pyOutputDataPtr);
// 	LOG_DEBUG << "vtkFullMonteArrayAdaptor::source(\"" << pyOutputDataPtr << "\") decoded to " << d << endl;
// 	source(d);
// }
// #endif
#endif
