package provide FullMonte @FULLMONTE_VERSION_MAJOR@.@FULLMONTE_VERSION_MINOR@

namespace eval ::FullMonte {
    namespace export datadir
    
    
    ### Default values set by CMake at compile time
    
    # data directory (type ..../src/FullMonteSW/data)
    set datadir "@FULLMONTE_CONFIG_DATA_DIR@"

    set sourcedir "@FULLMONTE_INCLUDE_DIR@"

    set exampledir "@FULLMONTE_EXAMPLE_DIR@"

    # default number of threads
    set defaultThreadCount @FULLMONTE_THREAD_COUNT@
    
    # location of MMC executable (for testing)
    set mmcpath "@FULLMONTE_MMC@"
    
    # location of TIMOS executable (for testing)
    set timospath "@FULLMONTE_TIMOS@"

    # build type of FullMonte
    set buildtype "@CMAKE_BUILD_TYPE@"
}

proc ::FullMonte::runKernelWithProgress { k } {
    $k runAsync
}

proc ::FullMonte::defaultProgressCommand { } {
}

proc ::FullMonte::close_window_if_present { } { if { [info commands wm] != "" } { wm withdraw . } }



###### Load required FullMonte libraries
# Logging
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteLoggingTCL.so

# TIMOS format reader (for Digimouse) with TCL bindings
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteTIMOSTCL.so

# Geometry model with TCL bindings
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteGeometryTCL.so
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMontePlacementTCL.so

# P8 kernels with TCL bindings (only if enabled by USE_HW)
if {"@USE_HW@" eq "ON"} {
    load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMontePowerKernelTCL.so
}
# CUDA kernels with TCL bindings (only if enabled by BUILD_CUDAACCEL)
if {@_build_cudaaccel@} {
    load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteCUDAKernelTCL.so
}

# FPGACL kernels with TCL bindings (only if enabled by BUILD_FPGACLACCEL)
if {@_build_fpgaclaccel@} {
    load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteFPGACLKernelTCL.so
}

# Software kernels with TCL bindings
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteSWKernelTCL.so
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteKernelsTCL.so

# Data output manipulation
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteDataTCL.so

# Data queries
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteQueriesTCL.so

# TIMOS-format loading and writing
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteTIMOSTCL.so

# Text file reading/writing
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteTextFileTCL.so

# MCML file input/output
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteMCMLFileTCL.so

# MMC file input/output
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteMMCFileTCL.so

if { "@WRAP_VTK@" eq "ON" } {
# VTK interface
load @FullMonteSW_LIBRARY_DIR@/libvtkFullMonteTCL-@VTK_VERSION_MAJOR@.@VTK_VERSION_MINOR@.so
}

if { "@VTK_FOUND@" eq "1" } { 
load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteVTKFilesTCL.so
}

load @FullMonteSW_LIBRARY_DIR@/@_tcl_lib_prefix@FullMonteCOMSOLFileTCL.so
