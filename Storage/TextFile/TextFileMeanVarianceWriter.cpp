/*
 * TextFileMeanVarianceSet.cpp
 *
 *  Created on: Oct 26, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include "TextFileMeanVarianceWriter.hpp"
#include <iostream>
#include <fstream>

using namespace std;

TextFileMeanVarianceWriter::TextFileMeanVarianceWriter()
{
}

TextFileMeanVarianceWriter::~TextFileMeanVarianceWriter()
{
}

void TextFileMeanVarianceWriter::filename(string fn)
{
	m_filename=fn;
}

void TextFileMeanVarianceWriter::input(MeanVarianceSet* S)
{
	m_data=S;
}

void TextFileMeanVarianceWriter::write() const
{
	ofstream os(m_filename);

	os << "# mean-variance dataset" << endl;
	os << "# " << endl;
	os << m_data->runs()  << 'x' << m_data->packetsPerRun() << " # runs x packets_per_run" << endl;
	os << m_data->dim()-1 << " # vector length N (indices 0..N-1)" << endl;

	SpatialMap<float>* mu 		= m_data->mean();
	SpatialMap<float>* sigma2 	= m_data->variance();

	for(unsigned i=1;i<m_data->dim();++i)
		os << mu->get(i) << ' ' << sigma2->get(i) << endl;
}
