/*
 * TextFileMeanVarianceReader.hpp
 *
 *  Created on: Nov 1, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEREADER_HPP_
#define STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEREADER_HPP_

#include <string>

class MeanVarianceSet;

class TextFileMeanVarianceReader
{
public:
	TextFileMeanVarianceReader();
	virtual ~TextFileMeanVarianceReader();

	void filename(std::string fn);
	void read();

	MeanVarianceSet*	result();


private:
	std::string			m_filename;
	MeanVarianceSet*	m_data=nullptr;
};


#endif /* STORAGE_TEXTFILE_TEXTFILEMEANVARIANCEREADER_HPP_ */
