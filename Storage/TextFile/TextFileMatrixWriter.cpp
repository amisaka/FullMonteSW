/*
 * TextFileMatrixWriter.cpp
 *
 *  Created on: Jun 5, 2017
 *      Author: jcassidy
 */

#include "TextFileMatrixWriter.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <fstream>
#include <iomanip>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>
#include <FullMonteSW/Queries/DoseHistogram.hpp>

using namespace std;

TextFileMatrixWriter::TextFileMatrixWriter()
{

}

TextFileMatrixWriter::~TextFileMatrixWriter()
{
}

void TextFileMatrixWriter::source(OutputData* d)
{
	m_data=d;
}

OutputData* TextFileMatrixWriter::source() const
{
	return m_data;
}

void TextFileMatrixWriter::filename(string fn)
{
	m_filename=fn;
}

void TextFileMatrixWriter::precision(unsigned p)
{
	m_precision=p;
}

void TextFileMatrixWriter::width(unsigned w)
{
	m_width=w;
}

void TextFileMatrixWriter::columns(unsigned c)
{
    m_columns=c;
}

void TextFileMatrixWriter::write()
{
	ofstream os(m_filename.c_str());

	if (m_precision != 0)
		os << scientific << setprecision(m_precision);

	if(!m_data) {
		LOG_ERROR << "TextFileMatrixWriter::write() provided null data" << endl;
    }
	else if (const SpatialMap<float>* vf = dynamic_cast<const SpatialMap<float>*>(m_data))
	{
		switch(vf->outputType())
		{
			case AbstractSpatialMap::Energy:
			switch(vf->unitType())
			{
				case AbstractSpatialMap::J_m:
					os << "Energy (J;m) ";
					break;
				case AbstractSpatialMap::J_cm:
					os << "Energy (J;cm) ";
					break;
				case AbstractSpatialMap::J_mm:
					os << "Energy (J;mm) ";
					break;
				case AbstractSpatialMap::W_m:
					os << "Power (W;m) ";
					break;
				case AbstractSpatialMap::W_cm:
					os << "Power (W;cm) ";
					break;
				case AbstractSpatialMap::W_mm:
					os << "Power (W;mm) ";
					break;
				default:
					os << "Energy (?) ";
					break;
			}	
				break;
			case AbstractSpatialMap::Fluence:
				os << "Fluence ";
				switch(vf->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^2) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^2) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^2) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^2) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^2) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^2) ";
						break;
					default:
						os << "(?) ";
						break;
				}	
				break;
			case AbstractSpatialMap::PhotonWeight:
				os << "PhotonWeight (?) ";
				break;
			case AbstractSpatialMap::EnergyPerVolume:
				os << "Energy/Volume ";
				switch(vf->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^3) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^3) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^3) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^3) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^3) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^3) ";
						break;
					default:
						os << "(?) ";
						break;
				}
				break;
			default:
				os << "Unknown (?) ";
				break;
		}
		if (const SpatialMap2D<float>* vf2 = dynamic_cast<const SpatialMap2D<float>*>(vf))
			os << "2 " << vf2->dims()[0] << ' ' << vf2->dims()[1];
		else
			os << "1 " << vf->dim();
		os << " float" << endl;

		for(unsigned i=0;i < vf->dim(); ++i)
			os << setw(m_width) << vf->get(i) << ((i % m_columns) == m_columns-1 ? '\n' : ' ');
	}
	else if (const SpatialMap<unsigned>* vu = dynamic_cast<const SpatialMap<unsigned>*>(m_data))
	{
		switch(vu->outputType())
		{
			case AbstractSpatialMap::Energy:
			switch(vu->unitType())
			{
				case AbstractSpatialMap::J_m:
					os << "Energy (J;m) ";
					break;
				case AbstractSpatialMap::J_cm:
					os << "Energy (J;cm) ";
					break;
				case AbstractSpatialMap::J_mm:
					os << "Energy (J;mm) ";
					break;
				case AbstractSpatialMap::W_m:
					os << "Power (W;m) ";
					break;
				case AbstractSpatialMap::W_cm:
					os << "Power (W;cm) ";
					break;
				case AbstractSpatialMap::W_mm:
					os << "Power (W;mm) ";
					break;
				default:
					os << "Energy (?) ";
					break;
			}	
				break;
			case AbstractSpatialMap::Fluence:
				os << "Fluence ";
				switch(vu->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^2) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^2) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^2) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^2) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^2) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^2) ";
						break;
					default:
						os << "(?) ";
						break;
				}	
				break;
			case AbstractSpatialMap::PhotonWeight:
				os << "PhotonWeight (?) ";
				break;
			case AbstractSpatialMap::EnergyPerVolume:
				os << "Energy/Volume ";
				switch(vu->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^3) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^3) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^3) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^3) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^3) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^3) ";
						break;
					default:
						os << "(?) ";
						break;
				}
				break;
			default:
				os << "Unknown (?) ";
				break;
		}
		if (const SpatialMap2D<unsigned>* vu2 = dynamic_cast<const SpatialMap2D<unsigned>*>(vu))
			os << "2 " << vu2->dims()[0] << ' ' << vu2->dims()[1];
		else
			os << "1 " << vu->dim();
		os << " unsigned" << endl;

		for(unsigned i=0;i < vu->dim(); ++i)
			os << setw(m_width) << vu->get(i) << ((i % m_columns) == m_columns-1 ? '\n' : ' ');
	}
	else if (const DirectedSurface* ds = dynamic_cast<const DirectedSurface*>(m_data))
	{
		switch(ds->outputType())
		{
			case AbstractSpatialMap::Energy:
			switch(ds->unitType())
			{
				case AbstractSpatialMap::J_m:
					os << "Energy (J;m) ";
					break;
				case AbstractSpatialMap::J_cm:
					os << "Energy (J;cm) ";
					break;
				case AbstractSpatialMap::J_mm:
					os << "Energy (J;mm) ";
					break;
				case AbstractSpatialMap::W_m:
					os << "Power (W;m) ";
					break;
				case AbstractSpatialMap::W_cm:
					os << "Power (W;cm) ";
					break;
				case AbstractSpatialMap::W_mm:
					os << "Power (W;mm) ";
					break;
				default:
					os << "Energy (?) ";
					break;
			}	
				break;
			case AbstractSpatialMap::Fluence:
				os << "Fluence ";
				switch(ds->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^2) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^2) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^2) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^2) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^2) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^2) ";
						break;
					default:
						os << "(?) ";
						break;
				}	
				break;
			case AbstractSpatialMap::PhotonWeight:
				os << "PhotonWeight (?) ";
				break;
			case AbstractSpatialMap::EnergyPerVolume:
				os << "Energy/Volume ";
				switch(ds->unitType())
				{
					case AbstractSpatialMap::J_m:
						os << "(J/m^3) ";
						break;
					case AbstractSpatialMap::J_cm:
						os << "(J/cm^3) ";
						break;
					case AbstractSpatialMap::J_mm:
						os << "(J/mm^3) ";
						break;
					case AbstractSpatialMap::W_m:
						os << "(W/m^3) ";
						break;
					case AbstractSpatialMap::W_cm:
						os << "(W/cm^3) ";
						break;
					case AbstractSpatialMap::W_mm:
						os << "(W/mm^3) ";
						break;
					default:
						os << "(?) ";
						break;
				}
				break;
			default:
				os << "Unknown (?) ";
				break;
		}
		columns(1);
		os << "3 " << ds->exiting()->dim() << ' ' << ds->entering()->dim() << ' ' << ds->calculateTotal()->dim() << " float" << endl;
		os << "exiting entering total" << endl;
		for(unsigned i=0;i < ds->exiting()->dim(); ++i)
			os << setw(m_width) << ds->exiting()->get(i) << ' ' << ds->entering()->get(i) << ' ' << ds->calculateTotal()->get(i) << ((i % m_columns) == m_columns-1 ? '\n' : ' ');
		
		columns(10);
	}
	else if (const DoseHistogram* dh = dynamic_cast<const DoseHistogram*>(m_data))
	{
		columns(1);
		os << "Unknown "<< "4 " << dh->dim() << ' ' << dh->dim()<< ' ' << dh->dim() << ' ' << dh->dim() << " float/unsigned" << endl;
		os << "CumulativeMeasure | CDF | Dose | TetraID" << endl;
		for(unsigned i=0;i < dh->dim(); ++i)
			os << setw(m_width) << dh->get(i).cmeasure << ' ' << dh->get(i).cdf << ' ' << dh->get(i).dose << ' ' << dh->get(i).tetID << ((i % m_columns) == m_columns-1 ? '\n' : ' ');
		columns(10);
	}
	else
    {
		LOG_ERROR << "TextFileMatrixWriter::write() unrecognized data type '" << m_data->type()->name() << "'" << endl;
    }
}

