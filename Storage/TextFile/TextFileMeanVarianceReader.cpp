/*
 * TextFileMeanVarianceReader.cpp
 *
 *  Created on: Nov 1, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/MeanVarianceSet.hpp>
#include "TextFileMeanVarianceReader.hpp"
#include <FullMonteSW/Storage/Common/CommentFilter.hpp>
#include <fstream>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

using namespace std;

TextFileMeanVarianceReader::TextFileMeanVarianceReader()
{
}

TextFileMeanVarianceReader::~TextFileMeanVarianceReader()
{
}

void TextFileMeanVarianceReader::filename(string fn)
{
	m_filename=fn;
}

void TextFileMeanVarianceReader::read()
{
	ifstream is(m_filename.c_str());

	if (!is.good())
	{
		LOG_ERROR << "TextFileMeanVarianceReader::read() failed to open " << m_filename << endl;
		return;
	}


	boost::iostreams::filtering_stream<boost::iostreams::input,char> F;

	single_delim_comment_filter CF;

	F.push(CF);
	F.push(is);

	unsigned Nr;
	unsigned long long Np;
	char x;

	F >> Nr >> x >> Np;

	if (F.fail())
	{
		LOG_ERROR << "TextFileMeanVarianceReader::read() I/O failure" << endl;
		return;
	}
	else if (x != 'x')
	{
		LOG_ERROR << "TextFileMeanVarianceReader::read() unexpected character '" << x << "' in place of 'x'" << endl;
		return;

	}

	unsigned Ne;
	F >> Ne;


	m_data=new MeanVarianceSet();

	SpatialMap<float>* mu = new SpatialMap<float>();
	SpatialMap<float>* sigma2 = new SpatialMap<float>();

	m_data->mean(mu);
	m_data->variance(sigma2);
	m_data->packetsPerRun(Np);
	m_data->runs(Nr);

	mu->dim(Ne);
	sigma2->dim(Ne);

	unsigned i;

	for(i=0;i<Ne && !is.fail();++i)
	{
		float mu_i,sigma2_i;
		F >> mu_i >> sigma2_i;
		mu->set(i,mu_i);
		sigma2->set(i,sigma2_i);
	}

	LOG_DEBUG << "Read " << i << " mean-variance samples" << endl;

	if (is.fail())
	{
		LOG_ERROR << "TextFileMeanVarianceReader::read() failed" << endl;
	}
}

MeanVarianceSet*	TextFileMeanVarianceReader::result()
{
	return m_data;
}
