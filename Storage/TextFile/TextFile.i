#if defined(SWIGTCL)
%module FullMonteTextFileTCL
#elif defined(SWIGPYTHON)
%module TextFile
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"

%{
#include "TextFileDoseHistogramWriter.hpp"
#include "TextFileMatrixReader.hpp"
#include "TextFileMatrixWriter.hpp"
#include <FullMonteSW/Geometry/TetraMesh.hpp>
%}

//%include "TextFile.hpp"
%include "TextFileMatrixReader.hpp"
%include "TextFileMatrixWriter.hpp"
%include "TextFileDoseHistogramWriter.hpp"