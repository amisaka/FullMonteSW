/*
 * TextFileMatrixReader.hpp
 *
 *  Created on: Jul 14, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_TEXTFILE_TEXTFILEMATRIXREADER_HPP_
#define STORAGE_TEXTFILE_TEXTFILEMATRIXREADER_HPP_

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <string>

class TextFileMatrixReader
{
public:
	TextFileMatrixReader();
	~TextFileMatrixReader();

	void 			filename(std::string fn);

	void			read();
	OutputData*		output() const;

private:
	void 			clear();
	unsigned		size() const;

	std::string		m_filename;

	unsigned		m_D=0;
	unsigned*		m_dims=nullptr;
	float* 			m_data=nullptr;
	OutputData::OutputType m_dataType=OutputData::UnknownOutputType;
	OutputData::UnitType m_unitType=OutputData::UnknownUnitType;
};

#endif /* STORAGE_TEXTFILE_TEXTFILEMATRIXREADER_HPP_ */
