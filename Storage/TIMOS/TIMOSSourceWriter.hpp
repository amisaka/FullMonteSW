/*
 * TIMOSSourceWriter.hpp
 *
 *  Created on: Sep 20, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_TIMOS_TIMOSSOURCEWRITER_HPP_
#define STORAGE_TIMOS_TIMOSSOURCEWRITER_HPP_

#include <string>

namespace Source { class Abstract; }

class TIMOSSourceWriter
{
public:
	TIMOSSourceWriter();
	virtual ~TIMOSSourceWriter();

	void filename(std::string fn);
	void source(Source::Abstract* S);
	void packets(unsigned long long Np);

	void write();

private:
	class CountVisitor;
	class SourceVisitor;

	std::string 		m_filename;
	unsigned long long	m_packets;
	Source::Abstract*	m_source=nullptr;
};

#endif /* STORAGE_TIMOS_TIMOSSOURCEWRITER_HPP_ */
