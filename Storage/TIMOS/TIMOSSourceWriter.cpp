/*
 * TIMOSSourceWriter.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: jcassidy
 */

#include "TIMOSSourceWriter.hpp"
#include <iostream>
#include <fstream>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Sources/Composite.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/SurfaceTri.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>

using namespace std;


class TIMOSSourceWriter::CountVisitor : public Source::Abstract::Visitor
{
public:
	 virtual void undefinedVisitMethod(Source::Abstract*) override { ++m_count; }
	 unsigned count() const { return m_count; }

private:
	 unsigned	m_count=0;
};

class TIMOSSourceWriter::SourceVisitor : public Source::Abstract::Visitor
{
public:
	SourceVisitor(std::ostream& os,unsigned long long Np);
	~SourceVisitor();

	 virtual void doVisit(Source::Point*) override;
	 virtual void doVisit(Source::Volume*) override;
	 virtual void doVisit(Source::PencilBeam*) override;
	 virtual void doVisit(Source::SurfaceTri*) override;

	 virtual void preVisitComposite(Source::Composite*) override;
	 virtual void postVisitComposite(Source::Composite*) override;

	 virtual void undefinedVisitMethod(Source::Abstract*) override;

	 unsigned long long allocatePackets(float weight);

private:
	 std::ostream& 			m_os;
	 unsigned				m_sources=0;
	 unsigned long long 	m_packets=0ULL;
	 unsigned long long 	m_packetsAllocated=0ULL;
	 unsigned long long 	m_packetsDebug=0ULL;

	 std::vector<float>		m_weightStack{1,0.0f};
	 float					m_weightAllocated=0.0f;
};

TIMOSSourceWriter::SourceVisitor::SourceVisitor(ostream& os,unsigned long long Np) :
		m_os(os),
		m_packets(Np)
{
}

TIMOSSourceWriter::SourceVisitor::~SourceVisitor()
{
	LOG_DEBUG << "SourceVisitor finished" << endl;
	LOG_DEBUG << "  Generated " << m_sources << " sources" << endl;
	LOG_DEBUG << "  Allocated " << m_weightAllocated << " weight" << endl;
	LOG_DEBUG << "  Allocated " << m_packetsAllocated << " packets (debug count " << m_packetsDebug << ")" << endl;
}

void TIMOSSourceWriter::SourceVisitor::doVisit(Source::Point* s)
{
	array<float,3> p = s->position();
	m_os << "1 " << p[0] << ' ' << p[1] << ' ' << p[2] << ' ' << allocatePackets(s->power()) << endl;
	++m_sources;
}

void TIMOSSourceWriter::SourceVisitor::doVisit(Source::SurfaceTri* s)
{
	std::array<unsigned,3> IDps = s->triPointIDs();
	m_os << "1 " << IDps[0] << ' ' << IDps[1] << ' ' << IDps[2] << ' ' << allocatePackets(s->power()) << endl;
	++m_sources;
}

void TIMOSSourceWriter::SourceVisitor::doVisit(Source::Volume* s)
{
	m_os << "2 " << s->elementID() << ' ' << allocatePackets(s->power()) << endl;
	++m_sources;
}

void TIMOSSourceWriter::SourceVisitor::doVisit(Source::PencilBeam* s)
{
	array<float,3> pos = s->position();
	array<float,3> dir = s->direction();
	m_os << "11 " << s->elementHint() << ' ' << pos[0] << ' ' << pos[1] << ' ' << pos[2] << ' ' <<
			dir[0] << ' ' << dir[1] << ' ' << dir[2] << ' ' <<
			allocatePackets(s->power()) << endl;
	++m_sources;
}

void TIMOSSourceWriter::SourceVisitor::undefinedVisitMethod(Source::Abstract*)
{
	LOG_ERROR << "TIMOSSourceWriter::SourceVisitor::doVisit(S) called on invalid source type" << endl;
}

void TIMOSSourceWriter::SourceVisitor::preVisitComposite(Source::Composite* C)
{
	// gather total weight of sub-elements
	float wTotal=0.0f;
	for(const auto S : C->elements())
		wTotal += S->power();

	if (m_weightStack.back() == 0.0f)
		m_weightStack.push_back(1.0f/wTotal);
	else
		m_weightStack.push_back(m_weightStack.back()*C->power()/wTotal);
}

void TIMOSSourceWriter::SourceVisitor::postVisitComposite(Source::Composite*)
{
	m_weightStack.pop_back();
}

unsigned long long TIMOSSourceWriter::SourceVisitor::allocatePackets(float w)
{
	unsigned long long Npa;
	float dw=0.0f;

	if (m_weightStack.back()==0.0f)		// single source -> allocate all packets here
		Npa = m_packets;
	else								// part of a composite -> allocate w*weightMultiplier
	{
		dw = w*m_weightStack.back();
		unsigned long long NpPrime = (m_weightAllocated + dw)*float(m_packets);
		Npa = NpPrime-m_packetsAllocated;
	}

	m_packetsAllocated += Npa;			// track total packets allocated
	m_weightAllocated += dw;

	return Npa;
}


TIMOSSourceWriter::TIMOSSourceWriter()
{
}

TIMOSSourceWriter::~TIMOSSourceWriter()
{
}

void TIMOSSourceWriter::filename(string fn)
{
	m_filename=fn;
}

void TIMOSSourceWriter::source(Source::Abstract* S)
{
	m_source=S;
}

void TIMOSSourceWriter::packets(unsigned long long Np)
{
	m_packets=Np;
}

void TIMOSSourceWriter::write()
{
	ofstream os(m_filename.c_str());
	if (!os.good())
	{
		LOG_ERROR << "TIMOSSourceWriter::write() failed to open file " << m_filename << " for writing" << endl;
		return;
	}

	// Count total number of sources
	CountVisitor CV;
	CV.visit(m_source);
	os << CV.count() << endl;

	// Visit them all and print them out
	SourceVisitor SV(os,m_packets);
	SV.visit(m_source);
}
