/*
 * TIMOSMaterialWriter.hpp
 *
 *  Created on: Sep 26, 2017
 *      Author: jcassidy
 */

#include <string>

class MaterialSet;

class TIMOSMaterialWriter
{
public:

	TIMOSMaterialWriter();
	~TIMOSMaterialWriter();

	void filename(std::string fn);
	void materials(MaterialSet* MS);

	void write();

private:

	unsigned m_precision=6;
	MaterialSet*	m_materials=nullptr;
	std::string		m_filename;
};




