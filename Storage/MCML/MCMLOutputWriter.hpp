/*
 * MCMLOutputWriter.hpp
 *
 *  Created on: Jun 22, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MCML_MCMLOUTPUTWRITER_HPP_
#define STORAGE_MCML_MCMLOUTPUTWRITER_HPP_

class MCMLOutputWriter
{
public:
	MCMLOutputWriter();
	~MCMLOutputWriter();

	void filename(std::string fn);

	void write();
};



#endif /* STORAGE_MCML_MCMLOUTPUTWRITER_HPP_ */
