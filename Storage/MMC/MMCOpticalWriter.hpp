/*
 * MMCOpticalWriter.hpp
 *
 *  Created on: Sep 13, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCOPTICALWRITER_HPP_
#define STORAGE_MMC_MMCOPTICALWRITER_HPP_

#include <string>

class MaterialSet;

/** Writes optical properties to an MMC file
 *
 * Format
 * ======
 *
 * 1 <# lines>
 * Each line: <material ID> <mu_a> <mu_s> <g> <n>
 *
 *
 * Conventionally, the filename will be prop_<casename>.dat
 *
 * Drops the exterior material 0. ??Assumed by MMC to be air??
 */

class MMCOpticalWriter
{
public:
	MMCOpticalWriter();
	virtual ~MMCOpticalWriter();

	void filename(std::string fn);
	void materials(MaterialSet* M);

	void write();

private:
	std::string		m_filename;
	MaterialSet*	m_materials=nullptr;
};



#endif /* STORAGE_MMC_MMCOPTICALWRITER_HPP_ */
