/*
 * MMCTextMeshReader.hpp
 *
 *  Created on: Sep 19, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCTEXTMESHREADER_HPP_
#define STORAGE_MMC_MMCTEXTMESHREADER_HPP_

class TetraMeshBuilder;
class PTTetraMeshBuilder;
class TetraMesh;

#include <string>

class MMCTextMeshReader
{
public:
	MMCTextMeshReader();
	virtual ~MMCTextMeshReader();

	void prefix(std::string pfx);
	void path(std::string path);

	void read();

	TetraMeshBuilder*		builder();

	TetraMesh*				mesh();

private:
	std::string				m_path;
	std::string				m_fileprefix;
	PTTetraMeshBuilder*		m_builder=nullptr;
};

#endif /* STORAGE_MMC_MMCTEXTMESHREADER_HPP_ */
