/*
 * MMCOpticalWriter.cpp
 *
 *  Created on: Sep 13, 2017
 *      Author: jcassidy
 */

#include "MMCOpticalWriter.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

using namespace std;

MMCOpticalWriter::MMCOpticalWriter()
{

}

MMCOpticalWriter::~MMCOpticalWriter()
{

}

void MMCOpticalWriter::filename(string fn)
{
	m_filename=fn;
}

void MMCOpticalWriter::materials(MaterialSet* M)
{
	m_materials=M;
}

void MMCOpticalWriter::write()
{
	ofstream os(m_filename.c_str());

	unsigned Nm=0;

	if (!m_materials)
	{
		LOG_ERROR << "MMCOpticalWriter::write() failed for lack of materials" << endl;
	}
	else if ((Nm = m_materials->size()) == 0)
	{
		LOG_ERROR << "MMCOpticalWriter::write() failed due to empty material set" << endl;
	}
	else
	{
		os << 1 << ' ' << Nm-1 << endl;
		for(unsigned i=1;i<Nm;++i)
		{
			Material* m = m_materials->get(i);
			if (m)
			{
				os << setw(2) << i << ' ' << m->absorptionCoeff() << ' ' << m->scatteringCoeff() << ' ' << m->anisotropy() << ' ' << m->refractiveIndex() << endl;
			}
			else
			{
				LOG_ERROR << "MMCOpticalWriter::write() encountered null material" << endl;
			}
		}
	}
}


