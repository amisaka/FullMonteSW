/*
 * MMCJSONWriter.hpp
 *
 *  Created on: Sep 18, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_MMC_MMCJSONWRITER_HPP_
#define STORAGE_MMC_MMCJSONWRITER_HPP_

#include <string>
#include <array>

class MMCJSONWriter
{
public:
	MMCJSONWriter();
	virtual ~MMCJSONWriter();

	void filename(std::string fn);

	void scriptfilename(std::string fn);

	void meshfilename(std::string fn);
	void packets(unsigned long long N);
	void initelem(unsigned t);
	void seed(unsigned i);
	void sessionname(std::string fn);
	void mmccommand(std::string path);
	void timestep(float t0,float t1,float dt);
	void srcpos(std::array<float,3> p);
	void direction(std::array<float,3> d);

	void write() const;
	void writeCmdLine() const;

private:
	std::string 		m_filename;
	std::string			m_scriptfilename;
	std::string			m_meshfilename;
	unsigned long long 	m_packets=0ULL;
	unsigned			m_initelem=0U;
	unsigned			m_seed=1U;
	std::string			m_sessionname;
	std::string			m_mmccommand{"mmc_sfmt"};
	float				m_t0=0.0f,m_t1=1.0f,m_dt=1.0f;
	std::array<float,3>	m_srcpos{{0.0f,0.0f,0.0f}};
	std::array<float,3>	m_direction{{0.0f,0.0f,1.0f}};
};



#endif /* STORAGE_MMC_MMCJSONWRITER_HPP_ */
