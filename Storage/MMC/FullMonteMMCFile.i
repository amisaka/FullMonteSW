
#if defined(SWIGTCL)
%module FullMonteMMCFileTCL
#elif defined(SWIGPYTHON)
%module MMCFile
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

#if defined(SWIGTCL)
	%typemap(in) std::array<float,3>
	{
		std::stringstream ss(Tcl_GetString($input));
		std::array<float,3> a;
		ss >> a[0] >> a[1] >> a[2];
		$1=a;
	}
#elif defined(SWIGPYTHON)
	%typemap(in) std::array<float,3> (std::array<float,3> temp)
	{
		temp = {0, 0, 0};
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "fff", temp[0], temp[1], temp[2])) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				SWIG_fail;
			}	
		$1 = temp;
		} 
		else 
		{
		PyErr_SetString(PyExc_TypeError, "expected a tuple.");
		SWIG_fail;
		}

	}
#else
	#warning Requested wrapping language not supported
#endif

%include "std_string.i"

%{
#include <sstream>
#include "MMCMeshWriter.hpp"
#include "MMCOpticalWriter.hpp"
#include "MMCResultReader.hpp"
#include "MMCJSONWriter.hpp"
#include "MMCTextMeshReader.hpp"
%}

%include "MMCMeshWriter.hpp"
%include "MMCOpticalWriter.hpp"
%include "MMCResultReader.hpp"
%include "MMCJSONWriter.hpp"
%include "MMCTextMeshReader.hpp"