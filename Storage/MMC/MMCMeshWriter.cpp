/*
 * MMCMeshWriter.cpp
 *
 *  Created on: Sep 13, 2017
 *      Author: jcassidy
 */

#include "MMCMeshWriter.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

MMCMeshWriter::MMCMeshWriter()
{

}

MMCMeshWriter::~MMCMeshWriter()
{

}

void MMCMeshWriter::filename(const std::string fn)
{
	m_filename=fn;
}

void MMCMeshWriter::mesh(TetraMesh* M)
{
	m_mesh=M;
}

void MMCMeshWriter::write()
{
	if (!m_mesh) {
		LOG_ERROR << "MMCMeshWriter::write() failed for lack of input mesh" << endl;
	}

	writeTetrahedra("elem_" + m_filename);
	writeVolumes("velem_" + m_filename);
	writeFaces("facenb_" + m_filename);
	writePoints("node_" + m_filename);

}

void MMCMeshWriter::writeTetrahedra(std::string fn)
{
	ofstream os(fn.c_str());

	unsigned Nt=0;

	if ((Nt=m_mesh->tetraCells()->size()) == 0)
	{
		LOG_ERROR << "MMCMeshWriter::writeTetrahedra(fn) failed due to empty input mesh - no tetras" << endl;
	}
	else
	{
		os << 1 << " " << Nt-1 << endl;

		for(unsigned i=1;i<Nt;++i)
		{
			os << setw(6) << i << ' ';

			TetraByPointID IDps = m_mesh->tetraCells()->get(i);

			for(unsigned j : { 0,1,3,2 })
				os << ' ' << setw(6) << IDps[j];
			os << ' ' << setw(2) << (m_mesh->regions() ? m_mesh->regions()->get(i) : 1U) << endl;
		}
	}
}

void MMCMeshWriter::writeFaces(std::string fn)
{
	ofstream os(fn.c_str());

	unsigned Nt = 0;

	if (!m_mesh->tetraFaceLinks())
	{
		LOG_ERROR << "MMCMeshWriter::writeFaces(fn) failed due to lack of tetra-face links in mesh" << endl;
	}
	else if ((Nt = m_mesh->tetraFaceLinks()->size()) == 0)
	{
		LOG_ERROR << "MMCMeshWriter::writeFaces(fn) failed due to empty tetra-face links in mesh" << endl;
	}
	else
	{
		os << 1 << ' ' << Nt-1 << endl;

		for(unsigned i=1;i<Nt;++i)
		{
			array<TetraFaceLink,4> links = m_mesh->tetraFaceLinks()->get(i);

			//// MMC uses a clockwise ordering while FullMonte uses CCW so need to permute the adjacent tetras
			//// so that they match
			for(unsigned j : { 1, 0, 2, 3 })
				os << setw(6) << get(id,*m_mesh,links[j].tetraID) << ' ';
			os << endl;
		}
	}
}

void MMCMeshWriter::writePoints(std::string fn)
{
	ofstream os(fn.c_str());

	unsigned Np = m_mesh->points()->size();

	if (Np == 0)
	{
		LOG_ERROR << "MMCMeshWriter::writePoints(fn) failed for lack of points" << endl;
	}
	else
	{
		os << 1 << ' ' << Np-1 << endl;

		for(unsigned i=1;i<Np;++i)
		{
			os << setw(6) << i;

			Point<3,double> P = m_mesh->points()->get(i);

			for(unsigned j=0;j<3;++j)
				os << ' ' << P[j];
			os << endl;
		}
	}
}

void MMCMeshWriter::writeVolumes(std::string fn)
{
	ofstream os(fn.c_str());

	unsigned Nt=0;

	if ((Nt=m_mesh->tetraCells()->size()) == 0)
	{
		LOG_ERROR << "MMCMeshWriter::writeVolumes(fn) failed due to empty input mesh - no tetras" << endl;
	}
	else
	{
		os << 1 << ' ' << Nt-1 << endl;

		auto T = m_mesh->tetras();

		auto it = begin(T);
		it++;

		for(unsigned i=1;it != end(T) && i<Nt;++i,++it)
			os << setw(6) << i << ' ' << get(volume,*m_mesh,*it) << endl;
	}
}

