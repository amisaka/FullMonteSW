/*
 * MMCOpticalReader.cpp
 *
 *  Created on: Nov 21, 2017
 *      Author: jcassidy
 */

#include "MMCOpticalReader.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <fstream>
#include <iostream>
using namespace std;


MMCOpticalReader::MMCOpticalReader()
{

}

MMCOpticalReader::~MMCOpticalReader()
{

}

void MMCOpticalReader::filename(string fn)
{
	m_filename=fn;
}

void MMCOpticalReader::read()
{
	ifstream is(m_filename);

	if (!is.good())
	{
		LOG_ERROR << "MMCOpticalReader::read() failed to open " << m_filename << endl;
		return;
	}

	unsigned startIndex;
	unsigned Nm;

	is >> startIndex >> Nm;

	delete m_materials;
	m_materials = new MaterialSet();

	Material* air = new Material();
	air->refractiveIndex(1.0f);
	air->anisotropy(0.0f);
	air->scatteringCoeff(0.0f);
	air->absorptionCoeff(0.0f);

	m_materials->exterior(air);

	for(unsigned i=0;i<Nm;++i)
	{
		unsigned idx;
		float muA, muS, g, n;
		is >> idx >> muA >> muS >> g >> n;

		Material* mat = new Material();
		mat->refractiveIndex(n);
		mat->anisotropy(g);
		mat->scatteringCoeff(muS);
		mat->absorptionCoeff(muA);

		m_materials->append(mat);
	}
}

MaterialSet* MMCOpticalReader::materials() const
{
	return m_materials->clone();
}

