/*
 * COMSOLWriter.hpp
 *
 *  Created on: Oct 16, 2017
 *      Author: jcassidy
 */

#include <string>

class TetraMesh;

class COMSOLWriter
{
public:
	COMSOLWriter();
	virtual ~COMSOLWriter();

	void filename(std::string fn);

	void mesh(TetraMesh* M);

	void write();

private:
	std::string		m_filename;
	TetraMesh*		m_mesh=nullptr;

	unsigned		m_precision=9;
	unsigned		m_width=6;
	unsigned 		m_tetraIDWidth=7;
};

