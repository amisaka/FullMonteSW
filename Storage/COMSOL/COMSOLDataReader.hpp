/*
 * COMSOLDataReader.hpp
 *
 *  Created on: Apr 25, 2018
 *      Author: jcassidy
 */

#ifndef STORAGE_COMSOL_COMSOLDATAREADER_HPP_
#define STORAGE_COMSOL_COMSOLDATAREADER_HPP_

#include <string>
#include <array>
#include <vector>

class Points;
template<typename T>class SpatialMap;

/** Reads COMSOL point data
 *
 */

class COMSOLDataReader
{
public:
	COMSOLDataReader();
	~COMSOLDataReader();

	void 				filename(std::string s);
	void 				read();
	void				setZeroOffset(unsigned toggle);

	Points*				points() const;
	SpatialMap<float>*	data() const;

private:
	std::string 						m_filename;

	std::vector<std::array<double,3>>	m_points;
	std::vector<float>					m_data;

	unsigned							m_zeroOffset=0;
};



#endif /* STORAGE_COMSOL_COMSOLDATAREADER_HPP_ */
