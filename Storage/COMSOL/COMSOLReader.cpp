/*
 * COMSOLReader.cpp
 *
 *  Created on: Oct 16, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <FullMonteSW/Geometry/PTTetraMeshBuilder.hpp>

#include <FullMonteSW/Storage/Common/CommentFilter.hpp>
#include <FullMonteSW/Storage/Common/FlattenWhitespace.hpp>

#include <fstream>
#include <iostream>
#include "COMSOLReader.hpp"

using namespace std;

COMSOLReader::COMSOLReader()
{

}

COMSOLReader::~COMSOLReader()
{

}

void COMSOLReader::filename(string fn)
{
	m_filename=fn;
}

void COMSOLReader::read()
{
	m_points.clear();
	m_tetras.clear();
	m_faces.clear();
	m_edges.clear();
	m_verts.clear();

	boost::iostreams::basic_file_source<char> is(m_filename);

	single_delim_comment_filter CF;
	flatten_whitespace fw;

	boost::iostreams::filtering_stream<boost::iostreams::input,char> f;

	f.push(fw);
	f.push(CF);
	f.push(is);

	if (!f.good()) {
		LOG_ERROR << "Failed to read " << m_filename << endl;
    }


	unsigned zero;
	unsigned one;
	unsigned major, minor;
	unsigned sdim;
	unsigned version;
	unsigned tagCount;
	unsigned typeCount;

	unsigned indexStart;


	f >> major >> minor >> tagCount;

	LOG_DEBUG << "Opened " << m_filename << " which has " << tagCount << " tags " << endl;

	for(unsigned i=0;i<tagCount;++i)
	{
		unsigned len;
		string tag;
		f >> len;
		f >> tag;
	}

	f >> typeCount;

	for(unsigned i=0;i<typeCount;++i)
	{
		unsigned len;
		string type;
		f >> len;
		f >> type;
	}

	f >> zero >> zero >> one;

	unsigned len;
	string cl;
	f >> len >> cl;

	f >> version;
	f >> sdim;

	unsigned Np;

	f >> Np;

	f >> indexStart;

	LOG_DEBUG << Np << " points starting at index " << indexStart << endl;

	if (f.fail())
	{
		LOG_ERROR << "Failed to read!" << endl;
		return;
	}

	m_points.resize(Np);

	for(unsigned i=0;i<Np;++i)
		for(unsigned j=0;j<3;++j)
			f >> m_points[i][j];

	unsigned elementTypeCount;

	f >> elementTypeCount;

	for(unsigned i=0;i<elementTypeCount;++i)
	{
		unsigned charCount;
		string elType;
		unsigned nodesPerElement;
		unsigned nElement;

		f >> charCount >> elType;
		f >> nodesPerElement;
		f >> nElement;

		if (f.fail())
			break;

		LOG_DEBUG << "Section with " << nElement << " elements of type '" << elType << "'" << endl;

		if (elType == "vtx")
		{
			// 2 nodes/elem
			m_verts.resize(nElement);
			for(unsigned j=0;j<nElement;++j)
				f >> m_verts[j];

		}
		else if (elType == "edg")
		{
			m_edges.resize(nElement);
			for(unsigned j=0;j<nElement;++j)
				for(unsigned k=0;k<2;++k)
					f >> m_edges[j][k];
		}
		else if (elType == "tri")
		{
			m_faces.resize(nElement);
			for(unsigned j=0;j<nElement;++j)
				for(unsigned k=0;k<3;++k)
					f >> m_faces[j][k];

		}
		else if (elType == "tet")
		{
			m_tetras.resize(nElement);
			for(unsigned j=0;j<nElement;++j)
				for(unsigned k=0;k<4;++k)
					f >> m_tetras[j][k];
		}

		unsigned nGeometricEntityIndices;

		f >> nGeometricEntityIndices;

		if (f.fail())
			break;

		LOG_DEBUG << "  " << nGeometricEntityIndices << " entity indices" << endl;

		vector<unsigned> indices(nGeometricEntityIndices,0U);
		for(unsigned j=0;j<nGeometricEntityIndices;++j)
			f >> indices[j];

		if (elType == "tet")
			m_tetRegions=move(indices);
		else if (elType == "tri")
			m_faceRegions=move(indices);
		else if (elType == "edg")
			m_edgeRegions=move(indices);
		else if (elType == "vtx")
			m_vertRegions=move(indices);
	}

	m_tetRegionsOK = m_tetRegions.size() == m_tetras.size();


}

TetraMesh* COMSOLReader::mesh()
{
	if(m_builder)
		delete m_builder;
	m_builder = new PTTetraMeshBuilder();

	// Both variables are currently hard coded to true because we are inserting an additional zero point and 
	// zero tetra regardless if they have existed in the input mesh beforehand.
	// If for some reason, it turns out we have to check for the zero point and zero tetra again, just uncomment the
	// variable assignments after the true statements below.
	bool insertZeroPoint = true;
	bool insertZeroTetra = true;

	if (insertZeroPoint) {
		LOG_DEBUG << "\tFullMonte will insert an artificial zero point into the mesh in case the existing zero point is already referenced by a tetra" << endl
			        << "\t\tThis will not change the output mesh from FullMonte." << endl
			        << "\t\tThe additional zero point is internally required by FullMonte" << endl << endl;
    }
    
    if (insertZeroTetra) {
		LOG_DEBUG << "\tFullMonte will insert a zero cell (tetra) in case the existing zero cell (tetra) has nonzero point references." << endl
			        << "\t\tInternally, this means that the mesh FullMonte is operating on has an additional tetra." << endl
			        << "\t\tThis additional zero cell (tetra) will be removed when writing the output to a file." << endl << endl;
    }

	m_builder->setNumberOfPoints(m_points.size()+insertZeroPoint);

	if (insertZeroPoint)
	{
		Point<3,double> P(0,0,0);
		// This code sets the zero point to the coordinates of the first point of the mesh
		// I think the coordinates do not matter for the zero point in FullMonte but in the
		// output method, we would get odd bounding boxes when visualizing the mesh with the zero point inserted
		// Since we aren't writing the zero points to the output mesh, it is currently commented out
		//m_ug->GetPoint(0,P.data());
		m_builder->setPoint(0,P);
	}

	for(const auto p : m_points | boost::adaptors::indexed(0U))
	{
		m_builder->setPoint(p.index()+insertZeroTetra,p.value());
	}

	m_builder->setNumberOfTetras(m_tetras.size()+insertZeroTetra);

	if (m_tetRegions.size() != m_tetras.size())
	{
		LOG_WARNING << "WARNING! Tetra region data size (" << m_tetRegions.size() << ") does not match number of tetras (" << m_tetras.size() << ")" << endl
		            << "  All tetras without region code assigned will be assigned to 0 (mesh exterior). This is almost certainly not what you want." << endl
		            << "  When exporting the mesh from COMSOL, you must include geometric entity indices." << endl;
	}

	// If insertZeroTetra = true; (which it currently is hardcoded to be), we will insert
	// a zero tetra into the mesh where its 4 corners are assigned to the same point, essentially
	//  reating a tetra with 0 volume. It's material ID is also set to 0.
	if (insertZeroTetra)
		m_builder->setTetra(0,TetraByPointID{{0U,0U,0U,0U}},0U);

	for(unsigned i=0;i<m_tetras.size();++i)
	{
		// check if tetra ID exceeds available region codes
		TetraByPointID pointIDs = {{m_tetras[i][0]+insertZeroTetra,m_tetras[i][1]+insertZeroTetra,m_tetras[i][2]+insertZeroTetra,m_tetras[i][3]+insertZeroTetra}};
		m_builder->setTetra(i+insertZeroTetra,pointIDs, i<m_tetRegions.size() ? m_tetRegions[i] : 0U);
	}

	m_builder->build();

	return m_builder->mesh();

}


#ifdef VTK_FOUND


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkUnstructuredGrid.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnsignedShortArray.h>
#include <vtkCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>

void COMSOLReader::vtkExport(string fn)
{
	vtkUnstructuredGridWriter* 	W  = vtkUnstructuredGridWriter::New();
	vtkUnstructuredGrid* 		ug = vtkUnstructuredGrid::New();

	W->SetFileName(fn.c_str());
	W->SetInputData(ug);


	//// Points

	vtkPoints* P = vtkPoints::New();
	P->SetNumberOfPoints(m_points.size());

	for(unsigned i=0;i<m_points.size();++i)
		P->SetPoint(i,m_points[i].data());

	ug->SetPoints(P);

	//// Cells

	vtkCellArray* ca = vtkCellArray::New();

	vtkIdTypeArray* ids = vtkIdTypeArray::New();
	ids->SetNumberOfTuples(5*m_tetras.size());

	for(unsigned i=0;i<m_tetras.size();++i)
	{
		ids->SetTuple1(5*i,4U);
		for(unsigned j=0;j<4;++j)
			ids->SetTuple1(5*i+j+1, m_tetras[i][j]);
	}

	ca->SetCells(m_tetras.size(),ids);
	ug->SetCells(VTK_TETRA, ca);


	//// Region tags

	vtkUnsignedShortArray* R = vtkUnsignedShortArray::New();
	R->SetNumberOfTuples(m_tetRegions.size());
	for(unsigned i=0;i<m_tetRegions.size(); ++i)
		R->SetValue(i,m_tetRegions[i]);

	ug->GetCellData()->AddArray(R);


	W->Update();
	W->Delete();

	// delete our reference to the data (ug will keep its own)
	P->Delete();
	ca->Delete();
	R->Delete();
}
#endif
