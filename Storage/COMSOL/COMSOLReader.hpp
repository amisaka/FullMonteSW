/*
 * COMSOLReader.hpp
 *
 *  Created on: Oct 16, 2017
 *      Author: jcassidy
 */

#ifndef STORAGE_COMSOL_COMSOLREADER_HPP_
#define STORAGE_COMSOL_COMSOLREADER_HPP_

#include <string>
#include <vector>
#include <array>

#include <FullMonteSW/Config.h>

#include <FullMonteSW/Geometry/PTTetraMeshBuilder.hpp>

class TetraMesh;

class COMSOLReader
{
public:
	COMSOLReader();
	virtual ~COMSOLReader();

	void 		filename(std::string fn);

	void 		read();
	TetraMesh* 	mesh();

	bool 		tetRegionsReadOK() const { return m_tetRegionsOK; }

#ifdef VTK_FOUND
	void vtkExport(std::string fn);
#endif

private:
	std::string							m_filename;

	PTTetraMeshBuilder*					m_builder=nullptr;

	std::vector<std::array<double,3>>	m_points;
	std::vector<std::array<unsigned,4>>	m_tetras;
	std::vector<std::array<unsigned,3>> m_faces;
	std::vector<std::array<unsigned,2>> m_edges;
	std::vector<unsigned>				m_verts;

	std::vector<unsigned>				m_tetRegions;
	std::vector<unsigned>				m_faceRegions;
	std::vector<unsigned>				m_edgeRegions;
	std::vector<unsigned>				m_vertRegions;

	bool 								m_tetRegionsOK=false;
};



#endif /* STORAGE_COMSOL_COMSOLREADER_HPP_ */
