PROJECT(FullMonte)
CMAKE_MINIMUM_REQUIRED(VERSION 3.12.0)

#############################Enalbe Color Coded Messages######################
IF(NOT WIN32)                                                               ##
  STRING(ASCII 27 Esc)                                                      ##
  SET(ColourReset "${Esc}[m")                                               ##
  SET(ColourBold  "${Esc}[1m")                                              ##
  SET(Red         "${Esc}[31m")                                             ##
  SET(Green       "${Esc}[32m")                                             ##
  SET(Yellow      "${Esc}[33m")                                             ##
  SET(Blue        "${Esc}[34m")                                             ##
  SET(Magenta     "${Esc}[35m")                                             ##
  SET(Cyan        "${Esc}[36m")                                             ##
  SET(White       "${Esc}[37m")                                             ##
  SET(BoldRed     "${Esc}[1;31m")                                           ##
  SET(BoldGreen   "${Esc}[1;32m")                                           ##
  SET(BoldYellow  "${Esc}[1;33m")                                           ##
  SET(BoldBlue    "${Esc}[1;34m")                                           ##
  SET(BoldMagenta "${Esc}[1;35m")                                           ##
  SET(BoldCyan    "${Esc}[1;36m")                                           ##
  SET(BoldWhite   "${Esc}[1;37m")                                           ##
ENDIF()                                                                     ##
##############################################################################

SET(FULLMONTE_VERSION_MAJOR 1)
SET(FULLMONTE_VERSION_MINOR 0)

# languages for the project
ENABLE_LANGUAGE(CXX)
ENABLE_LANGUAGE(C)
IF(BUILD_CUDAACCEL)
    ENABLE_LANGUAGE(CUDA)
ENDIF()

SET(CMAKE_INSTALL_SO_NO_EXE OFF)

INCLUDE(ExternalProject)

# Add our own CMake dir to the end of the path
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake ${CMAKE_SOURCE_DIR}/External/CAPICMake ${CMAKE_SOURCE_DIR}/External/BlueCMake )
LIST(APPEND CMAKE_PREFIX_PATH ${CMAKE_BINARY_DIR}/cmake)

MESSAGE("\ 
${BoldMagenta}\ 
-----------------------\ 
Configuring FullMonte |\ 
-----------------------\  
${ColourReset}")

# Turning off ABI warnings and notes.
# This is safe since all of our cross-libraries are built together (i.e. we don't actually need dynamic linking, but we do it anyways).
# Source: https://stackoverflow.com/questions/52020305/what-exactly-does-gccs-wpsabi-option-do-what-are-the-implications-of-supressi
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-psabi")

IF(DEFINED WRAP_PYTHON)
    MESSAGE("'WRAP_PYTHON=${WRAP_PYTHON}' provided by user.")
ELSE()
    SET(WRAP_PYTHON ON)
    MESSAGE("WRAP_PYTHON not specified. Default setting is 'WRAP_PYTHON=${WRAP_PYTHON}'")
ENDIF()

IF(DEFINED WRAP_TCL)
    MESSAGE("'WRAP_TCL=${WRAP_TCL}' provided by user.")
ELSE()
    SET(WRAP_TCL ON)
    MESSAGE("WRAP_TCL not specified. Default setting is 'WRAP_TCL=${WRAP_TCL}'")
ENDIF()

IF(DEFINED WRAP_VTK)
    MESSAGE("'WRAP_VTK=${WRAP_VTK}' provided by user.")
ELSE()
    SET(WRAP_VTK OFF)
    MESSAGE("WRAP_VTK not specified. Default setting is 'WRAP_VTK=${WRAP_VTK}'")
ENDIF()

IF(DEFINED CMAKE_EXPORT_COMPILE_COMMANDS)
    MESSAGE("'CMAKE_EXPORT_COMPILE_COMMANDS=${CMAKE_EXPORT_COMPILE_COMMANDS}' provided by user.")
ELSE()
    MESSAGE("Creating compile_commands.json file for IDE usage. You can disable the creation by adding '-DCMAKE_EXPORT_COMPILE_COMMANDS=OFF' to the cmake call \
    ")
    SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)
ENDIF()
MESSAGE("")

IF(DEFINED BUILD_CUDAACCEL)
    MESSAGE("'BUILD_CUDAACCEL=${BUILD_CUDAACCEL}' provided by user.")
ELSE()
    SET(BUILD_CUDAACCEL OFF)
    MESSAGE("BUILD_CUDAACCEL not specified. Default setting is 'BUILD_CUDAACCEL=${BUILD_CUDAACCEL}'")
ENDIF()

IF(DEFINED BUILD_FPGACLACCEL)
    MESSAGE("'BUILD_FPGACLACCEL=${BUILD_FPGACLACCEL}' provided by user.")
ELSE()
    SET(BUILD_CUDAACCEL OFF)
    MESSAGE("BUILD_FPGACLACCEL not specified. Default setting is 'BUILD_FPGACLACCEL=${BUILD_FPGACLACCEL}'")
ENDIF()

### Architecture
IF(DEFINED ARCH)
    MESSAGE("'ARCH=${ARCH}' provided by user. Not detecting system architecture.")
ELSE()
    # try inspecting /proc/cpuinfo (should work on Linux, but not Mac)
    MESSAGE("ARCH not specified. Searching /proc/cpuinfo")
    EXECUTE_PROCESS(
        COMMAND cat /proc/cpuinfo
        RESULT_VARIABLE CPUINFO_EXITCODE
        OUTPUT_VARIABLE CPUINFO)

    IF("${CPUINFO_EXITCODE}" EQUAL 0)
        STRING(FIND "${CPUINFO}" avx2       AVX2_POSITION)
        STRING(FIND "${CPUINFO}" avx        AVX_POSITION)
        STRING(FIND "${CPUINFO}" POWER8E    POWER8E_POSITION)

        IF(AVX2_POSITION)
            MESSAGE("  AVX2 found")
            SET(ARCH AVX2)
        ELSEIF(AVX_POSITION)
            MESSAGE("  AVX found")
            SET(ARCH AVX)
        ELSEIF(POWER8E_POSITION)
            MESSAGE("  POWER8E found")
            SET(ARCH P8)
            SET(NonSSE ON)
        ELSE()
            MESSAGE(WARNING "  ${Yellow}No valid architecture found (AVX, AVX2, POWER8E)${ColourReset}")
        ENDIF()
    ELSE()
        MESSAGE("  ${Red}FAILED${ColourReset}")
    ENDIF()
ENDIF()

IF(NOT ARCH)
    MESSAGE("Searching cpuid")

    EXECUTE_PROCESS(
        COMMAND cpuid
        RESULT_VARIABLE CPUID_EXITCODE
        OUTPUT_VARIABLE CPUID)

    IF("${CPUID_EXITCODE}" EQUAL 0)
        STRING(FIND "${CPUID}" avx2 AVX2_POSITION)
        STRING(FIND "${CPUID}" avx  AVX_POSITON)
        # cpuid not installed on Power8 Ubuntu by default

        IF(AVX2_POSITION)
            SET(ARCH AVX2)
            MESSAGE("  ${Green}AVX2 found${ColourReset}")
        ELSEIF(AVX_POSITION)
            SET(ARCH AVX)
            MESSAGE("  ${Green}AVX found${ColourReset}")
        ELSE()
            MESSAGE("  ${Yellow}Neither AVX nor AVX2 found${ColourReset}")
        ENDIF()
    ENDIF()
ENDIF()

IF(NOT ARCH)
    MESSAGE(SEND_ERROR "${Red}Could not determine CPU type. Please specify ARCH=AVX|AVX2|P8.${ColourReset}")
ELSE()
    SET(ARCH ${ARCH} CACHE STRING "Architecture flag AVX|AVX2|P8")
ENDIF()

MESSAGE("")
# Check if the user wants to run the hardware on the Power8 or in siulation and set the variables accordingly
IF("${ARCH}" STREQUAL "P8")
    IF("${USE_HW}" STREQUAL "ON")
        MESSAGE("Setting up environment to run FullMonte with hardware acceleration \ 
        ")
        SET(NonSSE ON)
        SET(CAPI_SYN ON)
        SET(CAPI_SIM OFF)
        SET(USE_Bluespec OFF)
        SET(USE_VSIM OFF)
        SET(USE_BlueLink ON)
        SET(USE_BDPIDevice ON)
        SET(USE_Quartus OFF)
    ELSE()
        MESSAGE("Setting up environment to run FullMonte on the P8 only in software \ 
        ")
        SET(NonSSE ON)
        SET(CAPI_SYN OFF)
        SET(CAPI_SIM OFF)
        SET(USE_Bluespec OFF)
        SET(USE_VSIM OFF)
        SET(USE_BlueLink OFF)
        SET(USE_BDPIDevice OFF)
        SET(USE_Quartus OFF)
    ENDIF()
ELSE()
    IF("${USE_HW}" STREQUAL "ON")
        MESSAGE("Setting up environment to run FullMonte with simulated hardware acceleration")
        SET(NonSSE OFF)
        SET(CAPI_SYN OFF)
        SET(CAPI_SIM ON)
        SET(USE_Bluespec ON)
        SET(USE_VSIM ON)
        SET(USE_BlueLink ON)
        SET(USE_BDPIDevice ON)
        SET(USE_Quartus ON)
        IF(VSIM_DIR)
            MESSAGE("  HW Simulator directory VSIM_DIR set to: ${VSIM_DIR}")
        ELSE()
            MESSAGE(SEND_ERROR "  ${Red}You need to specify with VSIM_DIR in which directory your hardware simulator is located to use hardware simulation. Specify the variable with -DVSIM_DIR when calling cmake.${ColourReset}")
        ENDIF()
        IF(Quartus_DIR)
            MESSAGE("  Quartus directory Quartus_DIR set to: ${Quartus_DIR} \ 
            ")
        ELSE()
            MESSAGE(SEND_ERROR "  ${Red}You need to specify with Quartus_DIR in which directory your hardware simulator is located to use hardware simulation. Specify the variable with -DQuartus_DIR when calling cmake.${ColourReset} \ 
            ")
        ENDIF()
    ELSE()             
        MESSAGE("Setting up environment to run Fullmonte only in software \ 
        ")
        SET(NonSSE OFF)
        SET(CAPI_SYN OFF)
        SET(CAPI_SIM OFF)
        SET(USE_Bluespec OFF)
        SET(USE_VSIM OFF)
        SET(USE_BlueLink OFF)
        SET(USE_BDPIDevice OFF)
        SET(USE_Quartus OFF)
    ENDIF()
ENDIF()

## Boost setup
FIND_PACKAGE(Boost 1.58.0 REQUIRED COMPONENTS serialization system program_options timer filesystem unit_test_framework log log_setup)
ADD_DEFINITIONS(-DBOOST_SYSTEM_NO_DEPRECATED=1 -DBOOST_LOG_DYN_LINK)
INCLUDE(cmake/BoostUnitTest.cmake)
INCLUDE(cmake/FullMonteUtils.cmake)

INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
LINK_DIRECTORIES(${Boost_LIBRARY_DIRS})

INCLUDE_DIRECTORIES(${CMAKE_BINARY_DIR})

## Include dirs for FullMonte
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/..)



SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
SET(CMAKE_INCLUDE_CURRENT_DIR ON)

## Compiler setup
SET(CMAKE_CXX_STANDARD 11)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

SET(RUNTIME_LINKER_PATH_VARNAME LD_LIBRARY_PATH)

IF(APPLE)
    ## Require .so extension (Mac likes to make it .dylib, causing a lot of the TCL scripts to bomb)
    SET(CMAKE_SHARED_LIBRARY_SUFFIX ".so")
    
    
    # Apple uses DYLD_LIBRARY_PATH for runtime link path, whereas most others use LD_LIBRARY_PATH
    SET(RUNTIME_LINKER_PATH_VARNAME DYLD_LIBRARY_PATH)


    MESSAGE("Using Apple rpath settings")
    
    # Prepends @rpath/ to install names of generated libraries
    SET(CMAKE_MACOSX_RPATH ON)
ENDIF()

# use, i.e. don't skip the full RPATH for the build tree
SET(CMAKE_SKIP_BUILD_RPATH 				FALSE)

# when building, don't use the install RPATH already
# (but later on when installing)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH 		FALSE)
SET(CMAKE_INSTALL_RPATH 					"${CMAKE_INSTALL_PREFIX}/lib")

# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH 	TRUE)


# the RPATH to be used when installing, but only if it's not a system directory
LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
IF("${isSystemDir}" STREQUAL "-1")
   SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
ENDIF("${isSystemDir}" STREQUAL "-1")

IF(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	ADD_DEFINITIONS(-DCMAKE_COMPILER_GCC)
ELSEIF(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	ADD_DEFINITIONS(-DCMAKE_COMPILER_CLANG)
    SET(${CMAKE_CXX_FLAGS} "${CMAKE_CXX_FLAGS} -pthreads")
ELSE()
ENDIF()

IF (CAPI_SYN OR CAPI_SIM)

	MESSAGE("")
# Compile steps for FullMonteHW support
    IF (CAPI_SYN) # HW Synthesis
        
        MESSAGE("Compiling libcxl for hardware execution...${Cyan}")
        # Clean Hardware libcxl.so and cxl.h
        EXECUTE_PROCESS(COMMAND make clean
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/libcxl" )
        # Make Hardware libcxl.so and cxl.h
        EXECUTE_PROCESS(COMMAND make
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/libcxl" )
        SET(CAPI_KERNEL_CXL_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/External/libcxl/include")
        SET(CAPI_SYN_LIBCXL_DIR "${CMAKE_SOURCE_DIR}/External/libcxl")
	MESSAGE("${ColourReset}...DONE / 
")
    ELSEIF (CAPI_SIM) # HW Simulation    

        MESSAGE("Compiling pslse and libcxl for hardware simulation...${Cyan}")
        # Clean afu_driver
        EXECUTE_PROCESS(COMMAND make clean PSLVER=8 VPI_USER_H_DIR=${VSIM_DIR}/include
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/afu_driver/src" )
        # Make afu_driver
        EXECUTE_PROCESS(COMMAND make PSLVER=8 VPI_USER_H_DIR=${VSIM_DIR}/include
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/afu_driver/src" )


        # Clean pslse
        EXECUTE_PROCESS(COMMAND make clean PSLVER=8
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/pslse" )
        # Make pslse
        EXECUTE_PROCESS(COMMAND make PSLVER=8
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/pslse" )

        # Clean libcxl
        EXECUTE_PROCESS(COMMAND make clean PSLVER=8
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/libcxl" )
        # Make libcxl
        EXECUTE_PROCESS(COMMAND make PSLVER=8
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/External/fm-pslse/libcxl" )
        SET(CAPI_KERNEL_CXL_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/External/fm-pslse/common/misc")
        SET(CAPI_SIM_LIBCXL_DIR "${CMAKE_SOURCE_DIR}/External/fm-pslse/libcxl")
        SET(CAPI_SOURCE_DIR "${CMAKE_SOURCE_DIR}/External/fm-pslse")
    	MESSAGE("${ColourReset}...DONE \ 
")
        FIND_PACKAGE(VSIM REQUIRED)
        IF(VSIM_FOUND)
            INCLUDE(${VSIM_USE_FILE})
        ENDIF()

    ELSE()

    ENDIF()
    
    FIND_PACKAGE(CAPI REQUIRED)
    INCLUDE(${CAPI_USE_FILE})
ENDIF()

# Include externals before testing to skip their tests
MESSAGE("${Blue}Entering subdirectory External${ColourReset}")
ADD_SUBDIRECTORY(External)

ENABLE_TESTING()

###### BDPIDevice CMake module                                                      
IF(USE_BDPIDevice)                         
	FIND_PACKAGE(BDPIDevice REQUIRED)                          
	INCLUDE(${BDPIDevice_USE_FILE})                        
ENDIF()                                
                                       
###### BDPIDevice CMake module                                 
IF(USE_BlueLink)                           
	FIND_PACKAGE(BlueLink REQUIRED)                        
	INCLUDE(${BlueLink_USE_FILE})                          
ENDIF()                                
                                               
IF(CAPI_SIM OR CAPI_SYN)                   
	FIND_PACKAGE(FullMonteHW REQUIRED)                     
	INCLUDE(${FullMonteHW_USE_FILE})                       
ENDIF()                                


###### Find other simulators (optional)
FIND_PACKAGE(TIMOS)
IF(TIMOS_FOUND)
    MESSAGE("Found TIMOS at ${TIMOS_EXECUTABLE}")
ELSE()
    MESSAGE("TIMOS executable was not found. This is optional, for comparison purposes only. FullMonte's regression test suite does NOT require it, and all targets will build OK without.")
ENDIF()

FIND_PACKAGE(MMC)
IF(MMC_FOUND)
    MESSAGE("Found MMC at ${MMC_EXECUTABLE}")
ELSE()
    MESSAGE("MMC executable was not found. This is optional, for comparison purposes only. FullMonte's regression test suite does NOT require it, and all targets will build OK without.")
ENDIF()
MESSAGE("")

IF("${ARCH}" STREQUAL "AVX2")
    IF(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
        # Apple provides some intrinsics that GCC does not (see source ssemath.h)
		OPTION(FULLMONTE_DISABLE_EXTRA_AVX2_DEFS "Disable extra AVX2 definitions" ON)
	ELSE()
		OPTION(FULLMONTE_DISABLE_EXTRA_AVX2_DEFS "Disable extra AVX2 definitions" OFF)
	ENDIF()

	IF(FULLMONTE_DISABLE_EXTRA_AVX2_DEFS)
        ADD_DEFINITIONS(-DAPPLE_AVX2)
    ENDIF()
    ADD_DEFINITIONS(-DUSE_AVX2 -DHAVE_AVX2)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx2 -mavx -msse4.1")
	MESSAGE("AVX2 compilation enabled")
ELSEIF("${ARCH}" STREQUAL "AVX")
    ADD_DEFINITIONS(-DUSE_AVX -DHAVE_AVX)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx -msse4.1")
	MESSAGE("AVX compilation enabled")
ELSEIF("${ARCH}" STREQUAL "P8")
    MESSAGE("Power8 compilation enabled")
ELSE()
	MESSAGE("Invalid ARCH string '${ARCH}'")
ENDIF()
	
###### FullMonte Language Wrapping
IF(WRAP_TCL OR WRAP_PYTHON)
    FIND_PACKAGE(SWIG REQUIRED VERSION 3.0)
    INCLUDE(${SWIG_USE_FILE})
    MESSAGE("SWIG Use file located at ${SWIG_USE_FILE}")
    SET(CMAKE_SWIG_FLAGS ${CMAKE_SWIG_FLAGS} -Wall)
    SET(SWIG_OUTFILE_DIR ${CMAKE_BINARY_DIR}/WrappersSWIG)
ENDIF()

###### Python Wrapping
OPTION(WRAP_PYTHON "Generate Python interface" ON)

IF(WRAP_PYTHON)
    FIND_PACKAGE(Python3 COMPONENTS Interpreter Development)
    IF(Python3_FOUND)
        MESSAGE("Python located at ${Python3_INCLUDE_DIRS} and ${Python3_LIBRARIES}")
    ELSE()
        MESSAGE(ERROR "${Red}: Could not find Python. Either provide hints or deselect WRAP_PYTHON${ColourReset}")
    ENDIF()
    INCLUDE_DIRECTORIES(${Python3_INCLUDE_DIRS})
    SET(CMAKE_SWIG_OUTDIR ${CMAKE_BINARY_DIR}/lib/fmpy)
    SET(DEFINE_WRAP_PYTHON true)
ELSE()
    SET(DEFINE_WRAP_PYTHON false)
ENDIF()
MESSAGE("")

###### TCL Wrapping
OPTION(WRAP_TCL "Generate TCL interface" ON)

IF(WRAP_TCL)    
	# Obviously Tcl is required for WRAP_TCL, but find will fail if it can't also locate Tk
    FIND_PACKAGE(TCL)
    IF(TCL_FOUND)
        MESSAGE("TCL located at ${TCL_INCLUDE_PATH} and ${TCL_LIBRARY}")
    ELSE()
        MESSAGE(ERROR "${Red}Could not find TCL. Either provide hints or deselect WRAP_TCL${ColourReset}")
    ENDIF()
    INCLUDE_DIRECTORIES(${TCL_INCLUDE_PATH})
    
	FIND_PACKAGE(TclStub)
	
	IF(TclStub_FOUND)
		ADD_DEFINITIONS(-DUSE_TCL_STUBS)
		SET(TCL_LINK_LIBRARY tclstubs)
	ELSE()
		MESSAGE("${Yellow}Could not find TCL Stubs. This is not required to run FullMonte${ColourReset}")
		SET(TCL_LINK_LIBRARY ${TCL_LIBRARY})
    ENDIF()
    
    SET(DEFINE_WRAP_TCL true)
ELSE()
    SET(DEFINE_WRAP_TCL false)
ENDIF()
MESSAGE("")

###### VTK bindings
OPTION(WRAP_VTK "Generate VTK interface" OFF)

IF (WRAP_VTK)
    FIND_PACKAGE(VTK 7.1.1 EXACT REQUIRED)
    SET(DEFINE_WRAP_VTK true)
ELSE()
    FIND_PACKAGE(VTK 7.1.1 EXACT)
    SET(DEFINE_WRAP_VTK false)
ENDIF()

IF (VTK_FOUND)
	INCLUDE(${VTK_USE_FILE})
    MESSAGE("${Blue}Entering subdirectory VTK${ColourReset}")
    ADD_SUBDIRECTORY(VTK)
    
    MESSAGE("${Blue}Leaving subdirectory VTK${ColourReset} \ 
")
ENDIF()

MESSAGE("Determining Number of available CPU cores...")

    EXECUTE_PROCESS(
        COMMAND lscpu
        RESULT_VARIABLE LSCPU_EXITCODE
        OUTPUT_VARIABLE LSCPU)

    IF("${LSCPU_EXITCODE}" EQUAL 0)
        STRING(REGEX MATCH "CPU\\(s\\):[ ]*[0-9]+" LSCPU_LINE "${LSCPU}")

        STRING(REGEX MATCH "[0-9]+" CPU_NUMBER "${LSCPU_LINE}")

        SET(FULLMONTE_THREAD_COUNT ${CPU_NUMBER} CACHE STRING "Number of threads for test cases and for default FullMonte runs")
        MESSAGE("  Number of CPU cores is: ${FULLMONTE_THREAD_COUNT}.") 
        MESSAGE("  This will be used to set the number of threads which FullMonte executes.")
    ELSE()
        MESSAGE("  ${Yellow}Could not execute lscpu. The default Thread value for FullMonte is now set to 8. \ 
          You can change the number of threads in the TCL file.${ColourReset}")
    SET(FULLMONTE_THREAD_COUNT 8 CACHE STRING "Number of threads for test cases and for default FullMonte runs")
    ENDIF()
MESSAGE("")
###### Examples
OPTION(INSTALL_EXAMPLES "Copy examples to install directory" ON)






###### Configure the run scripts

IF(CMAKE_BUILD_TYPE STREQUAL "Debug" OR CMAKE_BUILD_TYPE STREQUAL "DEBUG")
    SET(Boost_LIBRARY_DIR ${Boost_LIBRARY_DIR_DEBUG})
    #ADD_COMPILE_OPTIONS("--coverage")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
    #LINK_LIBRARIES("--coverage")
    SET(DEBUG_MODE "ON")
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
ELSE()
    SET(Boost_LIBRARY_DIR ${Boost_LIBRARY_DIR_RELEASE})
ENDIF()

MESSAGE("  Compiler ID: '${CMAKE_CXX_COMPILER_ID}' with flags '${CMAKE_CXX_FLAGS}' \ 
")

## Create a safe DYLD path
# Mac OS runs into trouble if /usr/local/lib is included here

FUNCTION(MAKE_DYLD_PATH O L)
    UNSET(${O})
    SET(TMP ${L})
    LIST(REMOVE_ITEM TMP /usr/local/lib)
    LIST(REMOVE_DUPLICATES TMP)

    FOREACH(P ${TMP})
        IF(OP)
            SET(OP "${OP}:${P}")
        ELSE()
            SET(OP "${P}")
        ENDIF()
    ENDFOREACH()

    SET(${O} ${OP} PARENT_SCOPE)
ENDFUNCTION()

# Currently not used, perhaps we need this in the future
#GET_FILENAME_COMPONENT(Qt5LIBPATH ${_qt5_install_prefix}/.. ABSOLUTE)
#MESSAGE("Qt5LIBPATH: ${Qt5LIBPATH}")

IF(WRAP_TCL)
	IF(CMAKE_VERSION VERSION_LESS 3.9)
        SET(_tcl_lib_prefix "lib")
	ELSE()
        SET(_tcl_lib_prefix "")
	ENDIF()

    # check for enabled accelerators
    # without these statements the variables in the .install file would be blank
    # CUDA
    IF(BUILD_CUDAACCEL)
        SET(_build_cudaaccel true)
    ELSE()
        SET(_build_cudaaccel false)
    ENDIF()

    # Intel FPGA OpenCL
    IF(BUILD_FPGACLACCEL)
        SET(_build_fpgaclaccel true)
    ELSE()
        SET(_build_fpgaclaccel false)
    ENDIF()

    GET_FILENAME_COMPONENT(VTK_TCLLIB_DIR ${VTK_DIR}/../../tcltk/vtk-7.1 ABSOLUTE)
    GET_FILENAME_COMPONENT(VTK_VTKTCL_PATH ${VTK_DIR}/../../../bin/vtk ABSOLUTE)

    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_BINARY_DIR}/lib)
    SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_CURRENT_SOURCE_DIR}/data)
    SET(FULLMONTE_EXAMPLE_DIR ${CMAKE_SOURCE_DIR}/Examples)
    SET(FULLMONTE_INCLUDE_DIR ${CMAKE_SOURCE_DIR})
    SET(FULLMONTE_MMC ${MMC_EXECUTABLE})
    SET(FULLMONTE_TIMOS ${TIMOS_EXECUTABLE})
    CONFIGURE_FILE(Utilities/pkgIndex.tcl.in lib/tcl/pkgIndex.tcl)
    MAKE_DYLD_PATH(RUNTIME_DYLD_PATH "${Qt5LIBPATH};${VTK_INSTALL_PREFIX}/lib;${CMAKE_BINARY_DIR}/lib;${Boost_LIBRARY_DIR}")
    CONFIGURE_FILE(Utilities/tclmonte.sh.in ${CMAKE_BINARY_DIR}/tclmonte.sh)
    CONFIGURE_FILE(Utilities/FullMonte.tcl.in ${CMAKE_BINARY_DIR}/lib/tcl/FullMonte.tcl)
    CONFIGURE_FILE(Utilities/tclmontevtk.sh.in ${CMAKE_BINARY_DIR}/tclmontevtk.sh)

    SET(FullMonteSW_TCLMONTE_COMMAND ${CMAKE_BINARY_DIR}/tclmonte.sh)

    SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/data)
    SET(FULLMONTE_EXAMPLE_DIR ${CMAKE_INSTALL_PREFIX}/share/Examples)
    SET(FULLMONTE_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include)
    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}$AVX/lib)
    SET(FULLMONTE_MMC ${CMAKE_INSTALL_PREFIX}/bin/mmc_linux_sfmt)
    SET(FULLMONTE_TIMOS ${CMAKE_INSTALL_PREFIX}/bin/timos_linux)
    MAKE_DYLD_PATH(RUNTIME_DYLD_PATH "${Qt5LIBPATH};${VTK_INSTALL_PREFIX}/lib;${FullMonteSW_LIBRARY_DIR};${Boost_LIBRARY_DIR}")
    CONFIGURE_FILE(Utilities/tclmonte.sh.in ${CMAKE_BINARY_DIR}/tclmonte.sh.install)
    
    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib)
    CONFIGURE_FILE(Utilities/pkgIndex.tcl.in lib/tcl/pkgIndex.tcl.install)
    CONFIGURE_FILE(Utilities/FullMonte.tcl.in ${CMAKE_BINARY_DIR}/lib/tcl/FullMonte.tcl.install)
   	CONFIGURE_FILE(Utilities/tclmontevtk.sh.in ${CMAKE_BINARY_DIR}/tclmontevtk.sh.install)

    FILE(COPY
            ${CMAKE_BINARY_DIR}/tclmonte.sh
            # ${CMAKE_BINARY_DIR}/tclmonte.sh.install
            ${CMAKE_BINARY_DIR}/tclmontevtk.sh
            # ${CMAKE_BINARY_DIR}/tclmontevtk.sh.install
        DESTINATION ${CMAKE_BINARY_DIR}/bin
        FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

    INSTALL(
        FILES ${CMAKE_BINARY_DIR}/lib/tcl/FullMonte.tcl.install
        DESTINATION 
            ${CMAKE_INSTALL_PREFIX}/lib/tcl
        RENAME
            FullMonte.tcl
        PERMISSIONS OWNER_READ GROUP_READ WORLD_READ)

    INSTALL(
        FILES       ${CMAKE_BINARY_DIR}/lib/tcl/pkgIndex.tcl.install
        DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/tcl
        RENAME pkgIndex.tcl
        PERMISSIONS OWNER_READ GROUP_READ WORLD_READ)

    INSTALL(
        PROGRAMS
            ${CMAKE_BINARY_DIR}/tclmonte.sh.install
        DESTINATION bin
        RENAME
            tclmonte.sh)
            
  	INSTALL(
        PROGRAMS
            ${CMAKE_BINARY_DIR}/tclmontevtk.sh.install
        DESTINATION bin
        RENAME
            tclmontevtk.sh)
ENDIF()

IF(WRAP_PYTHON)
	IF(CMAKE_VERSION VERSION_LESS 3.9)
        SET(_python_lib_prefix "lib")
	ELSE()
        SET(_python_lib_prefix "")
	ENDIF()

    # check for enabled accelerators
    # without these statements the variables in the .install file would be blank
    # CUDA
    IF(BUILD_CUDAACCEL)
        SET(python_build_cudaaccel True)
    ELSE()
        SET(python_build_cudaaccel False)
    ENDIF()

    # Intel FPGA OpenCL
    IF(BUILD_FPGACLACCEL)
        SET(python_build_fpgaclaccel True)
    ELSE()
        SET(python_build_fpgaclaccel False)
    ENDIF()

    GET_FILENAME_COMPONENT(VTK_PythonLIB_DIR ${VTK_DIR}/../../python3.5/site-packages ABSOLUTE)
    GET_FILENAME_COMPONENT(VTK_VTKPYTHON_PATH ${VTK_DIR}/../../../bin/vtkpython ABSOLUTE)

    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_BINARY_DIR}/lib)
    SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_CURRENT_SOURCE_DIR}/data)
    SET(FULLMONTE_EXAMPLE_DIR ${CMAKE_SOURCE_DIR}/Examples)
    SET(FULLMONTE_INCLUDE_DIR ${CMAKE_SOURCE_DIR})
    SET(FULLMONTE_MMC ${MMC_EXECUTABLE})
    SET(FULLMONTE_TIMOS ${TIMOS_EXECUTABLE})
    MAKE_DYLD_PATH(RUNTIME_DYLD_PATH "${Qt5LIBPATH};${VTK_INSTALL_PREFIX}/lib;${CMAKE_BINARY_DIR}/lib;${Boost_LIBRARY_DIR}")
    CONFIGURE_FILE(Utilities/pymonte.sh.in ${CMAKE_BINARY_DIR}/pymonte.sh)
    CONFIGURE_FILE(Utilities/pymontevtk.sh.in ${CMAKE_BINARY_DIR}/pymontevtk.sh)

    CONFIGURE_FILE(Utilities/__init__.py.in ${CMAKE_BINARY_DIR}/lib/fmpy/__init__.py)

    SET(FullMonteSW_PYMONTE_COMMAND ${CMAKE_BINARY_DIR}/pymonte.sh)

    SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/data)
    SET(FULLMONTE_EXAMPLE_DIR ${CMAKE_INSTALL_PREFIX}/share/Examples)
    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}$AVX/lib)
    SET(FULLMONTE_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include)
    SET(FULLMONTE_MMC ${CMAKE_INSTALL_PREFIX}/bin/mmc_linux_sfmt)
    SET(FULLMONTE_TIMOS ${CMAKE_INSTALL_PREFIX}/bin/timos_linux)
    MAKE_DYLD_PATH(RUNTIME_DYLD_PATH "${Qt5LIBPATH};${VTK_INSTALL_PREFIX}/lib;${FullMonteSW_LIBRARY_DIR};${Boost_LIBRARY_DIR}")
    CONFIGURE_FILE(Utilities/pymonte.sh.in ${CMAKE_BINARY_DIR}/pymonte.sh.install)
    
    
    SET(FullMonteSW_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib)
    CONFIGURE_FILE(Utilities/__init__.py.in ${CMAKE_BINARY_DIR}/lib/fmpy/__init__.py.install)
   	CONFIGURE_FILE(Utilities/pymontevtk.sh.in ${CMAKE_BINARY_DIR}/pymontevtk.sh.install)


    FILE(COPY
            ${CMAKE_BINARY_DIR}/pymonte.sh
            # ${CMAKE_BINARY_DIR}/pymonte.sh.install
            ${CMAKE_BINARY_DIR}/pymontevtk.sh
            # ${CMAKE_BINARY_DIR}/pymontevtk.sh.install
        DESTINATION ${CMAKE_BINARY_DIR}/bin
        FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

    INSTALL(DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy
        DESTINATION lib
        PATTERN "__init__*" EXCLUDE
        PERMISSIONS OWNER_READ GROUP_READ WORLD_READ
    )
    INSTALL(FILES ${CMAKE_BINARY_DIR}/lib/fmpy/__init__.py.install
        DESTINATION lib/fmpy
        RENAME __init__.py
    )
    INSTALL(PROGRAMS ${CMAKE_BINARY_DIR}/pymonte.sh.install
        DESTINATION bin
        RENAME pymonte.sh
    )
            
  	# INSTALL(
    #     PROGRAMS
    #         ${CMAKE_BINARY_DIR}/pymontevtk.sh.install
    #     DESTINATION bin
    #     RENAME
    #         pymontevtk.sh)
ENDIF()


###### Configure headers to point to data (for test cases)
######### Modified to test NonSSE first #################
IF(NonSSE)
    SET(DEFINE_USE_SSE false)
ELSE()
    SET(DEFINE_USE_SSE true)
ENDIF()

SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_CURRENT_SOURCE_DIR}/data)
SET(FULLMONTE_CONFIG_SOURCE_DIR ${CMAKE_SOURCE_DIR})
SET(FULLMONTE_CONFIG_BINARY_DIR ${CMAKE_BINARY_DIR})
CONFIGURE_FILE(Utilities/Config.h.in ${CMAKE_BINARY_DIR}/FullMonteSW/Config.h)

SET(FULLMONTE_CONFIG_DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/data)
SET(FULLMONTE_CONFIG_SOURCE_DIR ${CMAKE_INSTALL_PREFIX}/include)
SET(FULLMONTE_CONFIG_BINARY_DIR ${CMAKE_INSTALL_PREFIX}/bin)
CONFIGURE_FILE(Utilities/Config.h.in ${CMAKE_BINARY_DIR}/FullMonteSW/Config.install.h)


INSTALL(FILES ${CMAKE_BINARY_DIR}/FullMonteSW/Config.install.h 
    DESTINATION ${CMAKE_INSTALL_PREFIX}/include
    RENAME Config.h
    )




###### Create CMake config files

SET(FullMonteSW_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/..)
SET(FullMonteSW_LIBRARY_DIR ${CMAKE_BINARY_DIR}/lib)
SET(FullMonteSW_CMAKE_DIR ${CMAKE_BINARY_DIR}/cmake)
CONFIGURE_FILE(Utilities/FullMonteSWConfig.cmake.in ${CMAKE_BINARY_DIR}/cmake/FullMonteSWConfig.cmake)

SET(FullMonteSW_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include)
SET(FullMonteSW_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib)
SET(FullMonteSW_CMAKE_DIR ${CMAKE_INSTALL_PREFIX}/cmake)
CONFIGURE_FILE(Utilities/FullMonteSWConfig.cmake.in ${CMAKE_BINARY_DIR}/cmake/FullMonteSWConfig.install.cmake)

INSTALL(FILES ${CMAKE_BINARY_DIR}/cmake/FullMonteSWConfig.install.cmake 
    DESTINATION ${CMAKE_INSTALL_PREFIX}/include
    )

INSTALL(FILES ${MMC_EXECUTABLE} 
    DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
    PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
    )

INSTALL(FILES ${TIMOS_EXECUTABLE} 
    DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
    PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
    )

FILE(COPY cmake/UseFullMonteSW.cmake DESTINATION ${CMAKE_BINARY_DIR}/cmake)

######
INSTALL(DIRECTORY data/
    DESTINATION ${CMAKE_INSTALL_PREFIX}/data
    FILE_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ
    DIRECTORY_PERMISSIONS OWNER_READ GROUP_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
    )

INSTALL(DIRECTORY ${CMAKE_SOURCE_DIR}/
    DESTINATION ${CMAKE_INSTALL_PREFIX}/include
    FILE_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ
    DIRECTORY_PERMISSIONS OWNER_READ GROUP_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
    FILES_MATCHING PATTERN "*.hpp"
    PATTERN "Build" EXCLUDE
    PATTERN "Docker" EXCLUDE
    PATTERN "External" EXCLUDE
    PATTERN "tcl" EXCLUDE
    PATTERN "matlab" EXCLUDE
    PATTERN "data" EXCLUDE
    PATTERN "SharedMem" EXCLUDE
    PATTERN "Examples" EXCLUDE
    PATTERN "cmake" EXCLUDE
    PATTERN ".git" EXCLUDE
    )


###### Build the program

# Custom target to check all regression tests against their reference simulators (MMC, TIM-OS, MCML)
ADD_CUSTOM_TARGET(regcheck)

# Custom target to generate all necessary test data
ADD_CUSTOM_TARGET(testdata)

INCLUDE(cmake/FullMonteRegressionMCML.cmake)
INCLUDE(cmake/FullMonteRegressionMMC.cmake)
INCLUDE(cmake/FullMonteRegressionTIMOS.cmake)
INCLUDE(cmake/FullMonteUtils.cmake)

MESSAGE("${Blue}Entering subdirectory Logging${ColourReset}")
ADD_SUBDIRECTORY(Logging)
MESSAGE("${Blue}Entering subdirectory Geometry${ColourReset}")
ADD_SUBDIRECTORY(Geometry)
MESSAGE("${Blue}Entering subdirectory Storage${ColourReset}")
ADD_SUBDIRECTORY(Storage)
MESSAGE("${Blue}Entering subdirectory Kernels${ColourReset}")
ADD_SUBDIRECTORY(Kernels)
MESSAGE("${Blue}Entering subdirectory OutputTypes${ColourReset}")
ADD_SUBDIRECTORY(OutputTypes)
MESSAGE("${Blue}Entering subdirectory Queries${ColourReset}")
ADD_SUBDIRECTORY(Queries)
MESSAGE("${Blue}Entering subdirectory Examples${ColourReset}")
ADD_SUBDIRECTORY(Examples)
MESSAGE("${Blue}Entering subdirectory data${ColourReset}")
ADD_SUBDIRECTORY(data)
MESSAGE("${Blue}Entering subdirectory Tests${ColourReset}")
ADD_SUBDIRECTORY(Tests)
MESSAGE("${ColourReset}")
