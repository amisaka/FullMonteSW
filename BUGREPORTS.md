# Reporting Bugs and Requesting Features with FullMonte

We use the [Gitlab issue tracker](https://gitlab.com/FullMonte/FullMonteSW/issues) to manage bug reports and feature requests.
Please check the open bugs to see if your report is a duplicate. 
Please also use the available documentation (README.md has a list) to the extent possible to try to resolve the issue yourself.
If you can't then please file an issue being as specific as possible - software version, command, output, why it's undesirable. 
You can attach scripts, text, images, etc to illustrate.

"Bugs" include defects in documentation, crashes, odd results, unintuitive user interfaces, missing features, etc.

The more specific your bug request, the better able developers will be to resolve it.

Thanks!
