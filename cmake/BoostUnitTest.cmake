# Create a Boost unit test with the given executable name from the named C++ source file
# Files to be tested can be placed in a library later specified with TARGET_LINK_LIBRARIES, or can be given as additional arguments

FUNCTION(ADD_BOOST_UNIT_TEST name src)
	ADD_EXECUTABLE(${name} ${src})
	TARGET_LINK_LIBRARIES(${name} ${Boost_UNIT_TEST_FRAMEWORK_LIBRARIES})
	
	SET_PROPERTY(SOURCE ${src}
        APPEND PROPERTY COMPILE_DEFINITIONS "BOOST_TEST_DYN_LINK" "BOOST_TEST_MODULE=${name}" "FULLMONTE_SOURCE_DIR=\"${CMAKE_SOURCE_DIR}\"")

    # Boost 1.58.0 has lots of deprecated declaration warnings for auto_ptr that get pulled up in boost::unit_test
    # unused-variable seems to be primarily concept checks
    SET_PROPERTY(SOURCE ${src}
        APPEND PROPERTY COMPILE_FLAGS "-Wno-deprecated-declarations -Wno-unused-variable" )

ENDFUNCTION()
