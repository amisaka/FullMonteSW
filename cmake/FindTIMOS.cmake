## Find TIM-OS executable
# Searches (in order):
#
#   ${TIMOS_EXECUTABLE}
#   ${TIMOS_ROOT}       for timos
#   /usr/bin
#   /usr/local/bin
#   ${CMAKE_SOURCE_DIR}/External/timos_SYS      Where SYS may be "mac" or "linux" depending on system

INCLUDE(FindPackageHandleStandardArgs)

IF(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
    SET(_timos_with_suffix "timos_mac")
ELSEIF(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
    SET(_timos_with_suffix "timos_linux")
ENDIF()

FIND_PROGRAM(
    TIMOS_EXECUTABLE
    timos
    HINTS ${TIMOS_ROOT} 
    PATHS /usr/bin /usr/local/bin
    )

FIND_PROGRAM(
    TIMOS_EXECUTABLE
    ${_timos_with_suffix}
    PATHS ${CMAKE_SOURCE_DIR}/External
    )

find_package_handle_standard_args(
    TIMOS
    REQUIRED_VARS   TIMOS_EXECUTABLE
    FOUND_VAR       TIMOS_FOUND
    FAIL_MESSAGE    "[Optional] TIMOS executable was not found - please specify TIMOS_ROOT (folder) or TIMOS_EXECUTABLE full path")

