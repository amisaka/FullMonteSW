####
# Adds an MCML regression test
#
# Arguments:
#    INPUT_FILE     Path to input .mci file  
#    CASE           Case number (starting at zero) to use from input file
#    NAME           Test case name
#    SWEEP_RUNS     Number of runs for seed sweep [default=64]
#    MCML_OUTPUT    Name of .mco file generated (must be in CMAKE_CURRENT_BINARY_DIR) - optional, for test case checking
#
# Requirements:
#    CMAKE_CURRENT_SOURCE_DIR has NAME.reference.mve.out reference mean-variance file
#
# Test cases created:
#    reg_sim_NAME        Test case that runs the simulator and generates NAME.Erz.out
#    reg_compare_NAME    Test case that compares simulator output to golden copy, producing reg.NAME.chi2.out
#
# Targets created:
#    sim_fullmonte_NAME      Target to run the specified case and create NAME.Erz.out
#    sweep_fullmonte_NAME    Target to execute a sweep of SWEEP_RUNS to produce NAME.mve.out mean-variance file
#    regcheck_mcml_NAME      Target to check MCML simulation results against
#                            Depends on MCML_OUTPUT being provided, otherwise not generated  
#

FUNCTION(ADD_MCML_REGRESSION_TEST)
    MESSAGE("Adding a MCML regression test to FullMonte...")
    # Parse inputs into MY_NAME, MY_CASE, etc.
    CMAKE_PARSE_ARGUMENTS("MY" "" "NAME;CASE;INPUT_FILE;SWEEP_RUNS;MCML_OUTPUT" "" ${ARGN})
    
    IF(NOT MY_SWEEP_RUNS)
        SET(MY_SWEEP_RUNS 64)
    ENDIF()
    
    IF(NOT MY_CASE)
        SET(MY_CASE 0)
        MESSAGE("  Using default case=0")
    ENDIF()
    
    IF(NOT (MY_INPUT_FILE AND MY_NAME))
        MESSAGE(FATAL_ERROR "  ${Red}ADD_MCML_REGRESSION_TEST required argument missing: must specify NAME, INPUT_FILE${ColourReset}")
    ENDIF()
    
    SET(TEST_FILE ${MY_NAME}.Erz.out)
    SET(REFERENCE_FILE ${CMAKE_CURRENT_SOURCE_DIR}/${MY_NAME}.reference.mve.out)
    
    # Custom target for single FullMonte simulation    
    ADD_CUSTOM_COMMAND(
        COMMAND sim_fullmonte_layered -i ${MY_INPUT_FILE} --case ${MY_CASE} -o ${MY_NAME}
        DEPENDS ${MY_INPUT_FILE}
        OUTPUT ${TEST_FILE}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    ADD_CUSTOM_TARGET(sim_fullmonte_${MY_NAME} DEPENDS ${TEST_FILE} sim_fullmonte_layered)
    
    # Custom target for FullMonte seed sweep    
    ADD_CUSTOM_COMMAND(
        COMMAND sim_fullmonte_layered -i ${MY_INPUT_FILE} --case ${MY_CASE} -o ${MY_NAME} -r ${MY_SWEEP_RUNS}
        DEPENDS ${MY_INPUT_FILE}
        OUTPUT ${MY_NAME}.mve.out
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    ADD_CUSTOM_TARGET(sweep_fullmonte_${MY_NAME} DEPENDS ${MY_NAME}.mve.out sim_fullmonte_layered)    
    
    # Simulate
    ADD_TEST(
        NAME reg_sim_${MY_NAME}
        COMMAND sim_fullmonte_layered -i ${MY_INPUT_FILE} --case ${MY_CASE} -o ${MY_NAME}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        )
    SET_TESTS_PROPERTIES(
        reg_sim_${MY_NAME}
        PROPERTIES FIXTURES_SETUP ${MY_NAME}_output
        )

    
    # Compare to saved reference    
    ADD_TEST(
        NAME reg_compare_${MY_NAME}
        COMMAND Compare
            --test ${TEST_FILE}
            --ref ${REFERENCE_FILE}
            --geometry ${MY_INPUT_FILE}
            --output reg.${MY_NAME}.chi2.out
            --case ${MY_CASE}
            --extra-stddev-mult 0.0
            --extra-cv 0.001
            --checkpoint-CE 0.95
            --checkpoint-CV 0.05
            --pcrit 0.95
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
        
    SET_TESTS_PROPERTIES(
        reg_compare_${MY_NAME}
        PROPERTIES
            FIXTURES_REQUIRED ${MY_NAME}_output
            DEPENDS reg_sim_${MY_NAME}
            REQUIRED_FILES "${REFERENCE_FILE};${TEST_FILE}")
            

    IF(MY_MCML_OUTPUT)
        # Check stored reference against MCML
        ADD_CUSTOM_COMMAND(
            COMMAND Compare
                --test ${MY_MCML_OUTPUT}
                --ref ${REFERENCE_FILE}
                --output regcheck.${MY_NAME}.chi2.out
                --case ${MY_CASE}
                --extra-stddev-mult 0.0
                --extra-cv 0.001
                --checkpoint-CE 0.95
                --checkpoint-CV 0.05
                --pcrit 0.95
            DEPENDS ${MY_MCML_OUTPUT} ${REFERENCE_FILE}
            OUTPUT regcheck.${MY_NAME}.chi2.out
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
        ADD_CUSTOM_TARGET(regcheck_mcml_${MY_NAME} DEPENDS regcheck.${MY_NAME}.chi2.out)
        ADD_DEPENDENCIES(regcheck_mcml regcheck_mcml_${MY_NAME})
    ELSE()
        MESSAGE("${Yellow}Warning: no MCML reference found for regression test ${MY_NAME} in ${CMAKE_CURRENT_SOURCE_DIR}${ColourReset}")
    ENDIF()
    MESSAGE("")
ENDFUNCTION()


#### ADD_MCML_SIMS
#
# Adds target to run MCML simulations from a single .mci file (may contain multiple test cases)
#
# Arguments
#    NAME             Name of the test set
#    INPUT_FILE       Path to the .mci file
#    OUTPUTS          List of names (must match contents of .mci file)
#
# Targets
#    sim_mcml_NAME    Target that runs MCML to produce the outputs
#
# Custom command output files
#    OUTPUTS          MCML uses the filenames embedded in the .mci file, in the current working dir.
#                     If properly set, those files are all specified as outputs of the custom command

FUNCTION(ADD_MCML_SIMS)
    CMAKE_PARSE_ARGUMENTS("MY" "" "NAME;INPUT_FILE" "OUTPUTS" ${ARGN})
        
    ADD_CUSTOM_COMMAND(
        COMMAND mcml ${MY_INPUT_FILE}
        OUTPUT ${MY_OUTPUTS}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
     
    ADD_CUSTOM_TARGET(sim_mcml_${MY_NAME} DEPENDS ${MY_OUTPUTS} mcml)
ENDFUNCTION()

ADD_CUSTOM_TARGET(regcheck_mcml)