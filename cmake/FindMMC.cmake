## Find MMC executable
# Searches (in order):
#
#   ${MMC_EXECUTABLE}
#   ${MMC_ROOT}         for mmc
#   /usr/bin
#   /usr/local/bin
#   ${CMAKE_SOURCE_DIR}/External/mmc_SYS      Where SYS may be "mac" or "linux" depending on system

INCLUDE(FindPackageHandleStandardArgs)

IF(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
    SET(_mmc_system_suffix "mac")
ELSEIF(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
    SET(_mmc_system_suffix "linux")
ENDIF()

FIND_PROGRAM(
    MMC_EXECUTABLE
    mmc_sfmt mmc
    HINTS ${MMC_ROOT} ${CMAKE_SOURCE_DIR}/../mmc/mmc/trunk/src/bin
    PATHS /usr/bin /usr/local/bin
    PATH_SUFFIXES mmc/trunkl mmc/trunk/src mmc/trunk/src/bin
    )

FIND_PROGRAM(
    MMC_EXECUTABLE
    mmc_${_mmc_system_suffix} mmc_${_mmc_system_suffix}_sfmt
    PATHS ${CMAKE_SOURCE_DIR}/External
    )

find_package_handle_standard_args(
    MMC
    REQUIRED_VARS   MMC_EXECUTABLE
    FOUND_VAR       MMC_FOUND
    FAIL_MESSAGE    "MMC executable was not found - please specify MMC_ROOT (folder) or MMC_EXECUTABLE full path")

