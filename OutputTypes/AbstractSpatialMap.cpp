/*
 * AbstractSpatialMap.cpp
 *
 *  Created on: Oct 20, 2016
 *      Author: jcassidy
 */

#include "AbstractSpatialMap.hpp"
#include "OutputDataType.hpp"

AbstractSpatialMap::AbstractSpatialMap(SpaceType stype,ValueType vtype,OutputType otype) :
	m_spaceType(stype),
	m_valueType(vtype)
{
	outputType(otype);
}

AbstractSpatialMap::~AbstractSpatialMap()
{
}

static const OutputDataType* p[] = { OutputData::staticType(), nullptr };

const OutputDataType abstractSpatialMapType{
	"AbstractSpatialMap",
	p
};

const OutputDataType* AbstractSpatialMap::type() const
{
	return &abstractSpatialMapType;
}
