#ifndef SPATIALMAP_INCLUDED_
#define SPATIALMAP_INCLUDED_

#include "OutputData.hpp"
#include "AbstractSpatialMap.hpp"


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


using namespace std;

/** Represents a spatial map of some property over elements in a geometry */


template<typename Value>class SpatialMap :
		public AbstractSpatialMap
{
public:
	explicit SpatialMap(std::size_t N=0,SpaceType stype=UnknownSpaceType,ValueType vtype=Scalar, OutputType otype=UnknownOutputType);

	SpatialMap(const std::vector<Value>&,SpaceType stype=UnknownSpaceType,ValueType vtype=Scalar, OutputType otype=UnknownOutputType);
	SpatialMap(std::vector<Value>&&,SpaceType stype=UnknownSpaceType,ValueType vtype=Scalar, OutputType otype=UnknownOutputType);

	virtual ~SpatialMap();

	virtual const OutputDataType* type() const override;
	static const OutputDataType* staticType();

#ifndef SWIG
	CLONE_METHOD(OutputData,SpatialMap)
	ACCEPT_VISITOR_METHOD(OutputData,SpatialMap)
#endif

	virtual void			dim(std::size_t N) 	override;
	virtual std::size_t		dim() 				const override;

#ifndef SWIG
	const Value& 	operator[](std::size_t i) const;
	Value& 			operator[](std::size_t i);
#endif

	Value			get(std::size_t i) const;
	void			set(std::size_t i,const Value& v);

	Value			sum() const;

	boost::iterator_range<typename std::vector<Value>::const_iterator> values() const { return m_values; }

private:
	std::vector<Value>		m_values;
};

template<typename Value>SpatialMap<Value>::SpatialMap
	(std::vector<Value>&& src,SpaceType stype,ValueType vtype,OutputType otype) :
		AbstractSpatialMap(stype,vtype,otype),
		m_values(std::move(src))
{
}

template<typename Value>SpatialMap<Value>::SpatialMap
	(const std::vector<Value>& src,SpaceType stype,ValueType vtype,OutputType otype) :
		AbstractSpatialMap(stype,vtype,otype),
		m_values(src)
{
}

template<typename Value>SpatialMap<Value>::SpatialMap
	(std::size_t N,SpaceType stype,ValueType vtype,OutputType otype) :
		AbstractSpatialMap(stype,vtype,otype),
		m_values(N,Value())
{
}

template<typename Value>SpatialMap<Value>::~SpatialMap()
{
}

template<typename Value>void SpatialMap<Value>::dim(std::size_t N)
{
	m_values.resize(N,Value());
}

template<typename Value>std::size_t SpatialMap<Value>::dim() const
{
	return m_values.size();
}

template<typename Value>const Value& SpatialMap<Value>::operator[](std::size_t i) const
{
	return m_values[i];
}

template<typename Value>Value& SpatialMap<Value>::operator[](std::size_t i)
{
	return m_values[i];
}

template<typename Value>Value SpatialMap<Value>::get(std::size_t i) const
{
	return m_values[i];
}

template<typename Value>void SpatialMap<Value>::set(std::size_t i,const Value& v)
{
	m_values[i]=v;
}

template<typename Value>Value SpatialMap<Value>::sum() const
{
	Value s = Value();
	for(const auto v : m_values)
		s += v;
	return s;
}

template<typename Value>const OutputDataType* SpatialMap<Value>::type() const
{
	return staticType();
}



#endif
