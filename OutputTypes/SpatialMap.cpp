/*
 * SpatialMap.cpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#include "OutputDataType.hpp"
#include "SpatialMap.hpp"

const OutputDataType *pf[] = { AbstractSpatialMap::staticType(), nullptr };
const OutputDataType smf{"SpatialMap<float>",pf};

template<>const OutputDataType* SpatialMap<float>::staticType()
{
	return &smf;
}

const OutputDataType *pu[] = { AbstractSpatialMap::staticType(), nullptr };
const OutputDataType smu{"SpatialMap<unsigned>",pu};

template<>const OutputDataType* SpatialMap<unsigned>::staticType()
{
	return &smu;
}
