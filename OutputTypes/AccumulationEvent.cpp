/*
 * AccumulationEvent.cpp
 *
 *  Created on: May 12, 2020
 *      Author: fynns
 */

#include "AccumulationEvent.hpp"

using namespace std;

AccumulationEvent::AccumulationEvent()
{

}

AccumulationEvent::~AccumulationEvent()
{

}

AccumulationEvent* AccumulationEvent::clone() const
{
	return new AccumulationEvent(*this);
}

unsigned AccumulationEvent::size() const
{
	return m_trace.size();
}

void AccumulationEvent::resize(unsigned N)
{
	m_trace.resize(N,AccumulationEventEntry());
}

AccumulationEvent::AccumulationEventEntry AccumulationEvent::get(unsigned i) const
{
	return m_trace.at(i);
}

void AccumulationEvent::set(unsigned i,unsigned addr,double weight)
{
	m_trace.at(i) = AccumulationEventEntry { addr, weight };
}

void AccumulationEvent::set(unsigned i,AccumulationEventEntry e)
{
	m_trace.at(i) = e;
}



