/*
 * OutputData.hpp
 *
 *  Created on: Feb 5, 2016
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_OUTPUTDATA_HPP_
#define OUTPUTTYPES_OUTPUTDATA_HPP_


class OutputData;

class AbstractSpatialMap;
class MCConservationCountsOutput;
class MCEventCountsOutput;

#include <string>
#include <iostream>

#include "clonable.hpp"
#include "visitable.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

class MCConservationCountsOutput;
class MCEventCountsOutput;
class DirectedSurface;
template<typename Value>class SpatialMap;
class DoseHistogram;

class OutputDataType;

class OutputData
{
public:
	virtual ~OutputData(){}

	enum UnitType {UnknownUnitType= -1, J_m=0, J_cm, J_mm, W_m, W_cm, W_mm};
	UnitType unitType() const { return m_unitType; }
	void unitType(UnitType t) { m_unitType = t;}

	enum OutputType { UnknownOutputType=-1, PhotonWeight=0, Energy, Fluence, EnergyPerVolume };
	OutputType outputType() const { return m_outputType; }
	void outputType(OutputType t) { m_outputType = t; }

	class Visitor;
	virtual void acceptVisitor(Visitor* v);

	virtual OutputData* clone() const=0;

	virtual const OutputDataType* 	type() const;
	static const OutputDataType* 	staticType();

	void 					name(std::string n);
	const std::string&		name() const;

    OutputData* deepCopy();

protected:
	UnitType	m_unitType=UnknownUnitType;
	OutputType	m_outputType=UnknownOutputType;

private:
	static const OutputDataType* 	s_type;
	std::string 					m_name;
	
};

#ifndef SWIG
class OutputData::Visitor
{
public:
	Visitor(){}
	virtual ~Visitor(){}

	virtual void doVisit(MCEventCountsOutput*)
	{
		LOG_DEBUG << "doVisit(MCEventCountsData*)" << std::endl;
	}
	virtual void doVisit(MCConservationCountsOutput*)
	{
		LOG_DEBUG << "doVisit(MCConservationCountsData*)" << std::endl;
	}
	virtual void doVisit(SpatialMap<float>*)
	{
		LOG_DEBUG << "doVisit(SpatialMap<float>*)" << std::endl;
	}
	virtual void doVisit(OutputData*)
	{
		LOG_DEBUG << "doVisit(OutputData*)" << std::endl;
	}
	virtual void doVisit(DirectedSurface*)
	{
	    LOG_DEBUG << "doVisit(DirectedSurface*)" << std::endl;
	}
	virtual void doVisit(DoseHistogram*)
	{
	    LOG_DEBUG << "doVisit(DoseHistogram*)" << std::endl;
	}
};

#endif

#endif /* OUTPUTTYPES_OUTPUTDATA_HPP_ */
