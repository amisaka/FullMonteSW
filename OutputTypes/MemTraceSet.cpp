/*
 * MemTraceSet.cpp
 *
 *  Created on: Nov 13, 2017
 *      Author: jcassidy
 */

#include "MemTraceSet.hpp"

using namespace std;

MemTraceSet::MemTraceSet()
{
}

MemTraceSet::~MemTraceSet()
{

}

MemTraceSet* MemTraceSet::clone() const
{
	return new MemTraceSet(*this);
}

void MemTraceSet::resize(unsigned N)
{
	m_traces.resize(N,nullptr);
}

unsigned MemTraceSet::size() const
{
	return m_traces.size();
}

MemTrace* MemTraceSet::get(unsigned i) const
{
	return m_traces.at(i);
}

void MemTraceSet::set(unsigned i,MemTrace* p)
{
	if (i >= m_traces.size())
		resize(i+1);

	m_traces.at(i) = p;
}

unsigned MemTraceSet::append(MemTrace* p)
{
	m_traces.push_back(p);
	return m_traces.size()-1;
}


