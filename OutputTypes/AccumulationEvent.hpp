/*
 * AccumulationEvent.hpp
 *
 *  Created on: May 12, 2020
 *      Author: fynns
 */

#ifndef OUTPUTTYPES_ACCUMULATIONEVENT_HPP_
#define OUTPUTTYPES_ACCUMULATIONEVENT_HPP_

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <vector>

class AccumulationEvent : public OutputData
{
public:
	AccumulationEvent();
	virtual ~AccumulationEvent();

	virtual AccumulationEvent* clone() const;

	unsigned 	size() const;
	void 		resize(unsigned N);

	struct AccumulationEventEntry
	{
		AccumulationEventEntry(unsigned address_=0,double weight_=0) : address(address_), weight(weight_){}
		unsigned address;
		double weight;
	};

	AccumulationEventEntry	get(unsigned i) const;
	void			set(unsigned i,AccumulationEventEntry e);
	void			set(unsigned i,unsigned address,double weight=1);

private:
	std::vector<AccumulationEventEntry>		m_trace;
};





#endif /* OUTPUTTYPES_ACCUMULATIONEVENT_HPP_ */
