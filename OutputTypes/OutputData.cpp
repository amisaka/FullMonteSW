/*
 * OutputData.cpp
 *
 *  Created on: Feb 5, 2016
 *      Author: jcassidy
 */

#include "OutputData.hpp"
#include "OutputDataType.hpp"

using namespace std;

void OutputData::acceptVisitor(Visitor* v)
{
	v->doVisit(this);
}

void OutputData::name(std::string n)
{
	m_name = n;
}

const string& OutputData::name() const
{
	return m_name;
}

const OutputDataType outputDataTypeInfo{ "OutputData", nullptr };

const OutputDataType* OutputData::s_type = &outputDataTypeInfo;

const OutputDataType* OutputData::staticType()
{
	return s_type;
}

const OutputDataType* OutputData::type() const
{
	return s_type;
}

OutputData* OutputData::deepCopy() {
    return this->clone();
}

