/*
 * DirectedSurface.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_DIRECTEDSURFACE_HPP_
#define OUTPUTTYPES_DIRECTEDSURFACE_HPP_

#include "OutputData.hpp"

template<typename T>class SpatialMap;

class Permutation;

class DirectedSurface : public OutputData
{
public:
	DirectedSurface();
	virtual ~DirectedSurface();

	ACCEPT_VISITOR_METHOD(OutputData,DirectedSurface);

	virtual DirectedSurface* clone() const override;

	virtual const OutputDataType* 	type() const override;
	static const OutputDataType* 	staticType();

	void					entering(SpatialMap<float>* e);
	void					exiting(SpatialMap<float>* e);

	SpatialMap<float>*	calculateTotal() const;
	SpatialMap<float>*	entering() const;
	SpatialMap<float>* 	exiting() const;

	void 				permutation(Permutation*);		///< Set the associated permutation
	Permutation*			permutation() const;				///< Get the associated permutation


private:
	SpatialMap<float>*	m_exiting=nullptr;
	SpatialMap<float>*	m_entering=nullptr;
	Permutation*			m_permutation=nullptr;

	static const OutputDataType*	s_type;
	std::string 					m_name;

};


#endif /* OUTPUTTYPES_DIRECTEDSURFACE_HPP_ */
