[![pipeline status](https://gitlab.com/FullMonte/FullMonteSW/badges/master/pipeline.svg)](https://gitlab.com/FullMonte/FullMonteSW/commits/master)

# BUILDING.md

|[Overview](https://gitlab.com/FullMonte/FullMonteSW/wikis/home) |[Installation Guide](https://gitlab.com/FullMonte/FullMonteSW/wikis/Install) | [User Guide](https://gitlab.com/FullMonte/FullMonteSW/wikis/Userguide) |
|:---: |:---:|:---:|
---

## Before You Start

This file only contains high level information on the system requirements, software prerequisites and on actual build instructions.
For installation guides and tutorials, please visit the [Install Overview](https://gitlab.com/FullMonte/FullMonteSW/wikis/Install) and the [Userguide Overview](https://gitlab.com/FullMonte/FullMonteSW/wikis/Userguide)

## System Requirements

- AVX instruction support minimum, AVX2 recommended for performance

## Prerequisites

- [CMake](https://www.cmake.org) version >= 3.8
- Modern compiler with C++11 support (recent G++, Clang; Ubuntu 16.04/Xenial and recent Mac OS Clang work fine)
- [Boost](www.boost.org) version >= 1.58.0 with serialization, iostreams, system, program\_options unit\_test\_framework compiled
  (Ubuntu package ``libboost-all-dev``)
- [Docker](www.docker.com), required for user installation, recommended if you do not want to build from source, not required for native installation

### Optional

- [VTK](https://www.vtk.org) 7.1.1 - needs to be built from source (it uses CMake, so see section below on CMake)
- [VTK](https://www.vtk.org) 7.1.1 Tcl bindings - enable when building from source if desired
- [Swig](www.swig.org) 3.0 if you want to generate Tcl script bindings
- Tcl (Ubuntu package ``tcl-dev``) for Tcl script bindings
- Tk  (Ubuntu package ``tk-dev``) for simple Tcl GUI
- `libcurses5-dev` (CCMake dependency)

## Build Scritps

You can either choose to build FullMonte natively or in the docker development container. To ease the setup of FullMonte in these two cases, we provide two build scripts that download all required packages and build FullMonte from source. They can be downloaded here:

|[Docker Build Script](https://gitlab.com/FullMonte/FullMonteSW/snippets/1745589) |[Native Linux Build Script](https://gitlab.com/FullMonte/FullMonteSW/snippets/1745612) |
|:---: |:---:|

**Note**: If you want to build FullMonte from source on Windows, you need to use docker.
It is also possible to manually build FullMonte. On our Wiki page, we listed the necessary steps in order to build FullMonte without using the provided scripts. Please refern to them if you want to know more.

|[Docker Development Install](https://gitlab.com/FullMonte/FullMonteSW/wikis/Developer-Guide#docker-development-install) |[Native Linux Development Install](https://gitlab.com/FullMonte/FullMonteSW/wikis/Developer-Guide#native-development-install-linux-only) |
|:---: |:---:|

## Configuring with CMake

FullMonte uses [CMake](https://cmake.org/documentation) to locate its dependencies and generate its build system in a cross-platform, standardized way.
It does not build the software itself, but generates the files so that one of many backends may do so including Make, Ninja, Visual Studio, etc.
 
CMake has a very portable - very rudimentary - GUI to help configuring called ``ccmake`` (Ubuntu package ``cmake-curses-gui``) that will show what options are available and
their current values. You can substitute it for ``cmake`` in the command line below for an interactive experience.

Generally CMake works best doing an out-of-source build, in which the generated files are located somewhere other than the source folder.
To invoke, create the build directory and _from that location_ run 

``cmake <options> -D<var1>=<value1> -D<var2>=<value2> <...> /path/to/source``

with the path to source replaced appropriately. To succeed, you will likely need to provide some variable values as ``var1`` ``value1`` etc. See common values below. If you have difficulty locating standard packages, CMake documentation may help by telling you what hints can be provided.

## Choosing a CMake Generator

The ``-G`` option allows choice of which generator is employed, defaulting to the ubiquitous Make.

### ninja-build

We find that [ninja-build](https://ninja-build.org) is a handy backend to use for CMake.
It is faster (basically instantaneous) at checking dependencies, so it builds faster when there are few changes.
It also builds in parallel using the number of available CPUs by default, which is nice.

Just add ``-G ninja`` to the CMake command line to choose it over the default Make.
Commands that use ``make`` can then generally be switched to ``ninja``.  


## Generating

Once CMake has run without errors, you will have project files for your chosen generator in the build directory.


## Building 

From the build directory, invoke your build tool (``make``, ``ninja``, etc) and the software should build. 

If the build system _generates correctly_ and _then_ fails, please log an issue in our bug tracker (see ``BUGREPORTS.md``). 
It's not a bug if prerequisites are not installed (unless they are undocumented), or if they are installed but not provided to
CMake.
Please include the command line you used to run ``cmake`` or ``ccmake``, a copy of ``CMakeCache.txt`` from your build directory, and
the error text.
Thank you!

## Installing

If you wish to install the software for use, it will be installed in the ``CMAKE_INSTALL_PREFIX`` dir when you run ``make install``. Note this may require ``sudo`` if installing
in ``/usr`` or other global dirs. 


## Troubleshooting Build Issues

### Using Multiple Toolchains (eg. G++ on Mac OS)

Be aware that G++ and Clang (or even G++ across versions) binaries aren't necessarily compatible due to C++ name-mangling, eg. if you build Boost or VTK with Clang
then you'll need to use Clang to build FullMonte as well. The same goes for any of the prerequisite binaries.

Any C++ libraries installed from binary will likely have been installed to match the *default* compiler on your system (eg. Ubuntu). If using a different compiler, 
you may run into issues with missing references at link time.


The compiler can be specified when CMake is _first_ run by using the ``CMAKE_C_COMPILER`` and ``CMAKE_CXX_COMPILER`` options.
To change the compiler, you must nuke the build directory and re-run CMake (this is a CMake quirk).

When using G++ on Mac OS, we found it necessary to use the native assembler instead of G++, hence the ```-Wa,-q`` option.


# Testing

A growing set of unit and regression tests is provided with FullMonte and aggregated automatically using CTest based on the CMakeLists.txt files.
CMake generates a ``test`` target that you can build to run all tests (``make test``, ``ninja test``, etc). See ``CONTRIBUTING.md`` for details on testing (eg. running/excluding specific tests).
