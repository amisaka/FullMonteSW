/*
 * DataSetStats.cpp
 *
 *  Created on: Jun 21, 2017
 *      Author: jcassidy
 */

#include "DataSetStats.hpp"

#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <iostream>

using namespace std;

DataSetStats::DataSetStats()
{
}

DataSetStats::~DataSetStats()
{
}

OutputDataCollection* DataSetStats::input() const
{
	return m_collection;
}


void DataSetStats::input(OutputDataCollection* C)
{
	m_collection = C;
}

void DataSetStats::update()
{
	unsigned r = m_collection->size();
	clear();

	// check input is provided
	if (!m_collection)
	{
		LOG_ERROR << "DataSetStats::update() called but no data set specified" << endl;
		return;
	}

	if (r < 2)
	{
	    LOG_ERROR << "DataSetStats::update() called but data set has <2 members for comparison" << endl;
		return;
	}


	// get first data element and cast it to SpatialMap<float>
	OutputData* d = m_collection->getByIndex(0);
	if (!d)
	{
		LOG_ERROR << "DataSetStats::update() couldn't fetch first result" << endl;
		return;
	}

	if (SpatialMap<float>* vf = dynamic_cast<SpatialMap<float>*>(d))
	{
		unsigned c = vf->dim();
		for(unsigned i=0;i<r;++i)
		{
			OutputData* d = m_collection->getByIndex(i);
			if (!d)
			{
				LOG_ERROR << "DataSetStats::update() couldn't get result #" << i << endl;
				return;
			}
			vf = dynamic_cast<SpatialMap<float>*>(d);

			if (!vf)
			{
				LOG_ERROR << "DataSetStats::update() unable to cast result to SpatialMap<float>" << endl;
				return;
			}
			else if (vf->dim() != c)
			{
				LOG_ERROR << "DataSetStats::update() dimension mismatch" << endl;
				return;
			}
		}

		doUpdate(r,vf);
	}
	else
	{
		LOG_ERROR << "DataSetStats::update() called but data can't be cast to a known type" << endl;
		return;
	}

}
