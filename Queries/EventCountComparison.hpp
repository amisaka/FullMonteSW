/*
 * EventCountCompare.hpp
 *
 *  Created on: Jun 27, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_EVENTCOUNTCOMPARISON_HPP_
#define QUERIES_EVENTCOUNTCOMPARISON_HPP_

class MCEventCountsOutput;

#include <iostream>
#include <limits>

class StatisticalTest
{
public:
	StatisticalTest();
	virtual ~StatisticalTest();

	virtual float		pValue() const=0;
	virtual float		cutoff(float p) const=0;
	void				print(std::ostream& os=std::cout) const;

	float				statistic() const;
	void				statistic(float s);

	const std::string& 	name() const;
	void				name(std::string n);

private:
	float		m_p=std::numeric_limits<float>::quiet_NaN();
	float		m_statistic=std::numeric_limits<float>::quiet_NaN();

	std::string	m_name;
};


class Chi2StatisticalTest : public StatisticalTest
{
public:
	Chi2StatisticalTest();
	virtual ~Chi2StatisticalTest();

	virtual float	pValue() const override;
	virtual float 	cutoff(float p) const override;

	void		df(unsigned df);
	unsigned	df() const;

private:
	unsigned	m_df=0;
};

/** Compares the two provided event counts to see if they are equal within statistical error.
 *
 * For events which happen only once per packet lifetime (exit, roulette death), we assume binomial distribution mu=Np, sigma^2=Np(1-p)
 * For events that may happen multiple times, Poisson statistics are assumed with rate constant lambda (mu=sigma^2=lambda*N)
 *
 * As p -> 0, 1-p -> 1 and these become nearly identical with p=lambda
 */

class EventCountComparison
{
public:
	EventCountComparison();
	~EventCountComparison();

	void		left(MCEventCountsOutput* lhs);
	void		right(MCEventCountsOutput* rhs);

	void 		update();

	void		print(std::ostream& os=std::cout) const;

private:
	Chi2StatisticalTest 		binomialCompare(unsigned long long MCEventCountsOutput::* stat);
	static Chi2StatisticalTest	binomialCompare(float N0,float x0,float N1,float x1);

	Chi2StatisticalTest			poissonCompare(unsigned long long MCEventCountsOutput::* stat);
	static Chi2StatisticalTest	poissonCompare(float N0,float x0,float N1,float x1);

	MCEventCountsOutput		*m_lhs=nullptr;
	MCEventCountsOutput		*m_rhs=nullptr;

};

#endif /* QUERIES_EVENTCOUNTCOMPARISON_HPP_ */
