/*
 * ZScoresComparison.hpp
 *
 *  Created on: Jun 22, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_ZSCORESCOMPARISON_HPP_
#define QUERIES_ZSCORESCOMPARISON_HPP_

class OutputData;

/** Compare a dataset to a reference dataset, computing a z score for each element.
 *
 * Output z scores should be standard normal if the difference is due to Gaussian noise.
 *
 * z = (input - ref) / ref_stddev
 *
 */

class ZScoresComparison
{
public:
	ZScoresComparison();
	~ZScoresComparison();

	void referenceMean	(OutputData* mu);
	void referenceStdDev(OutputData* sigma);

	void testInput		(OutputData* input);

	void update();

	OutputData* 		outputZ() const;

private:

	OutputData *m_referenceMean=nullptr;
	OutputData *m_referenceStdDev=nullptr;
	OutputData *m_input=nullptr;

	OutputData* m_zScore=nullptr;
};

#endif /* QUERIES_ZSCORESCOMPARISON_HPP_ */
