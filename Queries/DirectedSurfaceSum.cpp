#include <FullMonteSW/Queries/DirectedSurfaceSum.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

/**
 * Update data. Call this to populate m_surfaceEnteringWeight and m_surfaceExitingWeight
 */
void DirectedSurfaceSum::update() {
    // ensure we have some data
    if(!m_collection) {
        LOG_ERROR << "DirectedSurfaceSum::update() no input data provided\n";
        return;
    }

    // clear outputs
    m_surfaceEnteringWeight.clear();
    m_surfaceExitingWeight.clear();

    // iterate over every output data collection (extreme case is none or all are DirectedSurface)
    for(unsigned s = 0; s < m_collection->size(); s++) {
        // Try to get a DirectedSurface from the output data
        DirectedSurface* ds_phi = dynamic_cast<DirectedSurface*>(m_collection->getByType("DirectedSurface", s));
        if(ds_phi) {
            if (ds_phi->entering()->outputType() != AbstractSpatialMap::Fluence)
            {
                // Found a surface in the output data collection. Add a new set of total entering/exiting sums
                m_surfaceEnteringWeight.push_back(0.0);
                m_surfaceExitingWeight.push_back(0.0);
                
                // get the entering and exiting spatial maps for the DirectedSurface
                SpatialMap<float>* entering = ds_phi->entering();
                SpatialMap<float>* exiting = ds_phi->exiting();

                // ensure they are the same dimension
                if(entering->dim() != exiting->dim()) {
                    throw logic_error("DirectedSurfaceSum::update() exiting and entering have different dimensions. This is very odd.");
                }

                // sum the photon weight entering/exiting the faces of the current surface into a single number
                for(unsigned f = 0; f < entering->dim(); f++) {
                    float in = entering->get(f);
                    float out = exiting->get(f);

                    if(isnan(in)) {
                        LOG_WARNING << "DirectedSurfaceSum::update() NaN value for energy entering face " <<  f << " being set to zero\n";
                        in = 0;
                    }
                    if(isnan(out)) {
                        LOG_WARNING << "DirectedSurfaceSum::update() NaN value for energy exiting face " <<  f << " being set to zero\n";
                        out = 0;
                    }
                    
                    m_surfaceEnteringWeight.back() += in;
                    m_surfaceExitingWeight.back() += out;
                }
            }
            else
            {
                LOG_ERROR << "DirectedSurfaceSum::update() output type is Fluence. Accumulating the simulation results only makes sense with energy or raw photon weight data\n";
            }
        } else {
            // DirectedSurface indices monotonically increase so as soon as we don't get one we can break
            break;
        }
    }
}

/**
 * @param the index of the requested surface
 * @returns the total photon weight entering 'surfaceIdx' for all faces
 */
float DirectedSurfaceSum::totalEnteringWeight(unsigned int surfaceIdx) {
    if(surfaceIdx < m_surfaceEnteringWeight.size()) {
        return m_surfaceEnteringWeight[surfaceIdx];
    } else {
        throw std::logic_error("DirectedSurfaceSum::totalEnteringWeight() trying to access a surface that does not exist (out of bounds access).");
    }
}

/**
 * @param the index of the requested surface
 * @returns the total photon weight exiting 'surfaceIdx' for all faces
 */
float DirectedSurfaceSum::totalExitingWeight(unsigned int surfaceIdx) {
    if(surfaceIdx < m_surfaceExitingWeight.size()) {
        return m_surfaceExitingWeight[surfaceIdx];
    } else {
        throw std::logic_error("DirectedSurfaceSum::totalExitingWeight() trying to access a surface that does not exist (out of bounds access).");
    }
}


