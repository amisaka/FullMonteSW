/*
 * BasicStats.hpp
 *
 *  Created on: Jun 21, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_BASICSTATS_HPP_
#define QUERIES_BASICSTATS_HPP_

#include "DataSetStats.hpp"
#include <vector>

class BasicStats : public DataSetStats
{
public:
	BasicStats();
	~BasicStats();

	OutputData*		mean()		const;
	OutputData*		variance() 	const;		///< Sample variance (uses N-1 since mean is estimated)
	OutputData*		stddev()	const;		///< Sample std dev (uses N-1 since mean is estimated)
	OutputData*		sum() 		const;
	OutputData*		min()		const;
	OutputData*		max()		const;
	OutputData*		cv()		const;		///< Coefficient of variation
	OutputData*		nnz()		const;		///< Number of nonzero entries

protected:
	virtual void clear() override;
	virtual void doUpdate(unsigned Nds,SpatialMap<float>* v0) override;

private:
	unsigned				m_N=0;
	std::vector<double>		m_sum_x;
	std::vector<double>		m_sum_xx;
	std::vector<float>		m_min;
	std::vector<float>		m_max;
	std::vector<unsigned>	m_nnz;
};



#endif /* QUERIES_BASICSTATS_HPP_ */
