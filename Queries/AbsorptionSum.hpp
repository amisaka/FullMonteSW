/*
 * AbsorptionSum.hpp
 *
 *  Created on: Mar 24, 2016
 *      Author: jcassidy
 */

#ifndef OUTPUTTYPES_ABSORPTIONSUM_HPP_
#define OUTPUTTYPES_ABSORPTIONSUM_HPP_

#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

/** Sums field elements, optionally over a Partition.
 *
 * The data should be provided using the data(x) method.
 * If a Partition is specified, then the various sums will be computed for each element of the Partition.
 *
 */

class Geometry;
class OutputData;
class AbstractSpatialMap;
template<class T>class SpatialMap;

class AbsorptionSum
{
public:
	AbsorptionSum();
	~AbsorptionSum();

	/// Set the partition to be used
	void							partition(const Partition* p) {m_partition=p;}
	const Partition*				partition() const {return m_partition;}

	/// Get/set the data to be summed
	void 						data(const OutputData* E) {m_input = E;}
	const OutputData*			data() const {return m_input;}

	/// the geometry of mesh (must be a TetraMesh)
    void 						geometry(const Geometry* m) {m_geometry = m;}
	const Geometry* 			geometry() const {return m_geometry;}
    
	/// Compute
	void update();

	float						totalForAllElements() const;				///< Result over all data elements
	float						totalForPartition(unsigned i) const;		///< Return total for elements in a given partition

private:
	const Partition*				m_partition=nullptr;
	const OutputData* 				m_input=nullptr;
	// the geometry (must be a TetraMesh)
    const Geometry*         		m_geometry=nullptr;

	std::vector<double>				m_regionSum;
	std::vector<double>				m_regionVolumeSum;
	double							m_total=0.0;
	double							m_volume=0.0;
};



#endif /* OUTPUTTYPES_ABSORPTIONSUM_HPP_ */
