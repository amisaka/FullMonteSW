#include <FullMonteSW/Queries/AccumulationLineQuery.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

/**
 * Updates the 'total' of the probe based on the input parameters
 */
void AccumulationLineQuery::update() {
    //// checking inputs to make sure they are valid
    
    // input data must be SpatialMap<float>
    if(!m_input) {
        LOG_ERROR << "AccumulationLineQuery::update() no input data provided\n";
        return;
    }
    
    // get SpatialMap<float> data
    const SpatialMap<float>* in_smap = dynamic_cast<const SpatialMap<float>*>(m_input);

    // check we can get this data
    if(!in_smap) {
        LOG_ERROR << "AccumulationLineQuery::update() input data could not be cast to SpatialMap<float>\n";
        return;
    } else if(in_smap->spatialType() != AbstractSpatialMap::Volume) {
        LOG_ERROR << "AccumulationLineQuery::update() input data must be AbstractSpatialMap::Volume\n";
        return;
    }
    if (in_smap->outputType() == AbstractSpatialMap::UnknownOutputType)
    {
        LOG_ERROR << "Could not identify type of data. This probe expects fluence, energy or raw photon weight data!" << endl;
        return;
    }

    // ensure geometry is defined
    if(!m_geometry) {
        LOG_ERROR << "AccumulationLineQuery::update() no geometry provided\n";
        return;
    }

    // get the TetraMesh geometry
    const TetraMesh* mesh = dynamic_cast<const TetraMesh*>(m_geometry);

    // geometry must be TetraMesh
    if(!mesh) {
        LOG_ERROR << "AccumulationLineQuery::update() could not convert geometry to TetraMesh\n";
        return;
    }

    // must have 1 input value per tetra (-1 is for the 0 tetra automatically inserted by FullMonte, which is not present in the data)
    if((mesh->tetraCells()->size() - 1) != in_smap->dim()) {
        LOG_ERROR << "AccumulationLineQuery::update() input must have same number of entries as tetras in the mesh\n";
        return;
    }

    // a direction vector for the line probe
	const std::array<float,3> lineVec = {
        m_endpoint[1][0] - m_endpoint[0][0],
        m_endpoint[1][1] - m_endpoint[0][1],
        m_endpoint[1][2] - m_endpoint[0][2],
    };

    // length of line probe
    const float length = std::sqrt(lineVec[0]*lineVec[0] + lineVec[1]*lineVec[1] * lineVec[2]*lineVec[2]);

    // a normal vector for the line
    const std::array<float,3> lineVecNorm {
        lineVec[0]/length,
        lineVec[1]/length,
        lineVec[2]/length
    };

    // create the ray walker to grab all tetras intersected by this line
    RayWalkIterator it = RayWalkIterator::init(*mesh, m_endpoint[0], lineVecNorm);
    RayWalkIterator itEnd = RayWalkIterator::endAt(length);
    
    // using ray walker, grab each tetra intersected by the line probe and track its value in the input array (energy, fluence)
    m_total = 0.0f;
    m_volume = 0.0;

    // Get all volume data for each tetrahedron of the mesh
    const SpatialMap<float>* tetraVolumes = mesh->elementVolumes();

    unsigned counter = 0;
    for(; it != itEnd; ++it) {
        unsigned IDt = it->IDt;
        
        LOG_DEBUG << "AccumulationLineQuery::update grabbing accumulation data from tetra " << IDt << std::endl;
        counter++;
        if (in_smap->outputType() == AbstractSpatialMap::Fluence)
        {
            // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
            float fluence = in_smap->get(IDt - 1);
            float volume = tetraVolumes->get(IDt);
            float fluence_x_volume = fluence * volume;
            m_total += fluence_x_volume;
            m_volume += volume;
        }
        else  // data is energy
        {
            // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
            m_total += in_smap->get(IDt - 1);
        }
    }

    LOG_INFO << "AccumulationLineQuery::update grabbed accumulation values from " << counter << " tetrahedrons" << std::endl;
}


