/*
 * DataSetStats.hpp
 *
 *  Created on: Jun 21, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_DATASETSTATS_HPP_
#define QUERIES_DATASETSTATS_HPP_

/** Collects statistics across multiple spatial maps.
 *
 */

class OutputData;
class OutputDataCollection;
template<typename T>class SpatialMap;

#include <vector>

class DataSetStats
{
public:
	DataSetStats();
	virtual ~DataSetStats();

	void 		input(OutputDataCollection* C);
	void 		update();

	OutputDataCollection* input() const;

protected:
	virtual void clear()=0;
	virtual void doUpdate(unsigned Nds,SpatialMap<float>* v0)=0;

private:
	OutputDataCollection* 	m_collection=nullptr;

};


#endif /* QUERIES_DATASETSTATS_HPP_ */
