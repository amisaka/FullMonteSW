/*
 * Permute.cpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#include <vector>

#include "Permute.hpp"
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

Permute::Permute()
{
	m_inverse=new Permutation();

}

Permute::~Permute()
{
}

void Permute::permutation(Permutation* p)
{
	m_permutation=p;
}

void Permute::inverseMode(bool inv)
{
	m_inverseMode=inv;
}

Permutation* Permute::permutation() const
{
	return m_permutation;
}

void Permute::input(OutputData* d)
{
	m_input = d;
}

void Permute::update()
{
	//// Check for permutation & if present, compute inverse (default source index is -1U)
	if (!m_permutation)
	{
		cout << "Permute::update() called without permutation" << endl;
		return;
	}
	else if (m_permutation->sourceSize() == 0)
	{
		cout << "Permute::update() called on permutation without sourceSize set" << endl;
		return;
	}

	m_inverse->resize(m_permutation->sourceSize());
	for(unsigned i=0;i<m_inverse->dim();++i)
		m_inverse->set(i,-1U);

	for(unsigned i=0;i<m_permutation->dim();++i)
	{
		if (m_permutation->get(i) < m_inverse->dim())
			m_inverse->set(m_permutation->get(i),i);
//		else
//			cout << "Permute::update() invalid sourceSize setting in permutation" << endl;
	}


	//// Check and cast input vector
	SpatialMap<float>* f=nullptr;
	if (!m_input)
	{
		cout << "Permute::update() called without input data" << endl;
		return;
	}

	unsigned N=0,Np=0,N_in=0,N_out=0;

	if (!(f = dynamic_cast<SpatialMap<float>*>(m_input)))
	{
		cout << "Permute::update() cannot cast input data to an appropriate type" << endl;
		return;
	}

	Np = m_permutation->dim();
	N  = m_permutation->sourceSize();

	if (m_inverseMode)
	{
		N_in = Np;
		N_out = N;
	}
	else
	{
		N_in = N;
		N_out = Np;
	}

	// check input dims
	if (f->dim() != N_in)
	{
		cout << "Permute::update() called with invalid input size (" << f->dim() << "); expecting to match permutation source size (" << N_in << ")" << endl;
		return;
	}

	// create output
	SpatialMap<float>* of=new SpatialMap<float>();
	of->dim(N_out);

	if (m_inverseMode)
	{
		for(unsigned i=0;i<N_out;++i)
		{
			unsigned ip = m_inverse->get(i);
			of->set(i,
					ip == -1U ?
						0.0 :
						f->get(ip));
		}
	}
	else
	{
		for(unsigned i=0; i<m_permutation->dim(); ++i)		// do the permutation
		{
			unsigned op = m_permutation->get(i);
			of->set(i,f->get(op));
		}
	}

	m_output=of;
}

Permutation* Permute::inversePermutation() const
{
	return m_inverse;
}

OutputData* Permute::output()
{
	return m_output;
}


