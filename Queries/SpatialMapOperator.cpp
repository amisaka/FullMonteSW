#include <FullMonteSW/Queries/SpatialMapOperator.hpp>

/**
 * Logic for performing operator on SpatialMap.
 *
 * @param op the operator to perform.
 */
void SpatialMapOperator::compare(OP op) {
    // input data must be SpatialMap<float>
    if(!m_A || !m_B) {
        cout << "SpatialMapComparator::compare() A or B input data not provided\n";
        return;
    }

    // cast inputs to SpatialMap and check validity
    const SpatialMap<float>* smap_A = dynamic_cast<const SpatialMap<float>*>(m_A);
    const SpatialMap<float>* smap_B = dynamic_cast<const SpatialMap<float>*>(m_B);
    
    if(!smap_A || !smap_B) {
        cout << "SpatialMapComparator::compare() input data could not be cast to SpatialMap<float>\n";
        return;
    }

    if(smap_A->dim() != smap_B->dim()) {
        cout << "SpatialMapComparator::compare() A and B have different dimensions\n";
        return;
    }

    // delete any existing memory for output
    delete m_output;
    m_output = nullptr;

    // allocate memory for new output
    SpatialMap<float>* O = smap_A->clone();

    // for every element perform the operation 'op'
    for(unsigned i = 0; i < smap_A->dim(); i++) {
        // get values
        float a = smap_A->get(i);
        float b = smap_B->get(i);

        // perform operator
        switch(op) {
            case ADD:
                O->set(i, a+b);
                break;
            case SUB:
                O->set(i, a-b);
                break;
            case MUL:
                O->set(i, a*b);
                break;
            case DIV:
                O->set(i, a/b);  // TODO: handle DIV0 here?
                break;
        }
    }

    // assign output
    m_output = O;
}

/**
 * Add
 */
void SpatialMapOperator::add() {
    compare(ADD);
}

/**
 * Subtract
 */
void SpatialMapOperator::sub() {
    compare(SUB);
}

/**
 * Multiply
 */
void SpatialMapOperator::mul() {
    compare(MUL);
}

/**
 * Divide
 */
void SpatialMapOperator::div() {
    compare(DIV);
}

