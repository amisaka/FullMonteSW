/*
 * Chi2Homogeneity.cpp
 *
 *  Created on: Jun 15, 2017
 *      Author: jcassidy
 */


#include "Chi2Homogeneity.hpp"
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <vector>

using namespace std;


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>



#include <limits>

Chi2Homogeneity::Chi2Homogeneity() :
		m_leftPackets(std::numeric_limits<float>::quiet_NaN()),
		m_rightPackets(std::numeric_limits<float>::quiet_NaN()),
		m_rightRuns(std::numeric_limits<float>::quiet_NaN())
{
}

void Chi2Homogeneity::allowedCoefficientOfVariationOfMean(float ratio)
{
	m_allowedCoefficientOfVariationOfMean=ratio;
}

void Chi2Homogeneity::normalizeLeft(bool e)
{
	m_normalizeLeft=e;
}

void Chi2Homogeneity::normalizeRight(bool e)
{
	m_normalizeRight=e;
}

void Chi2Homogeneity::leftPackets(float pl)
{
	m_leftPackets=pl;
}

void Chi2Homogeneity::rightPackets(float pr)
{
	m_rightPackets=pr;
}

void Chi2Homogeneity::rightRuns(float Nr)
{
	m_rightRuns=Nr;
}

unsigned Chi2Homogeneity::rightRuns() const
{
	return m_rightRuns;
}

unsigned long long Chi2Homogeneity::rightPackets() const
{
	return m_rightPackets;
}

unsigned long long Chi2Homogeneity::leftPackets() const
{
	return m_leftPackets;
}

Chi2Homogeneity::~Chi2Homogeneity()
{

}

void Chi2Homogeneity::rightVariance(OutputData *sigma2)
{
	m_rightVariance=sigma2;

}

void Chi2Homogeneity::doUpdate()
{
	if  (!m_rightVariance)
	{
	    LOG_ERROR << "Chi2Homogeneity::doUpdate() called with no variance information" << endl;
	}
	else if (isnan(m_rightRuns))
	{
		LOG_ERROR << "Chi2Homogeneity::update() called but number of runs not specified (rightRuns)" << endl;
	}
	else if(!m_rightVariance->type()->derivedFrom(right()->type()))
	{
		LOG_ERROR << "Chi2Homogeneity::doUpdate() called with variance information of incorrect type (" << m_rightVariance->type()->name() << " vs "
				<< right()->type()->name() << " or derived type expected)" << endl;
	}
	else
	{
		double chi2=0,excl=0;

		double kr=1.0;
		double kl=1.0;

		if (m_normalizeLeft)
			kl = 1.0/double(m_leftPackets);

		if (m_normalizeRight)
			kr = 1.0/double(m_rightPackets);

		m_contributions.resize(left()->dim(), 0.0f);
		boost::fill(m_contributions,0.0f);

		unsigned N=0;
		SpatialMap<float>* rVarianceMap = static_cast<SpatialMap<float>*>(m_rightVariance);

		for(unsigned c=0;c<right()->dim();++c)
		{
			// variance provided for right dataset
			double rVariance = rVarianceMap->get(c);
			double xl = left()->get(c);

			if (rVariance > 0)
			{
				double xr = right()->get(c);

				// Null hypothesis:
				//		mean(left) = mean(right)
				//		packets independent -> var(left) = Npacket_right / Npacket_left * var(right)
				//		left, right independent
				//
				// Under the null hypothesis:
				//		L-R 	is zero mean
				//		L-R 	has variance varL + varR = (Npr/Npl + 1/Nr)*varR (by L/R independence and packet independence)
				//		sum_i (L_i - R_i)^2/(varL + varR) is chi2 with Nt degrees of freedom
				//
				// test the hypothesis by comparing chi2 value vs its CDF

				double sigma2 = rVariance*kr*kr*(m_rightPackets/m_leftPackets + 1/m_rightRuns);
				double extra_sigma2 = kr*kr*xr*xr*m_allowedCoefficientOfVariationOfMean*m_allowedCoefficientOfVariationOfMean;

				sigma2 += extra_sigma2;
				double dchi2 = (kl*xl-kr*xr)*(kl*xl-kr*xr)/sigma2;
				chi2 += dchi2;

				m_contributions[c] = dchi2;

				++N;
			}
			else
				excl += xl;
		}

		m_leftExcluded=excl;

		m_nonZeroSamples = N;
		m_chi2stat = chi2;
	}
}

void Chi2Homogeneity::clear()
{
	m_chi2stat=0.0;
}

float Chi2Homogeneity::pValue() const
{
	boost::math::chi_squared_distribution<double> chi2cdf(degreesOfFreedom());

	return 1.0-cdf(chi2cdf,m_chi2stat);
}

float Chi2Homogeneity::criticalValue(float p) const
{
	boost::math::chi_squared_distribution<double> chi2cdf(degreesOfFreedom());

	return quantile(chi2cdf,1.0f-p);
}

float Chi2Homogeneity::chi2stat() const
{
	return m_chi2stat;
}

unsigned Chi2Homogeneity::degreesOfFreedom() const
{
	// Chi2 table has R=2 rows (left/right dataset), and C=Nnz samples in each.
	// discount one row because the expected means (C values) are derived from the data
	return m_nonZeroSamples;
}

float Chi2Homogeneity::leftExcluded() const
{
	return m_leftExcluded;
}

OutputData* Chi2Homogeneity::contributions() const
{
	SpatialMap<float>* o = left()->clone();
	for(unsigned i=0;i<left()->dim();++i)
		o->set(i,m_contributions[i]);
	return o;
}
