/*
 * ZScoresComparison.cpp
 *
 *  Created on: Jun 22, 2017
 *      Author: jcassidy
 */

#include "ZScoresComparison.hpp"
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>


ZScoresComparison::ZScoresComparison()
{
}

ZScoresComparison::~ZScoresComparison()
{
}

void ZScoresComparison::referenceMean(OutputData* mu)
{
	m_referenceMean=mu;
}

void ZScoresComparison::referenceStdDev(OutputData* sigma)
{
	m_referenceStdDev=sigma;
}

void ZScoresComparison::testInput(OutputData* input)
{
	m_input=input;
}

void ZScoresComparison::update()
{
	if (!m_referenceMean)
	{
		cout << "ZScoresComparison::update() called but no reference mean provided" << endl;
		return;
	}

	if (!m_referenceStdDev)
	{
		cout << "ZScoresComparison::update() called but no reference std. deviation provided" << endl;
		return;
	}

	if (!m_input)
	{
		cout << "ZScoresComparison::update() called but no input provided" << endl;
		return;
	}


	if (SpatialMap<float>* mu = dynamic_cast<SpatialMap<float>*>(m_referenceMean))
	{
		SpatialMap<float>* sigma = dynamic_cast<SpatialMap<float>*>(m_referenceStdDev);
		SpatialMap<float>* x = dynamic_cast<SpatialMap<float>*>(m_input);

		if (!sigma)
			cout << "ZScoresComparison::update() called but provided reference standard deviation is not a recognized input type" << endl;
		else if (!x)
			cout << "ZScoresComparison::update() called but provided reference mean is not a recognized input type" << endl;
		else
		{
			SpatialMap<float>* z = x->clone();
			for(unsigned i=0;i<z->dim();++i)
				z->set(i,(x->get(i)-mu->get(i))/sigma->get(i));

			if (m_zScore)
				delete m_zScore;
			m_zScore=z;
		}
	}
	else
	{
		cout << "ZScoresComparison::update() called but provided reference mean is not a recognized input type" << endl;
		return;
	}
}

OutputData* ZScoresComparison::outputZ() const
{
	return m_zScore;
}



