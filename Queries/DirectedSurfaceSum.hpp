#ifndef QUERIES_DIRECTEDSURFACESUM_HPP_
#define QUERIES_DIRECTEDSURFACESUM_HPP_

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>

#include <array>
#include <vector>
#include <map>

/**
 * This class is used to find the sum of all photon weight entering and exiting
 * a set of tracked surfaces in a mesh.
 */
class DirectedSurfaceSum {
public:
    void 		    input(OutputDataCollection* C) { m_collection = C; }
	void 		    update();

    float           totalEnteringWeight(unsigned int surfaceIdx);
    float           totalExitingWeight(unsigned int surfaceIdx);

private:
    // the collection of output data
    OutputDataCollection*       m_collection;

    // maps region id (index) to total entering photon weight
    std::vector<float>          m_surfaceEnteringWeight;

    // maps region id (index) to total exiting photon weight
    std::vector<float>          m_surfaceExitingWeight;
};

#endif
