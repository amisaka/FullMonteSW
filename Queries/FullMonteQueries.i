#if defined(SWIGTCL)
%module FullMonteQueriesTCL
#elif defined(SWIGPYTHON)
%module Queries
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"
%include "../Geometry/FullMonteGeometry_types.i"

%{
#include "EnergyToFluence.hpp"
#include "VolumeAccumulationProbe.hpp"
#include "LayeredAccumulationProbe.hpp"
#include "SurfaceAccumulationProbe.hpp"
#include "AccumulationLineQuery.hpp"
#include "SpatialMapOperator.hpp"
#include "DirectedSurfaceSum.hpp"
#include "DoseHistogramGenerator.hpp"
#include "DoseHistogramCollection.hpp"
#include "DoseHistogram.hpp"
#include "DoseVolumeHistogramGenerator.hpp"
#include "DoseSurfaceHistogramGenerator.hpp"
#include "../OutputTypes/OutputData.hpp"
#include "Rescale.hpp"
#include "BasicStats.hpp"
#include "DataSetStats.hpp"
#include "AbsorptionSum.hpp"
%}

#if defined(SWIGPYTHON)
    %rename("fm_%s", match$name="type") "";
	%rename("fm_%s", match$name="input") "";
	%rename("fm_%s", match$name="sum") "";
	%rename("fm_%s", match$name="min") "";
	%rename("fm_%s", match$name="max") "";
#endif

%include "../OutputTypes/OutputData.hpp"
%include "EnergyToFluence.hpp"
%include "DoseHistogramCollection.hpp"
%include "VolumeAccumulationProbe.hpp"
%include "LayeredAccumulationProbe.hpp"
%include "SurfaceAccumulationProbe.hpp"
%include "AccumulationLineQuery.hpp"
%include "SpatialMapOperator.hpp"
%include "DirectedSurfaceSum.hpp"
%include "DoseHistogramGenerator.hpp"
%include "DoseHistogram.hpp"
%include "DoseVolumeHistogramGenerator.hpp"
%include "DoseSurfaceHistogramGenerator.hpp"
%include "AbsorptionSum.hpp"

%template(SpatialMapF) SpatialMap<float>;
%include "Rescale.hpp"
%include "DataSetStats.hpp"
%include "BasicStats.hpp" 
