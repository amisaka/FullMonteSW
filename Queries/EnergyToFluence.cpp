/*
 * FluenceConverter.cpp
 *
 *  Created on: Mar 6, 2016
 *      Author: jcassidy
 */

#include <cmath>


#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/DirectedSurface.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <FullMonteSW/Kernels/MCKernelBase.hpp>

#include "EnergyToFluence.hpp"

using namespace std;

EnergyToFluence::EnergyToFluence()
{
}

EnergyToFluence::~EnergyToFluence()
{
}

// Set the conversion parameters:
// What are we expecting as input? (usually photon_weight)
// What do we want as output?
void EnergyToFluence::inputPhotonWeight()
{
	m_requestedInputField = PhotonWeight;
}

void EnergyToFluence::inputEnergy()
{
	m_requestedInputField = Energy;
}

void EnergyToFluence::inputFluence()
{
	m_requestedInputField = Fluence;
}

void EnergyToFluence::inputEnergyPerVolume()
{
	m_requestedInputField = EnergyPerVolume;
}

// Same for the output
// What units do we want as output?
void EnergyToFluence::outputPhotonWeight()
{
	m_requestedOutputField = PhotonWeight;
}

void EnergyToFluence::outputEnergy()
{
	m_requestedOutputField = Energy;
}

void EnergyToFluence::outputFluence()
{
	m_requestedOutputField = Fluence;
}

void EnergyToFluence::outputEnergyPerVolume()
{
	m_requestedOutputField = EnergyPerVolume;
}

// Populate all fields required for the EnergyToFluence calculations

void EnergyToFluence::kernel(const MCKernelBase* K)
{
	m_kernel = K;
	m_materials = m_kernel->materials();
	m_geometry = m_kernel->geometry();
	m_simulatedPackets = m_kernel->packetCount();
	m_simulatedEnergy = m_kernel->energyPowerValue();
}

const MCKernelBase* EnergyToFluence::kernel() const
{
	return m_kernel;
}

void EnergyToFluence::data(OutputData* M)
{
	m_input = M;
}

const OutputData* EnergyToFluence::data() const
{
	return m_input;
}

void EnergyToFluence::updateMeasures()
{
	// TODO: try to reuse
	if(m_measure)
		delete m_measure;
	m_measure=nullptr;
	m_measureType=UnknownMeasureType;


	/// Various casts of geometry
	const TetraMesh* mesh=nullptr;
	if (m_geometry)
		mesh=dynamic_cast<const TetraMesh*>(m_geometry);


	/** With DirectedSurface input, there are two arrays (entering/exiting) that correspond to cells that are a permuted subset of
	 * the underlying geometry.
	 */

	if (const DirectedSurface* ds=dynamic_cast<const DirectedSurface*>(m_input))
	{
		// variable input is never used so uncommenting for now
		//SpatialMap<float>* input = ds->entering();
		m_measureType=Area;

		m_measure = new SpatialMap<float>();
		m_measure->dim(ds->permutation()->dim());

		if (!mesh)
		{
			LOG_ERROR << "EnergyToFluence::updateMeasures() failed for lack of a TetraMesh geometry" << endl;
			return;
		}

		LOG_DEBUG << "EnergyToFluence::updateMeasures() taking area for DirectedSurface subset of faces" << endl;

		for(unsigned i=0;i<ds->permutation()->dim();++i)
			m_measure->set(
					i,
					get(area,*mesh,TetraMesh::DirectedFaceDescriptor(ds->permutation()->get(i))));
	}
	else if (const AbstractSpatialMap* i=dynamic_cast<const AbstractSpatialMap*>(m_input))
	{
		unsigned new_dim;
		switch(i->spatialType())
		{
		case AbstractSpatialMap::Surface:
			m_measureType = Area;
			m_measure = m_geometry->surfaceAreas();
			LOG_DEBUG << "EnergyToFluence::updateMeasures() taking area measure" << endl;
			break;
		case AbstractSpatialMap::Volume:
			m_measureType=Volume;
			m_measure = m_geometry->elementVolumes();
			if (m_measure->get(0) == 0)
			{
				new_dim = m_measure->dim()-1;
				for(unsigned j=0; j<new_dim; ++j)
					m_measure->set(j, m_measure->get(j+1));
				m_measure->dim(new_dim);
			}
			LOG_DEBUG << "EnergyToFluence::updateMeasures() taking volume measure" << endl;
			break;
		default:
			m_measureType=UnknownMeasureType;
			LOG_ERROR << "EnergyToFluence::updateMeasures() failed because spatial type (area/volume/...) of input data is not recognized." << endl;
			return;
		}
	}
}

void EnergyToFluence::updateMaterials()
{
	if (m_muA)
		delete m_muA;
	m_muA=nullptr;

	if (!m_materials)
	{
		LOG_ERROR << "EnergyToFluence::updateMaterials() failed for lack of materials" << endl;
	}
	else if (!m_geometry)
	{
		LOG_ERROR << "EnergyToFluence::updateMaterials() failed for lack of geometry" << endl;
	}
	else if (!m_geometry->regions())
	{
		LOG_ERROR << "EnergyToFluence::updateMaterials() failed for lack of geometry region assignments" << endl;
	}
	else
	{
		LOG_DEBUG << "EnergyToFluence::updateMaterials() fetching muA" << endl;

		// We are resizing the SpatialMap here and adpating the code below because we are removing the zeroTetra from the output mesh.
		// Hopefully, there is no way to bypass the zeroTetra insertion. Otherwise we have to catch that here
		m_muA = new SpatialMap<float>(m_geometry->regions()->size()-1, AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar);

		// copy absorption coefficients
		for(unsigned i=0;i<m_geometry->regions()->size()-1;++i)
		{
			if (m_materials->size() <= m_geometry->regions()->get(i+1))
			{
				LOG_ERROR   << "EnergyToFluence::updateMaterials() trying to access a material  with index" << m_geometry->regions()->get(i)
                            << ". However, there are only " << m_materials->size() << " materials [0 - " << m_materials->size()-1 << "] defined."
				            << "Check if the layers in your mesh file and the materials (excluding 'air') defined in your .tcl file match." << endl;
				throw std::logic_error("EnergyToFluence::updateMaterials() trying to access non-existant material");
			}
			m_muA->set(
					i,
					m_materials->get(m_geometry->regions()->get(i+1))->absorptionCoeff());
		}
			
	}
}

void EnergyToFluence::multiplyFieldBy(SpatialMap<float>* x,const SpatialMap<float>* k,int power)
{
	if (!x || !k)
		throw std::logic_error("EnergyToFluence::multiplyFieldBy(x,k,power) null input on x or k");
	else if (x->dim() != k->dim())
		throw std::logic_error("EnergyToFluence::multiplyFieldBy(x,k,power) dimension mismatch");
	else if (power == 0)
		return;
	else if (power != 1 && power != -1)
		throw std::logic_error("EnergyToFluence::multiplyFieldBy(x,k,power) power is not +-1");

	for(unsigned i=0;i<x->dim();++i)
	{
		float xi = x->get(i);
		float ki = k->get(i);

		if (isnan(xi))
		{
			LOG_WARNING << "EnergyToFluence::multiplyFieldBy(x,k,power) NaN value for element " << i << " being set to zero" << endl;
			x->set(i,0.0f);
		}
		else if (xi == 0)
			x->set(i,0.0f);
		else if (power == -1)
		{
			if (ki == 0)
			{
				LOG_WARNING << "EnergyToFluence::multiplyFieldBy(x,k,power) element " << i << " would result in divide-by-zero, setting output to zero instead" << endl;
				x->set(i,0.0f);
			}
			else
				x->set(i,xi/ki);
		}
		else if (power == 1)
			x->set(i,xi*ki);
	}
}

SpatialMap<float>* EnergyToFluence::calculate(const SpatialMap<float>* input,Powers op)
{
	////// Check inputs & mode settings

	if (!op.muA)					// muA doesn't affect output
	{
	}
	else if (!m_muA)
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed for lack of material information" << endl;
		return nullptr;
	}
	else if (m_muA->dim() != input->dim())
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed due to dimension mismatch of input (" << input->dim() << ") vs material information (" << m_muA->dim() << ")" << endl;
		return nullptr;
	}


	if (!op.measure)			// measure (area/volume) doesn't affect output
	{
	}
	else if (!m_measure)
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed for lack of measure (area/volume) information" << endl;
		return nullptr;
	}
	else if (m_measureType == UnknownMeasureType)
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed due to unknown measure type (not clear if input is a surface or a volume)" << endl;
		return nullptr;
	}
	else if (
			(m_measureType == Volume && input->spatialType() != AbstractSpatialMap::Volume) ||
			(m_measureType == Area && input->spatialType() != AbstractSpatialMap::Surface))
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed due to mismatch of input data spatial type and measure type" << endl;
		return nullptr;
	}
	else if (m_measure->dim() != input->dim())
	{
		LOG_ERROR << "EnergyToFluence::calculate() failed due to dimension mismatch between measure vector (" << m_measure->dim() <<") and data vector (" << input->dim() << ")" << endl;
		return nullptr;
	}

	SpatialMap<float>* o = input->clone();

	if (op.muA)
	{
		if (op.muA == -1)
			LOG_DEBUG << "Dividing by muA" << endl;
		else if (op.muA == 1)
			LOG_DEBUG << "Multiplying by muA" << endl;
		else
		{
			LOG_ERROR << "EnergyToFluence::calculate(i,op) error: unexpected value of op.measure (" << op.measure << ")" << endl;
			throw std::logic_error("EnergyToFluence::calculate(i,op): invalid measure power in op");
		}

		multiplyFieldBy(o,m_muA,op.muA);
	}

	if (op.measure)
	{
		if (op.measure == 1)
			LOG_DEBUG << "Multiplying by measure" << endl;
		else if (op.measure == -1)
			LOG_DEBUG << "Dividing by measure" << endl;
		else
		{
			LOG_ERROR << "EnergyToFluence::calculate(i,op) error: unexpected value of op.measure (" << op.measure << ")" << endl;
			throw std::logic_error("EnergyToFluence::calculate(i,op): invalid measure power in op");
		}

		multiplyFieldBy(o,m_measure,op.measure);
	}

	if (op.energyUnit == 1)
	{
		LOG_DEBUG << "Multiplying by energy scaling factor to get actual energy unit" << endl;
		if (m_simulatedEnergy != 0 && m_simulatedPackets != 0)
		{
			for(unsigned i=0;i<o->dim();++i)
			{
				float oi = o->get(i);
				o->set(i,oi*m_simulatedEnergy/m_simulatedPackets);
			}
		}
		else
		{
			LOG_ERROR << "EnergyToFluence::calculate(i,op) error: invalid value of m_simulatedEnergy (" << op.measure << ") or m_simulatedPackets (" << m_simulatedPackets << ")" << endl;
			throw std::logic_error("EnergyToFluence::calculate(i,op): invalid energy scaling parameters");
		}
		
	}
	else if (op.energyUnit == -1)
	{
		LOG_DEBUG << "Dividing by energy scaling factor to get photon weight" << endl;
		if (m_simulatedEnergy != 0 && m_simulatedPackets != 0)
		{
			for(unsigned i=0;i<o->dim();++i)
			{
				float oi = o->get(i);
				o->set(i,oi*m_simulatedPackets/m_simulatedEnergy);
			}
		}
		else
		{
			LOG_ERROR << "EnergyToFluence::calculate(i,op) error: invalid value of m_simulatedEnergy (" << op.measure << ") or m_simulatedPackets (" << m_simulatedPackets << ")" << endl;
			throw std::logic_error("EnergyToFluence::calculate(i,op): invalid energy scaling parameters");
		}
		
	}
	else
	{
		LOG_DEBUG << "Energy scaling factor is either 0 or not valid -> Doing nothing" << endl;
	}
	
	switch (m_requestedOutputField)
	{
		case Fluence:
			o->outputType(AbstractSpatialMap::Fluence);
			break;
		case Energy:
			o->outputType(AbstractSpatialMap::Energy);
			break;
		case EnergyPerVolume:
			o->outputType(AbstractSpatialMap::EnergyPerVolume);
			break;
		case PhotonWeight:
			o->outputType(AbstractSpatialMap::PhotonWeight);
			break;
		default:
			o->outputType(AbstractSpatialMap::UnknownOutputType);
			break;
	}		

	return o;
}


EnergyToFluence::Powers EnergyToFluence::powersForField(Field f,MeasureType mt)
{
	Powers p{0,0,0};
	switch(f)
	{
	case Fluence:
		p.muA = mt==Area ? 0 : -1;
		p.measure = -1;
		p.energyUnit = 1;
		break;

	case Energy:
		p.energyUnit = 1;
		break;

	case EnergyPerVolume:
		p.measure = -1;
		p.energyUnit = 1;
		break;
	
	case PhotonWeight:
		break;

	default:
		LOG_ERROR << "EnergyToFluence::powersForField() passed an invalid field type" << endl;
	}
	return p;
}

const char* EnergyToFluence::measureTypeName(MeasureType mt)
{
	switch(mt)
	{
	case Area:
		return "Area";
	case Volume:
		return "Volume";
	default:
		return "<<ERROR>>";
	}
}

const char* EnergyToFluence::fieldName(Field f)
{
	switch(f)
	{
	case UnknownField:
		return "Unknown";
	case Fluence:
		return "Fluence";
	case Energy:
		return "Energy";
	case EnergyPerVolume:
		return "Energy/Volume";
	case PhotonWeight:
		return "PhotonWeight";
	default:
		return "<<ERROR>>";
	}
}

void EnergyToFluence::update()
{
	delete m_output;
	m_output=nullptr;

	const SpatialMap<float>* iSpatialMap = dynamic_cast<const SpatialMap<float>*>(m_input);
	const DirectedSurface* iDirectedSurface = dynamic_cast<const DirectedSurface*>(m_input);

	MeasureType measureType=UnknownMeasureType;

	/// Check inputs
	if (!m_input)
	{
		LOG_ERROR << "EnergyToFluence::update() no input data provided";
		return;
	}
	else if (iSpatialMap)
	{
		switch(iSpatialMap->spatialType())
		{
		case AbstractSpatialMap::Volume:
			measureType = Volume; break;
		case AbstractSpatialMap::Surface:
			measureType = Area; break;
		default:
		    LOG_ERROR << "EnergyToFluence::update() invalid input data spatial type" << endl;
			return;
		}

	}
	else if (iDirectedSurface)
	{
		measureType = Area;
	}
	else
	{
		LOG_ERROR << "EnergyToFluence::update() input data cannot be cast to a known type (SpatialMap<float> or DirectedSurface)" << endl;
		return;
	}

	if (m_requestedInputField == UnknownField)
	{
		switch(m_input->outputType())
		{
			case OutputData::PhotonWeight:
				m_requestedInputField = PhotonWeight;
				break;
			case OutputData::Energy:
				m_requestedInputField = Energy;
				break;
			case OutputData::Fluence:
				m_requestedInputField = Fluence;
				break;
			case OutputData::EnergyPerVolume:
				m_requestedInputField = EnergyPerVolume;
				break;
			default:
				if(m_requestedInputField == UnknownField)
				{
					LOG_ERROR << "EnergyToFluence::update() unknown input data output type" << endl;
					return;
				}
		}
	}

	LOG_INFO << "EnergyToFluence::update() info for geometry type " << measureTypeName(measureType) << endl;
	LOG_INFO << "  Input field " << fieldName(m_requestedInputField) << endl;
	LOG_INFO << "  Output field " << fieldName(m_requestedOutputField) << endl;

	// compute what needs to happen for the conversion
	Powers pIn = powersForField(m_requestedInputField,measureType);
	Powers pOut = powersForField(m_requestedOutputField,measureType);

	Powers pMult = pOut-pIn;

	LOG_DEBUG << "  muA^(" << pIn.muA << ") in, muA^(" << pOut.muA << ") out, so multiply by muA^(" << pMult.muA << ")" << endl;
	LOG_DEBUG << "  measure^(" << pIn.measure << ") in, measure^(" << pOut.measure << ") out, so multiply by measure^(" << pMult.measure << ")" << endl;
	LOG_DEBUG << "  energyUnit^(" << pIn.energyUnit << ") in, energyUnit^(" << pOut.energyUnit << ") out, so multiply by energyUnit^(" << pMult.energyUnit << ")" << endl;


	// get measure (area/volume) information if needed
	if (pMult.measure)
		updateMeasures();
	else
		LOG_DEBUG  << "  Skipping measure update (not needed)" << endl;


	// get material information if needed
	if (pMult.muA)
		updateMaterials();
	else
		LOG_DEBUG << "  Skipping material update (not needed)" << endl;


	// check input type and handle appropriately
	if (const SpatialMap<float>* input = dynamic_cast<const SpatialMap<float>*>(m_input))
	{
		m_output = calculate(input,pMult);
	}
	else if (const DirectedSurface* ds = dynamic_cast<const DirectedSurface*>(m_input))
	{
		DirectedSurface* o = new DirectedSurface();
		o->entering(calculate(ds->entering(),pMult));
		o->exiting(calculate(ds->exiting(),pMult));
		o->permutation(ds->permutation());
		m_output=o;
	}
}

OutputData* EnergyToFluence::result() const
{
	return m_output;
}
