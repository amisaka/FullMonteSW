/*
 * AbstractDataDifference.cpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include "DataDifference.hpp"
#include <iostream>

using namespace std;

AbstractDataDifference::AbstractDataDifference()
{
}

AbstractDataDifference::~AbstractDataDifference()
{
}

void AbstractDataDifference::left(OutputData* l)
{
	m_left=l;
}

OutputData* AbstractDataDifference::left() const
{
	return m_left;
}

void AbstractDataDifference::right(OutputData* r)
{
	m_right=r;
}

OutputData* AbstractDataDifference::right() const
{
	return m_right;
}

bool AbstractDataDifference::typeCheck() const
{
	return true;
}

bool AbstractDataDifference::dimCheck() const
{
	return true;
}


void AbstractDataDifference::update()
{
	if (!m_left)
	{
		LOG_ERROR << "AbstractDataDifference::update() called but no left data set" << endl;
	}
	else if (!m_right)
	{
		LOG_ERROR << "AbstractDataDifference::update() called but no right data set" << endl;
	}
	else if (!typeCheck())
	{
		LOG_ERROR << "AbstractDataDifference::update() called with incompatible types '" <<
				m_left->type()->name() << "' and '" <<
				m_right->type()->name() << "'" << endl;
	}
	else if (!dimCheck())
	{
		LOG_ERROR << "AbstractDataDifference::update() called with incompatible data dimensions" << endl;
	}
	else
		doUpdate();
}

