/*
 * ConservationCheck.cpp
 *
 *  Created on: Jun 27, 2017
 *      Author: jcassidy
 */

#include <FullMonteSW/OutputTypes/MCConservationCounts.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <iostream>

using namespace std;

ConservationCheck::ConservationCheck()
{

}

ConservationCheck::~ConservationCheck()
{

}

void ConservationCheck::events(MCConservationCountsOutput* C)
{
	m_counts=C;
}

bool ConservationCheck::pass() const
{
	return m_errors.size() == 0;
}

void ConservationCheck::update()
{
	for(const auto e : m_errors)
		delete e;
	m_errors.clear();

	if (!m_counts)
	{
		LOG_ERROR << "ConservationCheck::update() called but no counts provided to check" << endl;
		return;
	}

	// check total disposition for the weight
    double w_dispose = m_counts->w_absorb+m_counts->w_exit+m_counts->w_time+m_counts->w_abnormal+m_counts->w_noHit+m_counts->w_special_absorb;
    double w_diff = w_dispose-m_counts->w_launch;

	if (w_diff > m_counts->w_launch*m_maxDisposeDiff)
	{
		m_errors.push_back(new Error(""));
	}

	// check roulette balance
	double rouletteBal = w_counts->w_roulette - m_counts->w_die;

	if (abs(rouletteBal) > m_maxRouletteBal*m_counts->w_launch)
	{
		stringstream ss;
		ss << "";
		m_errors.push_back(new Error(ss.str()));
	}


	if (m_counts->w_abnormal > m_counts->w_launch*m_maxAbnormalFrac)
	{
		stringstream ss;
		ss << "";
		m_errors.push_back(new Error(ss.str()));
	}

	if (m_counts->w_noHit > m_counts->w_launch*m_maxNoHitFrac)
	{
		stringstream ss;
		ss << ""
		m_errors.push_back(new Error(ss.str()));
	}


}

void ConservationCheck::print(std::ostream& os) const
{
    double w_dispose = m_counts->w_absorb+m_counts->w_exit+m_counts->w_time+m_counts->w_special_absorb;
    double w_diff = w_dispose-m_counts->w_launch;

    os << "Energy conservation check" << endl;
    os << "  Launched: " << m_counts->w_launch << endl;
    os << "  Disposed: " << w_dispose << endl;
    os << "    Absorbed   " << m_counts->w_absorb << endl;
    os << "    Exited     " << m_counts->w_exit << endl;
    os << "    Time gated " << m_counts->w_time << endl;
    os << "    Special absorbing interface: " << m_counts->w_special_absorb << endl;
    os << "  Difference: " << w_diff << " (" << 100.0*w_diff/m_counts->w_launch << "%)" << endl;
    os << endl;

    os << "Roulette difference " << m_counts->w_roulette-m_counts->w_die << endl;
    os << "  Died " << m_counts->w_die << endl;
    os << "  Added " << m_counts->w_roulette << endl << endl;

    os << "Abnormal termination (total " << m_counts->w_abnormal+m_counts->w_nohit << ')' << endl;
    os << "  No intersection: " << m_counts->w_nohit << endl;
    os << "  Excessive steps: " << m_counts->w_abnormal << endl;
}
