/*
 * Permute.hpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_PERMUTE_HPP_
#define QUERIES_PERMUTE_HPP_

class Permutation;
class OutputData;

/** Applies or inverts permutations.
 *
 * i [0,Np)	is the permutation size
 * j [0,N)	is the size of the original array
 *
 * Inputs
 * 		a Permutation
 * 		an input data array (x[j] in forward mode, y[i] in inverse mode)
 *
 * Output
 * 		an output data array permuted from the input (y[i] in forward mode, z[j] in inverse mode)
 *
 * In forward mode, Permute accepts an input x[j] and produces a new array with y[i] = x[p[i]]
 * In inverse mode, accepts and input y[i] and produces an array z[j] such that z[p[i]] = y[i]
 *
 * NOTES ON INVERSE
 *
 * There may be elements of z[j] that are not specified by the definition above.
 * It is also possible to construct a y[i] such that the definition breaks (though not by using Permute)
 *
 * 		y[] = 10 11
 * 		p[] =  1  1
 *
 * 		z[0]		is undefined since input element zero is discarded and cannot be recovered
 *
 * 		z[1] 	is in a conflict: needs to be 10 to satisfy z[p[0]] = y[0] = 10, but also z[p[1]] = y[1] = 11 since p[0]=p[1]=1
 *
 * In such a case, the _last_ value (11) in the permutation is taken.
 *
 */

class Permute
{
public:
	Permute();
	virtual ~Permute();

	void				permutation(Permutation* p);		///< Set/get the permutation
	Permutation*		permutation() 		const;
	Permutation* 	inversePermutation() const;

	void				input(OutputData* o);			///< Set the input data

	void				inverseMode(bool inv);			///< Set inverse mode on or off

	void				update();						///< Compute

	OutputData*		output();						///< Provide output

private:
	OutputData*		m_input=nullptr;
	Permutation*		m_permutation=nullptr;
	Permutation*		m_inverse=nullptr;

	bool				m_inverseMode=false;

	OutputData*		m_output=nullptr;
};

#endif /* QUERIES_PERMUTE_HPP_ */
