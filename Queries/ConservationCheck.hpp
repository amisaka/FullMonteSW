/*
 * ConservationCheck.hpp
 *
 *  Created on: Jun 27, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_CONSERVATIONCHECK_HPP_
#define QUERIES_CONSERVATIONCHECK_HPP_

class MCConservationCountsOutput;

class ConservationCheck
{
public:
	ConservationCheck();
	~ConservationCheck();

	void counts(MCConservationCountsOutput* E);
	void update();

	bool pass() const;

	void print(std::ostream& os=std::cout) const;

private:
	MCConservationCountsOutput		*m_counts=nullptr;
};



#endif /* QUERIES_CONSERVATIONCHECK_HPP_ */
