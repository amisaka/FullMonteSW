#include <FullMonteSW/Queries/LayeredAccumulationProbe.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Geometry/RayWalk.hpp>

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

#include <functional>
#include <vector>
#include <fstream>
#include <iomanip>

LayeredAccumulationProbe::LayeredAccumulationProbe()
{

}

LayeredAccumulationProbe::~LayeredAccumulationProbe()
{
    delete m_filemap;
}

void LayeredAccumulationProbe::input_filename(std::string fn)
{
    m_filename_in = fn;

    ifstream infile;
	char tetraData[150];
    // Expected file structure (example):
    // Line1: Fluence 1 48000 float
    // Line2-x: 1.0 1.0 1.0 ...
    infile.open(m_filename_in);
    // The first entry of the file gives information on the type of data that is being loaded e.g. Fluence or Energy
    // This directly impacts the calculations we will perform for the accumulation
    string type;
    infile >> type;
    AbstractSpatialMap::OutputType dataType; // {UnknownOutputType=-1, PhotonWeight=0, Energy, Fluence, Energy/Volume }
    if (type.compare("Fluence") == 0)
    {
        dataType = AbstractSpatialMap::Fluence;
    }
    else if (type.compare("Energy") == 0)
    {
        dataType = AbstractSpatialMap::Energy;
    }
    else if (type.compare("PhotonWeight") == 0)
    {
        dataType = AbstractSpatialMap::PhotonWeight;
    }
    else
    {
        LOG_ERROR << "Could not identify type of data. This probe expects fluence, energy or raw photon weight data!" << endl;
        ofstream os("error.log");
        os << "Wrong_OutputType_Error -9";
        return;
    }
    
    string unit;
    // Ignore the second entry of the input file because it only provides the units of the input file (we do not need them for the calculations)
    infile >> unit;
    // Ignore the third entry of the input file because it only provides the number of different dates in the file (we know it is 1 in our case)
    infile >> tetraData;
    // The next entry provides the size of the input data. We can use this to allocate memory of our vector (faster than always using the push_back method)
    unsigned size;
    infile >> size;
    //unsigned size = atoi(tetraData);
    vector<float>data_in(size);
    // The next and last entry of the first line gives information on the type of data (usually float)
    infile >> tetraData;

    for(unsigned i=0; i < size; i++)
    {
        if(infile.eof())
        {
            LOG_ERROR << "Size of input extracted from file does not match the contents of the file!" << endl;
            ofstream os("error.log");
            os << "Input_File_Error -10";
            return;
        }
        infile >> data_in[i];
        //data_in[i] = atof(tetraData);
    }
    m_filemap = new SpatialMap<float>(std::move(data_in), AbstractSpatialMap::Volume, AbstractSpatialMap::Scalar, dataType);
	m_filemap->name("VolumeEnergy");
    m_input = m_filemap;
}

/**
 * Updates the 'total' of the probe based on the input parameters
 */
void LayeredAccumulationProbe::update() {
    // Delete old entries in m_bin and m_binVolume
    m_bin.clear();
    m_binVolume.clear();

    if(m_filename_out.empty()) {
        LOG_ERROR << "LayeredAccumulationProbe::update() No output file specified\n";
        ofstream os("error.log");
        os << "No_Output_File_Error 0";
        return;
    }
    // Opening files here. If there is only one negatvie value in them, we know an error has occured
    ofstream os(m_filename_out.c_str());
    
    std::string str = m_filename_out;
    str.append("_detailed");
    ofstream os_detailed(str.c_str());
    //// checking inputs to make sure they are valid
    
    // input data must be SpatialMap<float>
    if(!m_input) {
        LOG_ERROR << "LayeredAccumulationProbe::update() no input data provided\n";
        os << "No_Input_Error -1";
        os_detailed << "No_Input_Error -1";
        return;
    }
    
    // get SpatialMap<float> data
    const SpatialMap<float>* in_smap = dynamic_cast<const SpatialMap<float>*>(m_input);

    // check we can get this data
    if(!in_smap) {
        LOG_ERROR << "LayeredAccumulationProbe::update() input data could not be cast to SpatialMap<float>\n";
        os << "SpatialMap_Error -2";
        os_detailed << "SpatialMap_Error -2";
        return;
    } else if(in_smap->spatialType() != AbstractSpatialMap::Volume) {
        LOG_ERROR << "LayeredAccumulationProbe::update() input data must be AbstractSpatialMap::Volume\n";
        os << "SpatialMap_Volume_Error -3";
        os_detailed << "SpatialMap_Volume_Error -3";
        return;
    }
    if (in_smap->outputType() == AbstractSpatialMap::UnknownOutputType)
    {
        LOG_ERROR << "Could not identify type of data. This probe expects fluence, energy or raw photon weight data!" << endl;
        os << "SpatialMap_OutputType_Error -4";
        os_detailed << "SpatialMap_OutputType_Error -4";
        return;
    }

    // ensure geometry is defined
    if(!m_geometry) {
        LOG_ERROR << "LayeredAccumulationProbe::update() no geometry provided\n";
        os << "No_Geometry_Error -5";
        os_detailed << "No_Geometry_Error -5";
        return;
    }

    // get the TetraMesh geometry
    const TetraMesh* mesh = dynamic_cast<const TetraMesh*>(m_geometry);

    // geometry must be TetraMesh
    if(!mesh) {
        LOG_ERROR << "LayeredAccumulationProbe::update() could not convert geometry to TetraMesh\n";
        os << "TetraMesh_Error -6";
        os_detailed << "TetraMesh_Error -6";
        return;
    }

    // must have 1 input value per tetra (-1 is for the 0 tetra automatically inserted by FullMonte, which is not present in the data)
    if((mesh->tetraCells()->size() - 1) != in_smap->dim()) {
        LOG_ERROR << "LayeredAccumulationProbe::update() input must have same number of entries as tetras in the mesh\n";
        os << "Size_Mismatch_Error -7";
        os_detailed << "Size_Mismatch_Error -7";        
        return;
    }
    
    if(m_p0_init == false)
    {
        LOG_ERROR << "LayeredAccumulationProbe::update() origin point not defined\n";
        os << "No_Origin_Error -8";
        os_detailed << "No_Origin_Error -8";        
        return;
    }

    if(m_p1_init == false && m_shape == Cylinder)
    {
        LOG_WARNING << "LayeredAccumulationProbe::update() second point for cylinder not defined. Reverting to spherical shape\n";
        m_shape = Sphere;
    }

    if(m_increment <= 0.0)
    {
        LOG_WARNING << "LayeredAccumulationProbe::update() m_increment <= 0 is not defined. Taking absolute value.\n";
        m_increment = abs(m_increment);
    }
    
    if(m_r < 0.0) {
        LOG_WARNING << "LayeredAccumulationProbe::update() radius < 0 is not defined. Taking absolute value.\n";
        m_r = abs(m_r);
    }

    Vector<3, float> slope;
    Vector<3, float> n;
    float normL2 = 0;
    // Calculate the slope the two points define to create the line that later forms the cylinder
    if(m_shape == Cylinder)
    {
        slope[0] = m_p1[0] - m_p0[0];
        slope[1] = m_p1[1] - m_p0[1];
        slope[2] = m_p1[2] - m_p0[2];
        //  Calculate the plane normal for the cylinder planes at both ends
        normL2 = sqrt( pow(slope[0], 2) + pow(slope[1], 2) + pow(slope[2], 2) ); 
        n[0] = slope[0] / normL2;
        n[1] = slope[1] / normL2;
        n[2] = slope[2] / normL2;
        // Determine the number of bins that are needed to sort data based on the desired increment.
        m_bin.resize( ceil(normL2/m_increment) );
         for (unsigned i=0; i < m_bin.size(); i++)
                m_bin[i].clear();

        if (in_smap->outputType() == AbstractSpatialMap::Fluence)
        {
            m_binVolume.resize( ceil(normL2/m_increment) );
            for (unsigned i=0; i < m_binVolume.size(); i++)
                m_binVolume[i].clear();
        }
    }
    else // m_shape == Sphere
    {
        // Determine the number of bins that are needed to sort data based on the desired increment.
        m_bin.resize( ceil(m_r/m_increment) );
        if (in_smap->outputType() == AbstractSpatialMap::Fluence)
        {
            m_binVolume.resize( ceil(m_r/m_increment) );
            for (unsigned i=0; i < m_binVolume.size(); i++)
                m_binVolume[i].clear();
        }
    }
    // Get all volume data for each tetrahedron of the mesh
    const SpatialMap<float>* tetraVolumes = mesh->elementVolumes();
    if(m_r > 0) {
        // Accumulate all data in tetras in the probe (either fully or partially)
        for(unsigned t = 1; t < mesh->tetraCells()->size(); t++) {
            // get the points that make up the tetra
            TetraByPointID IDps = mesh->tetraCells()->get(t);
            // determine if tetra is in (entirely or partially) in probe
            // iterate over each point of the tetra
            unsigned points_in_probe = 0;
            float height[4];
            for(unsigned i = 0; i < 4; i++) {
                // current point
                Point<3,double> p = mesh->points()->get(IDps[i]);
                float dist;
                
                switch(m_shape)
                {
                    //// Spherical probe ////
                    case Sphere:
                        // distance to center of sphere probe
                        dist = sqrt(pow(p[0]-m_p0[0], 2) + pow(p[1]-m_p0[1], 2) + pow(p[2]-m_p0[2], 2));
                        height[i] = dist;
                        // if distance to point is less than the spheres radius, the probe contains the tetra
                        if(dist <= m_r) {
                            points_in_probe++;
                        }
                    break;
                    //// Cylindrical probe ////
                    case Cylinder:
                        // Calculate the vector between the point in the mesh and the point that defines the 1st end of cylinder plane
                        Vector<3, float> p_p0;
                        p_p0[0] = (float) m_p0[0]-p[0];
                        p_p0[1] = (float) m_p0[1]-p[1];
                        p_p0[2] = (float) m_p0[2]-p[2];

                        // Calculate the vector between the point in the mesh and the point that defines the 2nd end of cylinder plane
                        Vector<3, float> p_p1;
                        p_p1[0] = (float) m_p1[0]-p[0];
                        p_p1[1] = (float) m_p1[1]-p[1];
                        p_p1[2] = (float) m_p1[2]-p[2];

                        // Calculate the dot-product of slope and the plane-point vector
                        // this calculates the cos(theta) between the plane normal and the point-planepoint
                        float updownPlane_p0 = dot(slope, p_p0);
                        float updownPlane_p1 = dot(slope, p_p1);

                        // we need to check if the current point in the mesh lies between the two planes defined by the two points of the cylinder
                        // We are using the same normal for both cylinder plane definitions. This means that a point is inside the cylinder planes, if the
                        // point is on the same side as the normal vector for one plane (> 0) and on the opposite side of the normal vector for the other plane (< 0)
                        // < 0 opposite side of normal vector
                        // > 0 same side of normal vector
                        // = 0 on the plane
                        // p0 < 0 && p1 <  0 -> not valid,    *  <--|    <--| point is on the opposite side of both planes  
                        // p0 <= 0 && p1 >= 0 -> valid,        |-->  *    <--| point is on the opposite side of p0 and on the same side of p1
                        // p0 >=  0 && p1 <=  0 -> valid,        |-->  *    <--| point is on the same side of p0 and on the opposite side of p1
                        // p1 >  0 && p1 > 0 -> not valid,    |-->    |-->  * point is on the same side of both planes
                        if( (updownPlane_p0 <= 0 && updownPlane_p1 >= 0) || (updownPlane_p0 >= 0 && updownPlane_p1 <= 0) )
                        {
                            // The cross product between the slope of the line and the vector between the current point and the origin point of the cylinder
                            // the cross product of these two vectors is equal to the area of the parallelogram created by these vectors
                            // A = cross(p_p0, slope)
                            //
                            //        slope
                            //      _________
                            //     /        /
                            //    /    A   /  p_p0
                            //   /________/
                            Vector<3, float> crossprod = cross(p_p0, slope);
                            // distance to center line of cylinder probe
                            // The area A can also be calculated by the length of one side * the height (distance) to the opposite parallel line of the parallelogram
                            //
                            //        slope
                            //      _________
                            //     /     |  /
                            //    /    d | /  vector p_p0
                            //   /_______|/
                            // A = |slope| * distance
                            // In order to calculate the distance, we need to solve for the distance d
                            // d = A / |slope|
                            dist = sqrt(dot(crossprod, crossprod)) / sqrt(dot(slope, slope));
                            height[i] = abs( dot(p_p0, n) );
                            // if distance to point is less than the spheres radius, the probe contains the tetra
                            if(dist <= m_r) 
                            {
                                points_in_probe++;
                            }
                        }
                    break;
                }
                    
            }

            // add the data of this tetra based on number of points in probe sphere
            // 2 cases:
            //  1) all points of tetras are in the probe
            //  2) any point of tetra is in probe and we count partial tetras
            if((points_in_probe == 4) || (points_in_probe > 0 && m_partialTets)) {
                float mean_height = (height[0] + height[1] + height[2] + height[3]) / 4.0;
                unsigned idx = floor(mean_height / m_increment);
                // make sure we don't overflow the maximum number of bins
                if (idx >= m_bin.size())
                    idx = m_bin.size()-1;

                pair<unsigned, float> binInput;
                
                if (in_smap->outputType() == AbstractSpatialMap::Fluence)
                {
                    // if in_smap contains fluence values, we need to eliminate the volume component in order
                    // to add the values -> phi = E/V/mu_a => phi*V = E/mu_a
                    // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                    float fluence = in_smap->get(t - 1);
                    float volume = tetraVolumes->get(t);
                    float fluence_x_volume = fluence * volume;
                    binInput = make_pair(t, fluence_x_volume);
                    m_bin[idx].push_back(binInput);
                    m_binVolume[idx].push_back(volume);
                }
                else
                {
                    // if in_smap contains energy values, we just need to accumulate them
                    // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                    float energy = in_smap->get(t - 1);
                    binInput = make_pair(t, energy);
                    m_bin[idx].push_back(binInput);
                }
            }
        }
    } 
    else 
    {
        switch(m_shape)
        {
        //// Single point probe (i.e. single tetra) ////
            case Sphere:
            {
                // Locate the tetra enclosing the point
                TetraMesh::TetraDescriptor t = mesh->rtree()->search(m_p0);
                m_bin.resize(1);
                m_binVolume.resize(1);
                // data for this tetra is all we want (single point probe)
                // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                pair<unsigned, float> binInput;
                if (in_smap->outputType() == AbstractSpatialMap::Fluence)
                {
                    float fluence = in_smap->get(t.value() - 1);
                    float volume = tetraVolumes->get(t.value());
                    float fluence_x_volume = fluence * volume;
                    binInput = make_pair(t.value(), fluence_x_volume);
                    m_bin[0].push_back(binInput);
                    m_binVolume[0].push_back(volume);
                }
                else
                {
                    binInput = make_pair(t.value(), in_smap->get(t.value() - 1));
                    m_bin[0].push_back(binInput);
                }
            break;
            }
        //// Line probe (i.e. tetra along a line) ////
            case Cylinder:
            {
                // each segment is a (length, IDt) pair specifying the tetras that make up the line source
		        std::vector<std::pair<float,unsigned>> segments;
                RayWalkIterator it=RayWalkIterator::init(*mesh, m_p0, n);
		        RayWalkIterator itEnd = RayWalkIterator::endAt(normL2);

                for(; it != itEnd; ++it)
			        segments.emplace_back(it->dToOrigin+it->lSeg,it->IDt);

                for(const auto& s : segments) 
                {
                    unsigned idx = floor(s.first / m_increment);
                    // make sure we don't overflow the maximum number of bins
                    if (idx >= m_bin.size())
                        idx = m_bin.size()-1;

                    pair<unsigned, float> binInput;
                    if (in_smap->outputType() == AbstractSpatialMap::Fluence)
                    {
                        // if in_smap contains fluence values, we need to eliminate the volume component in order
                        // to add the values -> phi = E/V/mu_a => phi*V = E/mu_a
                        // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                        float fluence = in_smap->get(s.second - 1);
                        float volume = tetraVolumes->get(s.second);
                        float fluence_x_volume = fluence * volume;
                        binInput = make_pair(s.second, fluence_x_volume);
                        m_bin[idx].push_back(binInput);
                        m_binVolume[idx].push_back(volume);
                    }
                    else
                    {
                        // if in_smap contains energy values, we just need to accumulate them
                        // the -1 is because the mesh has the 0 tetra added but the volume data does not have this tetra
                        float energy = in_smap->get(s.second - 1);
                        binInput = make_pair(s.second, energy);
                        m_bin[idx].push_back(binInput);
                    }
                }
            break;
            }
        }
    }
        
    if (in_smap->outputType() == AbstractSpatialMap::Fluence)
        os_detailed << "Fluence ";
    else
        os_detailed << "Energy ";
    
    os_detailed << "3 x " << m_bin.size() << std::endl
                << "TetraID Value Volume" << std::endl;

    for(unsigned i=0; i<m_bin.size(); i++)
    {
        os << "<"<< (i+1)*m_increment << ' ';
    }
    os << std::endl;
    for(unsigned i=0; i<m_bin.size(); i++)
    {
        os_detailed << std::endl << "<"<< (i+1)*m_increment << std::endl;;
        float sum = 0;
        float vol_sum = 0;
        for(unsigned j=0; j<m_bin[i].size(); j++)
        {
            if (in_smap->outputType() == AbstractSpatialMap::Fluence)
            {
                os_detailed << m_bin[i][j].first << ' ' << m_bin[i][j].second << ' ' << m_binVolume[i][j] << std::endl;
                sum += m_bin[i][j].second;
                vol_sum += m_binVolume[i][j];
            }
            else
            {
                os_detailed << m_bin[i][j].first << ' ' << m_bin[i][j].second << std::endl;
                sum += m_bin[i][j].second;
            }
        }
        if (vol_sum != 0)
            os << sum/vol_sum << ' ';
        else
            os << sum << ' ';
    }
}

