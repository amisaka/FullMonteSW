/*
 * DataDifference.hpp
 *
 *  Created on: Jun 29, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_DATADIFFERENCE_HPP_
#define QUERIES_DATADIFFERENCE_HPP_

class OutputData;

#include <FullMonteSW/OutputTypes/OutputDataType.hpp>

class AbstractDataDifference
{
public:
	AbstractDataDifference();
	virtual ~AbstractDataDifference();

	void		left(OutputData* l);
	OutputData*	left() const;

	void		right(OutputData* r);
	OutputData*	right() const;

	void		update();

	OutputData*	output();

protected:
	/// Check the types, returning true if OK
	virtual bool typeCheck() const;

	/// Check dimensions, returning true if OK
	virtual bool dimCheck() const;

	/// Actually do the update, after checking input data presence and type
	virtual void doUpdate()=0;

protected:
	OutputData*	m_output=nullptr;

private:
	OutputData*	m_left=nullptr;
	OutputData*	m_right=nullptr;


};


template<class T>class DataDifference : public AbstractDataDifference
{
public:
	using AbstractDataDifference::left;
	using AbstractDataDifference::right;

protected:
	virtual bool typeCheck() const override;
	T* left() 	const { return static_cast<T*>(AbstractDataDifference::left()); 	}
	T* right() 	const { return static_cast<T*>(AbstractDataDifference::right()); 	}
};

template<class T>bool DataDifference<T>::typeCheck() const
{
	// check that types are both descended from T and that they are equal
	return AbstractDataDifference::left()->type()->derivedFrom(T::staticType()) &&
			AbstractDataDifference::right()->type()->derivedFrom(T::staticType()) &&
			AbstractDataDifference::left()->type() == AbstractDataDifference::right()->type();
}




#endif /* QUERIES_DATADIFFERENCE_HPP_ */
