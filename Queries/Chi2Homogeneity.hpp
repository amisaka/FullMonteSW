/*
 * Chi2Homogeneity.hpp
 *
 *  Created on: Jun 15, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_CHI2HOMOGENEITY_HPP_
#define QUERIES_CHI2HOMOGENEITY_HPP_

class OutputDataCollection;

#include <vector>
#include "DataDifference.hpp"
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

/** Chi2Homogeneity uses a chi2 test to determine if the provided set of inputs have the same distribution or not.
 *
 * The left data set is a vector of observations.
 * The user should specify the number of packets in the run, and whether the vector is normalized.
 *
 * The right data set is the reference with two vectors (mean, variance of mean) provided.
 * The user must specify the number of packets in each run, the number of runs used to derive the variance, and whether the
 * vectors are normalized (by 1/N for the mean and 1/N^2 for the variance).
 *
 * The i'th right element is described based on its mean mu_i and variance sigma2_i.
 * Both the mean and variance are sample values since the true population value is unknown.
 *
 *
 * LEFT VARIANCE ESTIMATE
 *
 * If it is drawn from the reference population with Nl packets, compared to the right dataset's N runs of Nr, then by packet
 * independence the left dataset should have a variance Nr/Nl times larger.
 *
 * VarL = Nr/Nl VarR
 *
 *
 * POPULATION MEAN ESTIMATE
 *
 * Since the true (population) mean is unknown and L, R datasets are independent, the minimum-variance estimator for population
 * mean is a linear combination of the two datasets in inverse proportion to their variances, ie.
 *
 * mu_i_est = wL_i + (1-w)R_i
 *
 * With w = VarR / (VarL + Var R)
 * so w = 1 / (Nr/Nl+1) by substitution of VarL
 *
 *
 * POPULATION VARIANCE ESTIMATE
 *
 * sigma_i_est = w^2*VarL + (1-w)^2 VarR
 *
 *
 *
 * Given the estimated population mean, the Chi2 test can be used to check if both datasets (left, right) come from that population.
 *
 */

class Chi2Homogeneity : public DataDifference<SpatialMap<float>>
{
public:
	Chi2Homogeneity();
	~Chi2Homogeneity();

	void		leftPackets(float pl);
	unsigned long long leftPackets() const;

	void		rightPackets(float pr);				///< Number of packets per run used to generate right dataset
	unsigned long long rightPackets() const;

	void		normalizeRight(bool e);				///< If true, normalize right by 1/Nr (default=off; assume already normalized)
	void		normalizeLeft(bool e);				///< If true, normalize left by 1/Nl (default=off; assume already normalized)

	void 		allowedCoefficientOfVariationOfMean(float ratio);	///< Allow mean to have a variance of ratio*mean

	float		pValue() const;
	float		chi2stat() const;

	float		criticalValue(float p) const;

	unsigned	degreesOfFreedom() const;

	void		rightVariance(OutputData* r);		///< Variance (measured or estimated) for right dataset
	void		rightRuns(float N);					///< Number of runs used to derive variance
	unsigned	rightRuns() const;

	float		leftExcluded() const;

	OutputData*	contributions() const;				///< Contribution of each value to the output sum

protected:
	virtual void doUpdate() override;
	void clear();

private:
	float		m_leftPackets;
	float		m_rightPackets;
	float		m_rightRuns;

	float		m_allowedCoefficientOfVariationOfMean=0.0f;

	bool		m_normalizeRight=false;
	bool		m_normalizeLeft=false;

	OutputData*	m_rightVariance=nullptr;

	double		m_chi2stat=0.0;				///< The chi-squared statistic
	unsigned	m_nonZeroSamples=0;			///< Number of nonzero samples

	std::vector<float>	m_contributions;

	float		m_leftExcluded=0;
};


#endif /* QUERIES_CHI2HOMOGENEITY_HPP_ */
