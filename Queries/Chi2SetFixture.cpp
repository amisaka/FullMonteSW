/*
 * Chi2SetFixture.cpp
 *
 *  Created on: Jul 14, 2017
 *      Author: jcassidy
 */

#include "Chi2SetFixture.hpp"

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>

void Chi2SetFixture::leftSet(OutputDataCollection* l)
{
	m_leftSet=l;
}

void Chi2SetFixture::run()
{
	m_rejects.resize(m_pCrit.size());
	boost::fill(m_rejects, 0);

	for(m_runs=0;m_runs<m_leftSet->size();++m_runs)
	{
		m_tester.left(m_leftSet->getByIndex(m_runs));
		m_tester.update();

		float p = m_tester.pValue();

		for(unsigned j=0;j<m_pCrit.size();++j)
			if(p < m_pCrit[j])
				++m_rejects[j];
	}
}

