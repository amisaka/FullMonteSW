/*
 * AbsorptionSum.cpp
 *
 *  Created on: Mar 24, 2016
 *      Author: jcassidy
 */

#include "AbsorptionSum.hpp"

#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputDataType.hpp>
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/algorithm.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>


#include <iomanip>

AbsorptionSum::AbsorptionSum()
{

}

AbsorptionSum::~AbsorptionSum()
{

}

void AbsorptionSum::update()
{
	const SpatialMap<float>* ism=nullptr;
	unsigned N=0;

	if (!m_input)
	{
		LOG_ERROR << "AbsorptionSum::update() called with null data" << endl;
		return;
	}
	else if ((ism = dynamic_cast<const SpatialMap<float>*>(m_input)))
	{
		N = ism->dim();
	}
	else
	{
		LOG_ERROR << "AbsorptionSum::update() can't convert provided data type '" << m_input->type()->name() << "'" << endl;
		return;
	}
	if (ism->outputType() != AbstractSpatialMap::Energy && ism->outputType() != AbstractSpatialMap::PhotonWeight)
    {
        LOG_ERROR << "Input data is not Energy or PhotonWeight. This probe expects raw photon weight or energy data!" << endl;
        return;
    }
	
	/**************************************************************************
	// ensure geometry is defined
	// TODO: Should we support this?
    // if(!m_geometry) {
    //     LOG_ERROR << "AbsorptionSum::update() no geometry provided\n";
    //     return;
    // }

    // get the TetraMesh geometry
    const TetraMesh* mesh = dynamic_cast<const TetraMesh*>(m_geometry);

    // geometry must be TetraMesh
    if(!mesh) {
        LOG_ERROR << "AbsorptionSum::update() could not convert geometry to TetraMesh\n";
        return;
    }

    // must have 1 input value per tetra (-1 is for the 0 tetra automatically inserted by FullMonte, which is not present in the data)
    if((mesh->tetraCells()->size() - 1) != ism->dim()) {
        LOG_ERROR << "AbsorptionSum::update() input must have same number of entries as tetras in the mesh\n";
        return;
    }
	****************************************************************************/

	if (!m_partition)
	{
		m_regionSum.clear();
		// if (ism->outputType() == AbstractSpatialMap::Fluence)
        //     m_regionVolumeSum.clear();
	}
	else if (m_partition->size() != N+1)
	{
	    LOG_ERROR << "AbsorptionSum::update() called with partition but its size (" << m_partition->size() << ") does not match data size (" << N << ")" << endl;
		return;
	}
	else
	{
		m_regionSum.resize(m_partition->count());
		boost::fill(m_regionSum, 0.0f);
		// if (ism->outputType() == AbstractSpatialMap::Fluence)
		// {
		// 	m_regionVolumeSum.resize(m_partition->count());
		// 	boost::fill(m_regionVolumeSum, 0.0f);
		// }
	}

	m_total=0;
	m_volume=0;

	// Get all volume data for each tetrahedron of the mesh
    // const SpatialMap<float>* tetraVolumes = mesh->elementVolumes();

	if (ism)
	{
		for(unsigned i=0;i<ism->dim(); ++i)
		{
			double x = ism->get(i);
			// float volume = 0;
			// float fluence_volume = 0;
			// if (ism->outputType() == AbstractSpatialMap::Fluence)
			// {
			// 	volume = tetraVolumes->get(i+1);
			// 	fluence_volume = x * volume;
			// }
			if (m_partition)
			{
				unsigned region = m_partition->get(i+1);

				if (region < m_regionSum.size())
				{
					// if (ism->outputType() == AbstractSpatialMap::Fluence)
					// {
					// 	m_regionSum[region] += fluence_volume;
					// 	m_regionVolumeSum[region] += volume;
					// }
					// else
					m_regionSum[region] += x;
				}
				else
					LOG_ERROR << "AbsorptionSum::update() partition reports its count as " << m_partition->count() << " but element " << i << " has region code " << region << endl;
			}

			// if (ism->outputType() == AbstractSpatialMap::Fluence)
			// {
			// 	m_total += fluence_volume;
			// 	m_volume += volume;
			// }
			// else
			m_total += x;
		}
	}
}

float AbsorptionSum::totalForAllElements() const
{
	return m_total;
	// if (m_volume == 0)
	// 	return m_total;
	// else
	// 	return m_total/m_volume;
}

float AbsorptionSum::totalForPartition(unsigned i) const
{
	if (i >= m_regionSum.size())
	{
		LOG_ERROR << "AbsorptionSum::totalForPartition(" << i << ") called but exceeds partition count (" << m_regionSum.size() << ")" << endl;
		return 0.0f;
	}
	return m_regionSum[i];
	// if (m_regionVolumeSum[i] == 0)
	// 	return m_regionSum[i];
	// else
	// 	return m_regionSum[i]/m_regionVolumeSum[i];
	
}
