#ifndef QUERIES_SPATIALMAPOPERATOR_HPP_
#define QUERIES_SPATIALMAPOPERATOR_HPP_

#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>
#include <FullMonteSW/OutputTypes/OutputData.hpp>

#include <vector>

/**
 * Used to operate on two SpatialMaps (A and B) of the same dimension.
 * Operations (element-wise):
 *      A + B (add)
 *      A - B (sub)
 *      A * B (mul)
 *      A / B (div)
 */
class SpatialMapOperator {
public:
    void add();
    void sub();
    void mul();
    void div();
    
    OutputData* result() const { return m_output; }

    void sourceA(OutputData* A) { m_A = A; }
    const OutputData* sourceA() const { return m_A; }

    void sourceB(OutputData* B) { m_B = B; }
    const OutputData* sourceB() const { return m_B; }

private:
    enum OP {ADD, SUB, MUL, DIV};

    // the inputs A and B
    const OutputData* m_A=nullptr;
    const OutputData* m_B=nullptr;

    // the result
    OutputData* m_output=nullptr;

    // does the comparison
    void compare(OP op);
};

#endif

