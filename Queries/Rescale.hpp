/*
 * Rescale.hpp
 *
 *  Created on: Jun 15, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_RESCALE_HPP_
#define QUERIES_RESCALE_HPP_

class OutputData;

class Rescale
{
public:
	Rescale();
	~Rescale();

	void 			source(OutputData* d);

	void 			update();
	OutputData*		output() const;

	void factor(float f);

private:
	float 		m_factor=1.0f;
	OutputData	*m_data=nullptr;
	OutputData	*m_output=nullptr;
};




#endif /* QUERIES_RESCALE_HPP_ */
