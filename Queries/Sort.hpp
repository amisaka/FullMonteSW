/*
 * Sort.hpp
 *
 *  Created on: Sep 11, 2017
 *      Author: jcassidy
 */

#ifndef QUERIES_SORT_HPP_
#define QUERIES_SORT_HPP_

class Permutation;
class OutputData;

/** Sorts a vector in order, returning a Permutation referring to the input vector.
 *
 * NaNs go last always
 * If excludeNaNs set, then omit them from the final permutation
 */

class Sort
{
public:
	Sort();
	virtual ~Sort();

	void			input(OutputData* d);
	void 			update();

	void			setAscending();
	void			setDescending();

	void			excludeNaNs(bool e);

	Permutation*	output() const;

private:
	OutputData		*m_input=nullptr;
	Permutation		*m_output=nullptr;
	bool			m_descending=false;
	bool			m_excludeNaNs=true;
};


#endif /* QUERIES_SORT_HPP_ */
