/*
 * ForwardDecl.hpp
 *
 *  Created on: Nov 15, 2016
 *      Author: jcassidy
 */

#ifndef GEOMETRY_SOURCES_FORWARDDECL_HPP_
#define GEOMETRY_SOURCES_FORWARDDECL_HPP_

namespace Source
{

class Abstract;
class Surface;
class SurfaceTri;
class PencilBeam;
class Volume;
class Point;
class Line;

};



#endif /* GEOMETRY_SOURCES_FORWARDDECL_HPP_ */
