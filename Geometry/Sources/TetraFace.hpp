#ifndef GEOMETRY_SOURCES_TETRAFACE_HPP_
#define GEOMETRY_SOURCES_TETRAFACE_HPP_

#include "Abstract.hpp"

#include <FullMonteSW/Geometry/StandardArrayKernel.hpp>

#include <array>
#include <cmath>
#include <string>
#include <algorithm>
#include <iostream>

/**
 * A source that emits light from a tetrahedral face.
 */
namespace Source {

class TetraFace : public Abstract
{
public:
	TetraFace(float w=1.0, unsigned IDfu=0) : Abstract(w), m_IDfu(IDfu) {}

	DERIVED_SOURCE_MACRO(Abstract,TetraFace)

    void faceID(unsigned IDfu) { m_IDfu = IDfu; }
    unsigned faceID() const { return m_IDfu; }

    void emitNormal(bool en) { m_emitNormal = en; }
    bool emitNormal() const { return m_emitNormal; }

    void emitIsotropic(bool en) { m_emitIsotropic = en; }
    bool emitIsotropic() const { return m_emitIsotropic; }
    
    void emitHemiSphere(bool en) { m_emitHemiSphere = en; }
    bool emitHemiSphere() const { return m_emitHemiSphere; }

    void numericalAperture(float NA) { m_NA = abs(NA); }
    float numericalAperture() const { return m_NA; }

    void hemiSphereEmitDistribution(std::string str) { m_hemiSphereEmitDistribution = str; }
    std::string hemiSphereEmitDistribution() const { return m_hemiSphereEmitDistribution; }
private:
    // the undirected face ID
    unsigned m_IDfu;
    
    // whether to exit normal (true) or anti-normal (false)
    bool m_emitNormal=true;
    
    // emit isotropically - mutually exclusive to emitHemiSphere AND takes precidence over it
    bool m_emitIsotropic=false;
    
    // whether to emit in a uniform direction (false) or randomly
    // in a hemisphere that is oriented by the face normal (or antinormal)
    bool m_emitHemiSphere=false;

    // the distribution type for a hemisphere (see Kernels/Software/Emitters/HemiSphere.hpp)
    // currently should be "UNIFORM" or "LAMBERT" using a string in case we want to extend these distributions in the future
    std::string m_hemiSphereEmitDistribution="UNIFORM";

    // defining the numerical aperture of a tetrahedral face (default value emits in a hemisphere)
    float m_NA=1.0;
};

};

#endif
