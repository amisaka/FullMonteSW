/**
 * \file CylDetector.hpp
 * Contains class definition of a cylindrical detector
 *
 * \author Abed Yassine
 * \date June 10th, 2020
 *
 */

#ifndef GEOMETRY_SOURCES_CYLDETECTOR_HPP_
#define GEOMETRY_SOURCES_CYLDETECTOR_HPP_

#include "Abstract.hpp"

#include <FullMonteSW/Geometry/StandardArrayKernel.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <array>
#include <cmath>
#include <string>
#include <algorithm>
#include <iostream>

namespace Source {

class CylDetector : public Abstract {
public:
	/**
     * This enum class is to choose how to detect photon packets. 
     * FULL: (default) detects the full weight of the hitting photon packets. 
     * PROBABILITY: model an exponential detection profile that returns the probability of detecting a photon hitting the diffuser at position X
     * ODE: Solution of the ODE that relates the detection profile to an exponential scattering profile. Solution is an exponential of an exponential
     */
    enum DetectionType { FULL, PROBABILITY, ODE}; 

	/** 
	 * Convert string to detection type 
	 */ 
	static DetectionType DetectionTypeFromString(std::string str) 
	{
		std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) {return std::tolower(c);});
		
		if (str == "full") {
			return DetectionType::FULL;
		} else if (str == "exponential") {
			return DetectionType::PROBABILITY;
		} else if (str == "ode") { 
			return DetectionType::ODE;
		} else {
			LOG_ERROR << "CylDetector::DetectionType unknown string " << str << " - defaulting to ODE" << std::endl;
			return DetectionType::ODE;
		}
	}
	
    // constructors
	explicit CylDetector(float w=1.0) : Abstract(w) {}

	DERIVED_SOURCE_MACRO(Abstract,CylDetector)

    // endpoint setter/getter
	std::array<float,3> endpoint(unsigned i) const { return m_endpoint[i]; }
	void endpoint(unsigned i,std::array<float,3> p) { m_endpoint[i] = p; }

    // radius setter/getter
    float radius() const { return m_radius; }
    void radius(float radius) { 
        if (radius < 0) {
             LOG_ERROR << "CylDetector::Radius should be greater than or equal to 0" << std::endl;
             std::exit(-1);
         }
         m_radius = radius; 
    }

    // numerical Aperture getter/setter 
    float numericalAperture() const { return m_numericalAperture; }
    void numericalAperture(float na) { 
         if (na < 0) {
             LOG_ERROR << "CylDetector::Numerical aperture should be greater than or equal to 0" << std::endl;
             std::exit(-1);
         }
         m_numericalAperture = na; 
    }

    // Region ID getter/setter 
    unsigned detectorID() const { return m_detectorID; }
    void detectorID(unsigned id) { m_detectorID = id; }

    // detectedWeight getter/setter
    float detectedWeight() const { return m_detectedWeight; }
    void detectedWeight(float det) { m_detectedWeight = det; }
	
	// detection type getter/setter
	std::string detectionType() { return m_detectionType; }
	void detectionType(std::string detectionType) { m_detectionType = detectionType; }
	
private:
	std::array<float,3> m_endpoint[2];
    float m_radius=1.0;
    float m_numericalAperture=0.22;
	std::string m_detectionType="ODE";

    // output 
    float m_detectedWeight = 0.0f;

    unsigned m_detectorID=-1U;
};

};

#endif
