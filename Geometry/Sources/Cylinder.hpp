#ifndef GEOMETRY_SOURCES_CYLINDER_HPP_
#define GEOMETRY_SOURCES_CYLINDER_HPP_

#include "Abstract.hpp"

#include <FullMonteSW/Geometry/StandardArrayKernel.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <array>
#include <cmath>
#include <string>
#include <algorithm>
#include <iostream>

namespace Source {

class Cylinder : public Abstract {
public:
    /**
     * Different distributions for theta angle
     */
	enum ThetaDistribution { UNIFORM, LAMBERT, CUSTOM };
    
    /**
     * Convert string to theta distribution angle
     */
    static ThetaDistribution ThetaDistributionFromString(std::string str) {
        // transform to lowercase
        std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c){ return std::tolower(c); });

        if(str == "lambert") {
            return ThetaDistribution::LAMBERT;
        } else if(str == "uniform") {
            return ThetaDistribution::UNIFORM;
        } else if (str == "custom") {
            return ThetaDistribution::CUSTOM;
        }else {
            LOG_ERROR << "Cylinder::ThetaDistribution unknown string " << str << " - defaulting to UNIFORM" << std::endl;
            return ThetaDistribution::UNIFORM;
        }
    }

    /**
     * Convert theta distribution angle to a string
     */
    static std::string ThetaDistributionToString(ThetaDistribution d) {
        if(d == ThetaDistribution::LAMBERT) {
            return "lambert";
        } else if (d == ThetaDistribution::CUSTOM) {
            return "custom";
        }else {
            return "uniform";
        }
    }

    // constructors
	explicit Cylinder(float w=1.0) : Abstract(w) {}

	DERIVED_SOURCE_MACRO(Abstract,Cylinder)

    // endpoint setter/getter
	std::array<float,3> endpoint(unsigned i) const { return m_endpoint[i]; }
	void endpoint(unsigned i,std::array<float,3> p) { m_endpoint[i] = p; }

    // radius setter/getter
    float radius() const { return m_radius; }
    void radius(float radius) { m_radius = radius; }

    // true = emit on hemisphere, false = emit normal to cylinder
    void emitHemiSphere(bool en) { m_emitHemiSphere = en; }
    bool emitHemiSphere() const { return m_emitHemiSphere; }

    // true = emit from within cylinder volume, false = emit from surface
    void emitVolume(bool en) { m_emitVolume = en; }
    bool emitVolume() const { return m_emitVolume; }

    void numericalAperture(float NA) { m_NA = abs(NA); }
    float numericalAperture() const { return m_NA; }

    // getter/setter for the theta distribution
    void hemiSphereEmitDistribution(std::string str) { m_hemiSphereEmitDistribution = str; }
    std::string hemiSphereEmitDistribution() const { return m_hemiSphereEmitDistribution; }

private:
	std::array<float,3> m_endpoint[2];
    float m_radius=1.0;
    bool m_emitHemiSphere=true;
    bool m_emitVolume=false;

    // defining the photon spread angle of a tetrahedral face (default value emits in a hemisphere)
    float m_NA=1.0;

    // the distribution type for a hemisphere (see Kernels/Software/Emitters/HemiSphere.hpp)
    // currently should be "UNIFORM" or "LAMBERT" using a string in case we want to extend these distributions in the future
    std::string m_hemiSphereEmitDistribution="UNIFORM";
};

};

#endif
