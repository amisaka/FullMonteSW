/*
 * SurfaceOfRegionPredicate.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include "ArrayPredicateEvaluator.hpp"
#include "SurfaceOfRegionPredicate.hpp"

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include "VolumeCellPredicate.hpp"

#include <functional>

#include <iostream>
using namespace std;

class SurfaceOfRegionPredicateEvaluator : public ArrayPredicateEvaluator
{
public:
	SurfaceOfRegionPredicateEvaluator(const TetraMesh* M,const ArrayPredicateEvaluator* e,bool directed) :
		m_mesh(M),
		m_vPredicateEval(e),
		m_directed(directed){}

	~SurfaceOfRegionPredicateEvaluator()
	{
		delete m_vPredicateEval;
	}

	virtual bool operator()(unsigned i) const override
	{

		// use the fact that bool->int conversion is true=1, false=0
		// in undirected case, >>0 is no-op
		// in directed case, >>1 gives the intended result
		unsigned IDt_up = get(id,*m_mesh,m_mesh->faceTetraLinks()->get(i >> m_directed).downTet().T);
		unsigned IDt_down = get(id,*m_mesh,m_mesh->faceTetraLinks()->get(i >> m_directed).upTet().T);
		return (*m_vPredicateEval)(IDt_up) != (*m_vPredicateEval)(IDt_down);
	}

	virtual unsigned size() const override
	{
		return m_mesh->faceTetraLinks()->size();
	}

private:
	const TetraMesh* 					m_mesh=nullptr;
	const ArrayPredicateEvaluator*		m_vPredicateEval=nullptr;
	bool									m_directed;
};


SurfaceOfRegionPredicate::SurfaceOfRegionPredicate()
{
}

SurfaceOfRegionPredicate::~SurfaceOfRegionPredicate()
{
}

void SurfaceOfRegionPredicate::setRegionPredicate(const VolumeCellPredicate* pred)
{
	m_vPredicate=pred;
}

ArrayPredicateEvaluator* SurfaceOfRegionPredicate::bind(const Geometry* G) const
{
	const TetraMesh* M = nullptr;
	if ((M=dynamic_cast<const TetraMesh*>(G)))
	{
		return new SurfaceOfRegionPredicateEvaluator(M,m_vPredicate->bind(G),m_directedOutput);
	}
	else
	{
		LOG_ERROR << "SurfaceOfRegionPredicate::bind(G) failed because geometry can't be cast to TetraMesh*" << endl;
		return nullptr;
	}
}
