/*
 * SurfaceCellPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_SURFACECELLPREDICATE_HPP_
#define GEOMETRY_PREDICATES_SURFACECELLPREDICATE_HPP_

#include "../Predicates/GeometryPredicate.hpp"

class Geometry;
class ArrayPredicateEvaluator;

/** Abstract class for evaluating a predicate on surface cells
 *
 */

class SurfaceCellPredicate : public GeometryPredicate
{
public:

	//virtual ArrayPredicateEvaluator* 		bind(const Geometry* G) const override;
};

#endif /* GEOMETRY_PREDICATES_SURFACECELLPREDICATE_HPP_ */
