/*
 * VolumeCellInRegionPredicate.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include "VolumeCellInRegionPredicate.hpp"

#include <FullMonteSW/Geometry/Geometry.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include "ArrayPredicateEvaluator.hpp"
#include "PredicateEvaluators.hpp"

#include <functional>


VolumeCellInRegionPredicate::VolumeCellInRegionPredicate()
{
}

VolumeCellInRegionPredicate::~VolumeCellInRegionPredicate()
{
}

void VolumeCellInRegionPredicate::setRegion(unsigned r)
{
	m_region=r;
}

unsigned VolumeCellInRegionPredicate::region() const
{
	return m_region;
}

ArrayPredicateEvaluator* VolumeCellInRegionPredicate::bind(const Geometry* G) const
{
	return make_volume_predicate_evaluator(this,G,
		[](const VolumeCellInRegionPredicate* pred,const Geometry* G,unsigned i){ return G->regions()->get(i)==pred->region(); });
}
