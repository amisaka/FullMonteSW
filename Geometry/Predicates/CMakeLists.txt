ADD_LIBRARY(FullMonteGeometryPredicates SHARED
	ArrayPredicateEvaluator.cpp
	SurfaceOfRegionPredicate.cpp
	VolumeCellInRegionPredicate.cpp)

INSTALL(TARGETS FullMonteGeometryPredicates
    DESTINATION lib)
	
TARGET_LINK_LIBRARIES(FullMonteGeometryPredicates FullMonteGeometry FullMonteQueries)