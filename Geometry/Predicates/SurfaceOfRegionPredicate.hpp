/*
 * SurfaceOfRegionPredicate.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_PREDICATES_SURFACEOFREGIONPREDICATE_HPP_
#define GEOMETRY_PREDICATES_SURFACEOFREGIONPREDICATE_HPP_

#include "../Predicates/SurfaceCellPredicate.hpp"

class VolumeCellPredicate;

/** Predicate to operate on surface cells, returning true if they are on the boundary of the specified region.
 *
 * For a face F with adjacent tetras T_up and T_down, works by checking if in_region(T_up) != in_region(T_down).
 *
 *
 * Output may be returned as either undirected or directed face index.
 * See TetraMesh.hpp for explanation of directed vs. undirected face index.
 */

class SurfaceOfRegionPredicate : public SurfaceCellPredicate
{
public:
	SurfaceOfRegionPredicate();
	~SurfaceOfRegionPredicate();

	void setRegionPredicate(const VolumeCellPredicate*);			///< Set the region definition
	void setDirectedOutput(bool enDir);							///< If true, output _directed_ face indices

	virtual ArrayPredicateEvaluator* bind(const Geometry* G) const override;

private:
	const VolumeCellPredicate*	m_vPredicate=nullptr;
	bool 						m_directedOutput=false;
};

#endif /* GEOMETRY_PREDICATES_SURFACEOFREGIONPREDICATE_HPP_ */
