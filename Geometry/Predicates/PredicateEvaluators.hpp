/*
 * PredicateEvaluators.hpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#ifndef PREDICATEEVALUATORS_HPP_
#define PREDICATEEVALUATORS_HPP_

template<class Pred,class Geom,class Func>class VolumeCellPredicateEvaluator : public ArrayPredicateEvaluator
{
public:
	VolumeCellPredicateEvaluator(
			const Pred* pred,
			const Geom* geom,
			const Func& f) :
				m_pred(pred),
				m_geom(geom),
				m_func(std::move(f))
		{
		}

	virtual bool operator()(unsigned i) 	const 	{ return m_func(m_pred,m_geom,i); }
	virtual unsigned size() 				const 	{ return m_geom->regions()->size(); }

private:
	const Pred* m_pred=nullptr;
	const Geom*	m_geom=nullptr;
	Func			m_func;
};

template<class Pred,class Geom,class Func>VolumeCellPredicateEvaluator<Pred,Geom,Func>* make_volume_predicate_evaluator(const Pred* p,const Geom* g,Func&& f)
{
	return new VolumeCellPredicateEvaluator<Pred,Geom,Func>(p,g,f);
}



template<class Pred,class Geom,class Func>class SurfaceCellPredicateEvaluator : public ArrayPredicateEvaluator
{
public:
	SurfaceCellPredicateEvaluator(
			const Pred* pred,
			const Geom* geom,
			const Func& f) :
				m_pred(pred),
				m_geom(geom),
				m_func(std::move(f))
		{
		}

	virtual bool operator()(unsigned i) 	const 	{ return m_func(m_pred,m_geom,i); }
	virtual unsigned size() 				const 	{ return m_geom->faceTetraLinks()->size(); }

private:
	const Pred* m_pred=nullptr;
	const Geom*	m_geom=nullptr;
	Func			m_func;
};

template<class Pred,class Geom,class Func>VolumeCellPredicateEvaluator<Pred,Geom,Func>* make_surface_predicate_evaluator(const Pred* p,const Geom* g,Func&& f)
{
	return new SurfaceCellPredicateEvaluator<Pred,Geom,Func>(p,g,f);
}



#endif /* PREDICATEEVALUATORS_HPP_ */
