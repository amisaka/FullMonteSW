/*
 * ArrayPredicateEvaluator.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include <FullMonteSW/Geometry/Permutation.hpp>
#include "ArrayPredicateEvaluator.hpp"

#include <vector>

using namespace std;

ArrayPredicateEvaluator::ArrayPredicateEvaluator()
{

}

ArrayPredicateEvaluator::~ArrayPredicateEvaluator()
{

}

Permutation* ArrayPredicateEvaluator::matchingElements() const
{
	vector<bool> match(size(),false);
	unsigned N=0;

	for(unsigned i=0;i<size();++i)
	{
		match[i] = (*this)(i);
		N += match[i];
	}

	Permutation* P = new Permutation();
	P->resize(N);
	P->sourceSize(size());

	unsigned j=0;
	for(unsigned i=0;i<match.size();++i)
		if (match[i])
			P->set(j++,i);

	return P;
}
