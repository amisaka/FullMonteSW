#if defined(SWIGTCL)
%module FullMontePlacementTCL
#elif defined(SWIGPYTHON)
%module Placement
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"
%include "std_vector.i"

%include "../FullMonteGeometry_types.i"

%{
#include "SurfaceSourceBuilder.hpp"
#include "CylinderSurfaceSourceBuilder.hpp"
%}

%include "SurfaceSourceBuilder.hpp"
%include "CylinderSurfaceSourceBuilder.hpp"

