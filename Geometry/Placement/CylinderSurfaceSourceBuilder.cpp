#include <FullMonteSW/Geometry/Placement/CylinderSurfaceSourceBuilder.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

/**
 * Override this method to determine if a tetra face should be included in the surface
 *
 * @param IDfud the undirected face descriptor for the face to check
 * @return whether the face should be included in the surface
 */
bool CylinderSurfaceSourceBuilder::includeFace(TetraMesh::FaceDescriptor IDfud) {

    if(!m_emitFromEnds) {

        // the normal vector of the face
        UnitVector<3,double> n = get(face_normal, *m_mesh, TetraMesh::FaceDescriptor(IDfud));
    
        // here we check if a tetra face is (likely) on the ends of the cylinder
        // the assumption here is that tetra faces on the ends will have a normal that is (roughly)
        // parellel (or anti-parallel) with the direction vector of the cylinder
        // tetra faces on the sides of the cylinder that should be emitted from will (roughly)
        // have normals orthogonal to the direction of the cylinder
        return abs(dot(n, m_cylinderDirNorm)) < (double)(m_orthogonalTolerance);
    } else {
        // whatever the base class implemented
        return SurfaceSourceBuilder::includeFace(IDfud);
    }
}

/**
 * Override the base method from SurfaceSourceBuilder to do the same thing
 * but remove tetras that may be on the ends of the cylinder if we don't want to emit from there.
 */
void CylinderSurfaceSourceBuilder::update() {
    // compute direction vector for the cylinder
    Vector<3,double> cylinderDir;
    cylinderDir[0] = (double)(m_endpoint[0][0] - m_endpoint[1][0]);
    cylinderDir[1] = (double)(m_endpoint[0][1] - m_endpoint[1][1]);
    cylinderDir[2] = (double)(m_endpoint[0][2] - m_endpoint[1][2]);

    // normalize the direction vector
    m_cylinderDirNorm = normalize(cylinderDir);
    
    // call SurfaceSourceBuilder update to create Composite source from surface tetras
    SurfaceSourceBuilder::update();
}


