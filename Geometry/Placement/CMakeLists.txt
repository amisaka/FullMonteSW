ADD_LIBRARY(FullMontePlacement SHARED
    PlacementBase.cpp
    PlacementMediatorBase.cpp
    PlanePlacement.cpp
    PlanePlacementLineSource.cpp
    PlanePlacementPencilBeam.cpp
    SurfaceSourceBuilder.cpp
    CylinderSurfaceSourceBuilder.cpp
)

TARGET_LINK_LIBRARIES(FullMontePlacement
    FullMonteGeometry
    FullMonteGeometryPredicates
)

IF (WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMontePlacement.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMontePlacementTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMontePlacement.i
    )
    SWIG_LINK_LIBRARIES(FullMontePlacementTCL
        ${TCL_LIBRARY}
        FullMonteGeometry
        FullMonteGeometryPredicates
        FullMontePlacement
    )

    INSTALL(TARGETS FullMontePlacementTCL LIBRARY 
        DESTINATION lib
    )
ENDIF()

IF(WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMontePlacement.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(Placement
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMontePlacement.i
    )
    SWIG_LINK_LIBRARIES(Placement
        ${Python3_LIBRARIES} 
        FullMonteGeometry 
        FullMonteGeometryPredicates 
        FullMontePlacement
    )
    SET_TARGET_PROPERTIES(_Placement PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()

INSTALL(TARGETS FullMontePlacement LIBRARY 
    DESTINATION lib
)

