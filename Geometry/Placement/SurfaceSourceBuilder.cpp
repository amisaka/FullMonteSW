#include <FullMonteSW/Geometry/Placement/SurfaceSourceBuilder.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceCellPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>

/**
 * Create the composite source given the mesh and region in the mesh
 */
void SurfaceSourceBuilder::update() {
    //// pre processing ////
    if(!m_mesh) {
        LOG_ERROR << "SurfaceSourceBuilder::update no mesh is defined" << std::endl;
    }

    if(!m_region) {
        LOG_ERROR << "SurfaceSourceBuilder::update no region is  defined" << std::endl;
    }

    if(m_emitHemiSphere) {
        LOG_DEBUG << "SurfaceSourceBuilder::update I will create a surface source which emits randomly on a hemisphere centered on the normal of each tetrahedral face" << std::endl;
    } else {
        LOG_DEBUG << "SurfaceSourceBuilder::update I will create a surface source which emits in the direction of the face normal (pointing outside of the surface)" << std::endl;
    }

    //// create the composite source ////
    // get the surface region of the given volume region
    SurfaceOfRegionPredicate* s= new SurfaceOfRegionPredicate();
    s->setRegionPredicate(m_region);
    const SurfaceCellPredicate* surfaceRegion = s;

    // evaluators for the surface and volume
    ArrayPredicateEvaluator* surEval = surfaceRegion->bind(m_mesh);
    ArrayPredicateEvaluator* volEval = m_region->bind(m_mesh);

    // first find the region surface area
    double regionSurfaceArea = 0.0f;
    for(unsigned i = 0; i < m_mesh->faceTetraLinks()->size(); i++) {
        // is the face of the surface of the region
        if((*surEval)(i) && includeFace(TetraMesh::FaceDescriptor(i))) {
            regionSurfaceArea += get(area, *m_mesh, TetraMesh::FaceDescriptor(i));
        }
    }

    // loop over all undirected faces
    for(unsigned i = 0; i < m_mesh->faceTetraLinks()->size(); i++) {
        // is the face of the surface of the region
        if((*surEval)(i) && includeFace(TetraMesh::FaceDescriptor(i))) {
            // if the tetra above the face (i.e. the one along the normal, i.e. 'upTet') is
            // INSIDE the region interior (the volume evaluator 'volEval' tells us this),
            // this means that the normal points inside the volume region and,
            // therefore, we need to flip the normal so it points outside the volume region
            bool flip = (*volEval)(m_mesh->faceTetraLinks()->get(i).upTet().T.value());

            // surface area of this triangle face
            // power of this face emitter is the fraction of the total power (m_power) based on the
            // surface are of this triangular face compared to the surface area of the whole region (sum of all face areas)
            const double sa = get(area, *m_mesh, TetraMesh::FaceDescriptor(i));
            const float face_power = (float)(sa / regionSurfaceArea) * m_power;

            // create a TetraFace source using the info we just gathered
            // TODO: WE ARE LEAKING THIS MEMORY - NO GREAT WAY TO DELETE IT!
            Source::TetraFace* tf_src = new Source::TetraFace(face_power, i);   // (power, undirected face ID)
            tf_src->emitNormal(!flip);                                          // if flip, emit anti-normal, otherwise emit normal
            tf_src->emitHemiSphere(m_emitHemiSphere);                           // whether to emit in a hemi-sphere
            tf_src->hemiSphereEmitDistribution(m_hemiSphereEmitDistribution);   // what distribution to use for the theta angle IF emitting in a hemi-sphere
            tf_src->numericalAperture(m_NA);

            // add the source we created to our list
            m_sources.push_back(tf_src);
        }
    }
}

/**
 * @return the composite source created by the builder
 */
Source::Composite SurfaceSourceBuilder::output() {
    // the composite source we will make
    Source::Composite C;
    for(unsigned i = 0; i < m_sources.size(); i++) {
        C.add(m_sources[i]);
    }

    LOG_INFO << "SurfaceSourceBuilder::output built a Composite source out of " << m_sources.size() << " tetrahedral faces" << std::endl;

    return C;
}

/**
 * @return whether to inclue the face.
 */
bool SurfaceSourceBuilder::includeFace(TetraMesh::FaceDescriptor IDfud) {
    UnitVector<3, double> n = get(face_normal, *m_mesh, TetraMesh::FaceDescriptor(IDfud));
    ArrayPredicateEvaluator* volEval = m_region->bind(m_mesh);
    bool flip = (*volEval)(m_mesh->faceTetraLinks()->get(get(id, *m_mesh, IDfud)).upTet().T.value());

    // return true if m_checkDirection set to false
    // return true if dot product of normal and m_emitDirection is greater than m_directionTolerance
    // if needs to emit anti-normal, flip sign of dot product
    return (!m_checkDirection || ((flip) ? (-1) : 1) * dot(n, m_emitDirection) > (double) m_directionTolerance);
}

/**
 * Sets the power for the final composite source (sum of all TetraFace powers)
 */
void SurfaceSourceBuilder::power(float p) {
    if(p < 1.0f) {
        m_power = 1.0f;
        LOG_WARNING << "SurfaceSourceBuilder::power must be greater than or equal to 1.0 - defaulting to 1.0" << std::endl;
    } else {
        m_power = p;
    }
}

/**
 * Sets the direction where light emits.
 */
void SurfaceSourceBuilder::emitDirection(std::array<double, 3> emitDirection) {
    Vector<3, double> dir;
    dir[0] = emitDirection[0];
    dir[1] = emitDirection[1];
    dir[2] = emitDirection[2];

    m_emitDirection = normalize(dir);
    //cout << m_emitDirection[0] << " " << m_emitDirection[1] << " " << m_emitDirection[2] << endl;
}
