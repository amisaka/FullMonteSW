/*
 * NonSSE.cpp
 *
 *  Created on: Oct 2, 2018
 *      Author: fynns
 *
 */

#include "NonSSE.hpp"

#include <cmath>

//shuffle128  is the non-SSE version of {m128 _mm_shuffle_ps (m128 lo, m128 hi, unsigned int imm8)}
////Shuffle single-precision (32-bit) floating-point elements in lo and hi using the controls hi3, hi2, lo1 and lo0 and return the result.
m128 shuffle128(m128 const &lo, m128 const &hi, int hi3, int hi2, int lo1, int lo0)
{
	m128 out;
	out[0] = lo[lo0];
	out[1] = lo[lo1];
	out[2] = hi[hi2];
	out[3] = hi[hi3];
	return out;
}

//mult128  is the non-SSE version of {m128 _mm_mul_ps (m128 a, m128 b)} 
//Multiply packed single-precision (32-bit) floating-point elements in a and b, and store the results in dst.
m128 mult128(m128 const &mul1, m128 const &mul2)
{


	m128 out;
	for (int i=0; i<4; i++)
		out[i]= mul1[i]*mul2[i];

	return out;
}

m128 add128(m128 const &add1, m128 const &add2)
{

	m128 out;
	for (int i=0; i<4; i++)
		out[i]= add1[i]+add2[i];

	return out;
}

//sub128  is the non-SSE version of {m128 _mm_sub_ps (m128 a, m128 b)}
//Subtract packed single-precision (32-bit) floating-point elements in b from packed single-precision (32-bit) floating-point elements in a, and store the results in dst.
m128 sub128(m128 const &sub1, m128 const &sub2)
{

	m128 out;
	for (int i=0; i<4; i++)
		out[i]= sub1[i]-sub2[i];

	return out;
}

//div128  is the non-SSE version of {m128 _mm_div_ps (m128 a, m128 b)}
//Divide packed single-precision (32-bit) floating-point elements in a by packed elements in b, and store the results in dst.
m128 div128(m128 const &div1, m128 const &div2)
{

	m128 out;
	for (int i=0; i<4; i++)
		out[i]= div1[i]/div2[i];

	return out;
}

//blend128  is the non-SSE version of {m128 _mm_blend_ps (m128 a, m128 b, m128 mask)}
//Blend packed single-precision (32-bit) floating-point elements from a and b using control mask , and returns the results .
m128 blend128(m128 const &a, m128 const &b, m128 const &mask)
{

	m128 out;


	out[0]= (mask[0] < 0 ) ?  b[0] : a[0];
	out[1]= (mask[1] < 0 ) ?  b[1] : a[1];
	out[2]= (mask[2] < 0 ) ?  b[2] : a[2];
	out[3]= (mask[3] < 0 ) ?  b[3] : a[3];

	return out;

}

//complo128  is the non-SSE version of {int _mm_ucomilt_ss (m128 a, m128 b)}
//Compare the lower single-precision (32-bit) floating-point element in a and b for less-than, and return the boolean result (0 or 1).
bool complo128(m128 const &a, m128 const &b)
{
	return (a[0] < b[0]) ? 1: 0;
}

//min128  is the non-SSE version of {m128 _mm_min_ps (m128 a, m128 b)}
//Compare packed single-precision (32-bit) floating-point elements in a and b, and store packed minimum values in dst.
m128 min128(m128 const &a, m128 const &b)
{
	m128 out;

	for (int i=0; i<4; i++)
		out[i] = !(b[i]<a[i])?a[i]:b[i];

	return out;
}

//max128 is the non-SSE version of {m128 _mm_max_ps(m128 a , m128 b );}
///computes the maximums of the four single-precision, floating-point values of a and b.
m128 max128(m128 const &a, m128 const &b)
{
	m128 out;

	for (int i=0; i<4; i++)
		out[i] = (b[i]<a[i])?a[i]:b[i];

	return out;
}

//set_zero128  is the non-SSE version of {m128 _mm_setzero_ps (void)}
//Return array of type m128 with all elements set to zero.
m128 set_zero128()
{
	m128 out;
	for (int i=0; i<4; i++)
		out[i] = 0;

	return out;
}

//movemask128 is the non-SSE version of {int _mm_movemask_ps(m128 a);}
//Creates a 4-bit mask from the most significant bits of the four SP FP values.
int movemask128( m128 const &a )
{
	//Creates a 4-bit mask from the most significant bits of the four single-precision, floating-point values.
	int r=0;

	r= std::signbit(*reinterpret_cast<const int32_t*>(&a[3])) <<3 | std::signbit(*reinterpret_cast<const int32_t*>(&a[2]))<<2 | std::signbit(*reinterpret_cast<const int32_t*>(&a[1])) <<1 | std::signbit(*reinterpret_cast<const int32_t*>(&a[0]));
	return r;
}

//getMinIndex128 is the non-SSE version of {std::pair<unsigned,m128> getMinIndex4p(m128 v);} which is declared in sse.hpp 
std::pair<unsigned,m128> getMinIndex128(m128 const &v)
{

	int mask;
	m128 halfmin = min128(v,shuffle128(v,v,2,3,0,1));


	m128 allmin  = min128(halfmin,shuffle128(halfmin,halfmin,0,0,2,2));


	m128 eqmask = cmpeq128(v,allmin);


	mask = movemask128(eqmask);
	return std::make_pair(mask == 0 ? 4 : __builtin_ctz(mask),allmin);
}

//cmpeq128 is the non-SSE version of {m128 _mm_cmpeq_ps(m128 a, m128 b);} 
////Compares for equality
m128 cmpeq128(m128 const &a, m128 const &b)
{
	m128 out;
	int32_t temp = 0xffffffff;

	out[0] = (a[0] == b[0]) ? (static_cast<const float>(temp)) : 0x0;

	out[1] = (a[1] == b[1]) ? (static_cast<const float>(temp)) : 0x0;

	out[2] = (a[2] == b[2]) ? (static_cast<const float>(temp)) : 0x0;

	out[3] = (a[3] == b[3]) ? (static_cast<const float>(temp)) : 0x0;
	return out;
}


//set1_ps128  is the non-SSE version of {m128 _mm_set1_ps ( float  f) }
//Returns a vector of 4 SPFP values filled with the same SP value.
m128 set1_ps128(float value)
{

	m128 out;
	for (int i=0; i<4; i++)
		out[i]= value;

	return out;
}

//setr_ps128  is the non-SSE version of {m128 _mm_setr_ps ( const float a,const float  b,const float  c,const float d) }
//returns a vector of 4 SPFP values with the lowest SPFP value set to f, and other values set to 0.0f.
m128 setr_ps128(float z, float y, float x, float w)
{


	m128 out;

	out[0]= z;
	out[1]= y;
	out[2]= x;
	out[3]= w;


	return out;
}

//loadu_ps128 is the non-SSE version of {m128 _mm_loadu_ps (float const* mem_addr)}
//Load 128-bits (composed of 4 packed single-precision (32-bit) floating-point elements) from memory into dst. mem_addr does not need to be aligned on any particular boundary.
m128 loadu_ps128(float const* a)
{
	m128 out;

	out[0] = a[0];
	out[1] = a[1];
	out[2] = a[2];	
	out[3] = a[3];

	return out;
}

//load_ps128 is the non-SSE version of {m128 _mm_load_ps (float const* mem_addr)}
//Load 128-bits (composed of 4 packed single-precision (32-bit) floating-point elements) from memory into dst. mem_addr must be aligned on a 16-byte boundary or a general-protection exception may be generated.
m128 load_ps128(float const* a)
{
	m128 out;

	out[0] = a[0];
	out[1] = a[1];
	out[2] = a[2];	
	out[3] = a[3];

	return out;
}

//dp_ps128 is the non-SSE version of {m128 _mm_dp_ps (m128 a, m128 b, const int imm8)}
//Conditionally multiply the packed single-precision (32-bit) floating-point elements in a and b using the high 4 bits in imm8, sum the four products, and conditionally store the sum in dst using the low 4 bits of imm8.
m128 dp_ps128(m128 const &a, m128 const &b, const unsigned imm8)
{
	m128 temp, sum, out;

	for(int i=0; i<4; ++i)
	{
		if(((imm8 >> (i+4)) & 1) == 1)
		{
			temp[i] =  a[i] * b[i];
		}
		else
		{
			temp[i] = 0;
		}
	}
	
	sum[0] = (temp[3] + temp[2]) + (temp[1] + temp[0]);

	for(int i=0; i<4; ++i)
	{
		if(((imm8 >> i) & 1) == 1)
		{
			out[i] =  sum[0];
		}
		else
		{
			out[i] = 0;
		}
	}
	return out;
}

//_mm_sqrt_ss is the non-SSE version of{m128 _mm_sqrt_ss (m128 a)}
//Compute the square root of the lower single-precision (32-bit) floating-point element in a, store the result in the lower element of dst, and copy the upper 3 packed elements from a to the upper elements of dst.
m128 sqrt_ss128(m128 const &a)
{
	m128 out;

	out[0] = sqrtf(a[0]);
	out[1] = a[1];
	out[2] = a[2];
	out[3] = a[3];

	return out;	
}

//rsqrt_ss is the non-SSE version of {m128 _mm_rsqrt_ss (m128 a)
//Compute the approximate reciprocal square root of the lower single-precision (32-bit) floating-point element in a, store the result in the lower element of dst, and copy the upper 3 packed elements from a to the upper elements of dst. The maximum relative error for this approximation is less than 1.5*2^-12.
m128 rsqrt_ss(m128 const &a)
{
	m128 out;

	out[0] = 1.0 / sqrtf(a[0]);
	out[1] = a[1];
	out[2] = a[2];
	out[3] = a[3];

	return out;	
}

//addsub128 is the non-SSE version of {m128 _mm_addsub_ps (m128 a, m128 b)}
//Alternatively add and subtract packed single-precision (32-bit) floating-point elements in a to/from packed elements in b, and store the results in dst.
m128 addsub128(m128 const &a, m128 const &b)
{
    m128 addsub;
		for(int i=0; i<4; i++)
		{
			if(i%2 == 0)
				addsub[i] = a[i] - b[i];
			else
				addsub[i] = a[i] + b[i];
		}
    return addsub;
}