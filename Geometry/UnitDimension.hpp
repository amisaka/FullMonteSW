/*
 * UnitDimension.hpp
 *
 *  Created on: April 5, 2021
 *      Author: fynns
 */

#ifndef GEOMETRY_DIMENSIONUNIT_HPP_
#define GEOMETRY_DIMENSIONUNIT_HPP_

// This enum helps to have a consistent description of the dimension units
// between the Geometry and the Material classes

enum DimensionUnit { NONE, m, cm, mm };

#endif /* GEOMETRY_DIMENSIONUNIT_HPP_ */