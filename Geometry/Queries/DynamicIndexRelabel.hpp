/*
 * DynamicIndexRelabel.hpp
 *
 *  Created on: May 28, 2018
 *      Author: jcassidy
 */

#ifndef GEOMETRY_QUERIES_DYNAMICINDEXRELABEL_HPP_
#define GEOMETRY_QUERIES_DYNAMICINDEXRELABEL_HPP_

class Permutation;
#include <vector>

/** Dynamically relabels input indices in order of request, starting at zero.
 *
 * Construction
 * ------------
 *
 * 	reserve(unsigned N)			Allocate space for at least N elements
 *
 * 	operator()(unsigned i)		- First call for value i: assigns the next available index (starting at zero) and returns that value
 * 								- Subsequent calls for value i: returns the value previously associated with i
 *
 *
 * 		The Permutation returned by permutation() holds the indices to permute set E such that only elements that have been passed
 * 		as an argument to operator() will be present.
 *
 *
 * Example application
 * -------------------
 *
 * When taking a subset of tetrahedral cells from a mesh, this will efficiently permit relabeling the points so that only the
 * points incident on the chosen tetrahedra will be extracted.
 *
 */

class DynamicIndexRelabel
{
public:
	void reserve(unsigned N);						///< Pre-allocate at least N places
	unsigned operator()(unsigned i);					///< Return the new label for the specified index

	unsigned outputCount() const;					///< Return the number of points relabelled
	Permutation* permutation() const;				///< Return a Permutation of the original input that yields the given relabeling

private:
	std::vector<unsigned>		m_remap;
	unsigned						m_nextFreeIndex=0;
};

#endif /* GEOMETRY_QUERIES_DYNAMICINDEXRELABEL_HPP_ */
