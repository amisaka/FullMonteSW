/*
 * RayThroughMesh.hpp
 *
 *  Created on: May 17, 2017
 *      Author: jcassidy
 */

#ifndef GEOMETRY_QUERIES_RAYTHROUGHMESH_HPP_
#define GEOMETRY_QUERIES_RAYTHROUGHMESH_HPP_

/** Tracks a ray through a TetraMesh
 *
 */

class RayThroughMesh
{
public:

	void mesh(const TetraMesh* M);

private:
};




#endif /* GEOMETRY_QUERIES_RAYTHROUGHMESH_HPP_ */
