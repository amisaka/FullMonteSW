/*
 * Geometry.cpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#include "Geometry.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>

#include <algorithm>


Geometry::Geometry()
{
}

Geometry::~Geometry()
{
}

/**
 * @brief Convert input string into DimensionUnit enum
 * 
 * @param str Input string
 */
void Geometry::unitDimension(std::string str)
{
	//transfrom to lower case
	std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c){ return std::tolower(c); });
	if(str == "mm") 
	{
		m_unit = DimensionUnit::mm;
	} else if(str == "cm") 
	{
		m_unit = DimensionUnit::cm;
	} else if (str == "m") {
		m_unit = DimensionUnit::m;
	}else {
		LOG_ERROR << "Geometry::unitDimension unknown string " << str << " - defaulting to NONE" << std::endl;
		m_unit = DimensionUnit::NONE;
	}
}

/**
 * @brief Convert DimensionUnit enum to string
 * 
 * @return std::string -> enum as string
 */
std::string Geometry::unitDimension() const
{
	if(m_unit == DimensionUnit::mm) 
	{
		return "mm";
	} else if (m_unit == DimensionUnit::cm) 
	{
		return "cm";
	}else if (m_unit == DimensionUnit::m) 
	{
		return "m";
	} else
	{
		return "NONE";
	}
}

DimensionUnit Geometry::getUnit() const
{
	return m_unit;
}

void Geometry::regions(Partition* p)
{
	m_regions = p;
}

Partition* Geometry::regions() const
{
	return m_regions;
}
