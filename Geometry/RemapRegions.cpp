/*
 * RemapRegions.cpp
 *
 *  Created on: Mar 23, 2018
 *      Author: jcassidy
 */

#include "RemapRegions.hpp"

#include <vector>
#include <iostream>

#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>

using namespace std;

class RemapTable
{
public:
	void add(unsigned,unsigned);
	void remove(unsigned);

	void setDefault(unsigned);
	void unsetDefault();

	unsigned map(unsigned);

private:
	std::vector<unsigned>		m_mappings;
	unsigned						m_defaultValue=-1U;
};

void RemapTable::add(unsigned from,unsigned to)
{
	if(from >= m_mappings.size())
		m_mappings.resize(from+1);
	m_mappings[from]=to;
}

void RemapTable::remove(unsigned from)
{
	m_mappings[from] = -1U;
}

unsigned RemapTable::map(unsigned from)
{
	if (from >= m_mappings.size() || m_mappings[from]==-1U)
	{
		if (m_defaultValue == -1U)
			return from;
		else
			return m_defaultValue;
	}
	else
		return m_mappings[from];
}

void RemapTable::unsetDefault()
{
	m_defaultValue=-1U;
}

void RemapTable::setDefault(unsigned to)
{
	m_defaultValue=to;
}



RemapRegions::RemapRegions()
{
	m_remapTable = new RemapTable();
}

RemapRegions::~RemapRegions()
{
	delete m_remapTable;
}

void RemapRegions::addMapping(unsigned from,unsigned to)
{
	m_remapTable->add(from,to);
}

void RemapRegions::keepUnchanged(unsigned from)
{
	m_remapTable->add(from,from);
}

void RemapRegions::removeMapping(unsigned from)
{
	m_remapTable->remove(from);
}

void RemapRegions::leaveUnspecifiedAsIs()
{
	m_remapTable->unsetDefault();
}

void RemapRegions::remapUnspecifiedTo(unsigned to)
{
	m_remapTable->setDefault(to);
}


void RemapRegions::partition(Partition* p)
{
	m_inputPartition=p;
}

void RemapRegions::update()
{
	if (!m_inputPartition)
	{
		LOG_WARNING << "No input partition" << endl;
		if (m_result)
			delete m_result;
		return;
	}

	if(!m_result)
		m_result=new Partition();
	m_result->resize(m_inputPartition->size());

	for(unsigned i=0;i<m_inputPartition->size();++i)
		m_result->assign(i,m_remapTable->map(m_inputPartition->get(i)));
}

Partition* RemapRegions::result()
{
	return m_result;
}
