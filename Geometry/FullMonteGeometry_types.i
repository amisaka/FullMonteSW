#if defined(SWIGTCL)
	%typemap(out) std::array<float,3> {
		stringstream ss;
		std::array<float,3> a=$1;
		for(unsigned i=0;i<3;++i)
			ss << a[i] << ' ';
		Tcl_AppendResult(interp,ss.str().c_str(),nullptr);
	}

	%typemap(in) std::array<float,3>
	{
		std::array<float,3> p;
		stringstream ss(Tcl_GetString($input));
		for(unsigned i=0;i<3;++i)
			ss >> p[i];
		$1 = p;
	}

	%typemap(out) std::array<unsigned,3> {
		stringstream ss;
		std::array<unsigned,3> a=$1;
		for(unsigned i=0;i<3;++i)
			ss << a[i] << ' ';
		Tcl_AppendResult(interp,ss.str().c_str(),nullptr);
	}

	%typemap(in) std::array<unsigned,3>
	{
		std::array<unsigned,3> p;
		stringstream ss(Tcl_GetString($input));
		for(unsigned i=0;i<3;++i)
			ss >> p[i];
		$1 = p;
	}

	%typemap(out) Point<3,double> {
		stringstream ss;
		Point<3,double> a=$1;
		for(unsigned i=0;i<3;++i)
			ss << a[i] << ' ';
		Tcl_AppendResult(interp,ss.str().c_str(),nullptr);
	}


	%typemap(typecheck) std::array<float,3>
	{
		std::string str(Tcl_GetString($input));
		stringstream ss(str);
		$1 = 1;
		if (!ss.good())
			$1 = 0;
		float f;
		for(unsigned i=0;i<3;++i)
		{
			ss >> f;
			if (ss.fail())
			{
				$1=0;
				break;
			} 
		}
	}

	%typemap(in) std::array<double,3> {
		std::array<double,3> p;
		stringstream ss(Tcl_GetString($input));
		for(unsigned i=0;i<3;++i)
			ss >> p[i];
		$1 = p;
	}

	%typemap(typecheck) std::array<double,3> 
	{
		std::string str(Tcl_GetString($input));
		stringstream ss(str);
		$1 = 1;
		if (!ss.good())
			$1 = 0;
		double d;
		for(unsigned i=0;i<3;++i)
		{
			ss >> d;
			if (ss.fail())
			{
				$1=0;
				break;
			} 
		}
	}

	%typemap(in) Point<3,double> {
		Point<3,double> p;
		stringstream ss(Tcl_GetString($input));
		for(unsigned i=0;i<3;++i)
			ss >> p[i];
		$1 = p;
	}

	%typemap(typecheck) std::array<unsigned,3> 
	{
		std::string str(Tcl_GetString($input));
		stringstream ss(str);
		$1 = 1;
		if (!ss.good())
			$1 = 0;
		unsigned d;
		for(unsigned i=0;i<3;++i)
		{
			ss >> d;
			if (ss.fail())
			{
				$1=0;
				break;
			} 
		}
	}

	%typemap(typecheck, precedence=SWIG_TYPECHECK_DOUBLE_ARRAY) Point<3,double> 
	{
		std::string str(Tcl_GetString($input));
		stringstream ss(str);
		$1 = 1;
		if (!ss.good())
			$1 = 0;
		Point<3,double> p;
		for(unsigned i=0;i<3;++i)
		{
			ss >> p[i];
			if (ss.fail())
			{
				$1=0;
				break;
			} 
		}
	}
#elif defined(SWIGPYTHON)
	%typemap(out) std::array<float,3> 
	{
		int i;
		$result = PyList_New(3);
		for (i = 0; i < 3; i++)
		{
			PyObject *o = PyFloat_FromDouble((double) $1[i]);
			PyTuple_SetItem($result, i, o);
		}
	}

	%typemap(in) std::array<float,3> (float temp[3])
	{
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "fff", temp, temp+1, temp+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				SWIG_fail;
			}
		$1 = {temp[0], temp[1], temp[2]};
		} 
		else 
		{
		PyErr_SetString(PyExc_TypeError, "expected a tuple.");
		SWIG_fail;
		}

	}

	/* This typemap lets FullMonte accept a python list as input instead of a tuple
	%typemap(in) std::array<float,3> (float temp[3])
	{
		int i;
		if (!PySequence_Check($input)) 
		{
			PyErr_SetString(PyExc_ValueError, "Expected a sequence");
			SWIG_fail;
		}
		if (PySequence_Length($input) != 3)
		{
			PyErr_SetString(PyExc_ValueError, "Size mismatch. Expected 4 elements");
			SWIG_fail;
		}
		for (i = 0; i < 3; i++) 
		{
			PyObject *o = PySequence_GetItem($input, i);
			if (PyNumber_Check(o))
			{
				temp[i] = (float) PyFloat_AsDouble(o);
			}
			else 
			{
				PyErr_SetString(PyExc_ValueError, "Sequence elements must be numbers");      
				SWIG_fail;
			}
		}
		$1 = temp;
	}
	*/

	%typemap(out) std::array<unsigned,3>
	{
		int i;
		$result = PyList_New(3);
		for (i = 0; i < 3; i++)
		{
			PyObject *o = PyInt_FromLong((long) $1[i]);
			PyTuple_SetItem($result, i, o);
		}
	}

	%typemap(in) std::array<unsigned,3> (unsigned temp[4])
	{
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "III", temp, temp+1, temp+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				SWIG_fail;
			}	
		$1 = {temp[0], temp[1], temp[2]};
		} 
		else 
		{
		PyErr_SetString(PyExc_TypeError, "expected a tuple.");
		SWIG_fail;
		}
	}

	%typemap(out) Point<3,double>
	{
		int i;
		Point<3,double> a=$1;
		$result = PyList_New(3);
		for (i = 0; i < 3; i++)
		{
			PyObject *o = PyFloat_FromDouble((double) a[i]);
			PyTuple_SetItem($result, i, o);
		}
	}


	%typemap(typecheck) std::array<float,3>
	{
		float f[3];
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "fff", f, f+1, f+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				$1 = 0;
				SWIG_fail;
			}
			else
			{
				$1 = 1;
			}		
		} 
		else 
		{
			PyErr_SetString(PyExc_TypeError, "expected a tuple.");
			$1 = 0;
			SWIG_fail;
		}
	}

	%typemap(in) std::array<double,3> (double temp[3])
	{
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "ddd", temp, temp+1, temp+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				SWIG_fail;
			}	
		$1 = {temp[0], temp[1], temp[2]};
		} 
		else 
		{
		PyErr_SetString(PyExc_TypeError, "expected a tuple.");
		SWIG_fail;
		}
	}


	%typemap(typecheck) std::array<double,3> 
	{
		double d[3];
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "ddd", d, d+1, d+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				$1 = 0;
				SWIG_fail;
			}
			else
			{
				$1 = 1;
			}		
		} 
		else 
		{
			PyErr_SetString(PyExc_TypeError, "expected a tuple.");
			$1 = 0;
			SWIG_fail;
		}
	}

	%typemap(in) Point<3,double> (double temp[3])
	{
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "ddd", temp, temp+1, temp+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				SWIG_fail;
			}	
		$1 = Point<3, double>(temp[0], temp[1], temp[2]);
		} 
		else 
		{
		PyErr_SetString(PyExc_TypeError, "expected a tuple.");
		SWIG_fail;
		}
	}

	%typemap(typecheck, precedence=SWIG_TYPECHECK_DOUBLE_ARRAY) Point<3,double>
	{
		double temp[3];
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "ddd", temp, temp+1, temp+2))
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				$1 = 0;
				SWIG_fail;
			}
			else
			{
				$1 = 1;
			}		
		} 
		else 
		{
			PyErr_SetString(PyExc_TypeError, "expected a tuple.");
			$1 = 0;
			SWIG_fail;
		}
	}

	%typemap(typecheck) std::array<unsigned,3> 
	{
		unsigned u[3];
		if (PyTuple_Check($input))
		{
			if (!PyArg_ParseTuple($input, "III", u, u+1, u+2)) 
			{
				PyErr_SetString(PyExc_TypeError, "tuple must have 3 elements");
				$1 = 0;
				SWIG_fail;
			}
			else
			{
				$1 = 1;
			}		
		} 
		else 
		{
			PyErr_SetString(PyExc_TypeError, "expected a tuple.");
			$1 = 0;
			SWIG_fail;
		}
	}
#else
	#warning Requested wrapping language not supported
#endif