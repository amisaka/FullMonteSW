/*
 * Layered.hpp
 *
 *  Created on: Jun 1, 2017
 *      Author: jcassidy
 */

#ifndef GEOMETRY_LAYERED_HPP_
#define GEOMETRY_LAYERED_HPP_

#include "Geometry.hpp"
#include <array>
#include <vector>

class Layer;
class Materials;
class MaterialSet;

/** Layered geometry.
 *
 * Layers are oriented in the xy-plane, extending into +z.
 *
 * NOTE: Layers start at 1 (matching MCML convention), as do radial & depth bins; each has a thickness tN
 *
 * Layer[1] runs (r,z) = 	(0,t1)
 * Layer[2] runs 			(t1,t1+t2) etc.
 *
 *
 *
 * Also contains a radially-symmetric volume scoring system
 *
 * ri = 1..Nr		ri == 0 -> not-a-radial-bin
 * zi = 1..Nz		zi == 0 -> not-a-depth-bin
 *
 * Linear index runs 1..NrNz for a total of NrNz+1 bins
 * 1D iteration is done row-major over [r][z]
 *
 * A[1][1] A[1][2] .. A[1][Nz] A[2][1] A[2][2] ...
 *
 *
 * TODO: Produce a linear iteration scheme similar to TetraMesh so one can do
 * 		for(const auto v : l->volumeBins())
 */

struct CylIndex
{
	unsigned ri;
	unsigned zi;

	bool operator==(CylIndex rhs) const;
};

std::ostream& operator<<(std::ostream& os,CylIndex ci);

inline bool CylIndex::operator==(CylIndex rhs) const { return rhs.ri == ri && rhs.zi == zi; }


struct CylCoord
{
	float r;
	float z;
};

class Layered : public Geometry
{
public:
	Layered();
	virtual ~Layered();

	virtual SpatialMap<float>*		elementVolumes() const override;
	virtual SpatialMap<float>*		surfaceAreas() const override;
	virtual SpatialMap<float>*		directedSurfaceAreas() const override;

	/// Create regions map from layer definitions
	virtual void buildRegions();


	/// Get/set spatial domain
	void					resolution(float dr,float dz);
	CylCoord				resolution() const;

	void					extent(float R,float Z);
	CylCoord				extent() const;

	void					dims(unsigned Nr,unsigned Nz);
	CylIndex				dims() const;

	void					angularBins(unsigned Na);
	unsigned				angularBins() const;
	float					angularResolution() const;


	unsigned				bins() const;						///< Return the number of spatial bins


	/// Query layers
	unsigned				layerCount() const;
	Layer*					layer(unsigned i) const;

	/// Add layers
	void					addLayer(Layer* l);

	CylIndex				binFromIndex(unsigned i) const;	///< Get 2D (ri,zi) bin for a linear index
	unsigned				indexForBin(CylIndex ci) const;	///< Get linear index 1..Nr*Nz for 2D index (ri,zi)

	float					binVolume(unsigned i)	const;	///< Size of i'th bin, with (r,z) row-major
	float					binVolume(CylIndex ci) 	const;	///< Size of bin (r,z)

private:
	void updateMaterials();

	unsigned				m_Na=1U;
	unsigned				m_Nr=0U;
	unsigned				m_Nz=0U;

	float					m_dz=0.0f;
	float					m_dr=0.0f;

	std::vector<Layer*>		m_layers;

	MaterialSet*			m_materials=nullptr;
};





#endif /* GEOMETRY_LAYERED_HPP_ */
