#if defined(SWIGTCL)
%module FullMonteGeometryTCL
%begin %{

#include <FullMonteSW/Warnings/SWIG.hpp>

	// global var to hold interp* because VTK wrapping tools don't expose it
	struct Tcl_Interp;

	extern "C" {
		Tcl_Interp* global_interp=nullptr;
	}
%}
#elif defined(SWIGPYTHON)
%module Geometry
%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");


%include "std_string.i"
%include "std_vector.i"

%include "FullMonteGeometry_types.i"


%header %{

#include <FullMonteSW/Geometry/Sources/Abstract.hpp>
#include <FullMonteSW/Geometry/Sources/Ball.hpp>
#include <FullMonteSW/Geometry/Sources/Line.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/Composite.hpp>
#include <FullMonteSW/Geometry/Sources/Surface.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/SurfaceTri.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>
#include <FullMonteSW/Geometry/Sources/TetraFace.hpp>
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>
#include <FullMonteSW/Geometry/Sources/CylDetector.hpp>
#include <FullMonteSW/OutputTypes/clonable.hpp>
#include <FullMonteSW/OutputTypes/visitable.hpp>
#include <FullMonteSW/Geometry/Point.hpp>
#include <FullMonteSW/Geometry/Vector.hpp>
#include <FullMonteSW/Geometry/Geometry.hpp>
#include <FullMonteSW/Geometry/Layered.hpp>
#include <FullMonteSW/Geometry/Layer.hpp>
#include <FullMonteSW/Geometry/Ray.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/Cells.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/RemapRegions.hpp>

#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>

#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>

#include <sstream>

%}

%rename(__lshift__) operator<<;
#if defined(SWIGPYTHON)
    %rename("fm_%s", match$name="set") "";
#endif


%include "Sources/Abstract.hpp"
%include "Sources/Directed.hpp"
%include "Sources/Point.hpp"
%include "Sources/Volume.hpp"
%include "Sources/Ball.hpp"
%include "Sources/Composite.hpp"
%include "Sources/Surface.hpp"
%include "Sources/SurfaceTri.hpp"
%include "Sources/Line.hpp"
%include "Sources/Fiber.hpp"
%include "Sources/TetraFace.hpp"
%include "Sources/Cylinder.hpp"
%include "Sources/CylDetector.hpp"
%include "Sources/PencilBeam.hpp"
%include "Geometry.hpp"
%include "Layered.hpp"
%include "Layer.hpp"
%include "Material.hpp"
%include "MaterialSet.hpp"
%include "Points.hpp"
%include "RemapRegions.hpp"
%include "Partition.hpp"
%include "Predicates/GeometryPredicate.hpp"
%include "Predicates/VolumeCellPredicate.hpp"
%include "Predicates/VolumeCellInRegionPredicate.hpp"

%include "WrappedVector.hpp"
%template(WV4) WrappedVector<std::array<unsigned,4>,unsigned>;
%template(WV3) WrappedVector<std::array<unsigned,3>,unsigned>;

%include "Cells.hpp"
%template(TetraCells) 	Cells<4>;
%template(FaceCells) 	Cells<3>;


%include "Queries/TetraEnclosingPointByLinearSearch.hpp"
// %include "Queries/TetraMeshRTree.hpp"
%include "TetraMesh.hpp"

#if defined(SWIGTCL)
%typemap(in,numinputs=0) Tcl_Interp* interp
{
	$1=interp;
}




%init %{
	// set global_interp pointer because VTK wrapper generator doesn't expose it
	global_interp=interp;
%}




// Must be placed in header, because wrapper has extern "C" linkage
%header %{

// type info for templated SWIG pointer generation
#include <FullMonteSW/VTK/swig_traits.hpp>

class TetraMesh;

template<>const swig_type_info* 	swig_traits<TetraMesh*>::type_info(){ return SWIGTYPE_p_TetraMesh; }
template<>const char*			swig_traits<TetraMesh*>::type_name = "TetraMesh*";	

#include <iostream>
using namespace std;

void* decodeSwigPointerBase(const char* ptr,const swig_type_info* typeinfo)
{
	void* p = nullptr;
	if (!global_interp)
		std::cout << "global_interp null in decodeSwigPointerBase(ptr,typeinfo)" << std::endl;
	else if (SWIG_Tcl_ConvertPtrFromString(global_interp,ptr,&p,const_cast<swig_type_info*>(typeinfo),0))
	{
		std::cout << "Nonzero return code from SWIG_Tcl_ConvertPtrFromString(interp,ptr,&p,typeinfo,0)" << std::endl;
		std::cout << "  interp=" << global_interp << std::endl;
		std::cout << "  ptr=" << ptr << std::endl;
		p = nullptr;
	}
	else
	{

	}
	
	return p;
}

Tcl_Obj* createSwigObjectInstanceBase(void *ptr,const swig_type_info* objSwigType, const char* objName)
{
	if(!global_interp)
	{
		std::cout << "global_interp null in decodeSwigPointerBase(ptr,typeinfo)" << std::endl;
		return nullptr;
	}
	
	std::cout << "Creating object interp=" << global_interp << " type=" << objSwigType << " ptr=" << ptr << std::endl;
	
	// Create instance object (command of form _XXXXXXXXXXXXXXXX_p_TTTTT)
	Tcl_Obj* obj = SWIG_Tcl_NewInstanceObj(global_interp,ptr,const_cast<swig_type_info*>(objSwigType),0);
	
	std::cout << "New object at " << obj << " named " << Tcl_GetStringFromObj(obj,nullptr) << std::endl;

	// If name provided, also create command with name referring to same object
	if (objName)
	{
		Tcl_CmdInfo cmdInfo;

		if (!Tcl_GetCommandInfo(global_interp, Tcl_GetStringFromObj(obj,nullptr), &cmdInfo))
		{
			std::cout << "createSwigObjectInstanceBase(interp,ptr,objSwigType,objName) ERROR: bad return from Tcl_GetCommandInfo(interp,\"" << Tcl_GetStringFromObj(obj,nullptr) << "\",&cmdInfo);" << std::endl;
		}
		else {
			swig_instance* newinst = (swig_instance *) malloc(sizeof(swig_instance));

			newinst->thisptr = obj;
			Tcl_IncrRefCount(obj);

			newinst->thisvalue = ptr;
			newinst->classptr = static_cast<swig_class*>(objSwigType->clientdata);
			newinst->destroy = 0;

			if (newinst->destroy) {
				SWIG_Acquire(newinst->thisvalue);
			}

			Tcl_CreateObjCommand(global_interp, objName, (swig_wrapper_func) SWIG_MethodCommand, static_cast<void*>(newinst), (swig_delete_func) SWIG_ObjectDelete);
		}
	}
	
	return obj;
}


%}
#elif defined(SWIGPYTHON)
// TODO Support SWIG - VTK python interface
// // Must be placed in header, because wrapper has extern "C" linkage
// %header %{

// #include <vtkPythonUtil.h>
// // type info for templated SWIG pointer generation
// #include <FullMonteSW/VTK/swig_traits.hpp>

// class TetraMesh;

// template<>const swig_type_info* swig_traits<TetraMesh*>::type_info(){ return SWIGTYPE_p_TetraMesh; }
// template<>const char*			swig_traits<TetraMesh*>::type_name = "TetraMesh*";	

// #include <iostream>
// using namespace std;

// void* decodeSwigPointerBase(PyObject* obj,const swig_type_info* typeinfo)
// {
// 	void* p = nullptr;
// 	if (SWIG_ConvertPtr(obj,&p,const_cast<swig_type_info*>(typeinfo), SWIG_POINTER_EXCEPTION))
// 	{
// 		std::cout << "Nonzero return code from SWIG_ConvertPtr(obj,&p,typeinfo,SWIG_POINTER_EXCEPTION)" << std::endl;
// 		std::cout << "  obj=" << obj << std::endl;
// 		p = nullptr;
// 	}
// 	return p;
// }

// PyObject* createSwigObjectInstanceBase(void *ptr,const swig_type_info* objSwigType)
// {	
// 	std::cout << "Creating object type=" << objSwigType << " ptr=" << ptr << std::endl;
	
	// Create instance object (command of form _XXXXXXXXXXXXXXXX_p_TTTTT)
	// PyObject* obj = SWIG_Python3_NewPointerObj(NULL, ptr, const_cast<swig_type_info*>(objSwigType), 0);
	// PyObject *old_str = PyObject_Str(obj);
	// std::cout << "New object at " << obj << " named " << SWIG_Python3_str_AsChar(old_str) << std::endl;

	// If name provided, also create command with name referring to same object
	// if (objName)
	// {
	// 	Tcl_CmdInfo cmdInfo;

	// 	if (!Tcl_GetCommandInfo(global_interp, SWIG_Python3_str_AsChar(old_str), &cmdInfo))
	// 	{
	// 		std::cout << "createSwigObjectInstanceBase(interp,ptr,objSwigType,objName) ERROR: bad return from Tcl_GetCommandInfo(interp,\"" << Tcl_GetStringFromObj(obj,nullptr) << "\",&cmdInfo);" << std::endl;
	// 	}
	// 	else {
	// 		swig_instance* newinst = (swig_instance *) malloc(sizeof(swig_instance));

	// 		newinst->thisptr = obj;
	// 		Py_INCREF(obj);

	// 		newinst->thisvalue = ptr;
	// 		newinst->classptr = static_cast<swig_class*>(objSwigType->clientdata);
	// 		newinst->destroy = 0;

	// 		if (newinst->destroy) {
	// 			SWIG_Python3_AcquirePtr(newinst->thisptr, 0);
	// 		}

	// 		Tcl_CreateObjCommand(global_interp, objName, (swig_wrapper_func) SWIG_MethodCommand, static_cast<void*>(newinst), (swig_delete_func) SWIG_ObjectDelete);
	// 	}
	// }
	
// 	return obj;
// }


// %}

// %typemap(out) TetraMesh*
// {
// 	PyImport_ImportModule("vtk");
// 	$result = vtkPythonUtil::GetObjectFromPointer((TetraMesh*)$1);
// }

// %typemap(in) TetraMesh*
// {
// 	$1 = (TetraMesh* ) vtkPythonUtil::GetPointerFromObject($input, "TetraMesh");
// 	if ($1 == NULL)
// 		SWIG_fail;
// }

#else
	#warning Requested wrapping language not supported
#endif
