/*
 * Material.cpp
 *
 *  Created on: May 29, 2017
 *      Author: jcassidy
 */

#include "Material.hpp"
#include <FullMonteSW/Logging/FullMonteLogger.hpp>
#include <algorithm>

Material::Material()
{

}

Material::Material(float muA,float muS,float g_,float n_) :
		mu_s(muS), mu_a(muA), g(g_), n(n_)
{
}

Material::~Material()
{

}

/**
 * @brief Convert input string into DimensionUnit enum
 * 
 * @param str Input string
 */
void Material::unitDimension(std::string str)
{
	//transfrom to lower case
	std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c){ return std::tolower(c); });
	if(str == "mm") 
	{
		unit = DimensionUnit::mm;
	} else if(str == "cm") 
	{
		unit = DimensionUnit::cm;
	} else if (str == "m") {
		unit = DimensionUnit::m;
	}else {
		LOG_ERROR << "Material::unitDimension unknown string " << str << " - defaulting to NONE" << std::endl;
		unit = DimensionUnit::NONE;
	}
}

/**
 * @brief Convert DimensionUnit enum to string
 * 
 * @return std::string -> enum as string
 */
std::string Material::unitDimension()
{
	if(unit == DimensionUnit::mm) 
	{
		return "mm";
	} else if (unit == DimensionUnit::cm) 
	{
		return "cm";
	}else if (unit == DimensionUnit::m) 
	{
		return "m";
	} else
	{
		return "NONE";
	}
}

DimensionUnit Material::getUnit()
{
	return unit;
}

void Material::reducedScatteringCoeff(float mu_s_prime_)
{
	mu_s = mu_s_prime_/(1-g);
}

void Material::reducedScatteringCoeffWithG(float mu_s_prime_,float g_)
{
	g = g_;
	reducedScatteringCoeff(mu_s_prime_);
}

float Material::reducedScatteringCoeff() const
{
	return (1-g)*mu_s;
}

float Material::scatteringCoeff() const
{
	return mu_s;
}

void Material::scatteringCoeff(float mu_s_)
{
	mu_s = mu_s_;
}

float Material::absorptionCoeff() const
{
	return mu_a;
}

void Material::absorptionCoeff(float mu_a_)
{
	mu_a = mu_a_;
}

void Material::anisotropy(float g_)
{
	g = g_;
}

float Material::anisotropy() const
{
	return g;
}

float Material::refractiveIndex() const
{
	return n;
}

void Material::refractiveIndex(float n_)
{
	n = n_;
}
