#### Convert formats for other simulators
CONVERT_TIMOS_MESH_TO_VTK           (mouse.mesh mouse.vtk)
CONVERT_TIMOS_MESH_TO_MMC           (mouse.mesh mouse.mmc.dat)
CONVERT_TIMOS_MATERIALS_TO_MMC      (mouse.opt  mouse.mmc.dat)