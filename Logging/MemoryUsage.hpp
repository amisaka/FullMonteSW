/*
 * MemoryUsage.hpp
 *  Prints peak memory usage.
 *  Copied from VTR (url: https://github.com/verilog-to-routing/vtr-verilog-to-routing/blob/e5ff75cc76f83ee2a7a5c4bbda0a278e6980239c/libs/libvtrutil/src/vtr_rusage.cpp)
 *  
 *  Created on: September 2, 2021
 *      Author: Shuran Wang
 */

#ifndef LOGGING_MEMORYUSAGE_HPP_
#define LOGGING_MEMORYUSAGE_HPP_

#include "FullMonteLogger.hpp"
#ifdef __unix__
    #include <sys/time.h>
    #include <sys/resource.h>
#endif

///@brief Returns the maximum resident set size in bytes, or zero if unable to determine.
size_t get_max_rss() {
    size_t max_rss = 0;

#ifdef __unix__
    rusage usage;
    int result = getrusage(RUSAGE_SELF, &usage);

    if (result == 0) { //Success
        //ru_maxrss is in kilobytes, convert to bytes
        max_rss = usage.ru_maxrss * 1024;
    }
#else
    //Do nothing, other platform specific code could be added here
    //with appropriate defines
#endif
    return max_rss;
}

#endif // LOGGING_MEMORYUSAGE_HPP_