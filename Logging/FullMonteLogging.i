#if defined(SWIGTCL)
%module FullMonteLoggingTCL
#elif defined(SWIGPYTHON)
%module Logging
#else
	#warning Requested wrapping language not supported
#endif
%feature("autodoc", "3");

%begin %{
#include <FullMonteSW/Warnings/SWIG.hpp>
%}

%include "std_string.i"

%{
#include "FullMonteLogger.hpp"
#include "FullMonteTimer.hpp"
%}

%include "FullMonteLogger.hpp"
%include "FullMonteTimer.hpp"


