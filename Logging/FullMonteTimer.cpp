/*
 * FullMonteTimer.cpp
 *  Useful timer that keeps track of the time elapsed and peak memory usage.
 *  Copied from VTR (url: https://github.com/verilog-to-routing/vtr-verilog-to-routing/blob/e5ff75cc76f83ee2a7a5c4bbda0a278e6980239c/libs/libvtrutil/src/vtr_time.cpp)
 *  
 *  Created on: September 4, 2021
 *      Author: Shuran Wang
 */

#include "FullMonteTimer.hpp"

///@brief Constructor
FullMonteTimer::FullMonteTimer()
    : start_(clock::now())
    , initial_max_rss_(get_max_rss()) {
}

///@brief Returns the elapsed seconds since construction
float FullMonteTimer::elapsed_sec() const {
    return std::chrono::duration<float>(clock::now() - start_).count();
}

///@brief Returns the maximum resident size (rss) in Mebibytes
float FullMonteTimer::max_rss_mib() const {
    return get_max_rss() / BYTE_TO_MIB;
}

///@brief Returns the change in maximum resident size in Mebibytes
float FullMonteTimer::delta_max_rss_mib() const {
    return (get_max_rss() - initial_max_rss_) / BYTE_TO_MIB;
}