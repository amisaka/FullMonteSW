ADD_LIBRARY(FullMonteLogging SHARED
    FullMonteLogger.cpp
    FullMonteTimer.cpp
)

TARGET_LINK_LIBRARIES(FullMonteLogging ${Boost_LIBRARIES})

ADD_DEFINITIONS(-DBOOST_LOG_DYN_LINK)

IF (WRAP_TCL)
    SET_SOURCE_FILES_PROPERTIES(FullMonteLogging.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(FullMonteLoggingTCL 
        TYPE SHARED 
        LANGUAGE tcl 
        SOURCES FullMonteLogging.i
    )
    SWIG_LINK_LIBRARIES(FullMonteLoggingTCL 
        ${TCL_LIBRARY} 
        FullMonteLogging
    )
                    
    INSTALL(TARGETS FullMonteLoggingTCL LIBRARY 
        DESTINATION lib
    )
ENDIF()

IF (WRAP_PYTHON)
    SET_SOURCE_FILES_PROPERTIES(FullMonteLogging.i PROPERTIES CPLUSPLUS ON)
    SWIG_ADD_LIBRARY(Logging 
        TYPE SHARED 
        LANGUAGE python 
        SOURCES FullMonteLogging.i
    )
    SWIG_LINK_LIBRARIES(Logging 
        ${Python3_LIBRARIES} 
        FullMonteLogging
    )
    SET_TARGET_PROPERTIES(_Logging PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/fmpy)
ENDIF()


INSTALL(TARGETS FullMonteLogging LIBRARY DESTINATION lib)
