package require FullMonte

file delete "colin27.test.vtk"

FullMonte::close_window_if_present

TIMOSMeshReader R
    R filename "$FullMonte::datadir/Colin27/Colin27.mesh"
    R read

set M [R mesh]

VTKMeshWriter W
    W mesh $M
    W filename "colin27.test.vtk"
    W write

if { [file exists "colin27.test.vtk"] == 0 } { error "Mesh file not written due to error in VTKMeshwriter"}

exit
