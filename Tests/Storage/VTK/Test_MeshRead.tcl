package require FullMonte

FullMonte::close_window_if_present

VTKMeshReader R
    R filename "$FullMonte::datadir/Colin27/Colin27.vtk"
    R read

set M [R mesh]


if { [[$M points] size] != 70228 } { error "Expecting 70227 points, found [[$M points] size]"}

## Call to [[$M tetraCells] size] busted on Docker image, but not Mac OS. Different SWIG version (3.0.8 Docker v. 3.0.12 Mac?)
#puts "Read mesh with [[$M points] size] points and [[$M tetraCells] size]"
#if { [[$M tetraCells] size] != 423376 } { puts "Expecting 423376 cells, found [[$M tetraCells] size]"; return 1; }

exit
