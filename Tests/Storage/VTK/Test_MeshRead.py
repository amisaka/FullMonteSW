import os
import fmpy

R = fmpy.VTKFiles.VTKMeshReader()
meshpath = os.path.join(fmpy.datadir, 'Colin27/Colin27.vtk')
R.filename(meshpath)
R.read()

M = R.mesh()

if M.points().size() != 70228:
    msg = "Expecting 70228 points, found {}"
    raise Exception(msg.format(M.points().size()))

