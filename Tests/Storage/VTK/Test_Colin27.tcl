package require FullMonte

## Read VTK mesh

set fn "$FullMonte::datadir/Colin27/Colin27.vtk"

VTKMeshReader R
    R filename $fn
    R read

set M [R mesh]


## create material set

MaterialSet MS

Material air
    air   scatteringCoeff    0
    air   absorptionCoeff    0
    air   refractiveIndex    1.0
    air   anisotropy         0.0

Material skull
    skull scatteringCoeff    7.80
    skull absorptionCoeff    0.019
    skull anisotropy         0.89
    skull refractiveIndex    1.37

Material csf
    csf scatteringCoeff    0.009
    csf absorptionCoeff    0.004
    csf anisotropy         0.89
    csf refractiveIndex    1.37

Material gray
    gray scatteringCoeff    9.00
    gray absorptionCoeff    0.02
    gray anisotropy         0.89
    gray refractiveIndex    1.37

Material white
    white scatteringCoeff   40.90
    white absorptionCoeff    0.08
    white anisotropy         0.89
    white refractiveIndex    1.37

MS exterior air
MS append skull
MS append csf
MS append gray
MS append white

# Create source
Point P
    P position "71.2 149.8 127.5"

# Set up kernel

TetraVolumeKernel k
    k packetCount 100000
    k source P
    k geometry $M
    k materials MS

# Run and wait until completes
    k runSync

# Get results OutputDataCollection
set ODC [k results]


# List results (for demonstration purposes only)
puts "Results available: "
for { set i 0 } { $i < [$ODC size] } { incr i } {
    set res [$ODC getByIndex $i]
    puts "  type=[[$res type] name] name=[$res name]"
}

# Convert energy absorbed per volume element to volume average fluence
EnergyToFluence EF
    EF kernel k
    EF data [$ODC getByName "VolumeEnergy"]
    EF inputEnergy
    EF outputFluence
    EF update

# Write the mesh with fluence appended
VTKMeshWriter W
    W filename "colin27.out.vtk"
    W addData "Fluence" [EF result]
    W mesh $M
    W write

# Write the fluence values only to a text file
TextFileMatrixWriter TW
    TW filename "colin27.phi_v.txt"
    TW source [EF result]
    TW write
