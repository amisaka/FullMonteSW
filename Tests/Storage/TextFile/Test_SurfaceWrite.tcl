package require FullMonte

file delete "colin27.EV.txt"
file delete "colin27.ES.txt"
file delete "colin27_dirsurf.txt"

FullMonte::close_window_if_present

## Read VTK mesh

set fn "$FullMonte::datadir/Colin27/Colin27.vtk"

VTKMeshReader R
    R filename $fn
    R read

set M [R mesh]


## create material set

MaterialSet MS

Material air
    air   scatteringCoeff    0
    air   absorptionCoeff    0
    air   refractiveIndex    1.0
    air   anisotropy         0.0

Material skull
    skull scatteringCoeff    7.80
    skull absorptionCoeff    0.019
    skull anisotropy         0.89
    skull refractiveIndex    1.37

Material csf
    csf scatteringCoeff    0.009
    csf absorptionCoeff    0.004
    csf anisotropy         0.89
    csf refractiveIndex    1.37

Material gray
    gray scatteringCoeff    9.00
    gray absorptionCoeff    0.02
    gray anisotropy         0.89
    gray refractiveIndex    1.37

Material white
    white scatteringCoeff   40.90
    white absorptionCoeff    0.08
    white anisotropy         0.89
    white refractiveIndex    1.37

MS exterior air
MS append skull
MS append csf
MS append gray
MS append white

# Create source
Point P
    P position "71.2 149.8 127.5"

# Set up kernel

VolumeCellInRegionPredicate vol
vol setRegion 2

TetraInternalKernel k
    k packetCount 100
    k source P
    k geometry $M
    k materials MS
   [k directedSurfaceScorer] addScoringRegionBoundary vol

set isRelease [string equal $FullMonte::buildtype "Release"]
if {$isRelease} {
    k packetCount 1000000
} else {
    k packetCount 100
}
# Run and wait until completes
    k runSync

# Get results OutputDataCollection
set ODC [k results]

# Convert energy absorbed per volume element to volume average fluence
# Use DirectedSurface data instead of SurfaceExitEnergy
EnergyToFluence EF
    EF kernel k
    EF inputEnergy
    EF outputFluence
    EF data [$ODC getByName "VolumeEnergy"]
    EF update

# Write the surface mesh with fluence appended
# Test if TextFileMatrixWriter can still write out external surfaces
TextFileMatrixWriter W_vol
    W_vol filename "colin27.EV.txt"
    W_vol source [EF result]
    W_vol write

if { [file exists "colin27.EV.txt"] == 0 } { error "Mesh file not written for VolumeEnergy"}

EF data [$ODC getByName "SurfaceExitEnergy"]
EF update

# Write the surface mesh with fluence appended
# Test if TextFileMatrixWriter can still write out external surfaces
TextFileMatrixWriter W_surf
    W_surf filename "colin27.ES.txt"
    W_surf source [EF result]
    W_surf write

if { [file exists "colin27.ES.txt"] == 0 } { error "Mesh file not written for SurfaceExitEnergy"}

EF data [$ODC getByType "DirectedSurface"]
EF update

# Write the surface mesh with fluence appended
TextFileMatrixWriter W_dirsurf
    W_dirsurf filename "colin27_dirsurf.txt"
    W_dirsurf source [EF result]
    W_dirsurf write


if { [file exists "colin27_dirsurf.txt"] == 0 } { error "Mesh file not written for DirectedSurface"}

exit