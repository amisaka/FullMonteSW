IF(WRAP_TCL)

	ADD_TEST(NAME unit_storage_txt_writer
		COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_SurfaceWrite.tcl)
	ADD_TEST(NAME unit_storage_dose_histogram_writer
		COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_DoseHistogramWrite.tcl)
	
ENDIF()

IF(WRAP_PYTHON)
	ADD_TEST(NAME unit_storage_txt_writer_py
		COMMAND ${CMAKE_BINARY_DIR}/bin/pymonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_SurfaceWrite.py)
	ADD_TEST(NAME unit_storage_dose_histogram_writer_py
		COMMAND ${CMAKE_BINARY_DIR}/bin/pymonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_DoseHistogramWrite.py)
ENDIF()