#### Copy test files

# onecube.mesh is a TIM-OS formatted version of MMC's onecube test case
FILE(COPY onecube.mesh
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR})


#### Test case that reads onecube.mesh and writes {elem,node,velem,facenb}_onecube.mmc.dat
ADD_BOOST_UNIT_TEST(Test_MMCMeshWriter Test_MMCMeshWriter.cpp)
TARGET_LINK_LIBRARIES(Test_MMCMeshWriter FullMonteMMCFile FullMonteGeometry FullMonteTIMOS)

### Test case that reads TestData/
ADD_BOOST_UNIT_TEST(Test_MMCJSONReader Test_MMCJSONReader.cpp)
TARGET_LINK_LIBRARIES(Test_MMCJSONReader FullMonteMMCFile)


## Convert simple "onecube" case from TIM-OS format to MMC mesh
# Test that conversion runs
ADD_TEST(NAME Test_MMCMeshWriter
    COMMAND Test_MMCMeshWriter)

FILE(COPY 
        diff_mmc_mesh_text.sh
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    FILE_PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

    
# Test that output meshes are text identical
ADD_TEST(NAME Test_MMCMeshWriter_Output_Matches
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/diff_mmc_mesh_text.sh ${CMAKE_CURRENT_SOURCE_DIR} onecube.expected.dat ${CMAKE_CURRENT_BINARY_DIR} onecube.mmc.dat)


## Test code that writes JSON file and run script            
ADD_TEST(NAME Test_MakeJSON_Runs
    COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/MakeJSON.tcl)
    
ADD_TEST(NAME Test_MakeJSON_ConfigFile_Matches
    COMMAND diff onecube.json ${CMAKE_CURRENT_SOURCE_DIR}/onecube.expected.json)
    
ADD_TEST(NAME Test_MakeJSON_Script_Matches
    COMMAND diff onecube.mmc.sh ${CMAKE_CURRENT_SOURCE_DIR}/onecube.expected.sh)
    
ADD_TEST(NAME Test_MakeJSON_MMC_Runs
    COMMAND onecube.mmc.sh)
    
SET_TESTS_PROPERTIES(
    Test_MakeJSON_ConfigFile_Matches Test_MakeJSON_MMC_Runs
    PROPERTIES DEPENDS Test_MMCJSONWriter_Runs)
