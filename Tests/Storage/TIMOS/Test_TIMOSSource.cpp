/*
 * Test_TIMOSSource.cpp
 *
 *  Created on: Sep 20, 2017
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/SurfaceTri.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Composite.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSSourceWriter.hpp>

BOOST_AUTO_TEST_CASE(composite)
{
	Source::Composite C0;
		Source::Composite C1;
			Source::PencilBeam pb;
				pb.position({{1.0,2.0,3.0}});
				pb.elementHint(1001U);
				pb.direction({{0.0,1.0,0.0}});
				pb.power(0.01f);

			Source::Volume v(0.02f,1234);

			Source::SurfaceTri t(0.03f,{{101,102,103}});

			C1.add(&pb);
			C1.add(&v);
			C1.add(&t);
			C1.power(5.f);

		Source::Point p;
			p.position({{1.01,1.02,1.03}});
			p.power(10.f);

	C0.add(&p);
	C0.add(&C1);

	TIMOSSourceWriter W;
	W.filename("composite.source");
	W.source(&C0);
	W.packets(100);
	W.write();
}

BOOST_AUTO_TEST_CASE(composite10)
{
	Source::Composite C;
	for(unsigned i=0;i<10;++i)
	{
		Source::Point* P = new Source::Point();
			P->power(5.f);
			P->elementHint(10);
			P->position({{1.0,2.0,3.0}});
		C.add(P);
	}

	TIMOSSourceWriter W;
	W.filename("composite10.source");
	W.source(&C);
	W.packets(8);
	W.write();
}
