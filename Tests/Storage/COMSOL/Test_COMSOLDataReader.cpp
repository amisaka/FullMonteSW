/*
 * Test_COMSOLDataReader.cpp
 *
 *  Created on: Apr 25, 2018
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>

#include <FullMonteSW/Storage/COMSOL/COMSOLDataReader.hpp>
#include <FullMonteSW/Storage/COMSOL/COMSOLReader.hpp>

#include <FullMonteSW/Geometry/Points.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Queries/PointMatcher.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>

#include <iostream>
#include <iomanip>
#include <array>

#include <FullMonteSW/Config.h>

ostream& operator<<(ostream& os,Point<3,double> P)
{
	unsigned w = os.width();

	os << setw(w) << P[0];
	for(unsigned i=1;i<3;++i)
		os << ' ' << setw(w) << P[i];
	return os;
}

using namespace std;

#include <FullMonteSW/VTK/vtkFullMonteTetraMeshWrapper.h>


#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/VTK.hpp>
#include <vtkFloatArray.h>
#include <vtkPoints.h>
#include <vtkIdTypeArray.h>
#include <vtkCellArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataWriter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPointDataToCellData.h>
#include <FullMonteSW/Warnings/Pop.hpp>

BOOST_AUTO_TEST_CASE(readdata)
{
	COMSOLDataReader 	DR;
	DR.filename(FULLMONTE_SOURCE_DIR "/Tests/Storage/COMSOL/RB24_FluenceDistributionThroughoutEntireGeometry.txt");
	DR.read();
	Points* pts = DR.points();
	SpatialMap<float>* data=DR.data();

	COMSOLReader		MR;
	MR.filename(FULLMONTE_SOURCE_DIR "/Tests/Storage/COMSOL/RB201724_MeshWithFibers.mphtxt");
	MR.read();

	TetraMesh* M = MR.mesh();
	Points* MP = M->points();

	PointMatcher matcher;
	matcher.reference(MP);
	matcher.query(pts);
	matcher.update();

	Permutation* match = matcher.result();

	// print out matcher results
	// for(unsigned i=0;i<pts->size();++i)
	// {
	// 	//p is never used so uncommenting for now
	// 	Point<3,double> p = pts->get(i);
	// 	unsigned idx = match->get(i);
	// 	//q is never used so uncommenting for now
	// 	Point<3,double> q = MP->get(idx);
	// 	cout << "Point of data set: " << p << endl
	// 		 << "is matched with point of mesh: " << q << endl;
	// }

	////// VTKify the point cloud

	// Points
	vtkPoints* points = vtkPoints::New();
	points->SetNumberOfPoints(pts->size());

	// Fluence data array
	vtkFloatArray* phi = vtkFloatArray::New();
	phi->SetNumberOfTuples(pts->size());

	// IDs for cells
	vtkIdTypeArray* ids = vtkIdTypeArray::New();
	ids->SetNumberOfTuples(2*pts->size());

	for(unsigned i=0;i<pts->size();++i)
	{
		Point<3,double> p = pts->get(i);
		array<double,3> pa = p.as_array_of<double>();
		points->SetPoint(i,pa.data());

		phi->SetTuple1(i,data->get(i));

		ids->SetValue(2*i,1);
		ids->SetValue(2*i+1,i);
	}


	vtkCellArray* ca = vtkCellArray::New();
	ca->SetCells(pts->size(),ids);

	vtkUnstructuredGrid* pointUG = vtkUnstructuredGrid::New();
	pointUG->SetPoints(points);
	pointUG->SetCells(VTK_VERTEX,ca);
	pointUG->GetCellData()->SetScalars(phi);



	vtkUnstructuredGridWriter* W = vtkUnstructuredGridWriter::New();
	W->SetFileName("pointcloud.vtk");
	W->SetInputData(pointUG);
	W->Update();


	////// VTKify the matched surface data set into a polydata

	vtkFullMonteTetraMeshWrapper* VTKM = vtkFullMonteTetraMeshWrapper::New();
	VTKM->mesh(M);
	// VTKM->update();

	vtkPolyData* pd = VTKM->faces();

	vtkFloatArray* pointPhi = vtkFloatArray::New();
	pointPhi->SetName("Point Data");
	pointPhi->SetNumberOfTuples(VTKM->regionMesh()->GetNumberOfPoints());
	pd->GetPointData()->SetScalars(pointPhi);

	for(unsigned i=0;i<VTKM->regionMesh()->GetNumberOfPoints();++i)
		pointPhi->SetValue(i,0.0f);
	for(unsigned i=0;i<match->dim();++i)
	{
		unsigned idx = match->get(i);
		if (idx)
			pointPhi->SetValue(match->get(i),data->get(i));
		else
			cout << "  WARNING: No match for input point " << i << endl;		
	}

	cout << "There are " << matcher.unmatchedCount() << " unmatched points" << endl;

	vtkPolyDataWriter* GW = vtkPolyDataWriter::New();
	GW->SetFileName("meshpoints.vtk");
	GW->SetInputData(pd);
	GW->Update();

	cout << "Initialized vtkPolyDataWriter" << endl;

	////// VTKify into a full mesh

	vtkUnstructuredGrid* ug = VTKM->regionMesh();
	ug->GetPointData()->SetScalars(pointPhi);

	cout << "Set the PointData" << endl;

	vtkPointDataToCellData* pcd = vtkPointDataToCellData::New();
	pcd->SetInputData(ug);
	pcd->Update();

	cout << "Convert PointData to CellData" << endl;

	vtkUnstructuredGrid* ugOut = vtkUnstructuredGrid::SafeDownCast(pcd->GetOutput());

	vtkDataArray* phiV = ugOut->GetCellData()->GetScalars();
	phiV->SetName("Converted volume fluence");
	ug->GetCellData()->AddArray(phiV);

	vtkUnstructuredGridWriter* UGW = vtkUnstructuredGridWriter::New();
	UGW->SetFileName("volumemesh.vtk");
	UGW->SetInputData(ug);
	UGW->Update();
}
