package require FullMonte

set ofn "RB201724_MeshWithFibers.remap.vtk"

COMSOLReader R
	R filename "$FullMonte::sourcedir/Storage/COMSOL/RB201724_MeshWithFibers.mphtxt"
	R read

set M [R mesh]


RemapRegions	RM
#Legend			L

	RM partition [$M regions]
	RM keepUnchanged 1
#	L set 1 "Tissue"

	RM addMapping 6 1
#	L set 2 "Tumor"

	RM remapUnspecifiedTo 3
#	L set 3 "Catheters"

	RM update

$M regions [RM result]

VTKMeshWriter W
	W mesh $M
	W filename $ofn
	W write

puts "Saved to $ofn"
	
