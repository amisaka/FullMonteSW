ADD_BOOST_UNIT_TEST(Test_CommentFilter Test_CommentFilter.cpp)

ADD_BOOST_UNIT_TEST(Test_COMSOLReader Test_COMSOLReader.cpp)
TARGET_LINK_LIBRARIES(Test_COMSOLReader FullMonteCOMSOLFile)


ADD_BOOST_UNIT_TEST(Test_COMSOLDataReader Test_COMSOLDataReader.cpp)
TARGET_LINK_LIBRARIES(Test_COMSOLDataReader FullMonteCOMSOLFile FullMonteQueries vtkFullMonte)

ADD_TEST(NAME unit_comsol_datareader
	COMMAND Test_COMSOLDataReader)

###### comsol_point_data_to_vtk

ADD_EXECUTABLE(comsol_point_data_to_vtk comsol_point_data_to_vtk.cpp)
TARGET_LINK_LIBRARIES(comsol_point_data_to_vtk FullMonteGeometry vtkFullMonte FullMonteQueries FullMonteCOMSOLFile ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_FILESYSTEM_LIBRARY})

ADD_TEST(NAME unit_utility_comsol_point_data_to_vtk1
	COMMAND comsol_point_data_to_vtk
		--mesh ${CMAKE_CURRENT_SOURCE_DIR}/RB201724_MeshWithFibers.mphtxt
		--pointdata ${CMAKE_CURRENT_SOURCE_DIR}/RB24_FluenceDistributionThroughoutEntireGeometry.txt
		--surface_output rb24_comsol_surface.vtk
		--volume_output rb24_comsol_volume.vtk
		--point_output rb24_comsol_points.vtk
		--sparse_point_output rb24_sparse_points.txt
		--dense_tetra_output rb24_dense_tetras.txt)
		
ADD_TEST(NAME unit_utility_comsol_point_data_to_vtk1_no_geometry_indices_FAILS
	COMMAND comsol_point_data_to_vtk
		--mesh ${CMAKE_CURRENT_SOURCE_DIR}/RB201724_MeshWithFibers_WithoutGeoInd.mphtxt
		--pointdata ${CMAKE_CURRENT_SOURCE_DIR}/RB24_FluenceDistributionThroughoutEntireGeometry.txt
		--surface_output rb24_comsol_surface_fail.vtk
		--volume_output rb24_comsol_volume_fail.vtk
		--point_output rb24_comsol_points_fail.vtk
		--sparse_point_output rb24_sparse_points_fail.txt
		--dense_tetra_output rb24_dense_tetras_fail.txt)
		
SET_TESTS_PROPERTIES(unit_utility_comsol_point_data_to_vtk1_no_geometry_indices_FAILS
	PROPERTIES WILL_FAIL ON)
		
INSTALL(TARGETS comsol_point_data_to_vtk
	DESTINATION bin)

IF(VTK_FOUND)
	
	ADD_EXECUTABLE(comsol2vtk comsol2vtk.cpp)
	TARGET_LINK_LIBRARIES(comsol2vtk FullMonteCOMSOLFile)
	
	ADD_TEST(NAME comsol2vtk
		COMMAND comsol2vtk ${CMAKE_CURRENT_SOURCE_DIR}/HeadAndNeckModel.mphtxt HeadAndNeckModel.vtk)
		
	ADD_TEST(NAME comsol2vtk_rb201724
		COMMAND comsol2vtk ${CMAKE_CURRENT_SOURCE_DIR}/RB201724_MeshWithFibers.mphtxt Rabbit.vtk)
		
	IF(WRAP_TCL)
		ADD_TEST(NAME unit_storage_comsol_rabbit_tcl
			COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_Rabbit.tcl
		)
	ENDIF()

    IF(WRAP_PYTHON)
    # ADD_TEST(NAME unit_storage_comsol_rabbit_tcl
    #     COMMAND ${CMAKE_BINARY_DIR}/bin/tclmonte.sh ${CMAKE_CURRENT_SOURCE_DIR}/Test_Rabbit.tcl
    # )
    ENDIF()
ENDIF()