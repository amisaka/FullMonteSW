/*
 * comsol2vtk.cpp
 *
 *  Created on: Mar 21, 2018
 *      Author: jcassidy
 */

#include <FullMonteSW/Storage/COMSOL/COMSOLReader.hpp>

#include <FullMonteSW/Warnings/Push.hpp>
#include <FullMonteSW/Warnings/Boost.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/options_description.hpp>
#include <FullMonteSW/Warnings/Pop.hpp>

#include <iostream>

using namespace std;

int main(int argc,char **argv)
{
	if (argc != 3)
	{
		cout << "Usage: comsol2vtk <input> <output>" << endl;
		return 1;
	}

	COMSOLReader R;
	R.filename(argv[1]);
	R.read();

	R.vtkExport(argv[2]);

	return 0;
}



