/*
 * Test_Predicates_VTK.cpp
 *
 *  Created on: May 17, 2018
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>

#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Permutation.hpp>
#include <FullMonteSW/Geometry/FaceLinks.hpp>

#include <FullMonteSW/Geometry/Predicates/ArrayPredicateEvaluator.hpp>

#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Predicates/SurfaceOfRegionPredicate.hpp>

#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>
#include <vtkPolyDataWriter.h>
#include <FullMonteSW/VTK/vtkHelperFunctions.hpp>

BOOST_AUTO_TEST_CASE(mouse_brain_vtk)
{
	const unsigned brain=2;
	const unsigned N=2836;

	TIMOSMeshReader R;
	R.filename(FULLMONTE_SOURCE_DIR "/data/TIM-OS/mouse/mouse.mesh");
	R.read();

	TetraMesh* M = R.mesh();

	VolumeCellInRegionPredicate RP;
	RP.setRegion(brain);

	SurfaceOfRegionPredicate surf;
	surf.setRegionPredicate(&RP);

	ArrayPredicateEvaluator* eval = surf.bind(M);

	Permutation* Pbrainsurf = eval->matchingElements();

	// Output permutation should record the size of the set it's permuting from (ie. all faces)
	BOOST_CHECK_EQUAL(Pbrainsurf->sourceSize(), M->faceTetraLinks()->size());

	// check size, first, and last element of permutation, as well as max size
	BOOST_CHECK_EQUAL(Pbrainsurf->dim(), 	N);
	BOOST_CHECK_EQUAL(Pbrainsurf->get(0), 	2085U);
	BOOST_CHECK_EQUAL(Pbrainsurf->get(N-1), 	585741U);
	BOOST_CHECK_THROW(Pbrainsurf->get(N), std::out_of_range);

	vtkPoints* vtkP = vtkPoints::New();
	getVTKPoints(*M,vtkP, false);

	vtkIdTypeArray* ids = vtkIdTypeArray::New();
	ids->SetNumberOfTuples(4*Pbrainsurf->dim());
	for(unsigned i=0;i<Pbrainsurf->dim();++i)
	{
		unsigned IDf = Pbrainsurf->get(i);
		auto IDps = M->faceTetraLinks()->get(IDf).pointIDs;
		ids->SetValue(4*i, 3);
		for(unsigned j=0;j<3;++j)
			ids->SetValue(4*i+1+j, IDps[j].value());
	}

	vtkCellArray* ca = vtkCellArray::New();
	ca->SetCells(Pbrainsurf->dim(), ids);

	vtkPolyData* pd = vtkPolyData::New();
	pd->SetPoints(vtkP);
	pd->SetPolys(ca);

	vtkPolyDataWriter* W = vtkPolyDataWriter::New();
	W->SetInputData(pd);
	W->SetFileName("mousebrain.vtk");
	W->Update();
}

