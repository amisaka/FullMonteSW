/*
 * Test_EnergyToFluence.cpp
 *
 *  Created on: Jun 4, 2018
 *      Author: jcassidy
 */

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <FullMonteSW/Queries/EnergyToFluence.hpp>

#include <FullMonteSW/Geometry/Partition.hpp>
#include <FullMonteSW/Geometry/Geometry.hpp>
#include <FullMonteSW/Geometry/Material.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>

#include <FullMonteSW/OutputTypes/SpatialMap.hpp>

#include <FullMonteSW/Tests/DummyKernel.hpp>

/** Create a dummy geometry for testing energy <-> fluence conversion
 *
 * VOLUME ELEMENTS
 *
 * ID		Volume			Region
 * ---		------			------
 *  0		0				0
 *  1		10.0				2
 *  2		 5.0			0
 *  3		 1.0			  1
 *  4		 0.01				2
 *  5		 0.2				2
 *
 *
 * SURFACE ELEMENTS
 *
 * ID 		Area
 * ---		----
 *  0		0
 *  1		11.0
 *  2		201.0
 *  3		303.0
 *  4		0.04
 */

class DummyGeometry : public Geometry
{
public:
	DummyGeometry();
	~DummyGeometry();

	virtual SpatialMap<float>* elementVolumes() 			const override;
	virtual SpatialMap<float>* surfaceAreas() 			const override;
	virtual SpatialMap<float>* directedSurfaceAreas() 	const override;
};


DummyGeometry::DummyGeometry()
{
	Partition* p = new Partition();
	p->resize(7);
	p->assign(0,0);
	p->assign(1,0);
	p->assign(2,2);
	p->assign(3,0);
	p->assign(4,1);
	p->assign(5,2);
	p->assign(6,2);

	regions(p);
}

DummyGeometry::~DummyGeometry()
{

}

SpatialMap<float>* DummyGeometry::elementVolumes() const
{
	SpatialMap<float>* v = new SpatialMap<float>(7,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar);

	v->set(0,0.0f);
	v->set(1,0.0f);
	v->set(2,10.0f);
	v->set(3, 5.0f);
	v->set(4, 1.0f);
	v->set(5, 0.01f);
	v->set(6, 0.2f);
	return v;
}

SpatialMap<float>* DummyGeometry::surfaceAreas() const
{
	SpatialMap<float>* v = new SpatialMap<float>(5,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar);

	v->set(0,0.0f);
	v->set(1,11.0f);
	v->set(2,201.0f);
	v->set(3,303.0f);
	v->set(4,0.04f);
	return v;
}

SpatialMap<float>* DummyGeometry::directedSurfaceAreas() const
{
	throw std::logic_error("DummyGeometry doesn't support directedSurfaceAreas()");
}





/** Test fixture with a global
 *
 */

MCKernelBase*	fixture_kernel=nullptr;
Geometry* 		fixture_geom=nullptr;
MaterialSet* 	fixture_materials=nullptr;

struct E2F_Fixture
{
	E2F_Fixture();
	~E2F_Fixture();

};

E2F_Fixture::E2F_Fixture()
{
	fixture_kernel		= new DummyKernel();
	fixture_geom 		= new DummyGeometry();
	fixture_materials 	= new MaterialSet();

	Material* air = new Material();
	air->scatteringCoeff(0.0f);
	air->absorptionCoeff(0.0f);
	air->refractiveIndex(1.0f);
	air->anisotropy(0.0f);

	Material* white = new Material();
	white->scatteringCoeff(10.0f);
	white->absorptionCoeff(0.0f);
	white->refractiveIndex(1.37f);
	white->anisotropy(0.0f);

	Material* turbid = new Material();
	turbid->scatteringCoeff(0.0f);
	turbid->absorptionCoeff(0.2f);
	turbid->refractiveIndex(1.37f);
	turbid->anisotropy(0.0f);

	fixture_materials->append(air);
	fixture_materials->append(white);
	fixture_materials->append(turbid);

	fixture_kernel->geometry(fixture_geom);
	fixture_kernel->materials(fixture_materials);
	fixture_kernel->packetCount(10);
	fixture_kernel->energyPowerValue(1.0f);
}

E2F_Fixture::~E2F_Fixture()
{
	delete fixture_geom;
}

BOOST_GLOBAL_FIXTURE(E2F_Fixture);

/** Use existing dummy materials and geometry and check volume energy -> volume fluence conversion
 *
 */

BOOST_AUTO_TEST_CASE(volume_energy_to_fluence)
{
	SpatialMap<float>* E = new SpatialMap<float>(6,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);
	E->set(0,0.0f);
	E->set(1,1.0f);
	E->set(2,20.0f);
	E->set(3,0.3f);
	E->set(4,1e6f);
	E->set(5,1e-2f);

	EnergyToFluence EF;
	EF.data(E);
	EF.kernel(fixture_kernel);
	EF.outputFluence();
	EF.update();

	SpatialMap<float>* phi=nullptr;
	BOOST_REQUIRE(phi = dynamic_cast<SpatialMap<float>*>(EF.result()));
	BOOST_REQUIRE(phi->dim() == E->dim());

	BOOST_CHECK_SMALL(phi->get(0), 1e-5f);				// zero energy -> zero fluence
	BOOST_CHECK_CLOSE(phi->get(1), 0.05f,  1e-5f);		// PW=1.0 V=10, r=2, mu_a=0.2, Energy/Packets=1/10
	BOOST_CHECK_CLOSE(phi->get(2), 0.0f,  1e-5f);		// PW=20.0 V=5.0 r=0, mu_a=0.0, Energy/Packets=1/10		--> NaN rendered as zero
	BOOST_CHECK_CLOSE(phi->get(3), 0.0f,  1e-5f);		// PW=0.3 V=1.0 r=1, mu_a=0.0, Energy/Packets=1/10		--> NaN rendered as zero
	BOOST_CHECK_CLOSE(phi->get(4), 5e7f,  1e-5f);		// PW=1e6 V=0.01 r=2, mu_a=0.2, Energy/Packets=1/10
	BOOST_CHECK_CLOSE(phi->get(5), 0.025f, 1e-5f);		// PW=.01 V=0.2 r=2, mu_a=0.2, Energy/Packets=1/10


	/** Provide invalid dimensions, check that it does not produce output */
	E->dim(7);
	EF.update();
	BOOST_CHECK(!EF.result() && "dimension mismatch -> no output");
	E->dim(6);
}

BOOST_AUTO_TEST_CASE(surface_energy_to_fluence)
{
	SpatialMap<float>* surfE = new SpatialMap<float>(5,AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::PhotonWeight);

	surfE->set(0,0);
	surfE->set(1,101);
	surfE->set(2,20);
	surfE->set(3,3);
	surfE->set(4,444);

	EnergyToFluence EF;
	EF.kernel(fixture_kernel);
	fixture_kernel->materials(nullptr);
	EF.outputFluence();
	EF.data(surfE);
	EF.update();

	SpatialMap<float>* phi=nullptr;
	BOOST_REQUIRE(phi=dynamic_cast<SpatialMap<float>*>(EF.result()));

	BOOST_CHECK_EQUAL(phi->spatialType(), AbstractSpatialMap::Surface);
	BOOST_CHECK_EQUAL(phi->dim(), surfE->dim());

	BOOST_CHECK_SMALL(phi->get(0), 1e-5f);			// E = 0, 	A=0
	BOOST_CHECK_CLOSE(phi->get(1), 9.1818e-1f,	 	1e-3f);			// E = 101, A=11.0, Energy/Packets=1/10
	BOOST_CHECK_CLOSE(phi->get(2), 9.9502e-3f, 	1e-3f);			// E = 20, 	A=201, Energy/Packets=1/10
	BOOST_CHECK_CLOSE(phi->get(3), 9.9010e-4f, 	1e-3f);			// E = 3, 	A=303, Energy/Packets=1/10
	BOOST_CHECK_CLOSE(phi->get(4), 1.1100e3f,	1e-3f);			// E = 444, A=0.04, Energy/Packets=1/10

	SpatialMap<float>* volE = new SpatialMap<float>(5,AbstractSpatialMap::Volume,AbstractSpatialMap::Scalar);
	EF.data(volE);
	EF.update();
	BOOST_CHECK(!EF.result() && "Volume energy but no materials should cause failure");
	fixture_kernel->materials(fixture_materials);
	EF.update();
	BOOST_CHECK(!EF.result() && "Volume energy with materials but dimension mismatch should fail");
	volE->dim(6);
	EF.update();
	BOOST_CHECK(EF.result() && "Should pass once it's given materials and correct size of data");
}

BOOST_AUTO_TEST_CASE(check_invalid_inputs_fail)
{
	SpatialMap<float>* surfE = new SpatialMap<float>(5,AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::Energy);
	SpatialMap<float>*  volE = new SpatialMap<float>(6,AbstractSpatialMap::Surface,AbstractSpatialMap::Scalar,AbstractSpatialMap::Energy);

	EnergyToFluence EF;
	EF.data(nullptr);
	EF.kernel(fixture_kernel);
	fixture_kernel->geometry(nullptr);
	EF.outputFluence();
	EF.update();
	BOOST_CHECK(!EF.result() && "Should fail for lack of input data");
}
