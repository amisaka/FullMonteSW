/*
 * DummyKernel.hpp
 *
 *  Created on: April 9, 2021
 *      Author: fynns
 */

#include <FullMonteSW/Kernels/MCKernelBase.hpp>

class DummyKernel : public MCKernelBase
{
public:
	DummyKernel();
	~DummyKernel();


	virtual unsigned long long 	simulatedPacketCount() 	const override;

	virtual void 				prepare_()				override;
	virtual void				awaitFinish()			override;
	virtual void 				start_() 				override;
	virtual void				gatherResults()			override;
};

DummyKernel::DummyKernel()
{

}

DummyKernel::~DummyKernel()
{

}

unsigned long long DummyKernel::simulatedPacketCount() const
{
	return 0;
}

void DummyKernel::prepare_()
{

}

void DummyKernel::awaitFinish()
{

}

void DummyKernel::start_()
{

}

void DummyKernel::gatherResults()
{

}