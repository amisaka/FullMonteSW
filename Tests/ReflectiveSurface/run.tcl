package require FullMonte

# Driver for SurfaceInterface test cases based on cube_5med
# "Success" simply means running to completion. Post-check should be done by Python script.
#
# Can be run with a single argument specifying which case should be run
# (see switch statement around line 50 for valid options)
#
# 'baseline' does not specify any special-interface arguments
#


set case [lindex $argv 0]
puts "case $case"

set outfile [lindex $argv 1]
puts "outfile $outfile"

###### Set file names and load files
set datapath [file join $FullMonte::datadir TIM-OS cube_5med]
set meshfn [file join $datapath cube_5med.mesh]
set optfn  [file join $datapath cube_5med.opt]

TIMOSMeshReader R
R filename $meshfn
R read
set mesh [R mesh]

TIMOSMaterialReader MR
MR filename $optfn
MR read
set opt [MR materials]


# Point source centred in centre of cube

Point P
P position "0 0 0"

OutputDataSummarize S

EnergyToFluence EVF

TextFileMatrixWriter W

TetraSVKernel k
      k source P
      k materials $opt
      k roulettePrWin      0.1
      k rouletteWMin       1e-5
      k maxSteps           10000
      k maxHits            10000

      k threadCount        8

      k geometry $mesh

set isRelease [string equal $FullMonte::buildtype "Release"]
if {$isRelease} {
         puts "Build type of FullMonte is Release. Using large photon count for simulation."
         k packetCount 1000000
} else {
     puts "Build type of FullMonte is Debug. Using small photon count for simulation."
         k packetCount 2000
}


  switch $case {
    baseline {
      puts "No special interface calls"
    }
    disabled {
      puts "special interfaces disabled"
      k unmarkAllReflectiveSurfaces
    }
    misc_inner {
      puts "Region 2 (inclusion) normal"
      puts "Region 3 (inclusion) all absorbing"
      puts "Region 4 (inclusion) all reflecting"
      puts "Region 5 (inclusion) equal parts reflection/absorption/transmission"
      k unmarkAllReflectiveSurfaces
      k markReflectiveSurface 1 3 0.0 1.0 0
      k markReflectiveSurface 1 4 1.0 0.0 0
      k markReflectiveSurface 1 5 0.33333 0.33333 0
    }
    mixed_outer {
      puts "50% reflective / 50% transmissive outer boundary"
      k unmarkAllReflectiveSurfaces
      k markReflectiveSurface 0 1 0.5 0 0
    }
    reflect_outer {
      puts "reflective outer boundary condition"
      k unmarkAllReflectiveSurfaces
      k markReflectiveSurface 0 1 1.0 0.0 0
    }
    absorb_outer {
      puts "absorbing outer boundary condition - should be nearly identical to normal condition (except for some TIR/Fresnel)"
      k unmarkAllReflectiveSurfaces
      k markReflectiveSurface 0 1 0.0 1.0 0
    }
  }
  
  
  k randSeed 0
  k startAsync
  k finishAsync

  set ODC [k results]
  
  S visit $ODC

  set VE [$ODC getByName "VolumeEnergy"]
  set C  [$ODC getByName "ConservationCounts"]
  set SE [$ODC getByName "SurfaceExitEnergy"]
  
  EVF kernel k
  EVF data $VE 
  EVF update

  W filename "cube_5med.$case.vol_energy.out"
  W source $VE
  W write
  
  W filename "cube_5med.$case.vol_fluence.out"
  W source [EVF result]
  W write

  W filename "cube_5med.$case.surf_energy.out"
  W source $SE
  W write
