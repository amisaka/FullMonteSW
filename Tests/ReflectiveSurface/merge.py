import vtk.vtkCommonCore
import vtk.vtkIOLegacy

import numpy as np

import fullmontetext

import sys

"""
Python example of reading .vtk files and merging FullMonte fluence data onto them
"""

def np_to_vtk(x):
  """
  Return a VTK array (eg. vtkFloatArray) whose data is a copy of the 1D numpy array provided
  """

  if len(x.shape) != 1:
    raise Exception(f'Expecting 1-D array, found x.shape={x.shape}')

  n = x.shape[0]

  if x.dtype == np.float:
    vtk_x = vtk.vtkFloatArray()
  else:
    raise Exception('Not sure what to do with data of type {x.dtype:s}')

  vtk_x.SetNumberOfTuples(n)
  vtk_x.SetNumberOfComponents(1)

  for (idx,v) in enumerate(x,0):
    vtk_x.SetValue(idx,v)

  return vtk_x


def main():
  """
  Reads a .vtk mesh, and merges multiple FullMonte fluence files onto it, each as a separate field

  Input file names are cube_5med.<case>.vol_fluence.vtk for a hard-coded list of cases
  """
  input_fn = 'cube_5med.vtk'
  cases = ['baseline','disabled','misc_inner','mixed_outer','reflect_outer','absorb_outer']
  output_fn = 'cube_5med.merged.vtk'

  # Read mesh
  r = vtk.vtkUnstructuredGridReader()
  r.SetFileName(input_fn)
  r.Update()

  mesh = r.GetOutput()
  print(f'Mesh has {mesh.GetNumberOfCells():d} cells and {mesh.GetNumberOfPoints():d} points')


  # Read data
  for case in cases:
    data_file = f'cube_5med.{case:s}.vol_fluence.out'
    series_name = 'fluence:'+case
    x = fullmontetext.read_matrix(data_file)
  
    # Convert array types and merge
  
    vtk_phi = np_to_vtk(x)
    vtk_phi.SetName(series_name)
  
    vtk_cell_data = mesh.GetCellData()
    vtk_cell_data.SetActiveScalars(series_name)
    vtk_cell_data.SetScalars(vtk_phi)
  
  w = vtk.vtkUnstructuredGridWriter()
  w.SetFileName(output_fn)
  w.SetInputData(mesh)
  w.Update()

if __name__=='__main__':
  main()
