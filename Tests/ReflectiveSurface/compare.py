import sys
import fullmontetext
import numpy as np

import matplotlib.pyplot as plt

"""
Plots a comparison of two FullMonte matrix result sets as log(x1) vs log(x0)

It should be a straight line if they are the same
"""

def main():
  input_file_names = sys.argv[1:] if len(sys.argv)>2 else ['cube_5med.before.fluence.out','cube_5med.disabled.fluence.out']

  xs = None

  for fn in input_file_names:
    x = fullmontetext.read_matrix(fn)

    print(f'Read {x.shape} from {fn:s}')

    if xs is None:
      xs = x
    else:
      xs = np.vstack((xs,x))

  plt.loglog(xs[0,:],xs[1,:],'.')
  plt.legend(input_file_names)
  plt.show()

  print(f'xs.shape={xs.shape}')

if __name__=='__main__':
  main()
