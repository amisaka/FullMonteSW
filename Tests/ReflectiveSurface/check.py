import fullmontetext
import vtk
import numpy as np

"""
Python module for checking output from SurfaceInterface tests
"""

def sum_absorption_in_region(region_map,absorption_map,region):
  """
  Using the provided region_map and absorption_map, sum up all energy deposited in the specified region
  """
  cells = np.nonzero(region_map == region)
  return np.sum(absorption_map[cells])


def main():
  mesh_fn = 'cube_5med.vtk'
  cases = ['disabled','misc_inner','mixed_outer','reflect_outer','absorb_outer']

  case_data = {}

  for case in cases:
    input_vol_fluence_fn = 'cube_5med.'+case+'.vol_fluence.out'
    input_vol_energy_fn  = 'cube_5med.'+case+'.vol_energy.out'
    input_surf_energy_fn = 'cube_5med.'+case+'.surf_energy.out'

    case_data[case] = {
      'volume_fluence': fullmontetext.read_matrix(input_vol_fluence_fn),
      'volume_energy': fullmontetext.read_matrix(input_vol_energy_fn),
      'surface_energy': fullmontetext.read_matrix(input_surf_energy_fn)
    }

    print('Case %40s: absorbed energy %10.2f  exited energy %10.2f' %(case,sum(case_data[case]['volume_energy']),np.sum(case_data[case]['surface_energy'])))





  r = vtk.vtkUnstructuredGridReader()
  r.SetFileName(mesh_fn)
  r.Update()
  mesh = r.GetOutput()

  mesh.GetCellData().SetActiveScalars('Region')
  regions = mesh.GetCellData().GetScalars()
  print(regions)
  np_regions = np.array([regions.GetValue(i) for i in range(0,regions.GetNumberOfTuples())])

  # formated string literals are first available in Python3.6, FullMonte currently supports Python3.5
  # print(f'Read mesh with {mesh.GetNumberOfPoints():d} points and {mesh.GetNumberOfCells():d} cells')
  print('Read mesh with %d points and %d cells' %(mesh.GetNumberOfPoints(), mesh.GetNumberOfCells()))


  # Check no exiting energy where there shouldn't be

  for case in ['reflect_outer','absorb_outer']:
    e_exit = np.sum(case_data[case]['surface_energy'])
    # print(f'Check: {case:30s} sum(surface_energy) = {e_exit:10.3f} ',end='')
    print('Check: %30s sum(surface_energy) = %10.3f ' %(case, e_exit), end='')
    if e_exit > 0:
      raise Exception('ERROR')
    else:
      print('OK')

  for (case,region) in [('misc_inner',3),('misc_inner',4)]:
    e = sum_absorption_in_region(np_regions,case_data[case]['volume_energy'],region)
    # print(f'Check: {case:30s} sum(energy in region {region:2d}) = {e:10.2f} (expecting 0) '+('ERROR' if e > 0 else 'OK'))
    print('Check: %30s sum(energy in region %2d) = %10.2f (expecting 0) ' %(case, region, e), end='')
    if e > 0:
      raise Exception('ERROR')
    else:
      print( 'OK')



  for i in range(0,6):
    # print(f'Region {i:2d} size: {np.nonzero(np_regions==i)[0].shape[0]:d}')
    print('Region %2d size: %d' %(i, np.nonzero(np_regions==i)[0].shape[0]))



  for case in cases:
    print(case)
    for i in range(0,6):
      e_sum = sum_absorption_in_region(np_regions,case_data[case]['volume_energy'],i)
      print('  Region %2d total %10.2f' %(i, e_sum))
    e_surf = np.sum(case_data[case]['surface_energy'])
    print('  Surface: %10.2f' %(e_surf))
    print('  Total: %10.2f' %(e_surf+np.sum(case_data[case]["volume_energy"])))

  

if __name__=='__main__':
  main()
