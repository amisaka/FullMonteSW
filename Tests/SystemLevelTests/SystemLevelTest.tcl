package require FullMonte

FullMonteTimer T

puts "Reading mesh"
TIMOSMeshReader MR
MR filename "$FullMonte::datadir/TIM-OS/cube_5med/cube_5med.mesh"
MR read
set mesh [MR mesh]

# Set up materials
MaterialSet MS

Material air
   air   scatteringCoeff    0
   air   absorptionCoeff    0
   air   refractiveIndex    1.0
   air   anisotropy         0.0
   air   refractiveIndex    1.3

Material big_cube
    big_cube scatteringCoeff    20
    big_cube absorptionCoeff    0.05
    big_cube anisotropy         0.9
    big_cube refractiveIndex    1.3

Material small_cube1
    small_cube1 scatteringCoeff    10
    small_cube1 absorptionCoeff    0.1
    small_cube1 anisotropy         0.7
    small_cube1 refractiveIndex    1.1


Material small_cube2
    small_cube2 scatteringCoeff    20
    small_cube2 absorptionCoeff    0.2
    small_cube2 anisotropy         0.8
    small_cube2 refractiveIndex    1.2


Material small_cube3
    small_cube3 scatteringCoeff    10
    small_cube3 absorptionCoeff    0.1
    small_cube3 anisotropy         0.9
    small_cube3 refractiveIndex    1.4


Material small_cube4
    small_cube4 scatteringCoeff    20
    small_cube4 absorptionCoeff    0.2
    small_cube4 anisotropy         0.9
    small_cube4 refractiveIndex    1.5


MS exterior air
MS append big_cube
MS append small_cube1
MS append small_cube2
MS append small_cube3
MS append small_cube4


# Set up all available sources (Cut-end Fiber will be added once it is finished) 
Point P
P position "2.1 2.2 2.35"

PencilBeam PB
PB position "-2.1 2.2 2.35"
PB direction "0 0 -1"

Volume V
V elementID 11166

Ball B
B centre "-2.1 -2.2 -2.35"
B radius 0.5

Line L
L endpoint 0 "2.1 -2.2 -2.35"
L endpoint 1 "-2.1 2.2 2.35"

Fiber F
F radius 0.5
F numericalAperture 0.5
F fiberDir "-1.0 0.0 0.0"
F fiberPos "4.15 0.0 0.0"

Composite C
C add P
C add PB
C add V
C add B
C add L
C add F

# Set up scoring region (score photons entering/exiting region 2)
VolumeCellInRegionPredicate vol
vol setRegion 2

# Configure kernel
    
TetraInternalKernel K

K materials MS
K source C
[K directedSurfaceScorer] addScoringRegionBoundary vol
K geometry $mesh
K threadCount 1

# set the seed for the RNG to 15
K randSeed 15

set isRelease [string equal $FullMonte::buildtype "Release"]
if {$isRelease} {
         puts "Build type of FullMonte is Release. Using large photon count for simulation."
         K packetCount 1000000
} else {
     puts "Build type of FullMonte is Debug. Using small photon count for simulation."
         K packetCount 2000
}

puts "Running"
K runSync
puts "Done"

set results [K results]

set directedSurfE [$results getByType "DirectedSurface"]
set surfaceExitE [$results getByName "SurfaceExitEnergy"]
set volumeE [$results getByName "VolumeEnergy"]

EnergyToFluence EF
EF kernel K
EF inputEnergy
EF outputFluence


# We are only writing out the fluence values as .txt file since we want to verify the correct functionality of the simulator
# The mesh creation should have been verified by another test

# Write out the directed surface fluence
EF data $directedSurfE
EF update

TextFileMatrixWriter TW
TW filename "DirectedSurfaceFluence.txt"
TW source [EF result]
TW write

# Write out the exterior surface fluence
EF data $surfaceExitE
EF update


TW filename "SurfaceExitFluence.txt"
TW source [EF result]
TW write  

# Write out the exterior volume fluence
EF data $volumeE
EF update


TW filename "VolumeFluence.txt"
TW source [EF result]
TW write  

set time [T elapsed_sec]
set mem [T max_rss_mib]
puts "Runtime: $time s, peak memory usage: $mem MiB."
set RuntimeFile [open "Runtime.txt" w]
puts $RuntimeFile "$time"
close $RuntimeFile

OutputDataSummarize summ
summ visit [$results getByName "EventCounts"]
summ visit [$results getByName "ConservationCounts"]

# Compare the DirectedSurface mesh results with the golden sample 
set RegTest [open "DirectedSurfaceFluence.txt" r]
set RegTestData [read $RegTest]
close $RegTest
set RegDataDir [split $RegTestData " \n"]

# Compare the SurfaceExit results with the golden sample 
set RegTest [open "SurfaceExitFluence.txt" r]
set RegTestData [read $RegTest]
close $RegTest
set RegDataSurf [split $RegTestData " \n"]

# Compare the Volume results with the golden sample
set RegTest [open "VolumeFluence.txt" r]
set RegTestData [read $RegTest]
close $RegTest
set RegDataVol [split $RegTestData " \n"]

if {$isRelease} {
    set GoldTest [open "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_DirectedSurfaceFluence.txt" r]
    set GoldTestData [read $GoldTest]
    close $GoldTest
    set GoldData [split $GoldTestData " \n"]
    set column 0
    set DirErr 0
    set GoldDir 0
    for { set i 10 } { $i < [expr [llength $GoldData] -1] } { incr i } {
        if {$column == 2} {
            set DirErr [expr $DirErr + [expr abs([lindex $GoldData $i] - [lindex $RegDataDir $i])]]
            set GoldDir [expr $GoldDir + [lindex $GoldData $i]]
            set column 0
        } else {
            incr column
        }
    }
    set L1Dir [expr $DirErr/$GoldDir]
    puts [concat "DirectedSurfaceTotalEnergy - Normalized L1-Norm of errors: " $L1Dir]
    if {$L1Dir < 0.02} {
        puts "DirectedSurface results are within the expected statistical range of the golden sample!"
    } else {
        error "Absolute sum of errors for the DirectedSurface results are higher than expected!"
    }

    set GoldTest [open "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_SurfaceExitFluence.txt" r]
    set GoldTestData [read $GoldTest]
    close $GoldTest
    set GoldData [split $GoldTestData " \n"]
    set column 0
    set SurfErr 0
    set GoldSurf 0
    for { set i 5 } { $i < [expr [llength $GoldData] -1] } { incr i } {
        set SurfErr [expr $SurfErr + [expr abs([lindex $GoldData $i] - [lindex $RegDataSurf $i])]]
        set GoldSurf [expr $GoldSurf + [lindex $GoldData $i]]
    }
    set L1Surf [expr $SurfErr/$GoldSurf]
    puts [concat "SurfaceExitEnergy - Normalized L1-Norm of errors: " $L1Surf]
    if {$L1Surf < 0.08} {
        puts "SurfaceExit results are within the expected statistical range of the golden sample!"
    } else {
        error "Absolute sum of errors for the SurfaceExit results are higher than expected!"
    }

    set GoldTest [open "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_VolumeFluence.txt" r]
    set GoldTestData [read $GoldTest]
    close $GoldTest
    set GoldData [split $GoldTestData " \n"]
    set column 0
    set VolErr 0
    set GoldVol 0
    for { set i 5 } { $i < [expr [llength $GoldData] -1] } { incr i } {
        set VolErr [expr $VolErr + [expr abs([lindex $GoldData $i] - [lindex $RegDataVol $i])]]
        set GoldVol [expr $GoldVol + [lindex $GoldData $i]]
    }
    set L1Vol [expr $VolErr/$GoldVol]
    puts [concat "VolumeEnergy - Normalized L1-Norm of errors: " $L1Vol]
    if {$L1Vol < 0.03} {
        puts "VolumeExit results are within the expected statistical range of the golden sample!"
    } else {
        error "Absolute sum of errors for the VolumeExit results are higher than expected!"
    }
} else {
    puts "In Debug mode, this script always completes successfully. We only simulate 2000 packets to keept runtime low. Statistical comparisons of the results won't be accurate."
}

exit
