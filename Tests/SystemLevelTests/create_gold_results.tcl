set tStart [clock clicks -milliseconds]

package require FullMonte

puts "Reading mesh"
TIMOSMeshReader MR
MR filename "$FullMonte::datadir/TIM-OS/cube_5med/cube_5med.mesh"
MR read
set mesh [MR mesh]

# Set up materials
MaterialSet MS

Material air
   air   scatteringCoeff    0
   air   absorptionCoeff    0
   air   refractiveIndex    1.0
   air   anisotropy         0.0
   air   refractiveIndex    1.3

Material big_cube
    big_cube scatteringCoeff    20
    big_cube absorptionCoeff    0.05
    big_cube anisotropy         0.9
    big_cube refractiveIndex    1.3

Material small_cube1
    small_cube1 scatteringCoeff    10
    small_cube1 absorptionCoeff    0.1
    small_cube1 anisotropy         0.7
    small_cube1 refractiveIndex    1.1


Material small_cube2
    small_cube2 scatteringCoeff    20
    small_cube2 absorptionCoeff    0.2
    small_cube2 anisotropy         0.8
    small_cube2 refractiveIndex    1.2


Material small_cube3
    small_cube3 scatteringCoeff    10
    small_cube3 absorptionCoeff    0.1
    small_cube3 anisotropy         0.9
    small_cube3 refractiveIndex    1.4


Material small_cube4
    small_cube4 scatteringCoeff    20
    small_cube4 absorptionCoeff    0.2
    small_cube4 anisotropy         0.9
    small_cube4 refractiveIndex    1.5


MS exterior air
    MS append big_cube
    MS append small_cube1
    MS append small_cube2
    MS append small_cube3
    MS append small_cube4


# Set up all available sources (Cut-end Fiber will be added once it is finished) 
Point P
P position "2.1 2.2 2.35"

PencilBeam PB
PB position "-2.1 2.2 2.35"
PB direction "0 0 -1"

Volume V
V elementID 11166

Ball B
B centre "-2.1 -2.2 -2.35"
B radius 0.5

Line L
L endpoint 0 "2.1 -2.2 -2.35"
L endpoint 1 "-2.1 2.2 2.35"

Fiber F
F radius 0.5
F numericalAperture 0.5
F fiberDir "-1.0 0.0 0.0"
F fiberPos "4.15 0.0 0.0"

Composite C
C add P
C add PB
C add V
C add B
C add L
C add F

# Set up scoring region (score photons entering/exiting region 2)
VolumeCellInRegionPredicate vol
vol setRegion 2

# Configure kernel

TetraInternalKernel K

K materials MS
K source C
[K directedSurfaceScorer] addScoringRegionBoundary vol
K geometry $mesh
K threadCount 1

# set the seed for the RNG to 1
K randSeed 1

set isRelease [string equal $FullMonte::buildtype "Release"]

if {$isRelease} {
    K packetCount 1000000
} else {
    K packetCount 2000
}

puts "Running"
K runSync
puts "Done"

set results [K results]

OutputDataSummarize summ
summ visit [$results getByName "EventCounts"]
summ visit [$results getByName "ConservationCounts"]

set directedSurfE [$results getByType "DirectedSurface"]
set surfaceExitE [$results getByName "SurfaceExitEnergy"]
set volumeE [$results getByName "VolumeEnergy"]

EnergyToFluence EF
EF kernel K
EF inputEnergy
EF outputFluence


# We are only writing out the fluence values as .txt file since we want to verify the correct functionality of the simulator
# The mesh creation should have been verified by another test

# Write out the directed surface fluence
EF source $directedSurfE
EF update

TextFileMatrixWriter TW
TW filename "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_DirectedSurfaceFluence.txt"
TW source [EF result]
TW write

# Write out the exterior surface fluence
EF data $surfaceExitE
EF update

TW filename "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_SurfaceExitFluence.txt"
TW source [EF result]
TW write  

# Write out the exterior volume fluence
EF source $volumeE
EF update

TW filename "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_VolumeFluence.txt"
TW source [EF result]
TW write  


set tSimEnd [clock clicks -milliseconds]
if {$isRelease} {
    set RuntimeFile [open "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_ReleaseRuntime.txt" w]
} else {
    set RuntimeFile [open "$FullMonte::sourcedir/Tests/SystemLevelTests/gold_DebugRuntime.txt" w]
}
puts $RuntimeFile "[expr ($tSimEnd-$tStart)*1e-3]"
close $RuntimeFile
