//BOOST libraries
#include <boost/test/included/unit_test.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

//includes configurations for non-host related paths
#include <FullMonteSW/Config.h>

#include <iostream>
#include <array>
#include <math.h>
#include <cmath>
#include <ctime>
#include <chrono>
#include <stdio.h>
#include <numeric>
#include <vector>
#include <random>

//Sources
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>


#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Geometry/TetraMesh.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>



//Factories
#include <FullMonteSW/Kernels/Software/Emitters/TetraMeshEmitterFactory.hpp>
//

//Emitters
#include <FullMonteSW/Kernels/Software/Emitters/Base.hpp>
#include <FullMonteSW/Kernels/Software/Packet.hpp>
#include <FullMonteSW/Kernels/Software/Emitters/FiberConeEmitter.hpp>


//SFMT AVX Random number generator 
#include <FullMonteSW/Kernels/Software/RNG_SFMT_AVX.hpp>

using namespace std;
//Source namespace
using namespace Source;
//Emitter namespace
using namespace Emitter;

BOOST_AUTO_TEST_SUITE(Test_CylinderEmitter)

BOOST_AUTO_TEST_CASE( example )

{

	//number of launch packet
	const size_t Ndw=1024*64;
	
    //TIMOSMeshReader Class to read the mesh
	TIMOSMeshReader R;
	//load mesh
	R.filename(FULLMONTE_DATA_DIR "/TIM-OS/cube_5med/cube_5med.mesh");
	//R.filename(FULLMONTE_DATA_DIR "/Colin27/Colin27.mesh");
	R.read();
	//TetraMesh class stores the mesh
	TetraMesh* M = R.mesh();

	//randLaunch holds the launch packets generated 
	vector<LaunchPacket> randLaunch(Ndw);

    //Define MaterialSet to hold all materials for the supported meshes
    MaterialSet MS;
	//Material definitions
	Material bigcube(0.05,20.0,0.9,1.3);
	Material smallcube1(0.1,10.0,0.7,1.1);
	Material smallcube2(0.2,20.0,0.8,1.2);
	Material smallcube3(0.1,10.0,0.9,1.4);
	Material smallcube4(0.2,20.0,0.9,1.5);

	MS.append(&bigcube);
	MS.append(&smallcube1);
	MS.append(&smallcube2);
	MS.append(&smallcube3);
	MS.append(&smallcube4);

	//factory creates an emitter from the source light . It can generate photon packets 
	auto factory = new Emitter::TetraEmitterFactory<RNG_SFMT_AVX>(M, &MS);

	//cylinder source
	auto cyl = new Source::Cylinder(1.0f);
    cyl->radius(1.0);
    cyl->endpoint(0, {0,-2,0});
    cyl->endpoint(1, {0,2,0});
    cyl->emitVolume(false);
    cyl->emitHemiSphere(false);
	//cyl->hemiSphereEmitDistribution("LAMBERT");
    
    // add cylinder to factory
    cyl->acceptVisitor(factory);
	auto S = factory->cemitters();

    // the RNG
	RNG_SFMT_AVX rng;
	rng.seed(1);
    
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    // launch all of the packets
	for(unsigned i = 0; i < Ndw; ++i) {
		//fill the randLaunch vector with packets generated
		randLaunch[i]= S[0]->emit(rng, Ndw, i);
	}
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Launching " << Ndw << " packets took " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << "ms" << std::endl;

    const std::array<float,3> e0 = cyl->endpoint(0);
    const std::array<float,3> e1 = cyl->endpoint(1);
    const std::array<float,3> cylDir = {
        e1[0] - e0[0],
        e1[1] - e0[1],
        e1[2] - e0[2]
    };
    const std::array<float,3> cylDirNorm = normalize(cylDir);
    const float cylHeight = norm(cylDir);
    const float cylRadius = cyl->radius();


    // analyze the launch packets
    for(unsigned i = 0; i < Ndw; ++i) {
        LaunchPacket lpkt = randLaunch[i];

        //// check if launched packet direction is normal (with a small tolerancy) to the direction vector of the cylinder ////
        float d = dot(cylDirNorm, lpkt.dir.d.array());
        BOOST_REQUIRE(abs(d) < 0.1);

        //// check distance from point to center line of cylinder should be ~radius ////
        const std::array<float,3> p = lpkt.pos.array();

        // vector from 1 cylinder endpoint to random point
        std::array<float,3> vec = {
            p[0] - e0[0],
            p[1] - e0[1],
            p[2] - e0[2],
        };

        // the projected distance from from the random point p onto the line formed by the direction normal of the cylinder
        // i.e. the distance from endpoint 0 of the cylinder up is direction vector
        float dist = dot(vec, cylDirNorm);

        // this ensures that the point was generated between the circular faces of the cylinder
        BOOST_REQUIRE(dist>= 0 && dist<=cylHeight);

        // the closest point on the center line of the cylinder to the random point on the surface
        std::array<float,3> closestPointOnLine = {
            e0[0] + cylDirNorm[0]*dist,
            e0[1] + cylDirNorm[1]*dist,
            e0[2] + cylDirNorm[2]*dist
        };

        // the vector from the closest point on the cylinder center line to the random point
        std::array<float,3> pointVec = {
            p[0] - closestPointOnLine[0],
            p[1] - closestPointOnLine[1],
            p[2] - closestPointOnLine[2]
        };

        // the magnitude tells us how far the random point is from the center line of the cylinder
        d = norm(pointVec);

        // difference between distance to point and radius should be 'small' assuming we are generating points on the surface
        // of the cylinder (cyl->emitVolume(false))
        BOOST_REQUIRE(abs(cylRadius - d) < 1e-6);
    }
}

BOOST_AUTO_TEST_SUITE_END()









