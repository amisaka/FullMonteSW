/*
 *  Test_Lambertian_Reflect.cpp
 *
 *  Created on: July 9, 2021
 *      Author: Shuran Wang
 */

#include <boost/test/unit_test.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

#include <FullMonteSW/Kernels/Software/sse.hpp>

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

BOOST_AUTO_TEST_CASE(Test_Lambertian_Reflect) {

    // number of test iterations
    int n_iterations = 1000000;

    //random generator from boost library using mt19937: mersenne_twister_engine
    typedef boost::random::mt19937 base_generator_type;
    base_generator_type rng_f;

    //Random number generator with Uniform distribution between 0 and 1 
    boost::uniform_real<> uni_dist_01(0,1);
    boost::variate_generator<base_generator_type&, boost::uniform_real<> > rng(rng_f, uni_dist_01);

    float rng0;
    float rng1;

    // n set to +z direction. TODO: Change this to random direction.
    float n0 = 0; //rng();
    float n1 = 0; //rng();
    float n2 = 1; //rng();

    int phi_count[360] = {0};     // 360 degrees for phi angle
    int theta_count[180] = {0};    // 90 degrees for theta angle

    float avg_intensity0 = 0;
    float avg_intensity1 = 0;
    float avg_intensity2 = 0;
    
#ifdef USE_SSE
    __m128 n = _mm_setr_ps(n0, n1, n2, 0.0f);
#else
    m128 n = {n0, n1, n2, 0.0f};
#endif

    for (int i = 0; i < n_iterations; i++) {
        rng0 = rng();
        rng1 = rng();
        float phi, theta;
#ifdef USE_SSE
        __m128 ret = lambertian_reflect(n, _mm256_set1_ps(rng0), _mm_set1_ps(rng1));

        float newdir[4];
        _mm_store_ps(newdir, ret);

        phi = atan(newdir[1]/newdir[0]) * 180 / M_PI;
        if ((phi < 0 && newdir[1] > 0) || (phi > 0 && newdir[1] < 0)) {
            phi = 180 + phi;
        } else if (phi < 0) {
            phi = 360 + phi;
        }
        
        theta = atan(sqrt(newdir[0]*newdir[0]+newdir[1]*newdir[1]) / newdir[2]) * 180 / M_PI;
#else
        m128 newdir = lambertian_reflect(n, rng0, rng1);

        phi = atan(newdir[1]/newdir[0]) * 180 / M_PI;
        if ((phi < 0 && newdir[1] > 0) || (phi > 0 && newdir[1] < 0)) {
            phi = 180 + phi;
        } else if (phi < 0) {
            phi = 360 + phi;
        }

        theta = atan(sqrt(newdir[0]*newdir[0]+newdir[1]*newdir[1]) / newdir[2]) * 180 / M_PI;
#endif
        phi_count[(int)phi]++;
        theta_count[(int)(theta*2)]++;
    }

    // check for distribution
    float avg_phi = 0;
    for (int i = 0; i < 360; i++) {
        avg_phi += phi_count[i];
    }
    avg_phi /= 360.0f;

    for (int i = 0; i < 360; i++) {
        BOOST_CHECK_CLOSE (phi_count[i], avg_phi, 10);
    }

    int max_theta = 0;
    for (int i = 0; i < 180; i++) {
        max_theta = max(theta_count[i], max_theta);
    }

    // Add 5% toleration for max theta count
    max_theta *= 0.95;

    for (int i = 0; i < 90; i++) {
        // check for both cos(i) and cos(i+0.5) to tolerate for randomness.
        float cos_i = max_theta * cos((i/2.0f)*M_PI/180.0f);
        float cos_ip5 = max_theta * cos((i+0.5)*M_PI/180.0f);
        BOOST_CHECK_CLOSE (theta_count[i], cos_i, 10);
    }
}