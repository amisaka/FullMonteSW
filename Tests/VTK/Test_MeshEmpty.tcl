package require vtk
package require FullMonte

puts "Creating empty mesh"
TetraMesh M

puts "  info commands M returns '[info commands M]'"
puts "  M=[M cget -this]"

vtkFullMonteTetraMeshWrapper vtkm
    vtkm mesh M

puts "updating vtkFullMonteTetraMeshWrapper"
    vtkm update

exit

