package require vtk
package require FullMonte

vtkUnstructuredGridReader R
	R SetFileName "$FullMonte::datadir/Colin27/Colin27.vtk"
	R Update
	
set ug [R GetOutput]

puts "Read unstructured grid with [$ug GetNumberOfPoints] points and [$ug GetNumberOfCells] tetras"  
	
vtkFullMonteMeshFromUnstructuredGrid conv
	conv unstructuredGrid $ug
	conv update
	
puts "output of conv->mesh(foobar)='[conv mesh foobar]'"

puts "foobar cget -this = [foobar cget -this]"

exit
