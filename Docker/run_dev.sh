docker run -ti --name fullmonte-build                                   \
    -v /Users/jcassidy/src/FullMonteSW:/builds/FullMonte/FullMonteSW:ro  \
    -v FMSWBuild:/builds/FullMonte \
    -e CI_PROJECT_PATH=FullMonte/FullMonteSW \
    -e CI_PROJECT_DIR=/builds/FullMonte/FullMonteSW \
    fullmonte-dev
