if [ -z "$CI_PROJECT_DIR" ]; then echo "CI_PROJECT_DIR must be set"; exit 1; fi
if [ -z "$CI_BUILD_ROOT"  ]; then echo "CI_BUILD_ROOT must be set" ; exit 1; fi

BUILD_DIR=$CI_BUILD_ROOT/Debug

mkdir -p $BUILD_DIR
cd $BUILD_DIR

cmake -G Ninja \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=/dev/null \
    -DVTK_DIR=/usr/local/VTK-7.1.1/lib/cmake/vtk-7.1 \
    -DBOOST_ROOT=/usr/local/boost-1.58.0 \
    -DARCH=AVX2 \
    -DUSE_HW=OFF \
    -DWRAP_VTK=OFF \
    -DWRAP_TCL=ON \
    -DWRAP_PYTHON=ON \
    -DBUILD_CUDAACCEL=OFF \
    -DBUILD_FPGACLACCEL=OFF \
    $CI_PROJECT_DIR \
  && ninja
