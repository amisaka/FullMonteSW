# Release testing should always run _ALL TESTS_ with named exclusions if necessary
# Tests should never be commented out in CMakeLists.txt
#
# Why?
#
#   * If everything is run by default, much less chance of missing a test that you intended to include
#   * This file becomes the single collection place for all test exclusions (of which ideally there should be none)

ctest --output-on-failure -E 'Test_MakeJSON_Runs|Test_MakeJSON_ConfigFile_Matches|Test_MakeJSON_Script_Matches|Test_MakeJSON_MMC_Runs|Test_MCMLKernel_Sample|Test_OutputTypes|reg_sim_mouse-point0|reg_sim_colin27-line|reg_sim_colin27-point0|fullmonte_memtrace_Colin27|reg_compare_volume_mouse-point0|reg_compare_surface_mouse-point0|reg_compare_volume_colin27-point0|reg_compare_surface_colin27-point0|fullmonte_memtrace_Colin27'
