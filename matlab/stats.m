% Return the absorption probability and estimated std dev from a given weight vector

function [p,sigma_est] = stats(w,Np)

p = w/Np;

if (sum(p(:)) > 1)
    error('Probability sum cannot exceed 1');
end

sigma_est = sqrt(Np*p.*(1-p));
