% Load a series of matrices into a cell array
% fmt gives a filename format string, and idxs provides the indices for loading
% eg. loadSeries('foo%03d.txt',1:2:5) will load foo001.txt, foo003.txt, foo005.txt

function S = loadSeries(fmt,idxs)

S = cell(length(idxs),1);

for i = 1:length(idxs)
    fn = sprintf(fmt,idxs(i));
    printf('Loading %s\n',fn);
    S{i} = loadMatrix(fn);
end
